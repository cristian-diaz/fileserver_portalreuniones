Imports System.Data.OleDb
Imports System.Data
Imports Microsoft.VisualBasic

Public Class Consultas
    Inherits Conexion
    Private conjDatos As DataSet
    Private conjDatos2 As DataSet
    Private Dato As Integer
    Public maxi_IdaEVAl As Integer
    Public validoreunion As String

    Public maxi_evalxreunion As Integer ' maxima evaluacion realizada a una reunion
    Public id_reunion As String
    Private cnx As New OleDbConnection
    Private oDBdaSQL As OleDbDataAdapter
    Private Reader As OleDbDataReader
    Private dataSQL As DataSet
    Public valorindicador As String

    Public Sub New()
        cnx.ConnectionString = "Provider=MSDAORA.1;Password=reportesbo;User ID=reportesbo;Data Source=RISGCB25;Persist Security Info=True"
    End Sub


    Public Sub cerrarConexion()
        If cnx.State = ConnectionState.Open Then
            cnx.Close()
        End If
    End Sub

    Public Sub prcconsultarreuniones(ByVal Admon As Boolean) ' cargar el combo de reuniones
        Dim strcon As String = ""
        If Admon = True Then
            strcon = "SELECT Id,Nombre FROM TipoReunion WHERE tipo_nm = 'REUNION' AND activa = -1 ORDER BY Nombre "
        Else
            strcon = "SELECT Id,Nombre FROM TipoReunion where id in (select id_reunion from permisos where permiso IN ('M', 'R') and login ='" & HttpContext.Current.Session("Usuario") & "') and tipo_nm = 'REUNION' And Activa = -1 ORDER BY Nombre"
        End If
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub prcconsultarreunionesTodas() ' cargar el combo de reuniones
        Dim strcon As String = ""
        strcon = "SELECT Id,Nombre FROM TipoReunion WHERE tipo_nm = 'REUNION' AND activa = -1 ORDER BY Nombre "
        conjDatos = Me.Consultar(strcon)
    End Sub
    Public Sub prcconsultaritemreunion(Optional ByVal Item_Id As String = "", Optional ByVal Admon As Boolean = False) ' cargar la grilla de los items a evaluar 
        Dim strcon As String = ""
        strcon = "SELECT * from Item_Evaluacion ORDER BY ord ASC "
        'If Item_Id <> "" Then
        '    strcon &= " WHERE Id_Item = " & Item_Id
        'End If
        'If Admon = False And Item_Id = "" Then
        '    strcon &= " WHERE Activo = True"
        'End If
        'strcon = strcon & " ORDER BY ord ASC"
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub prcconsultarestructura()
        Dim strcon As String = ""
        strcon = "Select Id,login,lugar,fecha,totalpuntos,porcentaje,Comentario,hora_estimada_inicio,hora_estimada_final from Resultados_Eval where Id=9999999"
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Function consultar_Ideval()
        Dim sql As String
        sql = "select Max(No_Eval) from Resultados_Eval"
        Try
            maxi_IdaEVAl = Me.ConsultaDato(sql)
            maxi_IdaEVAl += 1
        Catch ex As Exception
            maxi_IdaEVAl = 1
        End Try

        Return (maxi_IdaEVAl)
    End Function

    Public Function maximo_evalreunion() ' maxima evaluacion creada para esa reunion
        Dim sql As String
        sql = "select Max(Cant_evareun) from Resultados_Eval where Id=" & id_reunion
        Try
            maxi_evalxreunion = Me.ConsultaDato(sql)
            maxi_evalxreunion += 1
        Catch ex As Exception
            maxi_evalxreunion = 1
        End Try

        Return (maxi_evalxreunion)
    End Function

    Public Sub prcguardar_encabezado(ByVal dataset As DataSet, ByVal fecha_ini As String, ByVal fecha_fin As String)
        Dim sql As String

        Dim fecha As Date
        id_reunion = dataset.Tables(3).Rows(0).Item(0)
        consultar_Ideval()
        maximo_evalreunion()
        fecha = dataset.Tables(3).Rows(0).Item(3)

        ' guardar encabezado
        sql = " Insert Into Resultados_Eval values ("
        sql = sql & CInt(maxi_IdaEVAl) & ","
        sql = sql & dataset.Tables(3).Rows(0).Item(0) & ",'"
        sql = sql & dataset.Tables(3).Rows(0).Item(1) & "','" 'login
        sql = sql & dataset.Tables(3).Rows(0).Item(2) & "',#" 'lugar
        sql = sql & Format(fecha, "yyyy/MM/dd") & "#," ' fecha
        sql = sql & dataset.Tables(3).Rows(0).Item(4) & ","
        sql = sql & dataset.Tables(3).Rows(0).Item(5) & ","
        sql = sql & CInt(maxi_evalxreunion) & ","

        Try
            sql = sql & "'" & Mid(dataset.Tables(3).Rows(0).Item(6), 1, 1500) & "'," ' Comentario
        Catch ex As Exception
            sql = sql & "'" & "Sin Observaciones" & "'," ' Comentario
        End Try
        sql = sql & "# " & Format(CDate(fecha_ini), "HH:mm:ss") & " #," ' fecha inicio real
        sql = sql & "# " & Format(CDate(fecha_fin), "HH:mm:ss") & "#," ' fecha final real
        sql = sql & "# " & Format(CDate(dataset.Tables(3).Rows(0).Item(7)), "HH:mm:ss") & "#," ' fecha inicio estimada
        sql = sql & "# " & Format(CDate(dataset.Tables(3).Rows(0).Item(8)), "HH:mm:ss") & "#)" ' fecha final estimada 
        Me.GuardarBD(sql)

        If FlagError = False Then
            ' guardar peso
            For i As Integer = 0 To dataset.Tables(1).Rows.Count - 1
                sql = " Insert Into Resultados_peso values ("
                sql = sql & CInt(maxi_IdaEVAl) & ",'"
                sql = sql & dataset.Tables(3).Rows(0).Item(0) & "'," 'id reunin
                sql = sql & dataset.Tables(1).Rows(i).Item(0) & "," ' item
                sql = sql & dataset.Tables(1).Rows(i).Item(2) & ")" ' peso
                Me.GuardarBD(sql)
            Next

            If FlagError = False Then
                ' guardar calificacion
                For i As Integer = 0 To dataset.Tables(1).Rows.Count - 1
                    sql = " Insert Into Resultados_Calif values ("
                    sql = sql & CInt(maxi_IdaEVAl) & ",'"
                    sql = sql & dataset.Tables(3).Rows(0).Item(0) & "'," ' id recuion
                    sql = sql & dataset.Tables(1).Rows(i).Item(0) & "," ' item
                    If dataset.Tables(1).Rows(i).Item(0) = 6 Then
                        If IsDBNull(dataset.Tables(1).Rows(i).Item("calif")) Then
                            sql = sql & "0" & ")" 'califi
                        Else
                            If dataset.Tables(1).Rows(i).Item("calif") = "Unos" Then
                                sql = sql & "1" & ")"
                            Else
                                sql = sql & "3" & ")"
                            End If
                        End If
                    Else
                        If IsDBNull(dataset.Tables(1).Rows(i).Item("calif")) Then
                            sql = sql & "0" & ")" 'califi
                        Else
                            If dataset.Tables(1).Rows(i).Item("calif") = "Si" Then
                                sql = sql & "3" & ")"
                            Else
                                sql = sql & "0" & ")"
                            End If
                        End If

                    End If
                    'calificacion
                    Me.GuardarBD(sql)
                Next
            End If

            If FlagError = False Then
                ' actualizar la tabla de asistnestes reunion
                sql = ""
                sql = " UPDATE ASISTENTESREUNION SET horafin='" & fecha_fin & "', bandera=0" & ",No_eval=" & CInt(maxi_IdaEVAl)
                sql = sql & " where idreunion=" & dataset.Tables(3).Rows(0).Item(0)
                sql = sql & " and bandera=1"
                Me.GuardarBD(sql)
            End If
        End If

    End Sub

    Public Sub prcconsultargrafica(ByVal Id_reunion As String, ByVal fecha_ini As Date, ByVal fecha_fin As Date)
        Dim sql As String

        sql = " Select fecha, porcentaje from Resultados_Eval where Id=" & Id_reunion & " and fecha >=#" & Format(fecha_ini, "yyyy/MM/dd") & "# AND fecha <=#" & Format(fecha_fin, "yyyy/MM/dd") & "# Order by Fecha"
        'sql = " Select fecha, porcentaje from Resultados_Eval where Id=" & Id_reunion & " and fecha Between #" & Format(fecha_ini, "dd/MM/yyyy") & "# AND #" & Format(fecha_fin, "dd/MM/yyyy") & "# Order by Fecha"

        conjDatos = Me.Consultar(sql)
    End Sub


    Public Sub prcconsultarultimas(ByVal Id_reunioneval As String)
        Dim sql As String
        Dim minimo As Integer
        Dim reunionesminimas As Integer

        reunionesminimas = CInt(System.Configuration.ConfigurationManager.AppSettings("reunionesamostrar").ToString) + 1

        id_reunion = Id_reunioneval
        maximo_evalreunion()

        minimo = CInt(maxi_evalxreunion) - CInt(reunionesminimas) ' se le quita uno mas ya qeu la funion la devuelve con 1 mas 

        If minimo > 0 Then
            sql = "Select fecha, porcentaje from Resultados_Eval where Id=" & id_reunion & " and Cant_evareun >" & minimo & " AND Cant_evareun <=" & CInt(maxi_evalxreunion - 1) & " Order by Fecha"
        Else
            sql = "Select fecha, porcentaje from Resultados_Eval where Id=" & id_reunion & " Order by Fecha"
        End If

        conjDatos = Me.Consultar(sql)

    End Sub

    Public Sub prcconsultarmes(ByVal Id_reunioneval As String, ByVal desde As String, ByVal hasta As String, ByVal anno As String)
        Dim sql As String

        sql = "SELECT Resultados_Eval.fecha, Resultados_Eval.porcentaje " & _
        "FROM(Resultados_Eval)" & _
        "WHERE(((Resultados_Eval.[id]) = " & Id_reunioneval & " ) And Month(Resultados_Eval.fecha) >= " & desde & " And Month(Resultados_Eval.fecha) <= " & hasta & " And Year(fecha) = " & anno & ")" & _
        "GROUP BY Resultados_Eval.fecha, Resultados_Eval.porcentaje"
        conjDatos = Me.Consultar(sql)

    End Sub

    Sub PrcInbserItems(ByVal Item As String, ByVal Peso As String, ByVal Comentario As String, ByVal Calificacion As String, ByVal Activo As Boolean, ByVal Max As Integer)

        Dim sql As String

        sql = "INSERT INTO Item_Evaluacion (item, Peso, Comentario, Calificacion, Activo, MaxPuntos) VALUES('"
        sql = sql & Item & "','"
        sql = sql & Peso & "','"
        sql = sql & Comentario & "','"
        sql = sql & Calificacion & "',"
        sql = sql & Activo & ","
        sql = sql & Max & ")"
        Me.GuardarBD(sql)

    End Sub

    Sub PrcActualizarItems(ByVal Item As String, ByVal Peso As String, ByVal Comentario As String, ByVal Calificacion As String, ByVal Activo As Boolean, ByVal id_item As String, ByVal Max As Integer)

        Dim sql As String

        sql = "UPDATE Item_Evaluacion SET Item ='"
        sql = sql & Item & "', Peso ='"
        sql = sql & Peso & "', Comentario ='"
        sql = sql & Comentario & "', Calificacion ='"
        sql = sql & Calificacion & "', MaxPuntos = " & Max & ", Activo ="
        sql = sql & Activo
        sql = sql & " WHERE id_item = " & id_item & ";"

        Me.GuardarBD(sql)
    End Sub

    Sub PrcActualizarEstado(ByVal dataset As DataSet)
        Dim sql As String

        For i As Integer = 0 To dataset.Tables(0).Rows.Count - 1
            sql = "UPDATE Item_Evaluacion SET activo = "
            sql = sql & Me.DatasetFill.Tables(0).Rows(i).Item(5)
            sql = sql & " WHERE id_item = " & Me.DatasetFill.Tables(0).Rows(i).Item(0)
            Me.GuardarBD(sql)
        Next
    End Sub


    Public Function validar_fechas(ByVal fecha As Date, ByVal id_reunion As String) As Integer
        Dim sql As String

        sql = "select count(No_eval) AS Evaluado, Max(No_eval) AS Evaluacion from Resultados_Eval where fecha=#" & Format(fecha, "yyyy/MM/dd") & "# and Id=" & id_reunion

        validoreunion = Me.Consultar(sql).Tables(0).Rows(0)(1).ToString

        Return Me.Consultar(sql).Tables(0).Rows(0)(0)

    End Function
    Public Function prcConsultar_Evaluacion(Optional ByVal Id_Reunion As String = "", Optional ByVal Fecha_Ini As String = "", Optional ByVal Fecha_Fin As String = "", Optional ByVal No_Eval As String = "") As DataSet

        Dim strcon As String

        strcon = "SELECT Resultados_Eval.No_Eval, Resultados_Eval.Id, Resultados_Eval.login, Resultados_Eval.lugar, Resultados_Eval.fecha, Resultados_Eval.totalpuntos, Resultados_Eval.porcentaje, Resultados_Eval.Comentario, t_logins.nombreusuario ,Resultados_Eval.lugar, Resultados_Eval.fecha, Resultados_Eval.totalpuntos, Resultados_Eval.porcentaje, Resultados_Eval.hora_final_real, Resultados_Eval.hora_inici_real,Resultados_Eval.hora_estimada_inicio,Resultados_Eval.hora_estimada_final"
        strcon &= " FROM t_logins INNER JOIN Resultados_Eval ON t_logins.Login = Resultados_Eval.login"

        If Id_Reunion <> "" Then
            strcon &= " WHERE Resultados_Eval.Id=" & Id_Reunion & ""
            If Fecha_Ini = "" Then
                If Fecha_Fin <> "" Then
                    strcon &= " AND Resultados_Eval.fecha <=#" & Format(CDate(Fecha_Fin), "yyyy/MM/dd") & "#"
                End If
            Else
                If Fecha_Fin <> "" Then
                    'Fecha de Inicio y Fecha Final especificada
                    strcon &= " AND Resultados_Eval.fecha >=#" & Format(CDate(Fecha_Ini), "yyyy/MM/dd") & "# AND Resultados_Eval.fecha <=#" & Format(CDate(Fecha_Fin), "yyyy/MM/dd") & "#"
                Else
                    strcon &= " AND Resultados_Eval.fecha=#" & Format(CDate(Fecha_Ini), "yyyy/MM/dd") & "#"
                End If
            End If
        Else
            strcon &= " WHERE Resultados_Eval.No_Eval=" & No_Eval & ""
        End If


        strcon &= " ORDER BY Resultados_Eval.No_Eval DESC"

        Return Me.Consultar(strcon)

    End Function
    Public Function FncDetalle_Evaluacion(ByVal Id_Eval As String) As DataTable
        Dim strcon As String
        Dim dtt As New DataTable

        strcon = "SELECT Resultados_Calif.No_eval, Resultados_Calif.Id, Resultados_Calif.Id_Item, Resultados_Calif.calificacion, Item_Evaluacion.Peso, Item_Evaluacion.Item, Item_Evaluacion.comentario, ( Resultados_Calif.calificacion *  Item_Evaluacion.Peso) AS Valor "
        strcon &= " FROM Item_Evaluacion INNER JOIN Resultados_Calif ON Item_Evaluacion.Id_item = Resultados_Calif.Id_Item"
        strcon &= " WHERE Resultados_Calif.No_eval=" & Id_Eval

        dtt = Me.Consultar(strcon).Tables(0)

        dtt.TableName = "Detalle"

        Return dtt

    End Function
    Public Sub prcupdate_encabezado(ByVal dataset As DataSet, ByVal No_Eval As String)
        Dim sql As String

        Dim fecha As Date
        id_reunion = dataset.Tables(3).Rows(0).Item(0)
        fecha = dataset.Tables(3).Rows(0).Item("fecha")

        'Actualizar Encabezado
        sql = " UPDATE Resultados_Eval SET "
        sql = sql & " lugar= '" & dataset.Tables(3).Rows(0).Item(2) & "',"
        sql = sql & " totalpuntos=" & dataset.Tables(3).Rows(0).Item(4) & ", "
        sql = sql & " porcentaje=" & dataset.Tables(3).Rows(0).Item(5) & ", "
        sql = sql & " Comentario= '" & dataset.Tables(3).Rows(0).Item(6) & "'" ' Comentario
        sql = sql & " WHERE No_Eval = " & No_Eval
        Me.GuardarBD(sql)

        'Actualizar pesos
        For i As Integer = 0 To dataset.Tables(1).Rows.Count - 1
            sql = " UPDATE Resultados_peso SET peso= "
            sql = sql & dataset.Tables(1).Rows(i).Item(2) ' peso
            sql = sql & " WHERE No_Eval= " & No_Eval 'Evaluaci�n
            sql = sql & " AND Id= " & dataset.Tables(3).Rows(0).Item(0) 'id reuni�n
            sql = sql & " AND Id_Item = " & dataset.Tables(1).Rows(i).Item(0) ' item
            Me.GuardarBD(sql)
        Next

        'Actualizar Calificaci�n

        For i As Integer = 0 To dataset.Tables(1).Rows.Count - 1
            sql = " UPDATE Resultados_Calif SET calificacion = "
            'If IsDBNull(dataset.Tables(1).Rows(i).Item("calif")) Then
            '    sql = sql & "0"  'califi
            'Else
            '    sql = sql & dataset.Tables(1).Rows(i).Item("calif") 'califi
            'End If
            If dataset.Tables(1).Rows(i).Item(0) = 6 Then
                If IsDBNull(dataset.Tables(1).Rows(i).Item("calif")) Then
                    sql = sql & "0" & ")" 'califi
                Else
                    If dataset.Tables(1).Rows(i).Item("calif") = "Unos" Then
                        sql = sql & "1" & ")"
                    Else
                        sql = sql & "3" & ")"
                    End If
                End If
            Else
                If IsDBNull(dataset.Tables(1).Rows(i).Item("calif")) Then
                    sql = sql & "0" & ")" 'califi
                Else
                    If dataset.Tables(1).Rows(i).Item("calif") = "Si" Then
                        sql = sql & "3" & ")"
                    Else
                        sql = sql & "0" & ")"
                    End If
                End If

            End If
            sql = sql & " WHERE No_Eval= " & No_Eval 'Evaluaci�n
            sql = sql & " AND Id= " & dataset.Tables(3).Rows(0).Item(0) 'id reuni�n
            sql = sql & " AND Id_Item = " & dataset.Tables(1).Rows(i).Item(0) ' item
            Me.GuardarBD(sql)
        Next
    End Sub
    Public Function FncCumplimiento_Compromisos(ByVal Reunion As String) As Boolean
        Dim strcon As String
        Dim Incumplidos As Integer

        strcon = "SELECT count(*) AS Incumplidos "
        strcon &= "FROM Compromisos, TipoReunion "
        strcon &= "WHERE (TIPO <> '') "
        strcon &= "AND TIPO = '" & Reunion & "' "
        strcon &= "AND Compromisos.Tipo = TipoReunion.Nombre And TipoReunion.Activa=-1 "
        strcon &= "AND (Status = 'ROJO' AND NOT(VERIFICADO))"

        Incumplidos = Me.ConsultaDato(strcon)

        If Incumplidos = 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function FncRol_Usuario(ByVal Login As String) As Boolean

        Dim strcon As String
        Dim Admon As Boolean
        strcon = " SELECT t_logins.administrador"
        strcon &= " FROM t_logins"
        strcon &= " WHERE login='" & Login & "'"

        Admon = Me.ConsultaDato(strcon)

        Return Admon

    End Function
    Public Function FncIngreso_Usuario(ByVal Login As String) As Boolean
        Dim strcon As String
        Dim reu As Integer

        strcon = "SELECT count(*) AS Reuniones  "
        strcon &= "FROM TipoReunion where id in (select id_reunion from permisos where permiso = 'M' and login ='" & Login & "') "
        strcon &= "AND TIPO_NM='REUNION' And Activa = -1 "

        reu = Me.ConsultaDato(strcon)

        If reu = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Sub deter_cumplehora(ByVal idReunion As String, ByVal fecha As Date) ' para validar si se empezo a timepo y termino atiempo
        Dim strcon As String

        'SELECT TipoReunion.id,TipoReunion.horaInicio as estimainicio,TipoReunion.horaFin as estimadafin,ASISTENTESREUNION.horaInicio  as realinicio
        'FROM TipoReunion INNER JOIN ASISTENTESREUNION ON TipoReunion.Id = ASISTENTESREUNION.idreunion;
        strcon = " Select distinct( tiporeunion.id),  tiporeunion.horainicio as estimadaini,  tiporeunion.horafin as  estimadafin , asistentesreunion.horaInicio as realincio  "
        strcon = strcon & " from tiporeunion,asistentesreunion"
        strcon = strcon & " where TipoReunion.Id =" & Trim(idReunion) & " and asistentesreunion.fecha=#" & Format(CDate(fecha), "MM/dd/yyyy") & "#"
        strcon = strcon & " And tiporeunion.id = asistentesreunion.idreunion and asistentesreunion.bandera=1"

        conjDatos = Me.Consultar(strcon)

    End Sub

    ' traer los datos de la horas planeadas cuando la reunion no es automatica

    Public Sub Prc_Horas_sin_automatico(Optional ByVal idReunion As Integer = 0, Optional ByVal tipo As Integer = 0, Optional ByVal No_Eval As Integer = 0, Optional ByVal fecha As String = "") ' para validar si se empezo a timepo y termino atiempo
        Dim strcon As String
        Select Case tipo

            Case 0
                'SELECT TipoReunion.id,TipoReunion.horaInicio as estimainicio,TipoReunion.horaFin as estimadafin,ASISTENTESREUNION.horaInicio  as realinicio
                'FROM TipoReunion INNER JOIN ASISTENTESREUNION ON TipoReunion.Id = ASISTENTESREUNION.idreunion;
                strcon = " Select distinct( tiporeunion.id),  tiporeunion.horainicio as estimadaini,  tiporeunion.horafin as  estimadafin"
                strcon = strcon & " from tiporeunion"
                strcon = strcon & " where TipoReunion.Id =" & Trim(idReunion)
                conjDatos = Me.Consultar(strcon)

            Case 1

                AsisRnion2(No_Eval, idReunion, fecha)

        End Select

    End Sub


    Public Function Fnccumplidofecha(ByVal Reunion As String) As Boolean
        Dim strcon As String
        Dim Incumplidos As Integer
        strcon = "SELECT count(*) AS Incumplidos "
        strcon = strcon & " FROM asistentesreunion "
        strcon = strcon & " where asistentesreunion.idreunion=" & Trim(Reunion)
        strcon = strcon & " and asistentesreunion.bandera=1"
        Incumplidos = Me.ConsultaDato(strcon)
        If Incumplidos = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Property str_validar_fecha() As String ' valida si ya se encuentra una reunion con esa fecha 
        Get
            Return validoreunion
        End Get
        Set(ByVal value As String)
            validoreunion = value
        End Set
    End Property


    Public Property DatasetFill() As DataSet
        Get
            Return conjDatos
        End Get
        Set(ByVal value As DataSet)
            conjDatos = value
        End Set
    End Property
    Public Property DatasetFill2() As DataSet
        Get
            Return conjDatos2
        End Get
        Set(ByVal value As DataSet)
            conjDatos2 = value
        End Set
    End Property

    ' si es obligatorio validar los participantes 
    Public Function FncObligatorio(ByVal Id_Eval As String) As Boolean
        Dim strcon As String
        Dim dtt As New DataTable
        Dim Incumplidos As Integer
        If Id_Eval <> "" Then

            strcon = "SELECT  count(*) from tiporeunion "
            strcon = strcon & " where(validarAsistentes = true )"
            strcon = strcon & " and id=" & Id_Eval

            Incumplidos = Me.ConsultaDato(strcon)

            If Incumplidos > 0 Then
                Return True
            Else
                Return False
            End If

        End If
    End Function


    ' funcion que validad cuantos participantes debian estar prensete en la reunion
    Public Function Fncpartiregistrados(ByVal Id_Eval As String) As DataTable
        Dim strcon As String
        Dim dtt As New DataTable

        strcon = " select  permisos.login"
        strcon = strcon & " from permisos"
        strcon = strcon & " where permisos.id_reunion=" & Id_Eval

        dtt = Me.Consultar(strcon).Tables(0)
        dtt.TableName = "Usu_registra"
        Return dtt
    End Function

    ' funcion qeu validad cuantos partiicpantes asisteieron a la reunion 
    Public Function Fncpartiasitieron(ByVal Id_Eval As String) As DataTable
        Dim strcon As String
        Dim dtt As New DataTable

        strcon = " select asistentesreunion.login"
        strcon = strcon & " from asistentesreunion"
        strcon = strcon & " where asistentesreunion.idreunion=" & Id_Eval
        strcon = strcon & " and asistentesreunion.bandera=1"

        dtt = Me.Consultar(strcon).Tables(0)
        dtt.TableName = "Usu_asis"
        Return dtt
    End Function

    Public Function Fncpartiasitieron1(ByVal Id_reun As String, ByVal id_eval As String) As DataTable
        Dim strcon As String
        Dim dtt As New DataTable

        strcon = " select asistentesreunion.login"
        strcon = strcon & " from asistentesreunion"
        strcon = strcon & " where asistentesreunion.idreunion=" & Id_reun
        strcon = strcon & " and asistentesreunion.No_eval=" & id_eval
        dtt = Me.Consultar(strcon).Tables(0)
        dtt.TableName = "Usu_asis5"
        Return dtt
    End Function

    'Public Function dsTraerFestivos() As DataSet
    '    dataSQL = New Data.DataSet
    '    oDBdaSQL = New OleDbDataAdapter(Trae_Festivos, cnx)
    '    oDBdaSQL.Fill(dataSQL)
    '    cnx.Close()
    '    Return dataSQL
    'End Function
    Public Sub Tp_Reunion()
        Dim strcon As String
        strcon = "SELECT Id,NombreReunion FROM T_REUNIONES"
        conjDatos = Me.Consultar(strcon)
    End Sub
    Public Function Tiene_Compromisos(ByVal name As String) As Boolean
        Dim strcon As String
        Dim Incumplidos As Integer
        strcon = "SELECT COUNT(*)FROM COMPROMISOS WHERE TIPO='" & name & "' "
        Incumplidos = Me.ConsultaDato(strcon)

        If Incumplidos = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub Tp_Moderador()
        Dim strcon As String
        strcon = "SELECT Login, Nombreusuario FROM t_logins WHERE Activo = -1 ORDER BY Nombreusuario"
        conjDatos = Me.Consultar(strcon)
    End Sub


    Public Sub Trae_area(ByVal tipo As String)
        Dim strcon As String
        If tipo = "Todos" Then
            strcon = "SELECT Id,NombreDependencia FROM Jerarquia"
        Else
            strcon = "SELECT Id,NombreDependencia FROM Jerarquia WHERE Tipo = '" & tipo & "' "
        End If
        conjDatos = Me.Consultar(strcon)
    End Sub
    Public Sub T_areas(ByVal id As Integer) ' cargar el combo de reuniones
        Dim strcon As String
        strcon = "SELECT Id,NombreDependencia FROM Jerarquia where Id=" & id
        conjDatos = Me.Consultar(strcon)
    End Sub



    Public Sub Trae_areas() ' cargar el combo de reuniones
        Dim strcon As String
        strcon = "SELECT Id,NombreDependencia FROM Jerarquia "
        conjDatos = Me.Consultar(strcon)
    End Sub
    Public Sub Source_NewReunion(ByVal name As String, ByVal fec_inicio As String, ByVal fec_fin As String, ByVal ar As Long, ByVal moderador As String, ByVal tpr As String, ByVal d_inicio As String, ByVal d_fin As String, ByVal drcion As String, ByVal frce As String, ByVal valida As String)

        Dim sql As String
        Dim sql2 As String
        Dim strcon As String
        Dim dts As Integer

        sql = "INSERT INTO TipoReunion (Nombre,FechaIngreso,FechaInicio,FechaFinaliza,area,moderador,tip_asoc,tipo_nm,horainicio,horaFin,duracion,Seguimiento,validarAsistentes) VALUES('"
        sql = sql & name & "',#" & Format(Now(), "yyyy/MM/dd") & "#,#"
        sql = sql & Format(CDate(fec_inicio), "yyyy/MM/dd") & "#,#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "#,"
        sql = sql & ar & ",'"
        sql = sql & moderador & "',"
        sql = sql & tpr & ",'REUNION',#" & Format(CDate(d_inicio), "HH:mm:ss") & "#,#" & Format(CDate(d_fin), "HH:mm:ss") & "#,'"
        sql = sql & drcion & "',"
        sql = sql & frce & ",'"
        sql = sql & valida & "')"
        Me.GuardarBD(sql)

        strcon = "SELECT Id FROM TipoReunion WHERE tipo_nm='REUNION' "
        strcon = strcon & "and Nombre='" & name & "'"
        dts = Me.ConsultaDato2(strcon)

        Acta("newconsecutivo", , dts, )

        sql2 = "INSERT INTO permisos (id_reunion,login,permiso) VALUES "
        sql2 = sql2 & "(" & dts & ",'" & moderador & "','M')"
        Me.GuardarBD(sql2)


    End Sub
    Public Sub Todas_Reunion(ByVal tp_r As String)

        Dim strcon As String
        strcon = "SELECT a.Nombre, a.FechaIngreso,a.tip_asoc,b.NombreReunion,a.horaInicio,a.horaFin FROM TipoReunion AS a ,T_REUNIONES AS b WHERE a.tipo_nm='REUNION' "
        strcon = strcon & "AND A.tip_asoc=" & tp_r & " AND a.tip_asoc=b.Id "
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub Reportes_Reunion(ByVal tp_r As String, ByVal tp_n As String, ByVal tp_a As String, ByVal tp_i As String, ByVal tp_f As String, ByVal op_1 As String, ByVal op_2 As String, ByVal op_a As String, ByVal op_3 As String, ByVal op_4 As String)

        Dim strcon As String
        strcon = "SELECT a.Nombre, a.FechaIngreso,a.tip_asoc,b.NombreReunion,a.horaInicio,a.horaFin FROM TipoReunion AS a ,T_REUNIONES AS b WHERE a.activa= -1 and a.tipo_nm='REUNION'"
        If op_1 = True Then
            strcon = strcon & " AND A.tip_asoc=" & tp_r
        End If
        If op_2 = True Then
            strcon = strcon & " AND a.Nombre='" & tp_n & "'"
        End If

        If op_a = True Then
            strcon = strcon & " AND a.area=" & tp_a
        End If

        If op_3 = True Then
            strcon = strcon & " AND a.horaInicio >=#" & Format(CDate(tp_i), "HH:mm:ss") & "#"
        End If

        If op_4 = True Then
            strcon = strcon & " AND a.horaFin <=#" & Format(CDate(tp_f), "HH:mm:ss") & "#"
        End If
        strcon = strcon & " AND a.tip_asoc=b.Id Order by a.Nombre"
        conjDatos = Me.Consultar(strcon)
    End Sub
    Public Sub name_Reunion()
        Dim strcon As String
        strcon = "SELECT Id,Nombre FROM TipoReunion WHERE Activa = -1 and tipo_nm='REUNION'ORDER BY Nombre"


        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub reunion_moderador(ByVal registro As String)
        Dim strcon As String
        Dim strusr As String
        Dim rol As Integer

        strusr = "Select Rol from t_logins where Login='" & registro & "'"
        rol = Me.ConsultaDato(strusr)

        If rol = 4 Then
            strcon = "Select Id,Nombre from TipoReunion order by nombre"
            conjDatos = Me.Consultar(strcon)
        Else
            strcon = "select a.Id,a.nombre,b.login from TipoReunion a, permisos b"
            strcon = strcon & " where a.Id = b.id_reunion "
            strcon = strcon & " and b.login='" & registro & "'"
            conjDatos = Me.Consultar(strcon)
        End If



    End Sub
    Public Sub reunion_participante(ByVal registro As String)
        Dim strcon As String

        strcon = "select a.Id,a.nombre,b.login from TipoReunion a, permisos b"
        strcon = strcon & " where a.Id = b.id_reunion"
        strcon = strcon & " and b.login='" & registro & "'"
        conjDatos = Me.Consultar(strcon)

    End Sub


    Public Sub Trae_archivos(ByVal identificacion As String)

        Dim strcon As String
        strcon = "SELECT * FROM Reunion_Archivos WHERE Reunion = " & identificacion & " ORDER BY Nombre"
        conjDatos = Me.Consultar(strcon)

    End Sub
    Public Sub Trae_DatosReunionPuntual(ByVal nombrearchivo As String)

        Dim strcon As String
        strcon = "SELECT * FROM Reunion_Archivos WHERE Nombre ='" & nombrearchivo & "' ORDER BY Nombre"
        conjDatos = Me.Consultar(strcon)

    End Sub


    Public Sub name_Reunion_Indicador(ByVal area As String)
        Dim strcon As String
        strcon = "SELECT A.Id,A.Nombre,B.Tipo FROM TipoReunion as A,Jerarquia as B "
        strcon = strcon & "WHERE A.area = B.Id AND B.Tipo='" & area & "'"

        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub name_Evaluacion()
        Dim strcon As String
        strcon = "SELECT Id_item,Item FROM Item_Evaluacion where Activo = -1 ORDER BY Id_item"
        conjDatos = Me.Consultar(strcon)
    End Sub
    Public Sub name_Evaluacion_tt(ByVal reporte As String, ByVal name As String, ByVal item As String, ByVal fec_ini As String, ByVal fec_fin As String, ByVal h_in As String, ByVal h_fin As String, ByVal ptj As String, ByVal op_5 As String, ByVal op_6 As String, ByVal op_7 As String, ByVal op_8 As String, ByVal op_9 As String, ByVal op_10 As String, ByVal op_11 As String)
        Dim strcon As String
        If reporte = "evaluacion" Then
            strcon = "SELECT DISTINCT t.nombre as nombre, t.tip_asoc, i.item  ,rc.calificacion, re.totalpuntos ,tl.login, tl.nombreusuario ,re.fecha"
            strcon = strcon & ",re.fecha  ,re.lugar  ,re.comentario  ,re.hora_inici_real as horaInicioReal  ,re.hora_final_real as horaFinReal  ,re.hora_estimada_inicio as horaInicioPlaneada  ,"
            strcon = strcon & "re.hora_estimada_final as horaFinPlaneada  "
            strcon = strcon & "FROM tiporeunion t, resultados_eval re, asistentesreunion a, resultados_calif rc, item_evaluacion i, t_logins tl, permisos p  "
            strcon = strcon & "WHERE t.tipo_nm = 'REUNION'  AND t.id = a.idreunion  "
            strcon = strcon & "AND p.id_reunion = t.id  "
            strcon = strcon & "AND re.no_eval = a.no_eval "
            strcon = strcon & "AND rc.no_eval = a.no_eval  "
            strcon = strcon & "AND rc.id_item = i.id_item "
            strcon = strcon & "AND p.id_reunion = t.id  "
            strcon = strcon & "AND p.login = tl.login "
            strcon = strcon & "AND p.permiso = 'M' "
            If op_5 = True Then
                strcon = strcon & "AND t.id = " & name
            End If
            If op_6 = True Then
                strcon = strcon & "AND i.id_item  = " & item
            End If

            If op_7 = True Then
                strcon = strcon & "AND re.fecha >=#" & fec_ini & "#"
            End If

            If op_8 = True Then
                strcon = strcon & "AND re.fecha <=#" & fec_fin & "#"
            End If

            If op_9 = True Then
                strcon = strcon & "AND re.hora_inici_real =#" & h_in & "#"
            End If

            If op_10 = True Then
                strcon = strcon & "AND re.hora_final_real =#" & h_fin & "#"
            End If

            If op_11 = True Then
                strcon = strcon & "AND re.totalpuntos >=" & ptj
            End If

            strcon = strcon & " ORDER BY re.fecha DESC"
            conjDatos = Me.Consultar(strcon)
        End If


    End Sub
    Public Function nombre_rnion(ByVal name As String) As Boolean
        Dim strcon As String
        Dim dts As Integer

        strcon = "SELECT Count(*) FROM TipoReunion WHERE Nombre='" & name & "'"
        dts = Me.ConsultaDato2(strcon)

        If dts = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    Public Sub Finder_Reunion(ByVal tipo As Integer, ByVal name As String, ByVal it1 As String, ByVal it2 As String, ByVal it3 As String, ByVal it4 As String, ByVal it5 As String, ByVal cmt As String)
        Dim strcon As String
        Dim dts As Integer
        Dim sql As String

        strcon = "SELECT Id FROM TipoReunion WHERE tipo_nm='REUNION' "
        strcon = strcon & "and Nombre='" & name & "'"
        dts = Me.ConsultaDato2(strcon)


        If tipo = 1 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 2 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 3 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,a,b,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & it1 & "','" & it2 & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 41 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,a,b,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & it1 & "','" & it2 & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 42 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,c,d,e,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & it3 & "','" & it4 & "','" & it5 & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

    End Sub
    Public Sub admon_usuarios(ByVal opt As Integer, ByVal registro As String, Optional ByVal ind_acta As Integer = 0)
        Dim strcon As String
        If opt = 1 Then
            strcon = "SELECT a.login,a.nombreusuario,b.Rol,a.email,a.administrador,a.activo from t_logins as a, Roles as b"
            strcon = strcon & " where a.rol = b.Id order by 2"
            conjDatos = Me.Consultar(strcon)
        End If


        If opt = 2 Then

            strcon = "SELECT a.login,a.nombreusuario,b.Rol,a.email,a.administrador,a.activo from t_logins as a, Roles as b"
            strcon = strcon & " where a.rol = b.Id"
            strcon = strcon & " and a.login='" & registro & "'"
            If ind_acta <> 1 Then
                conjDatos2 = Me.Consultar(strcon)
            Else
                conjDatos = Me.Consultar(strcon)
            End If

        End If


        If opt = 3 Then
            strcon = "SELECT a.login,a.nombreusuario,b.Rol,a.email,a.administrador,a.activo from t_logins as a, Roles as b where (((a.nombreusuario) Like '%" & registro & "%') AND ((a.rol)=[b].[Id]))"
            conjDatos = Me.Consultar(strcon)
        End If

    End Sub

    Public Function indicador_cumplimiento_dep(ByVal dependencia As String, ByVal fec_ini As Date, ByVal fec_fin As Date) As Integer
        Dim strcon, strcon1 As String
        Dim suma_porcentaje, dts, resultado As Integer


        strcon = "select count(*) as val  from TipoReunion as a , Jerarquia as b,Resultados_Eval as c"
        strcon = strcon & " where(a.area = b.Id)"
        strcon = strcon & " and c.id = a.id "
        strcon = strcon & " and a.tip_asoc = 3 "
        strcon = strcon & " and c.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon = strcon & " and c.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "
        strcon = strcon & " and b.NombreDependencia='" & dependencia & "'"
        dts = Me.ConsultaDato(strcon)

        strcon1 = "select sum(c.porcentaje)as suma  from TipoReunion as a , Jerarquia as b,Resultados_Eval as c"
        strcon1 = strcon1 & " where(a.area = b.Id)"
        strcon1 = strcon1 & " and c.id = a.id "
        strcon1 = strcon1 & " and a.tip_asoc = 3 "
        strcon1 = strcon1 & " and c.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon1 = strcon1 & " and c.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "
        strcon1 = strcon1 & " and b.NombreDependencia='" & dependencia & "'"

        Try
            suma_porcentaje = Me.ConsultaDato(strcon1)
        Catch ex As Exception
            suma_porcentaje = 0
        End Try

        If Not suma_porcentaje = 0 Then
            resultado = (suma_porcentaje / dts)
        End If
        Return resultado

    End Function
    Public Function indicador_cumplimiento(ByVal fec_ini As Date, ByVal fec_fin As Date) As Integer
        Dim strcon, strcon1 As String
        Dim suma_porcentaje, dts, resultado As Integer
        'CUENTA EL NUMERO DE REUNIONES EN EL RANGO DE FECHA ESTIPULADO

        strcon = "select  count(*) as val from Resultados_Eval as a, tipoReunion as b "
        strcon = strcon & "where a.id = b.id "
        strcon = strcon & "and b.tip_asoc = 3 "
        strcon = strcon & "and a.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon = strcon & "and a.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "
        dts = Me.ConsultaDato(strcon)

        'SUMA LOS VALORES DE LOS PORCENTAJES TRAIDOS EN UN RANGO DE FECHA

        strcon1 = "select sum(a.porcentaje)as suma from Resultados_Eval as a, tipoReunion as b "
        strcon1 = strcon1 & "where a.id = b.id "
        strcon1 = strcon1 & "and b.tip_asoc = 3 "
        strcon1 = strcon1 & "and a.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon1 = strcon1 & "and a.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "

        Try
            suma_porcentaje = Me.ConsultaDato(strcon1)
        Catch ex As Exception
            suma_porcentaje = 0
        End Try

        If Not suma_porcentaje = 0 Then
            resultado = (suma_porcentaje / dts)
        End If
        Return resultado

    End Function

    Public Sub indicador_cumplimiento_total(ByVal fec_ini As Date, ByVal fec_fin As Date)
        Dim strcon As String
        strcon = "select  (a.Id) as NumeroR ,(a.No_Eval) as NEval,b.nombre,a.porcentaje,a.fecha from Resultados_Eval as a, tipoReunion as b"
        strcon = strcon & " where a.id = b.id"
        strcon = strcon & " and b.tip_asoc = 3"
        strcon = strcon & " and a.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon = strcon & " and a.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "
        strcon = strcon & " order by 1,5"
        conjDatos = Me.Consultar(strcon)

    End Sub
    Public Function cumplimiento_compromisos_dep(ByVal dependencia As String, ByVal fec_ini As Date, ByVal fec_fin As Date) As Integer
        Dim ttal_compromisos, resultado As Integer
        Dim strcon, strcon2 As String
        Dim cumplidos As String

        strcon = "select count(a.NoID) AS TTAL from compromisos a,TipoReunion b,Jerarquia c "
        strcon = strcon & "where  a.fechai >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "#"
        strcon = strcon & " AND  a.fechai <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "#"
        strcon = strcon & " and a.nombre='ECG - REUNION'"
        strcon = strcon & " and a.status ='VERDE'"
        strcon = strcon & " and a.Tipo=b.Nombre"
        strcon = strcon & " and b.area=c.Id"
        strcon = strcon & " and a.fecharealcierre <= a.fechac"
        '  strcon = strcon & " and a.fecharealcierre is not null"
        strcon = strcon & " and  c.Nombredependencia='" & dependencia & "'"
        cumplidos = Me.ConsultaDato(strcon)


        strcon2 = "select count(a.NoID)as ttal from compromisos a,TipoReunion b,Jerarquia c"
        strcon2 = strcon2 & " where  a.fechai >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "#"
        strcon2 = strcon2 & " and  a.fechai <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "#"
        strcon2 = strcon2 & " and a.nombre='ECG - REUNION'"
        strcon2 = strcon2 & " and a.status <> 'SIN COLOR'"
        strcon2 = strcon2 & " and a.Tipo=b.Nombre"
        strcon2 = strcon2 & " and b.area=c.Id"
        ' strcon2 = strcon2 & " and a.fecharealcierre is not null"
        strcon2 = strcon2 & " and  c.Nombredependencia='" & dependencia & "'"
        Try
            ttal_compromisos = Me.ConsultaDato(strcon2)
        Catch ex As Exception
            ttal_compromisos = 0
        End Try

        If  ttal_compromisos = 0 Then
		     resultado =100
		else
            resultado = (cumplidos / ttal_compromisos) * 100
        End If
		
		
        Return resultado


    End Function


    Public Function cumplimiento_agendas_dep(ByVal dependencia As String, ByVal fec_ini As Date, ByVal fec_fin As Date) As Integer

        Dim strcon, strcon1 As String
        Dim ttal As Integer
        Dim var1 As Integer
        Dim resultado As Integer


        strcon = "select count(*)as ttal  from ("
        strcon = strcon & " select * from TipoReunion a,Jerarquia b,resultados_eval c"
        strcon = strcon & " where a.area = b.Id"
        strcon = strcon & " and a.id=c.Id"
        strcon = strcon & " and b.NombreDependencia='" & dependencia & "'"
        strcon = strcon & " and c.fecha >=  #" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon = strcon & " and c.fecha <=  #" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# )"
        ttal = Me.ConsultaDato(strcon)

        'Empezo a Tiempo la Reuni�n    

        strcon1 = "select count(*)as ttal  from ("
        strcon1 = strcon1 & " select * from TipoReunion a,Jerarquia b,resultados_eval c, resultados_calif d"
        strcon1 = strcon1 & " where a.area = b.Id "
        strcon1 = strcon1 & " and a.id=c.Id"
        strcon1 = strcon1 & " and b.NombreDependencia='" & dependencia & "'"
        strcon1 = strcon1 & " and d.No_eval = c.No_eval"
        strcon1 = strcon1 & " and d.Id_item = 1"
        strcon1 = strcon1 & " and d.calificacion ='3'"
        strcon1 = strcon1 & " and c.fecha >= #" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
        strcon1 = strcon1 & " and c.fecha <= #" & Format(CDate(fec_fin), "yyyy/MM/dd") & "#  "
        strcon1 = strcon1 & " and  c.No_eval in ( select c.no_eval from TipoReunion a,Jerarquia b,resultados_eval c, resultados_calif d"
        strcon1 = strcon1 & " where a.area = b.Id"
        strcon1 = strcon1 & " and a.id=c.Id"
        strcon1 = strcon1 & " and b.NombreDependencia='" & dependencia & "'"
        strcon1 = strcon1 & " and d.No_eval = c.No_eval"
        strcon1 = strcon1 & " and d.Id_item = 12"
        strcon1 = strcon1 & " and d.calificacion ='3'))"


        Try
            var1 = Me.ConsultaDato(strcon1)
        Catch ex As Exception
            var1 = 0
        End Try


        Try
            resultado = (var1 / ttal) * 100
        Catch ex As Exception
            resultado = 0
        End Try


        'End If

        Return resultado
    End Function

    Public Sub pro_jerarquico()
        Dim sql As String
        sql = "select idrecurso,hijo,hijo_tx,attachitem_tp,attachment_tp,locationitem_id,locationitem_tp,PADRE_tx, PADRE,order_nb FROM  jquias"
        conjDatos = Me.Consultar(sql)
    End Sub

    Public Sub Reuniones_edicion(ByVal cd As Integer)

        Dim strcon As String
        strcon = "SELECT Id,Nombre,moderador,horainicio,horafin,duracion FROM TipoReunion WHERE Id=" & cd
        conjDatos = Me.Consultar(strcon)

    End Sub

    Public Sub Reuniones_Update(ByVal Id As Integer, ByVal nombre As String, ByVal moderador As String, ByVal hinicio As String, ByVal hfin As String, ByVal duracion As String)

        Dim sql As String

        sql = "UPDATE TipoReunion SET "
        sql = sql & " Nombre= '" & nombre & "',"
        sql = sql & " moderador='" & moderador & "',"
        sql = sql & " horaInicio='" & hinicio & "',"
        sql = sql & " horaFin='" & hfin & "',"
        sql = sql & " duracion='" & duracion & "'"
        sql = sql & " WHERE Id = " & Id
        Me.GuardarBD(sql)
    End Sub
    Public Sub compromisos_update(ByVal n_antiguo As String, ByVal n_nuevo As String)
        Dim sql2 As String
        sql2 = "UPDATE compromisos SET "
        sql2 = sql2 & " Tipo= '" & n_nuevo & "'"
        sql2 = sql2 & " WHERE Tipo= '" & n_antiguo & "'"
        Me.GuardarBD(sql2)
    End Sub

    Public Sub rnion_Update_gral(ByVal Id As Integer, ByVal name As String, ByVal tipo As String, ByVal inis As String, ByVal fin As String, ByVal dura As String, ByVal fec_ini As String, ByVal fec_fin As String, ByVal area As String, ByVal op_1 As String, ByVal op_2 As String, ByVal op_3 As String, ByVal op_4 As String, ByVal op_5 As String)
        Dim sql As String

        If op_1 = True Then
            sql = "UPDATE TipoReunion SET "
            sql = sql & " Nombre= '" & name & "'"
            sql = sql & " WHERE Id = " & Id
            Me.GuardarBD(sql)

        End If

        If op_2 = True Then
            sql = "UPDATE TipoReunion SET "
            sql = sql & " tip_asoc= '" & tipo & "'"
            sql = sql & " WHERE Id = " & Id
            Me.GuardarBD(sql)

        End If

        If op_3 = True Then
            sql = "UPDATE TipoReunion SET "
            sql = sql & " horainicio= '" & inis & "',"
            sql = sql & " horafin= '" & fin & "',"
            sql = sql & " duracion= '" & dura & "'"
            sql = sql & " WHERE Id = " & Id
            Me.GuardarBD(sql)
        End If

        If op_4 = True Then
            sql = "UPDATE TipoReunion SET "
            sql = sql & " fechainicio= '" & fec_ini & "',"
            sql = sql & " fechafinaliza= '" & fec_fin & "'"
            sql = sql & " WHERE Id = " & Id
            Me.GuardarBD(sql)

        End If

        If op_5 = True Then
            sql = "UPDATE TipoReunion SET "
            sql = sql & " area= '" & area & "'"
            sql = sql & " WHERE Id = " & Id
            Me.GuardarBD(sql)
        End If



    End Sub

    Public Sub R_edicion(ByVal cd As Integer)
        Dim strcon As String

        strcon = "select a.Id,a.Nombre,a.FechaInicio,a.FechaFinaliza,a.FechaIngreso,a.area,a.tip_asoc,a.seguimiento,b.NombreDependencia,c.NombreReunion,a.tipo_nm,a.horainicio,a.horaFin,a.duracion,a.ValidarAsistentes"
        strcon = strcon & " from TipoReunion a, Jerarquia b,t_reuniones c"
        strcon = strcon & " where a.area = b.Id"
        strcon = strcon & " and a.tip_asoc=c.Id"
        strcon = strcon & " and a.Id=" & cd

        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub R_Frecuencia(ByVal cd As Integer)
        Dim strcon As String
        strcon = "SELECT comentario FROM Tbl_Frecuencia WHERE Id=" & cd
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub R_Moderador(ByVal lgin As String)
        Dim strcon As String
        strcon = "SELECT nombreusuario FROM t_logins WHERE Login='" & lgin & "'"
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub Tp_rnion(ByVal idrnion As String)
        Dim strcon As String
        strcon = "SELECT Id,NombreReunion FROM T_REUNIONES where Id=" & idrnion
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub Resul_eval_porcentaje(ByVal No_Eval As String)
        Dim strcon As String
        strcon = "SELECT No_Eval,Id,login,porcentaje FROM Resultados_Eval where No_Eval=" & No_Eval
        conjDatos = Me.Consultar(strcon)
    End Sub


    Public Sub Finder_Update(ByVal tipo As Integer, ByVal name As String, ByVal it1 As String, ByVal it2 As String, ByVal it3 As String, ByVal it4 As String, ByVal it5 As String, ByVal cmt As String)
        Dim strcon As String
        Dim dts As Integer
        Dim sql As String

        strcon = "SELECT Id FROM TipoReunion WHERE tipo_nm='REUNION' "
        strcon = strcon & "and Nombre='" & name & "'"
        dts = Me.ConsultaDato2(strcon)


        strcon = " delete from Tbl_Frecuencia where Id =" & dts
        Me.GuardarBD(strcon)


        If tipo = 1 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 2 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 3 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,a,b,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & it1 & "','" & it2 & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 41 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,a,b,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & it1 & "','" & it2 & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

        If tipo = 42 Then
            sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,c,d,e,comentario) VALUES "
            sql = sql & "(" & dts & ",'" & name & "','" & it3 & "','" & it4 & "','" & it5 & "','" & cmt & "')"
            Me.GuardarBD(sql)
        End If

    End Sub

    Public Sub Tp_Roles()
        Dim strcon As String
        strcon = "SELECT Id,Rol FROM Roles"
        conjDatos = Me.Consultar(strcon)
    End Sub


    Public Sub Usuarios_Update(ByVal login As String, ByVal nombre As String, ByVal rol As String, ByVal mail As String, ByVal act As String)

        Dim sql As String

        sql = "UPDATE t_logins SET "
        sql = sql & " Login= '" & login & "',"
        sql = sql & " nombreusuario='" & nombre & "',"
        sql = sql & " Rol='" & rol & "',"
        sql = sql & " EMail='" & mail & "',"
        sql = sql & " Activo='" & act & "'"
        sql = sql & " WHERE Login = '" & login & "'"
        Me.GuardarBD(sql)
    End Sub
    Public Sub Usuarios_Deleted(ByVal login As String)
        Dim sql As String
        sql = "DELETE FROM t_logins"
        sql = sql & " WHERE Login = '" & login & "'"
        Me.GuardarBD(sql)

    End Sub

    Public Sub Reunion_Deleted(ByVal nrnion As String)
        Dim sql As String


        sql = "DELETE FROM TipoReunion"
        sql = sql & " WHERE Id = '" & nrnion & "'"
        Me.GuardarBD(sql)

    End Sub
    Public Sub Archivo_Deleted(ByVal n_archivo As String)
        Dim sql As String
        sql = "DELETE FROM Reunion_archivos"
        sql = sql & " WHERE nombre = '" & n_archivo & "'"
        Me.GuardarBD(sql)

    End Sub
    Public Sub consulta_frecuencia(ByVal id As Integer)
        Dim strcon As String
        strcon = "SELECT Id,Reunion,a,b,c,d,e,comentario FROM Tbl_Frecuencia WHERE Id=" & id
        conjDatos = Me.Consultar(strcon)
    End Sub


    Public Sub Usuario_correo(ByVal nombre As String)
        Dim strcon As String
        strcon = "SELECT nombreusuario FROM t_logins WHERE Login='" & nombre & "'"
        conjDatos = Me.Consultar(strcon)
    End Sub

    'Public Function Trae_Festivos() As String
    '    Dim sql As String

    '    sql = "select starttick_dt,to_char(starttick_dt,'dd/mm/yyyy') Festivos " & _
    '        "from rdbmx.numberpv_starttick_vt np where(np.numberpv_id = 11160787 " & _
    '        "and np.starttick_dt >= to_date('01-01-2009','dd-mm-yyyy') " & _
    '        "and np.starttick_dt < to_date('01-01-2010','dd-mm-yyyy') " & _
    '        "order by 1"
    '    Return sql
    'End Function

    Public Sub Trae_Festivos()
        Dim sql As String
        sql = "select * from Cal_festivos"
        conjDatos = Me.Consultar(sql)
    End Sub


    Public Sub Tp_dependencias()
        Dim strcon As String
        strcon = "SELECT DISTINCT(TIPO) FROM JERARQUIA ORDER BY  1 DESC"
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub update_frecuencia(ByVal frecuencia As String, ByVal id As Integer, ByVal name As String, ByVal it1 As String, ByVal it2 As String, ByVal it3 As String, ByVal it4 As String, ByVal it5 As String, ByVal cmt As String)
        Dim sql As String


        sql = "DELETE FROM Tbl_Frecuencia"
        sql = sql & " WHERE Id = " & id
        Me.GuardarBD(sql)

        Select Case frecuencia
            Case "diaria"
                sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
                sql = sql & "(" & id & ",'" & name & "','" & cmt & "')"
                Me.GuardarBD(sql)

            Case "diaria_t"
                sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
                sql = sql & "(" & id & ",'" & name & "','" & cmt & "')"
                Me.GuardarBD(sql)

            Case "semanal"
                sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,a,b,comentario) VALUES "
                sql = sql & "(" & id & ",'" & name & "','" & it1 & "','" & it2 & "','" & cmt & "')"
                Me.GuardarBD(sql)

            Case "Mensual1"
                sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,a,b,comentario) VALUES "
                sql = sql & "(" & id & ",'" & name & "','" & it1 & "','" & it2 & "','" & cmt & "')"
                Me.GuardarBD(sql)

            Case "Mensual2"
                sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,c,d,e,comentario) VALUES "
                sql = sql & "(" & id & ",'" & name & "','" & it3 & "','" & it4 & "','" & it5 & "','" & cmt & "')"
                Me.GuardarBD(sql)

            Case "Mensual3"

                sql = "INSERT INTO Tbl_Frecuencia (Id,Reunion,comentario) VALUES "
                sql = sql & "(" & id & ",'" & name & "','" & cmt & "')"
                Me.GuardarBD(sql)

        End Select

    End Sub
    Public Sub update_reuniones(ByVal idrenion As Integer, ByVal nombre_reunion As String, ByVal tipo_reunion As Integer, ByVal h_inicio As Date, ByVal h_fin As Date, ByVal frecuencia As Integer, ByVal dependencia As Integer, ByVal val_asis As Integer)
        Dim sql As String
        sql = "UPDATE TipoReunion SET "
        sql = sql & " Nombre= '" & nombre_reunion & "',"
        sql = sql & " Seguimiento= '" & frecuencia & "',"
        sql = sql & " tip_asoc= '" & tipo_reunion & "',"
        sql = sql & " horainicio= #" & Format(CDate(h_inicio), "HH:mm:ss") & "#,"
        sql = sql & " horafin= #" & Format(CDate(h_fin), "HH:mm:ss") & "#,"
        sql = sql & " area= '" & dependencia & "',"
        sql = sql & " validarAsistentes= " & val_asis
        sql = sql & " WHERE Id = " & idrenion
        Me.GuardarBD(sql)


    End Sub

    Public Sub all_reuniones()
        Dim sql As String
        sql = "SELECT a.Id,b.NombreDependencia,a.Nombre,a.horainicio,a.horaFin,c.NombreReunion,a.Activa from TipoReunion a,Jerarquia b,T_REUNIONES c"
        sql = sql & " where a.area = b.Id And a.tip_asoc = c.Id ORDER BY 3"
        conjDatos = Me.Consultar(sql)
    End Sub


    Public Sub cargar_mnu()
        Dim strcon As String
        strcon = "select menuid,descripcion,posicion,padreid,icono,habilitado,url From Menu where habilitado='s' order by 1"
        conjDatos = Me.Consultar(strcon)
    End Sub



    Public Sub Guardar_Info_archivos(ByVal reunion As Integer, ByVal nombre As String, ByVal ruta As String, ByVal usuario As String, ByVal tp_documento As String)
        Dim sql As String

        Dim strcon As String
        Dim dts As Integer

        strcon = "SELECT Count(*) FROM Reunion_archivos WHERE reunion=" & reunion & " and nombre='" & nombre & "'"
        dts = Me.ConsultaDato2(strcon)

        If dts = 0 Then
            sql = "INSERT INTO Reunion_archivos (reunion,nombre,ruta,usuario,tp_Documento) VALUES "
            sql = sql & "(" & reunion & ",'" & nombre & "','" & ruta & "','" & usuario & "','" & tp_documento & "')"
            Me.GuardarBD(sql)
        End If

    End Sub


    Public Sub Consultar_Reuniones(ByVal tipo As String, Optional ByVal parametro As String = "", Optional ByVal estado_p As String = "", Optional ByVal tipo_p As String = "", Optional ByVal nombre_p As String = "")
        Dim strcon As String
        'strcon = "SELECT * FROM TipoReunion WHERE tipo_nm = 'REUNION' ORDER BY Nombre "
        'strcon = "SELECT * FROM TipoReunion WHERE tipo_nm = 'REUNION' AND activa = -1 ORDER BY Nombre "
        Select Case tipo

            'FILTRADO POR NOMBRE
            Case "filtro"
                Try
                    strcon = "SELECT TipoReunion.Id,TipoReunion.Nombre, TipoReunion.area,Jerarquia.NombreDependencia,"
                    strcon = strcon & " t_reuniones.Nombrereunion, TipoReunion.Activa,TipoReunion.horaInicio, TipoReunion.horaFin,Tbl_Frecuencia.comentario,"
                    strcon = strcon & " TipoReunion.duracion, TipoReunion.validarAsistentes"
                    strcon = strcon & " FROM TipoReunion,T_reuniones,Jerarquia,Tbl_Frecuencia WHERE  TipoReunion.tipo_nm='REUNION' "
                    strcon = strcon & " AND TipoReunion.tip_asoc=t_reuniones.Id "
                    strcon = strcon & " AND TipoReunion.area= Jerarquia.Id"
                    strcon = strcon & " AND TipoReunion.Id=Tbl_Frecuencia.Id"
                    strcon = strcon & " AND TipoReunion.Nombre LIKE '%" & parametro & "%'"
                    conjDatos = Me.Consultar(strcon)
                Catch ex As Exception
                End Try

            Case "tipo"
                strcon = "SELECT TipoReunion.Id,TipoReunion.Nombre, TipoReunion.area,Jerarquia.NombreDependencia,"
                strcon = strcon & " t_reuniones.Nombrereunion, TipoReunion.Activa,TipoReunion.horaInicio, TipoReunion.horaFin,Tbl_Frecuencia.comentario,"
                strcon = strcon & " TipoReunion.duracion, TipoReunion.validarAsistentes"
                strcon = strcon & " FROM TipoReunion,T_reuniones,Jerarquia,Tbl_Frecuencia WHERE  TipoReunion.tipo_nm='REUNION' "
                strcon = strcon & " AND TipoReunion.tip_asoc=t_reuniones.Id "
                strcon = strcon & " AND TipoReunion.area= Jerarquia.Id"
                strcon = strcon & " AND TipoReunion.Id=Tbl_Frecuencia.Id"
                strcon = strcon & " AND TipoReunion.tip_asoc=" & parametro & " ORDER BY 2"
                conjDatos = Me.Consultar(strcon)

            Case "estado"
                strcon = "SELECT TipoReunion.Id,TipoReunion.Nombre, TipoReunion.area,Jerarquia.NombreDependencia,"
                strcon = strcon & " t_reuniones.Nombrereunion, TipoReunion.Activa,TipoReunion.horaInicio, TipoReunion.horaFin,Tbl_Frecuencia.comentario,"
                strcon = strcon & " TipoReunion.duracion, TipoReunion.validarAsistentes"
                strcon = strcon & " FROM TipoReunion,T_reuniones,Jerarquia,Tbl_Frecuencia WHERE  TipoReunion.tipo_nm='REUNION' "
                strcon = strcon & " AND TipoReunion.tip_asoc=t_reuniones.Id "
                strcon = strcon & " AND TipoReunion.area= Jerarquia.Id"
                strcon = strcon & " AND TipoReunion.Id=Tbl_Frecuencia.Id"
                strcon = strcon & " AND TipoReunion.activa=" & parametro & " ORDER BY 2"
                conjDatos = Me.Consultar(strcon)

            Case "todas"
                strcon = "SELECT TipoReunion.Id,TipoReunion.Nombre, TipoReunion.area,Jerarquia.NombreDependencia,"
                strcon = strcon & " t_reuniones.Nombrereunion, TipoReunion.Activa,TipoReunion.horaInicio, TipoReunion.horaFin,Tbl_Frecuencia.comentario,"
                strcon = strcon & " TipoReunion.duracion, TipoReunion.validarAsistentes"
                strcon = strcon & " FROM TipoReunion,T_reuniones,Jerarquia,Tbl_Frecuencia WHERE  TipoReunion.tipo_nm='REUNION' "
                strcon = strcon & " AND TipoReunion.tip_asoc=t_reuniones.Id "
                strcon = strcon & " AND TipoReunion.area= Jerarquia.Id"
                strcon = strcon & " AND TipoReunion.Id=Tbl_Frecuencia.Id"
                strcon = strcon & " ORDER BY 2"
                conjDatos = Me.Consultar(strcon)

            Case "parametros"

                strcon = "SELECT TipoReunion.Id,TipoReunion.Nombre, TipoReunion.area,Jerarquia.NombreDependencia,"
                strcon = strcon & " t_reuniones.Nombrereunion, TipoReunion.Activa,TipoReunion.horaInicio, TipoReunion.horaFin,Tbl_Frecuencia.comentario,"
                strcon = strcon & " TipoReunion.duracion, TipoReunion.validarAsistentes"
                strcon = strcon & " FROM TipoReunion,T_reuniones,Jerarquia,Tbl_Frecuencia WHERE  TipoReunion.tipo_nm='REUNION' "
                strcon = strcon & " AND TipoReunion.tip_asoc=t_reuniones.Id "
                strcon = strcon & " AND TipoReunion.area= Jerarquia.Id"
                strcon = strcon & " AND TipoReunion.Id=Tbl_Frecuencia.Id"

                If estado_p <> "---Seleccione Estado---" Then
                    strcon = strcon & " AND TipoReunion.activa=" & estado_p & ""
                End If
                If tipo_p <> "---Seleccione Tipo---" Then
                    strcon = strcon & " AND TipoReunion.tip_asoc=" & tipo_p & ""
                End If
                If nombre_p <> "" Then
                    strcon = strcon & " AND TipoReunion.Nombre LIKE '%" & nombre_p & "%'"
                End If
                strcon = strcon & " ORDER BY 2"
                conjDatos = Me.Consultar(strcon)

        End Select

    End Sub

    Public Sub Reporte_asistencias(ByVal registro As String, ByVal bandera As Integer, ByVal idrnion As Integer, ByVal fec_ini As Date, ByVal fec_fin As Date)
        Dim sql As String


        Select Case bandera


            Case 2
                sql = "select a.Id,a.nombre,b.fecha,c.nombreusuario,b.horainicio,iif(b.puntual='NO','',b.puntual) as puntual,b.suplente,b.excusa,b.comentario "
                sql = sql & " from tiporeunion a, asistentesreunion b, t_logins c"
                sql = sql & " where a.id = b.idreunion"
                sql = sql & " and b.login=c.Login"
                sql = sql & " and b.login='" & registro & "'"
                sql = sql & " order by 2,3"

                conjDatos = Me.Consultar(sql)

            Case 3
                sql = "select a.Id,a.nombre,b.fecha,c.nombreusuario,b.horainicio,iif(b.puntual='NO','',b.puntual) as puntual,b.suplente,b.excusa,b.comentario "
                sql = sql & " from tiporeunion a, asistentesreunion b, t_logins c"
                sql = sql & " where a.id = b.idreunion"
                sql = sql & " and b.login=c.Login"
                sql = sql & " and b.idreunion=" & idrnion
                sql = sql & " and b.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
                sql = sql & " and b.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "
                sql = sql & " order by 2,3"

                conjDatos = Me.Consultar(sql)


            Case 1
                sql = "select a.Id,a.nombre,b.fecha,c.nombreusuario,b.horainicio,iif(b.puntual='NO','',b.puntual) as puntual,b.suplente,b.excusa,b.comentario "
                sql = sql & " from tiporeunion a, asistentesreunion b, t_logins c"
                sql = sql & " where a.id = b.idreunion"
                sql = sql & " and b.login=c.Login"
                sql = sql & " and b.idreunion=" & idrnion
                sql = sql & " and b.login='" & registro & "'"
                sql = sql & " and b.fecha >=#" & Format(CDate(fec_ini), "yyyy/MM/dd") & "# "
                sql = sql & " and b.fecha <=#" & Format(CDate(fec_fin), "yyyy/MM/dd") & "# "
                sql = sql & " order by 2,3"

                conjDatos = Me.Consultar(sql)



        End Select


    End Sub

    Public Sub prod_cambiar_estado(ByVal Id_reunion As Integer)
        Dim sql As String

        sql = "select Activa from tiporeunion where id=" & Id_reunion
        Dim dato As String = Me.ConsultaDato(sql)

        If dato = "True" Then
            sql = " UPDATE tiporeunion SET Activa=0"
            sql = sql & " where id=" & Id_reunion
            Me.GuardarBD(sql)
        End If

        If dato = "False" Then
            sql = " UPDATE tiporeunion SET Activa=-1"
            sql = sql & " where id=" & Id_reunion
            Me.GuardarBD(sql)
        End If

    End Sub
    Public Function prod_busca_compromisos(ByVal Id_reunion As String) As Boolean

        Dim sql, sql2 As String
        Dim verificados As Integer

        sql = "select Nombre from tiporeunion where id=" & id_reunion
        Dim dato As String = Me.ConsultaDato(sql)

        sql2 = "select count(*) from compromisos where tipo='" & dato & "' and verificado=0"
        verificados = Me.ConsultaDato(sql2)

        If verificados = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub pro_buscar_horas(ByVal reunion As String)


        Dim sql As String
        sql = "SELECT DISTINCT ASISTENTESREUNION.idreunion, ASISTENTESREUNION.fecha, Format(ASISTENTESREUNION.horaInicio,'hh:nn:ss') AS hora, Resultados_Eval.porcentaje, ASISTENTESREUNION.fecha " & _
              "FROM ASISTENTESREUNION INNER JOIN Resultados_Eval ON ASISTENTESREUNION.No_Eval = Resultados_Eval.No_Eval " & _
              "WHERE(((ASISTENTESREUNION.[idreunion]) = " & reunion & ") And year(ASISTENTESREUNION.fecha)>=" & Now.year & " And ((ASISTENTESREUNION.fecha) = (Resultados_Eval.fecha))) " & _
              "ORDER BY ASISTENTESREUNION.fecha DESC;"

        conjDatos = Me.Consultar(sql)

    End Sub

    Public Sub buscaagenda()
        Dim strcon As String
        strcon = "SELECT     tbl_pntos_rnion.agenda, tbl_pntos_rnion.Id_puntos, Acta_punto.id_acta FROM (Acta_punto INNER JOIN  tbl_pntos_rnion ON Acta_punto.id_punto = tbl_pntos_rnion.Id_puntos) WHERE     (Acta_punto.id_acta = " & id_acta & ")"
        conjDatos = Me.Consultar_nbre_tbl(strcon, , "agenda")
    End Sub
        
    Public Function guarda_punto(ByVal reunion As Integer, ByVal fecha As String, ByVal h_inicio As String, ByVal agenda As String, Optional ByVal accion As String = "") As String
        Dim sql, strcon As String
        Dim idpunto As String

        Select Case accion

            Case "Guardar"
                sql = "INSERT INTO tbl_pntos_rnion (id_reunion,fecha,hora,agenda) VALUES "
                sql = sql & "(" & reunion & ",#" & Format(CDate(fecha), "dd/MM/yyyy") & "#, #" & Format(CDate(h_inicio), "HH:mm:ss") & "#,'" & agenda & "')"
                Me.GuardarBD(sql)

            Case "Actualizar"
                sql = "Update tbl_pntos_rnion set agenda='" & agenda & "' " & _
                      "Where id_reunion=" & reunion & " and fecha=#" & Format(CDate(fecha), "dd/MM/yyyy") & "# and hora= #" & Format(CDate(h_inicio), "HH:mm:ss") & "#"
                Me.GuardarBD(sql)
        End Select

        strcon = "SELECT id_puntos"
        strcon &= " FROM tbl_pntos_rnion"
        strcon &= " WHERE id_reunion= " & reunion
        strcon &= " AND fecha = #" & Format(CDate(fecha), "dd/MM/yyyy") & "#"
        strcon &= " AND hora=#" & Format(CDate(h_inicio), "HH:mm:ss") & "#"
        idpunto = Me.ConsultaDato(strcon)

        Return idpunto

    End Function


    Public Sub puntoexiste(ByVal Reunion As String, ByVal fecha As String, ByVal hora As String)

        Dim strcon As String
        strcon = "SELECT id_puntos,id_reunion,fecha,hora,agenda "
        strcon &= " FROM tbl_pntos_rnion"
        strcon &= " WHERE id_reunion= " & Reunion
        strcon &= " AND fecha = #" & Format(CDate(fecha), "dd/MM/yyyy") & "#"
        strcon &= " AND hora=#" & Format(CDate(hora), "HH:mm:ss") & "#"
        conjDatos = Me.Consultar(strcon)

    End Sub


    Public Sub busqueda_comentario(ByVal id As String)

        Dim strcon As String
        strcon = "SELECT * "
        strcon &= " FROM tbl_gral_puntos"
        strcon &= " WHERE id_punto= " & id

        conjDatos = Me.Consultar(strcon)

    End Sub



    Public Sub desarrollo(ByVal punto As Integer, ByVal txPunto As String, ByVal responsable As String, ByVal tipo As String, Optional ByVal id_puntos_reunion As Integer = 0)

        Dim sql As String

        Select Case tipo
            Case "Insert"
                sql = "INSERT INTO tbl_gral_puntos (id_punto, punto,responsable) VALUES("
                sql = sql & punto & ",'"
                sql = sql & txPunto & "','"
                sql = sql & responsable & "')"
                Me.GuardarBD(sql)

            Case "Actualizar"
                sql = "UPDATE tbl_gral_puntos SET "
                sql = sql & " punto= '" & txPunto & "',"
                sql = sql & " responsable= '" & responsable & "'"
                sql = sql & " WHERE id_puntos_reunion = " & id_puntos_reunion & " and id_punto=" & punto
                Me.GuardarBD(sql)



        End Select

    End Sub

    Public Sub Tema_Deleted(ByVal punto As String)
        Dim sql As String
        sql = "DELETE FROM tbl_gral_puntos"
        sql = sql & " WHERE id_puntos_reunion = " & punto
        Me.GuardarBD(sql)

    End Sub


    Public Sub busqueda_moderadoresporreunion(ByVal id As String)

        Dim strcon As String
        strcon = "select a.login,b.nombreusuario  from permisos a,t_logins b"
        strcon &= " where a.login = b.login and b.Activo = -1"
        strcon &= " and a.id_reunion= " & id
        conjDatos = Me.Consultar(strcon)

    End Sub

    Public Sub asistentes_reunion(ByVal id_reunion As String, ByVal dia As String, ByVal hora As String)
        Dim strcon As String
        strcon = "SELECT idreunion, login, fecha, horaInicio, horaFin, puntual, suplente, excusa, bandera, No_Eval, comentario FROM ASISTENTESREUNION where idreunion=" & id_reunion & "and fecha= #" & Format(CDate(dia), "yyyy/MM/dd") & "# and horaInicio=#" & Format(CDate(hora), "hh:mm:ss") & "#"
        conjDatos = Me.Consultar(strcon)
    End Sub

    Private _id_acta As Integer
    Private _num_acta As Integer
    Private _fecha_acta As Date
    Private _estado As Integer
    Private _codigo_formato As String
    Private _version As String
    Private _nombre_acta As String
    Private _Departamento_acta As String
    Private _tema_acta As String
    Private _ubicacion As String
    Private _Fecha_eval As Date
    Private _hora_inicio As Date
    Private _hora_fin As Date
    Private _Objetivo As String
    Private _Eval_reunion As Integer
    Private _asistentes As String
    Private _reviso As String
    Private _Aprobo As String
    Private _correo_aprobo As String
    Private _id_compromisos As Integer
    Private _login As String
    Private _id_punto As Integer
    Private _agenda As String

    Private _id_puntos_reunion As Integer

    Public Property id_acta() As Integer
        Get
            Return _id_acta
        End Get
        Set(ByVal value As Integer)
            _id_acta = value
        End Set
    End Property
    Public Property num_acta() As Integer
        Get
            Return _num_acta
        End Get
        Set(ByVal value As Integer)
            _num_acta = value
        End Set
    End Property
    Public Property fecha_acta() As Date
        Get
            Return _fecha_acta
        End Get
        Set(ByVal value As Date)
            _fecha_acta = value
        End Set
    End Property
    Public Property estado() As Integer
        Get
            Return _estado
        End Get
        Set(ByVal value As Integer)
            _estado = value
        End Set
    End Property
    Public Property codigo_formato() As String
        Get
            Return _codigo_formato
        End Get
        Set(ByVal value As String)
            _codigo_formato = value
        End Set
    End Property
    Public Property version() As String
        Get
            Return _version
        End Get
        Set(ByVal value As String)
            _version = value
        End Set
    End Property
    Public Property nombre_acta() As String
        Get
            Return _nombre_acta
        End Get
        Set(ByVal value As String)
            _nombre_acta = value
        End Set
    End Property
    Public Property Departamento_acta() As String
        Get
            Return _Departamento_acta
        End Get
        Set(ByVal value As String)
            _Departamento_acta = value
        End Set
    End Property
    Public Property tema_acta() As String
        Get
            Return _tema_acta
        End Get
        Set(ByVal value As String)
            _tema_acta = value
        End Set
    End Property
    Public Property ubicacion() As String
        Get
            Return _ubicacion
        End Get
        Set(ByVal value As String)
            _ubicacion = value
        End Set
    End Property
    Public Property Fecha_eval() As Date
        Get
            Return _Fecha_eval
        End Get
        Set(ByVal value As Date)
            _Fecha_eval = value
        End Set
    End Property
    Public Property hora_inicio() As Date
        Get
            Return _hora_inicio
        End Get
        Set(ByVal value As Date)
            _hora_inicio = value
        End Set
    End Property
    Public Property hora_fin() As Date
        Get
            Return _hora_fin
        End Get
        Set(ByVal value As Date)
            _hora_fin = value
        End Set
    End Property
    Public Property Objetivo() As String
        Get
            Return _Objetivo
        End Get
        Set(ByVal value As String)
            _Objetivo = value
        End Set
    End Property
    Public Property Eval_reunion() As Integer
        Get
            Return _Eval_reunion
        End Get
        Set(ByVal value As Integer)
            _Eval_reunion = value
        End Set
    End Property
    Public Property asistentes() As String
        Get
            Return _asistentes
        End Get
        Set(ByVal value As String)
            _asistentes = value
        End Set
    End Property
    Public Property reviso() As String
        Get
            Return _reviso
        End Get
        Set(ByVal value As String)
            _reviso = value
        End Set
    End Property
    Public Property Aprobo() As String
        Get
            Return _Aprobo
        End Get
        Set(ByVal value As String)
            _Aprobo = value
        End Set
    End Property
    Public Property correo_aprobo() As String
        Get
            Return _correo_aprobo
        End Get
        Set(ByVal value As String)
            _correo_aprobo = value
        End Set
    End Property
    Public Property id_compromisos() As Integer
        Get
            Return _id_compromisos
        End Get
        Set(ByVal value As Integer)
            _id_compromisos = value
        End Set
    End Property
    Public Property login() As String
        Get
            Return _login
        End Get
        Set(ByVal value As String)
            _login = value
        End Set
    End Property
    Public Property id_punto() As Integer
        Get
            Return _id_punto
        End Get
        Set(ByVal value As Integer)
            _id_punto = value
        End Set
    End Property

    Public Property agenda() As String
        Get
            Return _agenda
        End Get
        Set(ByVal value As String)
            _agenda = value
        End Set
    End Property

    Public Property id_puntos_reunion() As Integer
        Get
            Return _id_puntos_reunion
        End Get
        Set(ByVal value As Integer)
            _id_puntos_reunion = value
        End Set
    End Property

    Public Sub Compromisos(ByVal tipo As Integer)
        Dim strcon As String

        Select Case tipo

            Case 1
                strcon = "SELECT  Accion, RESPONSABLE, FECHAC " & _
                         " FROM         Compromisos where tipo='" & nombre_acta & "' and fecha_reunion= #" & Format(CDate(Fecha_eval), "dd/MM/yyyy") & "# and hora_reunion = #" & Format(CDate(hora_inicio), "HH:mm:ss") & "#"
                conjDatos2 = Me.Consultar(strcon)
            Case 2
                strcon = "SELECT  NoID,Accion, RESPONSABLE, FECHAC " & _
                                      " FROM         Compromisos where tipo='" & nombre_acta & "' and fecha_reunion= #" & Format(CDate(Fecha_eval), "dd/MM/yyyy") & "# and hora_reunion = #" & Format(CDate(hora_inicio), "HH:mm:ss") & "#"
                conjDatos2 = Me.Consultar(strcon)
            Case 3
                strcon = "SELECT     Compromisos.Accion, Compromisos.RESPONSABLE as Responsable, Compromisos.FECHAC as Fecha  " & _
                         "FROM         (Acta_Compromisos INNER JOIN " & _
                         "Compromisos ON Acta_Compromisos.id_compromisos = Compromisos.NoID) " & _
                         "WHERE     (Acta_Compromisos.id_acta = " & id_acta & ")"
                conjDatos2 = Me.Consultar_nbre_tbl(strcon, , "Compromisos")
        End Select

    End Sub

    Public Sub Acta(ByVal accion As String, Optional ByVal estado As Integer = 0, Optional ByVal id_reunion As Integer = 0, Optional ByVal id_acta As Integer = 0)
        Dim strcon As String
        Try

            Select Case accion

                Case "newconsecutivo"
                    strcon = "insert into consecutivo_acta values(" & id_reunion & ",0) "
                    Me.GuardarBD(strcon)

                Case "Sig_Num_acta"
                    'si no existe la reunion que pasa? se debe crear el consecutivo en ceros
                    strcon = " SELECT     consecutivo + 1 as sig_acta " & _
                             " FROM     consecutivo_acta where id_reunion=" & id_reunion & ""
                    conjDatos = Me.Consultar(strcon)

                    strcon = "UPDATE    consecutivo_acta " & _
                                                    " SET     consecutivo = " & conjDatos.Tables(0).Rows(0).Item("sig_acta") & "" & _
                                                    " WHERE     id_reunion=" & id_reunion & ""
                    Me.GuardarBD(strcon)

                Case "CrearActaini"
                    strcon = " INSERT INTO Acta " & _
                                 " (id_acta,num_acta) " & _
                                 " VALUES     (" & id_acta & "," & num_acta & ") "
                    Me.GuardarBD(strcon)

                Case "CrearActa"

                    strcon = " INSERT INTO Acta " & _
                                    " (id_acta,num_acta, fecha_acta, estado, codigo_formato, version, nombre_acta, Departamento_acta, tema_acta, ubicacion, Fecha_eval, hora_inicio, hora_fin,  " & _
                                    " Objetivo, Eval_reunion, asistentes, reviso, Aprobo, correo_aprobo) " & _
                                    " VALUES     (" & id_acta & "," & num_acta & ",  #" & Format(CDate(fecha_acta), "dd/MM/yyyy") & "#, " & estado & ", '" & codigo_formato & "', '" & version & "', '" & nombre_acta & "', '" & Departamento_acta & "', '" & tema_acta & "', '" & ubicacion & "',   #" & Format(CDate(Fecha_eval), "dd/MM/yyyy") & "#,  #" & Format(CDate(hora_inicio), "HH:mm:ss") & "#, #" & Format(CDate(hora_fin), "HH:mm:ss") & "#, '" & Objetivo & "', " & Eval_reunion & ", '" & asistentes & "', '" & reviso & "', '" & aprobo & "', '" & correo_aprobo & "') "
                    Me.GuardarBD(strcon)

                Case "Actualizar"

                    'aqui estado me indica si el acta esta en revision o modificado puedo modificar menos cuando sea oficial
                    If estado = 1 Or estado = 2 Then
                        strcon = "UPDATE    Acta " & _
                                                 " SET              num_acta = " & num_acta & ", fecha_acta = #" & Format(CDate(fecha_acta), "dd/MM/yyyy") & "#, estado = " & estado & ", codigo_formato = '" & codigo_formato & "', version =  '" & version & "', nombre_acta = '" & nombre_acta & "', Departamento_acta = '" & Departamento_acta & "', tema_acta = '" & tema_acta & "', ubicacion = '" & ubicacion & "',  " & _
                                                 " Fecha_eval = #" & Format(CDate(Fecha_eval), "dd/MM/yyyy") & "#, hora_inicio = #" & Format(CDate(hora_inicio), "HH:mm:ss") & "#, hora_fin = #" & Format(CDate(hora_fin), "HH:mm:ss") & "#, Objetivo = '" & Objetivo & "', Eval_reunion = " & Eval_reunion & ", asistentes = '" & asistentes & "', reviso = '" & reviso & "', Aprobo = '" & aprobo & "', correo_aprobo =  '" & correo_aprobo & "'" & _
                                                 " WHERE     (id_acta = " & id_acta & ")"
                        Me.GuardarBD(strcon)

                    End If

                Case "VoBo"
                    'aqui estado me indica si el acta esta en revision o modificado puedo modificar menos cuando sea oficial
                    strcon = " UPDATE    Acta " & _
                             " SET       estado = 3" & _
                             " WHERE    (id_acta = " & id_acta & ")"
                    Me.GuardarBD(strcon)

                Case "buscar"
                    'aqui el estado me indica con que criterio de busqueda "numero de acta o id acta"
                    strcon = " SELECT     id_acta, num_acta, fecha_acta, estado, codigo_formato, version, nombre_acta, Departamento_acta, tema_acta, ubicacion, Fecha_eval, hora_inicio, " & _
                                                                    " hora_fin, Objetivo, Eval_reunion, asistentes, reviso, Aprobo, correo_aprobo " & _
                                                                    " FROM         Acta "
                    Select Case estado
                        Case 0
                            strcon = strcon & "where(num_acta = " & num_acta & ")"
                        Case 1
                            strcon = strcon & " where id_acta=" & id_acta & ""
                        Case 2
                            strcon = strcon & " where id_acta=" & id_acta & " and estado<>3"
                    End Select

                    conjDatos = Me.Consultar_nbre_tbl(strcon, , "Acta")

                Case "Anular"

                    If estado = 0 Then
                        strcon = " UPDATE    Acta " & _
                                                 " SET      estado = 2 " & _
                                                 " WHERE     (id_acta = " & id_acta & ")"
                        Me.GuardarBD(strcon)
                    End If

                Case "Acta_Compromisos"
                    strcon = "SELECT     Compromisos.Accion, Compromisos.RESPONSABLE, Compromisos.FECHAC, Acta_Compromisos.id_acta " & _
                             "FROM         (Compromisos INNER JOIN " & _
                             "Acta_Compromisos ON Compromisos.NoID = Acta_Compromisos.id_compromisos) " & _
                             "WHERE     (Acta_Compromisos.id_acta = " & id_acta & ")"
                    conjDatos = Me.Consultar_nbre_tbl(strcon, , "Compromiso")

            End Select

        Catch ex As Exception
            Throw New Exception
        End Try


    End Sub

    Public Sub Resultados_Eval(ByVal No_Eval As String)
        Dim strcon As String

        strcon = " SELECT     TipoReunion.Nombre as nombre_acta, Jerarquia.NombreDependencia as Departamento_acta, Resultados_Eval.No_Eval, Resultados_Eval.Id, Resultados_Eval.fecha as Fecha_eval, " & _
                 " Resultados_Eval.porcentaje AS Eval_Reunion, Resultados_Eval.hora_inici_real as hora_inicio, Resultados_Eval.hora_final_real as hora_fin,  " & _
                 " T_REUNIONES.NombreReunion as tema_acta, Resultados_Eval.lugar as ubicacion, t_logins.nombreusuario " & _
                 " FROM         ((((Resultados_Eval INNER JOIN " & _
                 " TipoReunion ON Resultados_Eval.Id = TipoReunion.Id) INNER JOIN " & _
                 " Jerarquia ON TipoReunion.area = Jerarquia.Id) INNER JOIN " & _
                 " T_REUNIONES ON TipoReunion.tip_asoc = T_REUNIONES.Id) INNER JOIN " & _
                 " t_logins ON Resultados_Eval.login = t_logins.Login) " & _
                 " WHERE     (Resultados_Eval.No_Eval = " & No_Eval & ")"
        conjDatos2 = Me.Consultar(strcon)

    End Sub

    Public Sub Acta_Participante(ByVal accion As String)

        Select Case accion
            Case "crear"
                Dim strcon As String
                strcon = " INSERT INTO Acta_Participante " & _
                         " (id_acta, login) " & _
                         " VALUES     (" & id_acta & ", '" & login & "')"

                Me.GuardarBD(strcon)
            Case "actualizar"
                Dim strcon As String
                strcon = " UPDATE    Acta_Participante " & _
                         " SET       id_acta = " & id_acta & ", login = " & login & " " & _
                         " WHERE     (id_acta = " & id_acta & ") AND (login = '" & login & "')"

                Me.GuardarBD(strcon)

            Case "buscar"
                Dim strcon As String
                strcon = "SELECT     t_logins.nombreusuario as Nombre, t_logins.Login " & _
                         "FROM         (Acta_Participante INNER JOIN " & _
                         "t_logins ON Acta_Participante.login = t_logins.Login AND Acta_Participante.login = t_logins.Login)" & _
                         "WHERE     Acta_Participante.id_acta =" & id_acta

                conjDatos = Me.Consultar_nbre_tbl(strcon, , "Participante")

        End Select

    End Sub

    Public Sub Acta_Compromiso(ByVal accion As String)
        Select Case accion
            Case "crear"
                Dim strcon As String
                strcon = "INSERT INTO Acta_Compromisos " & _
                         " (id_acta, id_compromisos) " & _
                         " VALUES     (" & id_acta & ", " & id_compromisos & ")"

                Me.GuardarBD(strcon)
            Case "actualizar"
                Dim strcon As String
                strcon = "UPDATE    Acta_Compromisos " & _
                          " SET              id_acta =  " & id_acta & ", id_compromisos = " & id_compromisos & "" & _
                          " WHERE     (id_acta = " & id_acta & ") AND (id_compromisos = " & id_compromisos & ")"

                Me.GuardarBD(strcon)

            Case "buscar"
                Dim strcon As String
                strcon = "SELECT     Compromisos.Accion as Compromisos, Compromisos.RESPONSABLE as Responsable, format(Compromisos.FECHAC,'dd/MM/yyyy') as Fecha " & _
                         "FROM         (Acta_Compromisos INNER JOIN " & _
                         "Compromisos ON Acta_Compromisos.id_compromisos = Compromisos.NoID) " & _
                         "WHERE     Acta_Compromisos.id_acta = " & id_acta

                conjDatos = Me.Consultar(strcon)



        End Select
    End Sub

    Public Sub Acta_Punto(ByVal accion As String)
        Select Case accion
            Case "crear"
                Dim strcon As String
                strcon = " INSERT INTO Acta_punto " & _
                         " (id_acta, id_puntos_reunion, id_punto) " & _
                         " VALUES     (" & id_acta & "," & id_puntos_reunion & ", " & id_punto & ") "

                Me.GuardarBD(strcon)
                'Case "actualizar"
                '    Dim strcon As String
                '    strcon = "UPDATE    Acta_punto " & _
                '             " SET              id_acta = ?, id_puntos_reunion = ? " & _
                '             " WHERE     (id_acta = " & id_acta & ") AND (id_puntos_reunion = " & id_punto & ")"

                '    Me.GuardarBD(strcon)

            Case "buscar"
                Dim strcon As String
                strcon = " SELECT     id_acta, id_puntos_reunion " & _
                         " FROM         Acta_punto " & _
                         " WHERE  id_acta=" & id_acta & ""

                conjDatos = Me.Consultar(strcon)

        End Select
    End Sub

    Public Sub Desarrollo_acta(ByVal Reunion As String, ByVal fecha As String, ByVal hora As Date, Optional ByVal tipo As Integer = 0, Optional ByVal responsable As String = "")
        Dim strcon As String
        Select Case tipo
            Case 1
                strcon = " SELECT     tbl_gral_puntos.id_puntos_reunion, tbl_gral_puntos.punto, tbl_gral_puntos.responsable,tbl_gral_puntos.id_punto " & _
                         " FROM         (tbl_gral_puntos INNER JOIN " & _
                         " tbl_pntos_rnion ON tbl_gral_puntos.id_punto = tbl_pntos_rnion.Id_puntos) " & _
                         " WHERE     (tbl_pntos_rnion.id_reunion = " & Reunion & ") AND (tbl_pntos_rnion.fecha = #" & Format(CDate(fecha), "dd/MM/yyyy") & "#) AND (tbl_pntos_rnion.hora = #" & Format(CDate(hora), "HH:mm:ss") & "#)"

                conjDatos = Me.Consultar(strcon)
            Case 2
                strcon = " SELECT     tbl_gral_puntos.id_puntos_reunion, tbl_gral_puntos.punto, tbl_gral_puntos.responsable " & _
                                   " FROM         (tbl_gral_puntos INNER JOIN " & _
                                   " tbl_pntos_rnion ON tbl_gral_puntos.id_punto = tbl_pntos_rnion.Id_puntos) " & _
                                   " WHERE     (tbl_pntos_rnion.id_reunion = " & Reunion & ") AND (tbl_pntos_rnion.fecha = #" & Format(CDate(fecha), "dd/MM/yyyy") & "#) AND (tbl_pntos_rnion.hora = #" & Format(CDate(hora), "HH:mm:ss") & "#) and tbl_gral_puntos.responsable='" & responsable & "'"

                conjDatos = Me.Consultar(strcon)


        End Select

    End Sub

    Public Sub Desarrollo()
        Dim strcon = "SELECT     tbl_gral_puntos.punto,tbl_gral_puntos.responsable " & _
                     "FROM         (tbl_gral_puntos INNER JOIN " & _
                     "Acta_punto ON tbl_gral_puntos.id_puntos_reunion = Acta_punto.id_puntos_reunion AND tbl_gral_puntos.id_punto = Acta_punto.id_punto) " & _
                     "WHERE     (Acta_punto.id_acta =" & id_acta & ")"

        conjDatos = Consultar_nbre_tbl(strcon, , "Desarrollo")
    End Sub

    Public Sub BucarEval_reunion(Optional ByVal id_reunion As Integer = 0, Optional ByVal fecha_reunion As String = "", Optional ByVal hora_reunion As String = "", Optional ByVal ind As Integer = 0)
        Dim strcon As String

        Select Case ind
            Case 0
                strcon = " SELECT distinct(ASISTENTESREUNION.No_Eval) " & _
                                             " FROM(ASISTENTESREUNION) " & _
                                             " where ASISTENTESREUNION.idreunion=" & id_reunion & " and ASISTENTESREUNION.fecha=#" & Format(CDate(fecha_reunion), "MM/dd/yyyy") & "# and ASISTENTESREUNION.horaInicio=#" & Format(CDate(hora_reunion), "HH:mm:ss") & "# "
                conjDatos = Me.Consultar(strcon)
            Case 1
                strcon = " SELECT DISTINCT t_logins.nombreusuario as Nombre,ASISTENTESREUNION.login " & _
                                     " FROM t_logins INNER JOIN ASISTENTESREUNION ON t_logins.Login = ASISTENTESREUNION.login " & _
                                     " WHERE ASISTENTESREUNION.No_Eval=" & id_acta & " "
                conjDatos2 = Consultar(strcon)
            Case 2
                strcon = " SELECT DISTINCT t_logins.nombreusuario as Nombre,t_logins.EMail " & _
                         " FROM t_logins INNER JOIN ASISTENTESREUNION ON t_logins.Login = ASISTENTESREUNION.login " & _
                         " where ASISTENTESREUNION.idreunion=" & id_reunion & " and ASISTENTESREUNION.fecha=#" & Format(CDate(fecha_reunion), "MM/dd/yyyy") & "# and ASISTENTESREUNION.horaInicio=#" & Format(CDate(hora_reunion), "HH:mm:ss") & "# "
                conjDatos2 = Consultar(strcon)
        End Select


    End Sub

    Public Sub Return_NoEval(ByVal id_reunion As Integer, ByVal fecha_reunion As String, ByVal hora_reunion As String)
        Dim strcon As String
        strcon = "SELECT No_eval " & _
                 "FROM Resultados_eval " & _
                 "WHERE id=" & id_reunion & "and fecha=#" & Format(CDate(fecha_reunion), "MM/dd/yyyy") & "# and hora_inici_Real=#" & Format(CDate(hora_reunion), "HH:mm:ss") & "# "
        conjDatos = Me.Consultar(strcon)

    End Sub

    Public Sub AsisRnion(ByVal No_Eval As Integer, ByVal Id_reunion As Integer, Optional ByVal fecha As String = "")
        Dim strcon As String
        strcon = " SELECT  ASISTENTESREUNION.idreunion, ASISTENTESREUNION.login, ASISTENTESREUNION.fecha, ASISTENTESREUNION.horaInicio, ASISTENTESREUNION.horaFin, ASISTENTESREUNION.puntual, ASISTENTESREUNION.suplente, ASISTENTESREUNION.excusa, ASISTENTESREUNION.bandera, ASISTENTESREUNION.No_Eval, ASISTENTESREUNION.comentario, TipoReunion.horainicio as estimadainicio, TipoReunion.horafin as estimadafin" & _
                 " FROM ASISTENTESREUNION , TipoReunion  " & _
                 " WHERE No_Eval=" & No_Eval & " and idreunion=" & Id_reunion & " and fecha=#" & Format(CDate(fecha), "MM/dd/yyyy") & "#"
        conjDatos = Me.Consultar(strcon)
    End Sub

    Public Sub AsisRnion2(ByVal No_Eval As Integer, ByVal Id_reunion As Integer, Optional ByVal fecha As String = "")
        Dim strcon As String
        strcon = " SELECT DISTINCT ASISTENTESREUNION.idreunion, ASISTENTESREUNION.fecha, ASISTENTESREUNION.horaInicio, ASISTENTESREUNION.horaFin, TipoReunion.horaInicio AS estimadainicio, TipoReunion.horaFin AS estimadafin" & _
                 " FROM  (ASISTENTESREUNION INNER JOIN   TipoReunion ON ASISTENTESREUNION.idreunion = TipoReunion.Id) " & _
                 " WHERE (ASISTENTESREUNION.No_Eval = " & No_Eval & ") AND (ASISTENTESREUNION.idreunion = " & Id_reunion & ") AND (ASISTENTESREUNION.fecha = #" & Format(CDate(fecha), "MM/dd/yyyy") & "#)"
        conjDatos = Me.Consultar(strcon)
    End Sub


End Class
