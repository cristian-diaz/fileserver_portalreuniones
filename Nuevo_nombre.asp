<%@ Language="VBScript" %>
<!--#include file="incluidos/ConexionBD.inc" -->
<!--#include file="Incluidos/libfunciones1.inc" -->
<!--#include file="Incluidos/libFuncValidacion.inc" -->

<%
 
Dim ADO_Conexion,ADO_RecordSet,Str_SQL,Int_Registros
Set ADO_Conexion = AbrirConexionBD(cadenaConexionDBQ)


Login = Session("Login")

'Variable para Validar el Cambio de Nombre de una Reuni�n
If Request("Cambio") = "No" Then%>

<script language="javascript">

	alert('El Cambio de Nombre Solicitado no se Realiz� porque el Nuevo Nombre est� vacio')

</script>
<%
Else
	If Request("Cambio") = "Denegar" Then %>
<script language="javascript">

	alert('El Cambio de Nombre Solicitado no se Realiz� porque el Nuevo Nombre solicitado Existe dentro de la Base de Datos (Reuniones, Compromisos, Auditorias o Eliminados)')

</script>

<% 	End If
End If%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Language" content="es-co">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>Administrar Reuniones</title>
<link href="Estilos/Interno.css" rel="stylesheet" type="text/css">
<link href="Estilos/GCBstyle.css" rel="stylesheet" type="text/css">
<link href="Estilos/estprincipal.css" rel="stylesheet" type="text/css">

<script src="mootools.v1.11.js" type="text/javascript"></script>
<script src="nogray_date_calendar_vs1_min.js" type="text/javascript"></script>
<script src="nogray_time_picker_min.js" type="text/javascript"></script>



<script language="javascript">
	window.addEvent("domready", function(){
		var today = new Date();
		var festivos = [{date:1,month:0},{date:"3rd Monday",month:0},
					{date:"3rd Monday",month:1},{date:29,month:3},
					{date:4,month:6},{date:"1st Monday",month:8},{date:"2nd Monday",month:9},
					{date:11,month:10},{date:"4th Thursday",month:10},
					{date:25,
					month:11}];
					
		 var tp1 = new TimePicker('time1_picker', 'time1', 'time1_toggler');
	    var tp2 = new TimePicker('time1_picker2', 'time2', 'time1_toggler');
		
		var calender1 = new Calendar("calendar1", "cal1_toggler", {inputField:'date1',		
		                                                           dateFormat:"d/m/Y",
																   datesOff:festivos												   
		                                                           });
																   
	
		var td_events = [];
		var d_16 = (today.getMonth()+1)+'-16-'+today.getFullYear();
		td_events[d_16] = {'click':function(td, date_str){alert(new Date().fromString(date_str));}};
		
		
		var dateon = [];
		var d_14 = (today.getMonth()+1)+'-14-'+today.getFullYear();
		dateon[d_14] = function(td, date_str){
			td.setStyle('background-color', '#aad7ea');
		};
		
		var calender2 = new Calendar("calendar2", "cal2_toggler", {inputField:{date:'date',
																	month:'month',
																	year:'year'},
																	inputType:'select',
																	idPrefix:'cal2',
																	numMonths:4,
																	tdEvents:td_events,
																	dateOnAvailable:dateon
																});		
												
		var calender3 = new Calendar("calendar1", "cal3_toggler", {inputField:'date1',
																	datesOff:us_holidays,
																	numMonths:3,
																	multiSelection:true,
																	maxSelection:5,
																	forceSelections:[
																			{date:'last Saturday'}
																		],
																	dateFormat:'D, d M Y ::  ',
																	idPrefix:'cal3'});
																	
		var calender4 = new Calendar("calendar4", null, {visible:true,
														allowSelection:false,
														datesOff:us_holidays,
														startDate:new Date(today.getFullYear(), 0, 1),
														endDate:new Date(today.getFullYear(), 11, 31),
														idPrefix:'cal4',
														inputType:'none',
														onSelect: function(){
															alert(this.options.selectedDate);
														}});
	
		
	
	
																		
	
	   
	});
</script>


<style type="text/css">
<!--
body {
	background-color: #EDF3E2;
}
.Estilo2 {color: #336600}
.style1 {
	font-size: 7pt;
	font-weight: bold;
}
-->

#calendar1, #calendar2, #calendar3, #calendar4 {border:solid #666666 1px;
		background:#ffffff;
		padding-bottom:5px;
		padding-top:5px;}
		
	#calendar1 {width:183px;}
	#calendar1 .ng-cal-header-table {width:173px;}	
		

    #calendar2 {width:183px;}
	#calendar2 .ng-cal-header-table {width:173px;}
	
	
	.time_picker_div {padding:5px;
		border:solid #999999 1px;
		background:#ffffff;}
		
.Estilo3 {font-weight: bold}
</style>
</head>
<body >


<TABLE WIDTH="691" align="center" BORDER=0 CELLSPACING=0 CELLPADDING=0>
  <tr> 
    <td width="691" colspan="2"  class="EncTabla2"><font size="2" color="#000080"><strong></font><font size="2"><span class="Estilo2">Nuevo Nombre de Reuni&oacute;n</span></font></td>
  </tr>
  <tr>
    <td colspan="2" class="EncTabla2"><span class="Estilo2"><font size="2">Por favor, diligencie los datos correspondientes al nuevo Nombre  de reunion que desea crear,
 tenga en cuenta que es requerido ingresar el nombre de la nueva reuni&oacute;n y el due&ntilde;o asignado.</font ></span></td>
  </tr>
</table>


  <script Language="JavaScript" type="text/javascript">

function FrontPage_Form1_Validator(theForm)
{

  if (theForm.reunion.value == "")
  {
    alert("Por favor ingrese un nombre de la Reunion.");
    theForm.reunion.focus();
    return (false);
  }

  if (theForm.reunion.value.length < 1)
  {
    alert("Por favor ingrese un nombre de la Reunion.");
    theForm.reunion.focus();
    return (false);
  }

  if (theForm.duenio.selectedIndex < 0)
  {
    alert("Por favor ingrese un nombre de la Reunion.");
    theForm.duenio.focus();
    return (false);
  }

  var numSelected = 0;
  var i;
  for (i = 0;  i < theForm.duenio.length;  i++)
  {
    if (theForm.duenio.options[i].selected)
        numSelected++;
  }
  if (numSelected < 1)
  {
    alert("Please select at least 1 of the \"duenio\" options.");
    theForm.duenio.focus();
    return (false);
  }
  return (true);
}
</script>
</p>
<link href="nogray_calendar_vs1.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="Estilos/nogray_calendar_vs1.css">

<form method="POST" action="Guardar_reunion.asp?nom=<%=reunion%>" name="FrontPage_Form1" onSubmit="return FrontPage_Form1_Validator(this)">
<div id="wrap">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="225"><span class="DetTablaNE"><b><b>Nombre Nueva Reuni�n:</b></b></span></td>
      <td width="360"><span class="DetTablaNE"><b><font face="Verdana" size="2">
        <input type="text" name="reunion" size="60" onKeyUp="this.value=this.value.toUpperCase()"/>
      </font></b></span></td>
    </tr>
    <tr>
    <%
		if session("Administrador") <> "NO" then
			Str_SQL1="SELECT * FROM T_REUNIONES WHERE NombreReunion <> '' ORDER BY NombreReunion"

		end if
		Set ADO_Recordset44=AbrirRecordset(ADO_Conexion,Str_SQL1)  
		
		If ADO_Recordset44.EOF Then
			ADO_Recordset44.Close 
			Set ADO_Conexion = Nothing
			Response.redirect("Presentar_Compromisos.asp")
		End If
		
  %>
      <td><span class="DetTablaNE"><b><b>Tipo Nueva Reuni&oacute;n:</b></b></span></td>
      <td><select name="txtTipo" class="Lista" id="txtTipo" style="height: 25; width: 400; font-family: Verdana"  >
        <option value="-1">-- Seleccione un Tipo de Reunion -- </option>
        <%
			 	'Response.Write("<OPTION value="-1">-- Seleccione un Tipo de Reunion -- </OPTION>")
				Do Until ADO_Recordset44.EOF
			    Response.Write("<OPTION Value='" & ADO_Recordset44("NombreReunion"))
					 If Cstr(txtReunion) = Cstr(ADO_Recordset44("Id")) Then
					 	Response.Write(" selected") 
					 end if
		   		Response.Write("'>" & ADO_Recordset44("NombreReunion") & "</OPTION>")
				ADO_Recordset44.MoveNext
				Loop
				ADO_Recordset44.Close 
		   %>
      </select></td>
    </tr>
    <tr>
      <td><span class="DetTablaNE"><strong>Moderador:</strong></span></td>
      <td><select name=duenio class="Lista" style="height: 25; width: 400; font-family: Verdana" >
        <%
				Str_SQL="SELECT Login, Nombreusuario FROM t_logins WHERE Activo = -1 ORDER BY Nombreusuario"
				Set ADO_Recordset = AbrirRecordset(ADO_Conexion, Str_SQL)
				While Not ADO_Recordset.EOF %>
        <option value="<%=ADO_RecordSet("Login")%>"
					<%If ADO_RecordSet("Login") = UCase(Login) Then %>
						selected
					<%End If%>
					><%=ADO_RecordSet("Nombreusuario")%></option>
        <%ADO_Recordset.MoveNext
			    WEnd
				Set ADO_RecordSet = Nothing
       %>
      </select></td>
    </tr>
    <tr>
      <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="DetTablaNE">
          <td><strong>Duracion de La Reunion:</strong></td>
          <td><strong>Hora Inicio</strong></td>
          <td><strong>Hora Fin</strong></td>
          <td colspan="2"><strong>Duracion Tiempo Reunion</strong></td>
        </tr>
        <tr>
          <td><span class="DetTablaNE">Validar Hora</span></td>
          <td><span class="DetTablaNE">
            <input name="time1" type="text" id="time1" onChange="javascript:validarDuracion2(FrontPage_Form1);" size="20"  on/>
          </span></td>
          <td><span class="DetTablaNE">
            <input name="time2" type="text" id="time2" onChange="javascript:validarDuracion2(FrontPage_Form1);" onDblClick="javascript:validarDuracion2(FrontPage_Form1);" size="20" />
          </span></td>
          <td><label>
            <input type="radio" name="radio" id="c" value="c"  onclick="javascript:validarDuracion2(FrontPage_Form1);"/>
          </label></td>
          <td><span class="DetTablaNE">
            <input name="duracion" type="text" id="duracion" size="15" readonly style="text-align:left; font-size:12px; text-align:center; border:none; background:none" />
          </span></td>
        </tr>
        <tr>
          <td><input name="validarAutomatico" type="checkbox" checked="checked" /></td>
          <td><div id="time1_picker" class="time_picker_div"></div></td>
          <td><div id="time1_picker2" class="time_picker_div"></div></td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="DetTablaNE">
          <td><span ><strong>Dias Reunion:</strong></span></td>
          <td><strong>Dia Inicial:</strong></td>
          <td><strong>Seguimiento Reunion:</strong></td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="text" name="date1" id="date1" /></td>
          <td><select name="prg" id="seguimiento"class="Lista" >
            <%
			  Set ADO_Recordset=CreateObject("ADODB.Recordset")
				Str_SQL="SELECT Periodo FROM Programacion "
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion,Str_SQL)
				While Not ADO_RecordSet.EOF %>
            <option value="<%=ADO_RecordSet("Periodo")%>"><%=ADO_RecordSet("Periodo")%></option>
            <%     ADO_RecordSet.MoveNext
				WEnd
				Set ADO_RecordSet = Nothing
		 %>
          </select></td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div id="calendar1"></div></td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td><INPUT name ="Bandera"  id="Bandera" value="<%=Request("Bandera")%>" Type=Hidden></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td class="DetTablaNE"><strong>Area:</strong></td>
          <td colspan="4"  class="DetTablaNE" ><label>
            <span class="Estilo3">
            <% If Request("Bandera")="" or  Request("Bandera")="D" then %>
            <input type="radio" name="radio" id="ger" value="ger"  />            
            <span class="DetTablaNE">Gerencia</span></span>
            <strong>
            <% else %>
            <input type="radio" name="radio" id="vice" value="vice" />
            Departamento
              <% end if %>
            <input type="radio" name="radio" id="cor" value="cor" />
            Coordinacion</strong></label></td>
          </tr>
        <tr >
          <td>&nbsp;</td>
          <td colspan="4">
          <% if Request("Bandera")="Gerencia" then %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="16%"><strong class="DetTablaNE">Gerencia:</strong></td>
                <td width="84%"><select name="gerencia" id="gerencia" class="Lista" style="height: 25; width: 400; font-family: Verdana" >
                   <%
			  Set ADO_Recordset=CreateObject("ADODB.Recordset")
				Str_SQL="SELECT Id,NombreDependencia FROM Jerarquia WHERE Tipo='GERENCIA'"
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion,Str_SQL)
				While Not ADO_RecordSet.EOF %>
            <option value="<%=ADO_RecordSet("Id")%>"><%=ADO_RecordSet("NombreDependencia")%></option>
            <%     ADO_RecordSet.MoveNext
				WEnd
				Set ADO_RecordSet = Nothing
		 %>
                </select></td>
              </tr>
            </table>
            <% end if %>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
              <tr>
                <td width="16%" ><strong class="DetTablaNE">Departamento:</strong></td>
                <td width="84%"><select name="departamento" id="departamento" class="Lista" style="height: 25; width: 400; font-family: Verdana">
                <%
			  Set ADO_Recordset=CreateObject("ADODB.Recordset")
				Str_SQL="SELECT Id,NombreDependencia FROM Jerarquia WHERE Tipo='DEPARTAMENTO'"
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion,Str_SQL)
				While Not ADO_RecordSet.EOF %>
            <option value="<%=ADO_RecordSet("Id")%>"><%=ADO_RecordSet("NombreDependencia")%></option>
            <%     ADO_RecordSet.MoveNext
				WEnd
				Set ADO_RecordSet = Nothing
		 %>
                
                </select></td>
              </tr>
            </table>
            
            
               <table width="100" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><strong class="DetTablaNE">Coordinacion:</strong></td>
                <td><select name="coordinacion" id="coordinacion" class="Lista" style="height: 25; width: 400; font-family: Verdana">
                  <%
			    Set ADO_Recordset=CreateObject("ADODB.Recordset")
				Str_SQL="SELECT Id,NombreDependencia FROM Jerarquia WHERE Tipo='COORDINACION'"
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion,Str_SQL)
				While Not ADO_RecordSet.EOF %>
            <option value="<%=ADO_RecordSet("Id")%>"><%=ADO_RecordSet("NombreDependencia")%></option>
            <%     ADO_RecordSet.MoveNext
				WEnd
				Set ADO_RecordSet = Nothing
		 %>
                </select></td>
              </tr>
            </table>            </td>
        </tr>
      </table></td>
      </tr>
    <tr>
      <td colspan="2"><div align="center">
        <select name=select class="Lista" style="width: 400">
          <%
			  Set ADO_Recordset=CreateObject("ADODB.Recordset")
				Str_SQL="SELECT Id, Nombre FROM TipoReunion WHERE tipo_nm='REUNION' ORDER BY Nombre"
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion,Str_SQL)
				While Not ADO_RecordSet.EOF %>
          <option value="<%=ADO_RecordSet("Id")%>"><%=ADO_RecordSet("Nombre")%></option>
          <%     ADO_RecordSet.MoveNext
				WEnd
				Set ADO_RecordSet = Nothing
		 %>
        </select>
      </div></td>
      </tr>
    <tr>
      <td colspan="2"><div align="center">
        <input name="B1" type="submit" class="Boton" value="     Enviar     " />
        <input name="B2" type="reset" class="Boton" value="Restablecer" />
      </div></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </div>

</form>

<script Language="JavaScript" type="text/javascript">

function FrontPage_Form1_Validator(theForm){

  if (theForm.reunion.value == ""){
    alert("Por favor ingrese un nombre de la Reunion.");
    theForm.reunion.focus();
    return (false);
  }else if (theForm.reunion.value.length < 1){
    alert("Por favor ingrese un nombre valido de la Reunion.");
    theForm.reunion.focus();
    return (false);
  }else if (theForm.txtTipo.value == '-1'){
    alert("Debe seleccionar un Tipo de Reuni�n.");
    theForm.txtTipo.focus();
    return (false); 
  }else if (theForm.duenio.selectedIndex < 0){
    alert("Por favor ingrese el due�o de la reuni�n.");
    theForm.duenio.focus();
    return (false);
  }
  else if ((theForm.validarAutomatico.checked) && (theForm.time1.value == theForm.time2.value)){
    alert("La hora de inicio debe ser diferente a la hora de fin.");
    theForm.time1.focus();
    return (false);
  }
  
  else if ((theForm.validarAutomatico.checked) && (theForm.time2.value < theForm.time1.value)){
    alert("La hora de Final debe ser Mayor a la hora de Inicio.");
    theForm.time2.focus();
    return (false);
  }
  else if ((theForm.validarAutomatico.checked) && ((theForm.duracion.value == "") || (theForm.duracion.value != "" && Number(theForm.duracion.value) == 0))){
    alert("Debe registrar las horas de inicio y fin de la reuni�n correctamente.");
    theForm.time1.focus();
    return (false);
  }else{
	return (true);
  }
}

function validarDuracion(theForm){
  
  var horaInicio = theForm.horaInicio.value; 
  var horaFin = theForm.horaFin.value;
  var minInicio = theForm.minInicio.value;
  var minFin = theForm.minFin.value;
  var ampmI = theForm.ampm1.value;
  var ampmF = theForm.ampm2.value;
  
  var horaUno = horaInicio+":"+minInicio+":00"+ampmI;
  var horaDos = horaFin+":"+minFin+":00"+ampmF;
  
  theForm.duracion.value = "";
  
  //if(ampmI == ampmF){
     
	 //if (Number(horaInicio+minInicio) < Number(horaFin+minFin)){
		dateDiff(horaUno, horaDos, theForm);
	 /*}
	 
  }else{
 
  	if(ampmI == 'AM' && ampmF == 'PM'){
		dateDiff(horaUno, horaDos, theForm);
	}
  }*/ 
}

function validarDuracion2(theForm){
  
  var horaInicio = theForm.time1.value; 
  var horaFin = theForm.time2.value;
 
  
  var horaUno = horaInicio;
  var horaDos = horaFin;
  
  theForm.duracion.value = "";
  dateDiff(horaUno, horaDos, theForm);
	
}



function isValidDate(dateStr) {
// Date validation function courtesty of 
// Sandeep V. Tamhankar (stamhankar@hotmail.com) -->

// Checks for the following valid date formats:
// MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY

var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/; // requires 4 digit year

var matchArray = dateStr.match(datePat); // is the format ok?
if (matchArray == null) {
alert(dateStr + " Date is not in a valid format.")
return false;
}
month = matchArray[1]; // parse date into variables
day = matchArray[3];
year = matchArray[4];
if (month < 1 || month > 12) { // check month range
alert("Month must be between 1 and 12.");
return false;
}
if (day < 1 || day > 31) {
alert("Day must be between 1 and 31.");
return false;
}
if ((month==4 || month==6 || month==9 || month==11) && day==31) {
alert("Month "+month+" doesn't have 31 days!")
return false;
}
if (month == 2) { // check for february 29th
var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
if (day>29 || (day==29 && !isleap)) {
alert("February " + year + " doesn't have " + day + " days!");
return false;
   }
}
return true;
}


function isValidTime(timeStr) {

// Checks if time is in HH:MM:SS AM/PM format.
// The seconds and AM/PM are optional.

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;

var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format.");
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Hour must be between 1 and 12. (or 0 and 23 for military time)");
return false;
}
if (hour <= 12 && ampm == null) {
if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time")) {
alert("You must specify AM or PM.");
return false;
   }
}
if  (hour > 12 && ampm != null) {
alert("You can't specify AM or PM for military time.");
return false;
}
if (minute < 0 || minute > 59) {
alert ("Minute must be between 0 and 59.");
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Second must be between 0 and 59.");
return false;
}
return true;
}

function dateDiff(horaUno, horaDos, theForm) {
//alert(horaUno + " " + horaDos);
date1 = new Date();
date2 = new Date();
diff  = new Date();

var fecha = "01/01/2008";

if (isValidDate(fecha) && isValidTime(horaUno)) { // Validates first date 
date1temp = new Date(fecha + " " + horaUno);
date1.setTime(date1temp.getTime());
}
else return false; // otherwise exits

if (isValidDate(fecha) && isValidTime(horaDos)) { // Validates second date 
date2temp = new Date(fecha + " " + horaDos);
date2.setTime(date2temp.getTime());
}
else return false; // otherwise exits

// sets difference date to difference of first date and second date

diff.setTime(Math.abs(date1.getTime() - date2.getTime()));

timediff = diff.getTime();

weeks = Math.floor(timediff / (1000 * 60 * 60 * 24 * 7));
timediff -= weeks * (1000 * 60 * 60 * 24 * 7);

days = Math.floor(timediff / (1000 * 60 * 60 * 24)); 
timediff -= days * (1000 * 60 * 60 * 24);

hours = Math.floor(timediff / (1000 * 60 * 60)); 
timediff -= hours * (1000 * 60 * 60);

mins = Math.floor(timediff / (1000 * 60)); 
timediff -= mins * (1000 * 60);

secs = Math.floor(timediff / 1000); 
timediff -= secs * 1000;

theForm.duracion.value =  hours + " HORA(S) " + mins + "MIN";

//theForm.dura.value =  hours + " HORA(S) " + mins + "MIN";

//return false; // form should never submit, returns false
}

  

</script>


<script Language="JavaScript" type="text/javascript">
/*
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.reunion.value == "") 
  {
    alert("Por favor ingrese un nombre de la Reunion.");
    theForm.reunion.focus();
    return (false);
  }

  if (theForm.txtTipo.value.length < 1)
  {
    alert("Debe ingresar un tipo de reuni�n");
    theForm.txtTipo.focus();
    return (false);
  }

  if (theForm.duenio.selectedIndex < 0)
  {
    alert("Please select one of the \"duenio\" options.");
    theForm.duenio.focus();
    return (false);
  }

  var numSelected = 0;
  var i;
  for (i = 0;  i < theForm.duenio.length;  i++)
  {
    if (theForm.duenio.options[i].selected)
        numSelected++;
  }
  if (numSelected < 1)
  {
    alert("Please select at least 1 of the \"duenio\" options.");
    theForm.duenio.focus();
    return (false);
  }
  return (true);
}*/
</script><!--webbot BOT="GeneratedScript" endspan -->

	

<p>&nbsp;</p>

</body>
<%
 if request("opc")= 1 then
 	Response.Write("<script language='javascript' type='text/javascript'>  alert('El Nuevo Nombre Existe dentro de la Base de Datos');</script>") 
 end if
%>
</html>
