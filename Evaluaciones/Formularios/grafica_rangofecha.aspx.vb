Imports System.Data
Imports ChartDirector

Partial Class Evaluaciones_Formularios_grafica_rangofecha
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Public cadena As String
    Public labels2 As String
    'Public data() As Double
    'Public labels() As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")
            Me.prcCargarOrigen("valores")
            Me.Button1.ValidationGroup = ""
            graficar_estadi()
            Label6.Text = "Ultimas " & System.Configuration.ConfigurationManager.AppSettings("reunionesamostrar").ToString() & " Evaluaciones Realizadas "
        End If

    End Sub

    Public Sub prcCrearDataset()
        Dim ds As New System.Data.DataSet ' DataSet
        Dim dtt_reunuion As System.Data.DataTable '---0
        Dim dtt_valores As System.Data.DataTable '---1

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunuion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_valores") 'combo de reuniones 
    End Sub
    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "reuniones"
                Me.evalu.prcconsultarreuniones(Me.evalu.FncRol_Usuario(Session("Usuario")))
                Me.ds_evalua().Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "graficar_fechas"
                Me.evalu.prcconsultargrafica(Me.DropDownList1.SelectedValue, Me.TxFecIni.Text, Me.TxFecFin.Text)
                Me.ds_evalua().Tables(1).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "graficar_ultimas"
                Me.evalu.prcconsultarultimas(Me.DropDownList1.SelectedValue)
                Me.ds_evalua().Tables(1).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "graficar_mes"
                ' Me.evalu.prcconsultarmes(Me.DropDownList1.SelectedValue, Me.dbdesde.SelectedValue, Me.dbhasta.SelectedValue)

                Dim fechaini As String
                Dim fechafin As String

                fechaini = Me.dbdesde.SelectedValue
                fechafin = Me.dbhasta.SelectedValue

                Me.evalu.prcconsultarmes(Me.DropDownList1.SelectedValue, fechaini, fechafin, DropDownList2.SelectedValue)
                Me.ds_evalua().Tables(1).Merge(Me.evalu.DatasetFill.Tables(0))
        End Select
    End Sub


    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Me.DropDownList1.DataSource = Me.ds_evalua.Tables(0).CreateDataReader
                Me.DropDownList1.DataTextField = "Nombre"
                Me.DropDownList1.DataValueField = "Id"
                Me.DropDownList1.DataBind()
                'Me.DropDownList1.SelectedValue = "0"
        End Select
    End Sub
    Public Property ds_evalua() As System.Data.DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As System.Data.DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property


    Sub graficar()
        'Me.ds_evalua.Tables(1).Clear()

        Dim mes As String
        Dim dia As String
        Dim PROMEDIO_SUMA As Integer
        Dim PROMEDIO As Double

        Dim data(Me.ds_evalua.Tables(1).Rows.Count - 1) As Double ' = {83, 92, 83, 83, 88, 88, 92, 83, 92}
        Dim labels(Me.ds_evalua.Tables(1).Rows.Count - 1) As String '= {"Ene 04", "Ene 11", "Ene 18", "Ene 25", "Feb 01", "Feb 08", "Feb 16", "Feb 23", "Feb 29"}


        ' The data for the line chart
        For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
            PROMEDIO_SUMA += Me.ds_evalua.Tables(1).Rows(i).Item(1)
            data(i) = Me.ds_evalua.Tables(1).Rows(i).Item(1)
            dia = Mid(Me.ds_evalua.Tables(1).Rows(i).Item(0), 1, 2)
            mes = Mid(Me.ds_evalua.Tables(1).Rows(i).Item(0), 4, 2)

            If mes = "01" Then
                mes = "Ene"
            ElseIf mes = "02" Then
                mes = "Feb"
            ElseIf mes = "03" Then
                mes = "Mar"
            ElseIf mes = "04" Then
                mes = "Abr"
            ElseIf mes = "05" Then
                mes = "May"
            ElseIf mes = "06" Then
                mes = "Jun"
            ElseIf mes = "07" Then
                mes = "Jul"
            ElseIf mes = "08" Then
                mes = "Ago"
            ElseIf mes = "09" Then
                mes = "Sep"
            ElseIf mes = "10" Then
                mes = "Oct"
            ElseIf mes = "11" Then
                mes = "Nov"
            ElseIf mes = "12" Then
                mes = "Dic"
            End If
            labels(i) = mes & "-" & dia
        Next

        PROMEDIO = Math.Round((PROMEDIO_SUMA / CInt(Me.ds_evalua.Tables(1).Rows.Count)), 2)


        ' The labels for the line chart


        ' Create a XYChart object of size 500 x 320 pixels, with a pale purpule

        ' (0xffccff) background, a black border, and 1 pixel 3D border effect.


        '********************* BORDE DE AFUERA '****
        '  Dim c As XYChart = New XYChart(500, 320, &HF0E090, &H0, 1) '  COLOR AL BORDE DE AFUERA

        ' Dim c As XYChart = New XYChart(500, 320, &HFF9966, &H0, 1) '  COLOR AL BORDE DE AFUERA

        '  Dim c As XYChart = New XYChart(500, 320, &H76CC7B, &HFFFFFF, 1) '  COLOR AL BORDE DE AFUERA

        Dim c As XYChart = New XYChart(500, 320, &H99CC99, &HFFFFFF, 1) '  COLOR AL BORDE DE AFUERA



        '*****


        ' Set the plotarea at (55, 45) and of size 420 x 210 pixels, with white

        ' background. Turn on both horizontal and vertical grid lines with light grey

        ' color (0xc0c0c0)




        ' *****COLOR CUADRO DE ADENTRO************* Y LA SLINEAS 
        ' c.setPlotArea(55, 45, 420, 210, &HFFFFFF, -1, -1, &H0, -1)
        c.setPlotArea(55, 50, 420, 205, &HFFF0C0, -1, &HA08040, &HA08040, &HA08040)

        '************




        ' Add a legend box at (55, 25) (top of the chart) with horizontal layout. Use 8

        ' pts Arial font. Set the background and border color to Transparent.
        c.addLegend(55, 22, False, "", 8).setBackground(Chart.Transparent)
        ' Add a title box to the chart using 13 pts Times Bold Italic font. The text is

        ' white (0xffffff) on a purple (0x800080) background, with a 1 pixel 3D border.



        '**** COLOR AL TITULO ******
        '  c.addTitle("METAS POR REUNI�N", "Times New Roman Bold Italic", 13, &HFFFFFF).setBackground(&H336633, -1, 1) ' LETRA Y FONDO
        ' c.addTitle("<*block,valign=absmiddle*><*img=..\Img\star.png*><*img=..\Img\star.png*> METAS POR REUNION <*img=..\Img\star.png*><*img=..\Img\star.png*><*/*>", "arialbi.ttf", 13, &HFFFFFF).setBackground(&H807040, -1, 1)
        c.addTitle("<*block,valign=absmiddle*><*img=..\Img\star.png*><*img=..\Img\star.png*> METAS REUNI�N <*img=..\Img\star.png*><*img=..\Img\star.png*><*/*>", "arialbi.ttf", 13, &HFFFFFF).setBackground(&H336633, -1, 1)
        '***************






        ' Add a title to the y axis
        c.yAxis().setTitle("Porcentaje Obtenido en cada Reuni�n")


        ' Set the labels on the x axis. Rotate the font by 90 degrees.
        c.xAxis().setLabels(labels).setFontAngle(90)



        ' Add a line layer to the chart
        Dim lineLayer As LineLayer = c.addLineLayer()
        ' Add the data to the line layer using light brown color (0xcc9966) with a 7

        ' pixel square symbol



        '** COLOR A LAINEA DE LA GRAFICA D ELA LINEA Y EL GRAFICO
        ' lineLayer.addDataSet(data, &H6600FF, "Prom. Evaluac.:" & "XXX").setDataSymbol(Chart.SquareSymbol, 7)
        ' lineLayer.addDataSet(data, &H6600FF, "Prom. Evaluac.:" & "XXX").setDataSymbol(Server.MapPath("..\Img\star.png"))
        lineLayer.addDataSet(data, &HC0, "Prom. Evaluac.:" & PROMEDIO & "%").setDataSymbol(Chart.CircleSymbol, 9, &HFFFF00)
        '*****



        ' Set the line width to 2 pixels
        lineLayer.setLineWidth(2)
        ' tool tip for the line layer
        lineLayer.setHTMLImageMap("", "", "title='{xLabel}: {value} %'")
        ' Add a trend line layer using the same data with a dark green (0x008000) color.
        ' Set the line width to 2 pixels
        'Dim dataMeta() As Double = {85, 85, 85, 85, 85, 85, 85, 85}
        Dim dataMeta(Me.ds_evalua.Tables(1).Rows.Count - 1) As Double ' = {85, 85, 85, 85, 85, 85, 85}
        For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
            dataMeta(i) = CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString)
        Next

        '*********** COLOR AL PROMEDIO DE LA META
        Dim trendLayer As TrendLayer = c.addTrendLayer(dataMeta, &HCC0000, "Prom. META =" & " 85%")
        '*******************

        trendLayer.setLineWidth(2)

        ' tool tip for the trend layer
        '        trendLayer.setHTMLImageMap("", "", "title='Change rate: {slope|2} Porcentaje Vrs Rango de Fechas'")
        trendLayer.setHTMLImageMap("", "", "title='Promedio Minimo de la meta a Superar'")

        ' output the chart
        CDGrafFechas.Image = c.makeWebImage(Chart.PNG)

        ' include tool tip for the chart
        CDGrafFechas.ImageMap = c.getHTMLImageMap("")
    End Sub

    Sub graficar_estadi()

        Try
            If Me.rdfecha.Checked = True Then
                prcCargarOrigen("graficar_fechas")
                Me.Label8.Text = "METAS REUNI�N : " & Me.DropDownList1.SelectedItem.Text & " ( Rango de Fechas )"
                graficar()
            ElseIf Me.rdmes.Checked = True Then
                prcCargarOrigen("graficar_mes")
                Me.Label8.Text = "METAS REUNI�N : " & Me.DropDownList1.SelectedItem.Text & " ( Consolidado por Mes )"
                graficar_Mes()
            ElseIf Me.rdultim.Checked = True Then
                prcCargarOrigen("graficar_ultimas")
                Me.Label8.Text = "METAS REUNI�N : " & Me.DropDownList1.SelectedItem.Text & " ( " & "Ultimas " & System.Configuration.ConfigurationManager.AppSettings("reunionesamostrar").ToString() & " Evaluaciones Realizadas " & ")"
                graficar()
            End If

            If Me.ds_evalua.Tables(1).Rows.Count > 0 Then
                Me.Panel1.Visible = True
                Me.Panel2.Visible = False
            Else
                Me.Panel1.Visible = False
                Me.Panel2.Visible = True
            End If
        Catch ex As Exception
            Me.Panel1.Visible = False
            Me.Panel2.Visible = True
        End Try
       
    End Sub


    Protected Sub rdultim_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdultim.CheckedChanged
        Me.Button1.ValidationGroup = ""
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
    End Sub

    Protected Sub rdfecha_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdfecha.CheckedChanged
        Button1.ValidationGroup = "VGFecha"
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
    End Sub
    Public Property str_titulo() As String
        Get
            Return ViewState("titulo_esta")
        End Get
        Set(ByVal value As String)
            ViewState("titulo_esta") = value
        End Set
    End Property


    ' GRAFICA EL ACUMULADO DEL LA METAS POR LOS MESES SELECIONADOS 

    Sub graficar_Mes()
        'Me.ds_evalua.Tables(1).Clear()

        Dim mes As String
        Dim dia As String
        Dim PROMEDIO_SUMA As Integer
        Dim PROMEDIO As Double

        Dim data(Me.ds_evalua.Tables(1).Rows.Count - 1) As Double ' = {83, 92, 83, 83, 88, 88, 92, 83, 92}
        Dim labels(Me.ds_evalua.Tables(1).Rows.Count - 1) As String '= {"Ene 04", "Ene 11", "Ene 18", "Ene 25", "Feb 01", "Feb 08", "Feb 16", "Feb 23", "Feb 29"}


        '        Select  sum(porcentaje)
        'FROM Resultados_Eval
        'where id = 144 and fecha>=#2008/07/01# and fecha<=#2008/09/30# 
        'group by  Month(fecha)


        ' The data for the line chart
        For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
            PROMEDIO_SUMA += Me.ds_evalua.Tables(1).Rows(i).Item(1) 'Item(4)
            data(i) = Math.Round(Me.ds_evalua.Tables(1).Rows(i).Item(1), 2) 'Item(4)

            mes = Mid(Me.ds_evalua.Tables(1).Rows(i).Item(0), 4, 2)

            If mes = "1" Then
                mes = "Ene"
            ElseIf mes = "2" Then
                mes = "Feb"
            ElseIf mes = "3" Then
                mes = "Mar"
            ElseIf mes = "4" Then
                mes = "Abr"
            ElseIf mes = "5" Then
                mes = "May"
            ElseIf mes = "6" Then
                mes = "Jun"
            ElseIf mes = "7" Then
                mes = "Jul"
            ElseIf mes = "8" Then
                mes = "Ago"
            ElseIf mes = "9" Then
                mes = "Sep"
            ElseIf mes = "10" Then
                mes = "Oct"
            ElseIf mes = "11" Then
                mes = "Nov"
            ElseIf mes = "12" Then
                mes = "Dic"
            End If
            labels(i) = mes
        Next

        PROMEDIO = Math.Round((PROMEDIO_SUMA / CInt(Me.ds_evalua.Tables(1).Rows.Count)), 2)

        ' The labels for the line chart

        ' Create a XYChart object of size 500 x 320 pixels, with a pale purpule
        ' (0xffccff) background, a black border, and 1 pixel 3D border effect.

        '********************* BORDE DE AFUERA '****
        '  Dim c As XYChart = New XYChart(500, 320, &HF0E090, &H0, 1) '  COLOR AL BORDE DE AFUERA

        ' Dim c As XYChart = New XYChart(500, 320, &HFF9966, &H0, 1) '  COLOR AL BORDE DE AFUERA

        '  Dim c As XYChart = New XYChart(500, 320, &H76CC7B, &HFFFFFF, 1) '  COLOR AL BORDE DE AFUERA

        Dim c As XYChart = New XYChart(500, 320, &H99CC99, &HFFFFFF, 1) '  COLOR AL BORDE DE AFUERA

        '*****

        ' Set the plotarea at (55, 45) and of size 420 x 210 pixels, with white

        ' background. Turn on both horizontal and vertical grid lines with light grey

        ' color (0xc0c0c0)

        ' *****COLOR CUADRO DE ADENTRO************* Y LA SLINEAS 
        ' c.setPlotArea(55, 45, 420, 210, &HFFFFFF, -1, -1, &H0, -1)
        c.setPlotArea(55, 50, 420, 205, &HFFF0C0, -1, &HA08040, &HA08040, &HA08040)

        '************

        ' Add a legend box at (55, 25) (top of the chart) with horizontal layout. Use 8

        ' pts Arial font. Set the background and border color to Transparent.
        c.addLegend(55, 22, False, "", 8).setBackground(Chart.Transparent)
        ' Add a title box to the chart using 13 pts Times Bold Italic font. The text is

        ' white (0xffffff) on a purple (0x800080) background, with a 1 pixel 3D border.

        '**** COLOR AL TITULO ******
        '  c.addTitle("METAS POR REUNI�N", "Times New Roman Bold Italic", 13, &HFFFFFF).setBackground(&H336633, -1, 1) ' LETRA Y FONDO
        ' c.addTitle("<*block,valign=absmiddle*><*img=..\Img\star.png*><*img=..\Img\star.png*> METAS POR REUNION <*img=..\Img\star.png*><*img=..\Img\star.png*><*/*>", "arialbi.ttf", 13, &HFFFFFF).setBackground(&H807040, -1, 1)
        c.addTitle("<*block,valign=absmiddle*><*img=..\Img\star.png*><*img=..\Img\star.png*> METAS REUNI�N <*img=..\Img\star.png*><*img=..\Img\star.png*><*/*>", "arialbi.ttf", 13, &HFFFFFF).setBackground(&H336633, -1, 1)
        '***************

        ' Add a title to the y axis
        c.yAxis().setTitle("Porcentaje Obtenido en cada Reuni�n")

        ' Set the labels on the x axis. Rotate the font by 90 degrees.
        c.xAxis().setLabels(labels).setFontAngle(90)

        ' Add a line layer to the chart
        Dim lineLayer As LineLayer = c.addLineLayer()
        ' Add the data to the line layer using light brown color (0xcc9966) with a 7

        ' pixel square symbol

        '** COLOR A LAINEA DE LA GRAFICA D ELA LINEA Y EL GRAFICO
        ' lineLayer.addDataSet(data, &H6600FF, "Prom. Evaluac.:" & "XXX").setDataSymbol(Chart.SquareSymbol, 7)
        ' lineLayer.addDataSet(data, &H6600FF, "Prom. Evaluac.:" & "XXX").setDataSymbol(Server.MapPath("..\Img\star.png"))
        lineLayer.addDataSet(data, &HC0, "Prom. Evaluac.:" & PROMEDIO & "%").setDataSymbol(Chart.CircleSymbol, 9, &HFFFF00)
        '*****

        ' Set the line width to 2 pixels
        lineLayer.setLineWidth(2)
        ' tool tip for the line layer
        lineLayer.setHTMLImageMap("", "", "title='{xLabel}: {value} %'")
        ' Add a trend line layer using the same data with a dark green (0x008000) color.
        ' Set the line width to 2 pixels
        'Dim dataMeta() As Double = {85, 85, 85, 85, 85, 85, 85, 85}
        Dim dataMeta(Me.ds_evalua.Tables(1).Rows.Count - 1) As Double ' = {85, 85, 85, 85, 85, 85, 85}
        For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
            dataMeta(i) = CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString)
        Next

        '*********** COLOR AL PROMEDIO DE LA META
        Dim trendLayer As TrendLayer = c.addTrendLayer(dataMeta, &HCC0000, "Prom. META =" & " 85%")
        '*******************

        trendLayer.setLineWidth(2)

        ' tool tip for the trend layer
        '        trendLayer.setHTMLImageMap("", "", "title='Change rate: {slope|2} Porcentaje Vrs Rango de Fechas'")
        trendLayer.setHTMLImageMap("", "", "title='Promedio Minimo de la meta a Superar'")

        ' output the chart
        CDGrafFechas.Image = c.makeWebImage(Chart.PNG)

        ' include tool tip for the chart
        CDGrafFechas.ImageMap = c.getHTMLImageMap("")
    End Sub

    Protected Sub rdmes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdmes.CheckedChanged
        Me.Button1.ValidationGroup = ""
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.ds_evalua.Tables(1).Clear()
        graficar_estadi()
    End Sub
 
   
End Class
