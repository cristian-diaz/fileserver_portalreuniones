<%@ Control Language="VB" AutoEventWireup="True" CodeFile="MsgBoxAjax.ascx.vb" Inherits="Controles_MsgBoxAjax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Panel style="DISPLAY: none" ID="PnlMsgBox" runat="server" CssClass="modalPopup2" DefaultButton="BtnCerrarMsgBox" Width="350px">
    <table width="350">
        <tr>
            <td align="center" class="Heather">
                <b>
                    <asp:Label ID="LblTituloMsgBox" runat="server"></asp:Label></b></td>
        </tr>
        <tr>
            <td align="center">
                <hr />
                <asp:Label ID="LblMensajeMsgBox" runat="server" SkinID="SubTitulo_Azul"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                <hr />
                <asp:Button ID="BtnCerrarMsgBox" runat="server" Font-Bold="True" Font-Size="X-Small" Text="Cerrar" /></td>
        </tr>
    </table>
</asp:Panel>
<cc1:ModalPopupExtender ID="MpeMsgBox" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="BtnCerrarMsgBox" DropShadow="True" Enabled="True" PopupControlID="PnlMsgBox"
    TargetControlID="LblShowMsgBox">
</cc1:ModalPopupExtender>
<asp:Label ID="LblShowMsgBox" runat="server"></asp:Label>
