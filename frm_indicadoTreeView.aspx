﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_indicadoTreeView.aspx.vb" Inherits="frm_indicadoTreeView" Theme="SkinCompromisos"  StylesheetTheme="SkinCompromisos" %>

<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PORTAL DE COMPROMISOS : INDICADOR</title>

</head>
<body>
    <form id="form1" runat="server">
  
        <div style="width: 100%; position: static; height: 100px; text-align: center;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="im1"
            TargetControlID="date1">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="im2"
            TargetControlID="date2">
        </cc1:CalendarExtender>
        

        <table border="0" cellpadding="0" cellspacing="0" style="text-align: center">
            <tr>
                <td colspan="5" style="font-weight: bold; font-size: 16px;color: green; font-family: Arial; text-align: center;">
                    Indicador de Control de Gestion</td>
            </tr>
            <tr>  
                <td colspan="5" rowspan="3">
                    </td>
            </tr>
            <tr>
            </tr>
            <tr>
            </tr>
            <tr>
                <td style="height: 43px" colspan="5">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 100px">
                                            <asp:Label ID="Label3" runat="server" Text="Fecha Inicio:"></asp:Label></td>
                            <td style="width: 100px">
                                            <asp:TextBox ID="date1" runat="server" Width="100px"></asp:TextBox></td>
                            <td style="width: 35px; text-align: left">
                                <asp:Image ID="im1" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                            <td style="width: 100px">
                                            <asp:Label ID="Label4" runat="server" Text="Fecha Final:"></asp:Label></td>
                            <td style="width: 100px">
                                            <asp:TextBox ID="date2" runat="server" Width="100px"></asp:TextBox></td>
                            <td style="width: 48px; text-align: left">
                                <asp:Image ID="im2" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                        </tr>
                    </table>
                    <asp:RequiredFieldValidator ID="requireddate1" runat="server" ControlToValidate="date1"
                        Display="None" ErrorMessage="Fecha Inicio No puede estar vacia"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="Requiredate2" runat="server" ControlToValidate="date2"
                        Display="None" ErrorMessage="Fecha Final No puede estar vacia"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="valfec" runat="server" ControlToCompare="date1" ControlToValidate="date2"
                        Display="None" ErrorMessage="La fecha final debe ser superior a la inicial" Operator="GreaterThan"
                        Type="Date" ValidationGroup="val"></asp:CompareValidator>
                </td>
            </tr>
        </table>
            <asp:ImageButton ID="btn_ayuda" runat="server" ImageUrl="~/img/system_help.png" />&nbsp;
            <div style="z-index: 101; left: 469px; width: 100px; position: absolute; top: 179px;
                height: 100px">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <table style="width: 250px; height: 52px; background-color: #ffffff; text-align: center" bordercolor="#000000" border="1" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="texto" style="width: 250px; height: 52px; background-color: #ffffff; text-align: center">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/img/ajax-loader.gif" /><br />
                                        Procesando Informe espere unos segundos......</td>
                                </tr>
                            </table>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
            </div>
        </div>
       
        <div style="width: 100%; height: 100%;background-color:White;">
                  
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%" >
                        <tr>
                            <td style="width: 616px; text-align: left; height: 19px;">
                
                    <asp:Label ID="Label1" runat="server" Text="Seleccionar Area:"></asp:Label></td>
                            <td style="height: 19px; width: 648px;"></td>
                        </tr>
                        <tr>
                            <td style="width: 616px; height: 473px">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                <asp:Panel ID="Panel1" runat="server" Height="390px" ScrollBars="Vertical" Width="500px">
        <asp:TreeView ID="tree_jerarquia" runat="server" Height="282px" ImageSet="Arrows" ShowLines="True" Width="475px">
            <ParentNodeStyle Font-Bold="False" />
            <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
            <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                VerticalPadding="0px" />
            <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                NodeSpacing="0px" VerticalPadding="0px" />
        </asp:TreeView>
                                
                                   </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="text-align: center" >
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; text-align: center;" id="TABLE1">
                                    <tr>
                                        <td style="width: 100px; text-align: center; height: 126px;">                          
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>                                           
                                            <asp:RequiredFieldValidator ID="req_dependencia" runat="server" ControlToValidate="txt_titulo"
                                                Display="None" ErrorMessage="Se Requiere Escoger un Nombre de Dependencia"></asp:RequiredFieldValidator>
                                  
                                            <asp:TextBox ID="txt_titulo" runat="server" BorderColor="Transparent" SkinID="txt_INC"></asp:TextBox>
                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="req_dependencia">
                                            </cc1:ValidatorCalloutExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                    
                                    <table class="tbCss2" style="width: 162px">
                                        <tr class="tbHeader2">
                                           <th colspan="3"> Implementacion ECG</th> 
                                      
                                        </tr>
                                        <tr class="tbSubHeader2">
                                            <th colspan="3"> 
                                           
                                               <asp:TextBox ID="txt_TTAL" runat="server" SkinID="txt_Indicador"></asp:TextBox>
                                               </th>
                                        </tr>
                                        <tr>
                                          <td class="tdCss2" colspan="3"> Meta 85 %</td> 
                                        </tr>
                                    </table>
                                <br />
                           <br />
<table class="tbCss2"> 

<tr class="tbHeader2"> 
<th colspan="3" style="height: 36px"> Evaluacion Reuniones Estrategicas</th> 
<th colspan="3" style="height: 36px"> Cumplimiento de Agendas</th> 
<th colspan="3" style="height: 36px"> Cumplimiento de Compromisos</th> 
</tr> 
<tr class="tbSubHeader2"> 
<th colspan="3"> 
                                <asp:TextBox ID="txt_indi1" runat="server" SkinID="txt_Indicador" > </asp:TextBox></th> 
<th colspan="3"> 
                                                  <asp:TextBox ID="txt_cag" runat="server" SkinID="txt_Indicador"></asp:TextBox></th> 
<th colspan="3"> 
                                                <asp:TextBox ID="txt_comp" runat="server" SkinID="txt_Indicador"></asp:TextBox></th> 
</tr> 
<tr> 
<td class="tdCss2" colspan="3"> Meta 80 %</td> 
<td class="tdCss2" colspan="3"> Meta 85 %</td> 
<td class="tdCss2" colspan="3"> Meta 85 %</td> 
</tr> 

</table> 
                                <br />
                           
                                <asp:Button ID="Button1" runat="server" BackColor="#E0E0E0" Font-Bold="False" Font-Italic="False"
                        Text="Ver Reporte" Width="146px" /><br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
        </div>
        <div style="width: 100%; position: static; height: 100px;text-align: center;">
           <uc1:logo ID="Logo1" runat="server" />
        </div>

    
                    <cc1:ValidatorCalloutExtender ID="vallidafec" runat="server" TargetControlID="valfec">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="validadat1" runat="server" TargetControlID="requireddate1">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="validadat2" runat="server" TargetControlID="Requiredate2">
                    </cc1:ValidatorCalloutExtender>
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                    <asp:GridView ID="grid1" runat="server" AutoGenerateColumns="False" SkinID="Gvw_Indicador" >
                        <Columns>
                            <asp:BoundField DataField="NumeroR" HeaderText="Numero Reunion" />
                            <asp:BoundField DataField="NEval" HeaderText="Numero de Evaluacion" />
                            <asp:BoundField DataField="nombre" HeaderText="Nombre Reunion" />
                            <asp:BoundField DataField="porcentaje" HeaderText="Porcentaje %" />
                            <asp:BoundField DataField="fecha" HeaderText="Fecha Inicial" DataFormatString="{0:dd/MM/yyyy}" />
                        </Columns>
                    </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
        &nbsp;<br />
                   <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btn_cancel" DropShadow="true" PopupControlID="Panel2" TargetControlID="btn_ayuda">
                    </cc1:ModalPopupExtender>
                    
                    
                    
                    <asp:Panel ID="Panel2" runat="server" CssClass="Popups_clases" Style="display: none" Width="400px" Height="200px" >
                        <asp:Panel ID="Panel4" runat="server" CssClass="EncTabla2" Height="25px" Width="100%">
                                    <asp:Label ID="Label10" runat="server" Text="INSTRUCCIONES" Width="101px"></asp:Label></asp:Panel>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 100%; height: 192px; font-weight: bold; font-family: Arial; text-align: center;">
                                    <table style="text-align: left">
                                        <tr>
                                            <td style="width: 386px">
                                            1. Seleccione la Fecha Inicio y Final 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 386px">
                                            2. Seleccione el Area a la Cual revisara el Indicador
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 386px">
                                            3. Oprima el Boton ver Reporte
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                        
                                    <asp:Button ID="btn_cancel" runat="server" CssClass="Botones" Text="Cerrar" /></td>
                            </tr>
                        </table>
                        <br />
                        &nbsp;</asp:Panel>
      
        
    </form>
</body>
</html>
