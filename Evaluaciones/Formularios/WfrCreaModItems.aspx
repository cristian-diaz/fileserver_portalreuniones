<%@ Page Language="VB" MasterPageFile="~/Evaluaciones/Plantilla/P_Plantilla_Aux.master" AutoEventWireup="false" CodeFile="WfrCreaModItems.aspx.vb" Inherits="Evaluaciones_Formularios_WfrCreaModItems" title="Untitled Page" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CntCuerpo" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td style="font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG);
                color: white; font-family: Verdana,Arial,Helvetica,sans-serif; height: 18px">
                Creación y Modificación de Items a Evaluar</td>
        </tr>
    </table>
    <div style="text-align: center">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 10%; height: 25px">
                </td>
                <td align="left" style="width: 20%; height: 25px">
                </td>
                <td align="left" style="width: 20%; height: 25px">
                </td>
                <td align="left" style="width: 20%; height: 25px">
                </td>
                <td align="left" style="width: 20%; height: 25px">
                </td>
                <td style="width: 10%; height: 25px">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td align="left" style="width: 20%; background-color: #d5ddbf">
                <asp:Label ID="Label1" runat="server" Text="Item a Evaluar " Font-Bold="True" SkinID="TitTabla"></asp:Label></td>
                <td align="left" style="width: 20%; background-color: #d5ddbf">
                </td>
                <td align="left" style="width: 20%; background-color: #d5ddbf">
                <asp:Label ID="Label3" runat="server" Text="Comentario" Font-Bold="True" SkinID="TitTabla"></asp:Label></td>
                <td align="left" style="width: 20%; background-color: #d5ddbf">
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td align="left" colspan="2" rowspan="4">
                <asp:TextBox ID="TxtItemEval" runat="server" Width="343px" Height="41px" ValidationGroup="GOne" MaxLength="255" TextMode="MultiLine"></asp:TextBox></td>
                <td align="left" colspan="2" rowspan="4">
                <asp:TextBox ID="TxtComentario" runat="server" Height="41px" ValidationGroup="GOne" Width="343px" MaxLength="255" TextMode="MultiLine"></asp:TextBox></td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%; height: 20px">
                </td>
                <td align="left" style="width: 20%; height: 20px">
                </td>
                <td align="left" style="width: 20%; height: 20px">
                </td>
                <td align="left" style="width: 20%; height: 20px">
                </td>
                <td align="left" style="width: 20%; height: 20px">
                </td>
                <td style="width: 10%; height: 20px">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td align="left" style="width: 20%; background-color: #d5ddbf">
                <asp:Label ID="Label2" runat="server" Text="Peso" Font-Bold="True" SkinID="TitTabla"></asp:Label></td>
                <td align="left" colspan="2" style="width: 20%; background-color: #d5ddbf">
                <asp:Label ID="Label4" runat="server" Text="Calificación" Font-Bold="True" SkinID="TitTabla"></asp:Label></td>
                <td align="left" style="width: 20%; background-color: #d5ddbf">
                <asp:Label ID="Label5" runat="server" Text="Activo" Font-Bold="True" SkinID="TitTabla"></asp:Label></td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td align="left" style="width: 20%">
                <asp:TextBox ID="TxtPeso" runat="server" Width="33px" ValidationGroup="GOne" MaxLength="2"></asp:TextBox></td>
                <td align="left" colspan="2">
                            <asp:CheckBoxList ID="CblCalificaion" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">0</asp:ListItem>
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                            </asp:CheckBoxList></td>
                <td align="left" style="width: 20%">
                            <asp:CheckBox ID="CkbActivo" runat="server" Checked="True" /></td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%; height: 20px">
                <cc2:msgbox id="MsgBox1" runat="server"></cc2:msgbox>
                </td>
                <td align="left" style="width: 20%; height: 20px">
                <asp:HiddenField ID="HfSinGuardar" runat="server" Value="False" />
                </td>
                <td align="left" colspan="2" style="height: 20px">
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                    TargetControlID="TxtPeso">
                </cc1:FilteredTextBoxExtender>
                <cc1:ConfirmButtonExtender ID="CbeGuardar" runat="server" ConfirmText="Guardar Cambios?"
                    Enabled="True" TargetControlID="BTNGUARDAR">
                </cc1:ConfirmButtonExtender>
                </td>
                <td align="left" style="width: 20%; height: 20px">
                </td>
                <td style="width: 10%; height: 20px">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td align="left" style="width: 20%">
                </td>
                <td align="center" style="width: 20%">
                <asp:Button ID="BTNGUARDAR" runat="server" Text="Enviar" ValidationGroup="GOne"
                    Width="105px" /></td>
                <td align="center" style="width: 20%">
                <asp:Button ID="Button2" runat="server" Text="Regresar"
                    Width="105px" CausesValidation="False" /></td>
                <td align="left" style="width: 20%">
                </td>
                <td style="width: 10%">
                </td>
            </tr>
        </table>
    </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtItemEval"
                    Display="None" ErrorMessage="Item a Evaluar es requerido" ValidationGroup="GOne"></asp:RequiredFieldValidator>
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                </cc1:ValidatorCalloutExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtComentario"
                    Display="None" ErrorMessage="Comentario es requerido" ValidationGroup="GOne"></asp:RequiredFieldValidator>
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                </cc1:ValidatorCalloutExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtPeso"
                    Display="None" ErrorMessage="Peso es requerido" ValidationGroup="GOne"></asp:RequiredFieldValidator>
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                </cc1:ValidatorCalloutExtender>
</asp:Content>

