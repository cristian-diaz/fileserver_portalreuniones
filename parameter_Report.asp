<%@ Language="VBScript" %>
<!--#include file="incluidos/ConexionBD.inc" -->
<!--#include file="Incluidos/libfunciones1.inc" -->
<!--#include file="Incluidos/libFuncValidacion.inc" -->

<%
Set ADO_Conexion=AbrirConexionBD(cadenaConexionDBQ)
Dim login, administrador, ObjRecord2, Str_SQL2
administrador = "NO"
login = mid(Session("NombreUsuario"),1,9)

Str_SQL2="SELECT * FROM t_logins WHERE login = '"&login&"' and administrador = -1"
set ObjRecord2 = AbrirRecordset(ADO_Conexion,Str_SQL2)

if not ObjRecord2.EOF then
	administrador = "SI"
end if       

if administrador = "NO" then
	response.Redirect("Guardar_denegado.asp")
end if  
%>
<html>
<head>
<title>Consultar Moderadores de Reuniones</title>
<link href="incluidos/estprincipal.css" rel="stylesheet" type="text/css">

<link href="Estilos/GCBstyle.css" rel="stylesheet" type="text/css">
<link href="Estilos/Interno.css" rel="stylesheet" type="text/css">
<link href="Estilos/estprincipal.css" rel="stylesheet" type="text/css">

     <link rel="stylesheet" type="text/css" href="codebase/dhtmlxtabbar.css">
     <script  src="codebase/dhtmlxcommon.js"></script>
     <script  src="codebase/dhtmlxtabbar.js"></script>
	 <script  src="codebase/dhtmlxtabbar_start.js"></script>
     
<script src="mootools.v1.11.js" type="text/javascript"></script>
<script src="nogray_date_calendar_vs1_min.js" type="text/javascript"></script>
     
<script language="javascript">
	window.addEvent("domready", function(){
		var today = new Date();
		var festivos = [{date:1,month:0},{date:"3rd Monday",month:0},
					{date:"3rd Monday",month:1},{date:29,month:3},
					{date:4,month:6},{date:"1st Monday",month:8},{date:"2nd Monday",month:9},
					{date:11,month:10},{date:"4th Thursday",month:10},
					{date:25,
					month:11}];
					
		
			
		var calender1 = new Calendar("calendar1", "cal1_toggler", {inputField:'date1',		
		                                                           dateFormat:"d/m/Y",
																   datesOff:festivos												   
		                                                           });
		
		
</script>		


     <style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
     </style>

<script Language="JavaScript">

var valueOpc = '';

function chequearOpcion(){
	var nombre_elemento = ''; 
	
	for (var i=0;i < document.forms[0].elements.length;i++){
		var elemento = document.forms[0].elements[i];
		if (elemento.type == "radio"){
			if (elemento.checked){
				return (true);
			}
		}		
	}
	return (false);
}


function ValidarDatos(f){
	
  if(!chequearOpcion()){
    alert('Debe seleccionar la consulta que desea realizar: Reuniones, Evaluaciones o Compromisos');
  }else if(f.valueOpc.value == 'R' && !(f.pr1.checked || f.pr2.checked || f.pr5.checked || f.pr6.checked)){
   	alert('Debe seleccionar por lo menos un par�metro de b�squeda de las Reuniones.');
  }else if(f.valueOpc.value == 'E' && !(f.pr7.checked || f.pr3.checked || f.pr4.checked || f.pr13.checked || f.pr14.checked || f.pr8.checked)){
   	alert('Debe seleccionar por lo menos un par�metro de b�squeda de las Evaluaciones.');
  }else if(f.valueOpc.value == 'C' && !(f.pr9.checked || f.pr10.checked || f.pr11.checked || f.pr12.checked)){
   	alert('Debe seleccionar por lo menos un par�metro de b�squeda de los Compromisos.');
  }else if(f.valueOpc.value == 'R' && f.pr2.checked && f.nombreReunion.value == ""){			
  	alert('Debe escribir un nombre de reuni�n.');
	f.nombreReunion.focus();
  }else if(f.valueOpc.value == 'E' && f.pr3.checked && (f.diaI.value == "" || f.mesI.value == "" || f.anioI.value == "")){
  	alert('La fecha no es v�lida.');
	f.diaI.focus();
  }else if(f.valueOpc.value == 'E' && f.pr4.checked && (f.diaF.value == "" || f.mesF.value == "" || f.anioF.value == "")){
  	alert('La fecha no es v�lida.');
	f.diaF.focus();
  }else if(f.valueOpc.value == 'E' && f.pr8.checked && (f.puntaje.value == "" || Number(f.puntaje.value) < 0 || Number(f.puntaje.value) > 360)){			
  	alert('Debe escribir un puntaje entre 0 y 360.');
	f.puntaje.focus();	
  }else if(f.valueOpc.value == 'C' && f.pr9.checked && f.tema.value == ""){			
  	alert('Debe escribir el tema o una palabra de b�squeda.');
	f.tema.focus();	 
  }else if(f.valueOpc.value == 'C' && f.pr10.checked && f.actividad.value == ""){			
  	alert('Debe escribir la actividad o una palabra de b�squeda.');
	f.actividad.focus();	
  }else if(f.valueOpc.value == 'C' && f.pr11.checked && f.nomReunion.value == ""){			
  	alert('Debe escribir el nombre de la reuni�n o una palabra de b�squeda.');
	f.nomReunion.focus();	 	
  }else if(f.valueOpc.value == 'C' && f.pr12.checked && f.responsable.value == ""){			
  	alert('Debe escribir el registro o el nombre del responsable del compromiso.');
	f.responsable.focus();	
  }else{
  	f.submit();
  }

}

function asignarValorRadio(frm, valor){
	frm.valueOpc.value = valor;
}

</script>

</head>



<body>

			<!--include file="Incluidos/menux.asp" -->
	<TABLE WIDTH="100%" height="30" BORDER=0 align="center" CELLPADDING=0 CELLSPACING=0>
		<tr>
    <td background="img/Barra2.JPG"><div align="center" ><strong><font color="#FFFFFF" face="Verdana">Par�metros del reporte</font> </strong></div></td>
  </tr>
  </table>   
<%	
	Dim Str_SQL, ADO_Recordset, ADO_RecordsetItem, SQL_Reporte
	Str_SQL = "SELECT * FROM TipoReunion where tipo_nm='TIPO' And Activa = -1 ORDER BY Nombre"
	Set ADO_Recordset = AbrirRecordset(ADO_Conexion,Str_SQL)
	Str_SQL = "SELECT * FROM Item_Evaluacion where Activo = -1 ORDER BY Id_item"
	Set ADO_RecordsetItem = AbrirRecordset(ADO_Conexion,Str_SQL)
	
	'SQL_Reporte
	
%>
<table width="54%" border="0" align="center" cellpadding="0" cellspacing="0" class="Cuerpo" >
  <!--DWLayoutTable-->
  <tr>
    <td height="100%" colspan="3"><form method="POST" name="FrontPage_Form1" action="rep_excel.asp" target="_blank">
        <!--form method="post" name="form1"-->
		<table width="100%" border="0" cellpadding="1">
          <tr class="EncTabla2">
            <td colspan="3"><div align="left">Reuniones</div></td>
          </tr>
          <tr class="EncTabla2">
            <td width="4%"><input name="opc" type="radio" onClick="javascript:asignarValorRadio(document.forms['FrontPage_Form1'], 'R');"></td>
            <td width="33%"><div align="center">Par&aacute;metro</div></td>
            <td width="63%"><div align="center">Valor</div></td>
          </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top">
		      <div align="center">
		        <input name="pr1" type="checkbox" id="pr1" value="checkbox">
	            </div></td><td valign="top">Tipo de Reuni&oacute;n </td>
		    <td valign="top">
			<select name="tipoReunion" class="Lista" id="tipoReunion" style="font-family: Verdana">
				
            <%  Do Until ADO_Recordset.EOF %>
			    <OPTION Value='<%=ADO_Recordset("Nombre")%>' ><%=ADO_Recordset("Nombre")%></OPTION>
			<%	ADO_Recordset.MoveNext
				Loop
				ADO_Recordset.Close 
			%>
                                    </select></td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top"><div align="center">
		      <input name="pr2" type="checkbox" id="pr2" value="checkbox">
		      </div></td>
		    <td valign="top">Nombre de la Reuni�n </td>
		    <td>
		      <select name="nombreReunion" id="nombreReunion"class="Lista" style="width: 400">
            <%
				Response.Write("<OPTION>(No Filtrar)</OPTION>")
				if session("Administrador") <> "SI" then
					Str_SQL="SELECT Nombre FROM TipoReunion where id in (select id_reunion from permisos where login ='"&Login&"') and tipo_nm = 'REUNION' And Activa = -1 ORDER BY Nombre"
				else
					Str_SQL="SELECT * FROM TipoReunion WHERE Activa = -1 and tipo_nm='REUNION'ORDER BY Nombre"
				end if
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion, Str_SQL)
				Do Until ADO_RecordSet.EOF
				 Response.Write("<OPTION Value='" & ADO_RecordSet("Nombre") & "'")
				 	If txtReunion=ADO_RecordSet("Nombre") Then	Response.Write(" selected")
				 Response.Write(">" & ADO_RecordSet("Nombre") & "</OPTION>")
				 ADO_RecordSet.MoveNext
				Loop
				ADO_RecordSet.Close 
				Set ADO_RecordSet = Nothing
			%>
		      </select>		    </td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top">
		      <div align="center">
		        <input name="pr5" type="checkbox" id="pr5" value="checkbox">
	            </div></td><td valign="top">Hora Inicio Planeada </td>
		    <td><select name="horaInicio" class="Lista_p" id="horaInicio" style="height: 25; width: 40; font-family: Verdana">
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06" selected>06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select>
		      :
		      <select name="minInicio" class="Lista_p" id="minInicio"  style="width: 40; font-family: Verdana">
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
              </select>
		      <select name="ampm1" class="Lista_p" id="ampm1"  style="width: 45; font-family: Verdana">
                <option value="AM">AM</option>
                <option value="PM">PM</option>
              </select> 
	        hh:mm am/pm </td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top">
		      <div align="center">
		        <input name="pr6" type="checkbox" id="pr6" value="checkbox">
	            </div></td><td valign="top">Hora Fin Planeada </td>
		    <td><select name="horaFin" class="Lista_p" id="select2" style="height: 25; width: 40; font-family: Verdana">
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06" selected>06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select>
		      :
		      <select name="minFin" class="Lista_p" id="select3" style="width: 40; font-family: Verdana">
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
              </select>
		      <select name="ampm2" class="Lista_p" id="ampm2"  style="width: 45; font-family: Verdana">
                <option value="AM">AM</option>
                <option value="PM">PM</option>
            </select>	          hh:mm am/pm  </td>
	      </tr>
		  <tr class="EncTabla2">
		    <td height="18" colspan="3" valign="top">Evaluaciones </td>
	      </tr>
		  <tr class="EncTabla2">
            <td><input name="opc" type="radio" onClick="javascript:asignarValorRadio(document.forms['FrontPage_Form1'], 'E');"></td>
		    <td><div align="center">Par&aacute;metro</div></td>
		    <td><div align="center">Valor</div></td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top"><input name="pr15" type="checkbox" id="pr15" value="checkbox"></td>
		    <td valign="top">Reuni&oacute;n</td>
		    <td>
		      
		       
		        <select name="nomReu" id="nomReu" class="Lista" style="width: 400" >
            <%
				Response.Write("<OPTION>(No Filtrar)</OPTION>")
				if session("Administrador") <> "SI" then
					Str_SQL="SELECT Nombre FROM TipoReunion where id in (select id_reunion from permisos where login ='"&Login&"') and tipo_nm = 'REUNION' And Activa = -1 ORDER BY Nombre"
				else
					Str_SQL="SELECT * FROM TipoReunion WHERE Activa = -1 and tipo_nm='REUNION'ORDER BY Nombre"
				end if
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion, Str_SQL)
				Do Until ADO_RecordSet.EOF
				 Response.Write("<OPTION Value='" & ADO_RecordSet("Nombre") & "'")
				 	If txtReunion=ADO_RecordSet("Nombre") Then	Response.Write(" selected")
				 Response.Write(">" & ADO_RecordSet("Nombre") & "</OPTION>")
				 ADO_RecordSet.MoveNext
				Loop
				ADO_RecordSet.Close 
				Set ADO_RecordSet = Nothing
			%>
                </select>
		      </td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top">
		      <div align="center">
		        <input name="pr7" type="checkbox" id="pr7" value="checkbox">
	            </div></td><td valign="top">Item Evaluado </td>
		    <td><select name="itemEvaluacion" class="Lista" id="itemEvaluacion" style="width: 400; font-family: Verdana">
              <%  Do Until ADO_RecordsetItem.EOF %>
              <OPTION Value='<%=ADO_RecordsetItem("Id_item")%>'><%=ADO_RecordsetItem("Item")%></OPTION>
              <%	ADO_RecordsetItem.MoveNext
				Loop
				ADO_RecordsetItem.Close 
			%>
            </select></td>
	      </tr>
		  <tr class="DetTablaNE">
            <td height="18" valign="top"><div align="center">
                <input name="pr3" type="checkbox" id="pr3" value="checkbox">
            </div></td>
		    <td valign="top">Fecha Inicial </td>
		    <td><label>
              <input name="diaI" type="text" class="CajaTexto" id="diaI" size="2">
		      /
		      <select name="mesI" class="Lista_p" id="mesI" style="font-family: Verdana">
		        <option value="01" selected>Ene</option>
		        <option value="02">Feb</option>
		        <option value="03">Mar</option>
		        <option value="04">Abr</option>
		        <option value="05">May</option>
		        <option value="06">Jun</option>
		        <option value="07">Jul</option>
		        <option value="08">Ago</option>
		        <option value="09">Sep</option>
		        <option value="10">Oct</option>
		        <option value="11">Nov</option>
		        <option value="12">Dic</option>
	          </select>
		      /
		      <input name="anioI" type="text" id="anioI" class="CajaTexto" size="4">
	        dd/mm/aaaa<img src="img/view_calendar_timeline.png" width="16" height="16"></label></td>
	      </tr>
		  <tr class="DetTablaNE">
            <td height="18" valign="top"><div align="center">
                <input name="pr4" type="checkbox" id="pr4" value="checkbox">
            </div></td>
		    <td valign="top">Fecha Final </td>
		    <td><input name="diaF" type="text" id="diaF" class="CajaTexto" size="2">
		      /
		      <select name="mesF" class="Lista_p" id="mesF" style="font-family: Verdana">
        <option value="01" selected>Ene</option>
        <option value="02">Feb</option>
        <option value="03">Mar</option>
        <option value="04">Abr</option>
        <option value="05">May</option>
        <option value="06">Jun</option>
        <option value="07">Jul</option>
        <option value="08">Ago</option>
        <option value="09">Sep</option>
        <option value="10">Oct</option>
        <option value="11">Nov</option>
        <option value="12">Dic</option>
      </select>
		      /
		      <input name="anioF" type="text" id="anioF" class="CajaTexto" size="4">
		      dd/mm/aaaa</td>
	      </tr>
		  <tr class="DetTablaNE">
            <td height="18" valign="top"><div align="center">
                <input name="pr13" type="checkbox" id="pr13" value="checkbox">
            </div></td>
		    <td valign="top">Hora Inicio Planeada </td>
		    <td><select name="horaInicioe" class="Lista_p" id="horaInicioe" style="height: 25; width: 40; font-family: Verdana">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06" selected>06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
		      :
		      <select name="minInicioe" class="Lista_p" id="minInicioe"  style="width: 40; font-family: Verdana">
		        <option value="00">00</option>
		        <option value="15">15</option>
		        <option value="30">30</option>
		        <option value="45">45</option>
	          </select>
		      <select name="ampm1e" class="Lista_p" id="ampm1e"  style="width: 45; font-family: Verdana">
		        <option value="AM">AM</option>
		        <option value="PM">PM</option>
	          </select>
		      hh:mm am/pm </td>
	      </tr>
		  <tr class="DetTablaNE">
            <td height="18" valign="top"><div align="center">
                <input name="pr14" type="checkbox" id="pr14" value="checkbox">
            </div></td>
		    <td valign="top">Hora Fin Planeada </td>
		    <td><select name="horaFine" class="Lista_p" id="select2" style="height: 25; width: 40; font-family: Verdana">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06" selected>06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
		      :
		      <select name="minFine" class="Lista_p" id="select3" style="width: 40; font-family: Verdana">
		        <option value="00">00</option>
		        <option value="15">15</option>
		        <option value="30">30</option>
		        <option value="45">45</option>
	          </select>
		      <select name="ampm2e" class="Lista_p" id="ampm2e"  style="width: 45; font-family: Verdana">
		        <option value="AM">AM</option>
		        <option value="PM">PM</option>
	          </select>
		      hh:mm am/pm </td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top"><div align="center">
		      <input name="pr8" type="checkbox" id="pr8" value="checkbox">
		      </div></td>
		    <td valign="top">Puntaje de la Evaluaci&oacute;n</td>
		    <td><input name="puntaje" type="text" id="puntaje" class="CajaTexto" size="10"></td>
	      </tr>
		  <tr class="EncTabla2">
            <td height="18" colspan="3" valign="top">Compromisos </td>
	      </tr>
		  <tr class="EncTabla2">
            <td><input name="opc" type="radio"  onClick="javascript:asignarValorRadio(document.forms['FrontPage_Form1'], 'C');"></td>
		    <td><div align="center">Par&aacute;metro</div></td>
		    <td><div align="center">Valor</div></td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top">
		      <div align="center">
		        <input name="pr9" type="checkbox" id="pr9" value="checkbox">
	          </div></td>
		    <td valign="top">Tema</td>
		    <td>
            
       <select name="tema" class="Lista_p" id="tema" style="height: 25; width: 460; font-family: Verdana" >              
 	<%
	Str_SQL1="SELECT * FROM TipoReunion where tipo_nm='TEMA' And Activa = -1 ORDER BY Nombre"

		'end if
		Set ADO_Recordset45=AbrirRecordset(ADO_Conexion,Str_SQL1)  
	
 		Response.Write("<OPTION>-- Seleccione un Tema para el Compromiso -- </OPTION>")
		Do Until ADO_Recordset45.EOF
		 %>
		 <OPTION Value="<%=ADO_Recordset45("Nombre") %>" 
		 <%
		 If Cstr(txtTema) = Cstr(ADO_Recordset45("id")) Then
			Response.Write(" selected") 
		 end if 
		 %>
		 > <% =ADO_Recordset45("Nombre") %></OPTION> 
		 <%
		 ADO_Recordset45.MoveNext
		Loop
		ADO_Recordset45.Close 
	%>
                  </select>	
            
            </td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="18" valign="top"><div align="center">
		      <input name="pr10" type="checkbox" id="pr10" value="checkbox">
	        </div></td>
		    <td valign="top">Acci&oacute;n</td>
		    <td><input name="actividad" type="text" class="CajaTexto" id="actividad" size="90"></td>
	      </tr>
		  <tr class="DetTablaNE">
		    <td height="28" valign="top"><div align="center">
		      <input name="pr11" type="checkbox" id="pr11" value="checkbox">
	        </div></td>
		    <td valign="top">Reuni&oacute;n</td>
		    <td>
            <select name="nomReunion" id="nomReunion"class="Lista" style="width: 400">
            <%
				Response.Write("<OPTION>(No Filtrar)</OPTION>")
				if session("Administrador") <> "SI" then
					Str_SQL="SELECT Nombre FROM TipoReunion where id in (select id_reunion from permisos where login ='"&Login&"') and tipo_nm = 'REUNION' And Activa = -1 ORDER BY Nombre"
				else
					Str_SQL="SELECT * FROM TipoReunion WHERE Activa = -1 and tipo_nm='REUNION'ORDER BY Nombre"
				end if
				Set ADO_RecordSet = AbrirRecordSet(ADO_Conexion, Str_SQL)
				Do Until ADO_RecordSet.EOF
				 Response.Write("<OPTION Value='" & ADO_RecordSet("Nombre") & "'")
				 	If txtReunion=ADO_RecordSet("Nombre") Then	Response.Write(" selected")
				 Response.Write(">" & ADO_RecordSet("Nombre") & "</OPTION>")
				 ADO_RecordSet.MoveNext
				Loop
				ADO_RecordSet.Close 
				Set ADO_RecordSet = Nothing
			%>
		      </select>
            
            </td>
	      </tr>
		  <tr class="DetTablaNE">
            <td height="28" valign="top">
              
              <div align="center">
                <input name="pr12" type="checkbox" id="pr12" value="checkbox">
                </div></td>
            <td valign="top">Responsable</td>
            <td><input name="responsable" type="text" id="responsable" class="CajaTexto" size="60"></td>
	 </tr>
        </table>
		<!--/form>
		<form method="post" name="form2"-->
		
	    <table width="100%" border="0" cellpadding="1">
     <!--
	  
  <tr class="EncTabla2">
    <td height="16" colspan="4" valign="top"><div align="left">Marque las variables que desea ver en el reporte </div></td>
  </tr>
  	
  <tr class="DetTabla">
    <td height="18" valign="top" class="DetTablaNE"><input name="vr1" type="checkbox" id="vr1" value="checkbox"></td>
    <td width="277" valign="top" class="DetTablaNE">Nombre de la Reuni&oacute;n</td>
    <td width="20" height="18" valign="top" class="DetTablaNE"><input name="vr7" type="checkbox" id="vr7" value="checkbox"></td>
    <td width="312" valign="top" class="DetTablaNE">Lugar de la Reuni&oacute;n </td>
  </tr>
  <tr class="DetTabla">
    <td height="18" valign="top" class="DetTablaNE"><input name="vr2" type="checkbox" id="vr2" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Tipo de Reuni&oacute;n </td>
    <td height="18" valign="top" class="DetTablaNE"><input name="vr8" type="checkbox" id="vr8" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Comentarios</td>
  </tr>
  <tr class="DetTabla">
    <td height="18" valign="top" class="DetTablaNE"><input name="vr3" type="checkbox" id="vr3" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Item Evaluado </td>
    <td height="18" valign="top" class="DetTablaNE"><input name="vr9" type="checkbox" id="vr9" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Hora Inicio Planeada </td>
  </tr>
  <tr class="DetTabla">
    <td width="21" height="18" valign="top" class="DetTablaNE"><input name="vr4" type="checkbox" id="vr4" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Puntaje de la Evaluaci&oacute;n </td>
    <td height="18" valign="top" class="DetTablaNE"><input name="vr10" type="checkbox" id="vr10" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Hora Fin Planeada </td>
  </tr>
  <tr class="DetTabla">
    <td height="18" valign="top" class="DetTablaNE"><input name="vr5" type="checkbox" id="vr5" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Usuario</td>
    <td height="18" valign="top" class="DetTablaNE"><input name="vr11" type="checkbox" id="vr11" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Hora Inicio Real </td>
  </tr>
  <tr class="DetTabla">
    <td height="18" valign="top" class="DetTablaNE"><input name="vr6" type="checkbox" id="vr6" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Fecha Evaluaci&oacute;n </td>
    <td height="18" valign="top" class="DetTablaNE"><input name="vr12" type="checkbox" id="vr12" value="checkbox"></td>
    <td valign="top" class="DetTablaNE">Hora Fin Real </td>
  </tr>
  -->
<tr>
 <td  align="center" colspan="4"><p>
   <input name="Submit" type="button" class="Boton" value="Exportar a Excel" onClick="javascript:ValidarDatos(document.forms['FrontPage_Form1']);"></td>
 </tr>
</table>
<!--/form-->
<input type="hidden" name="valueOpc" id="valueOpc">
 </form>
</td>
<td height="2">
  </tr>
</table>



<!-- #include file="incluidos/CopyRight.asp" -->
</body>
</html>
