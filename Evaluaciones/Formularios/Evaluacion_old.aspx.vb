Imports System.Data
Imports System.Data.OleDb

Partial Class Evaluaciones_Formularios_Evaluacion
    Inherits System.Web.UI.Page

    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            If Me.evalu.FncIngreso_Usuario(Session("Usuario")) = False Then
                Response.Redirect("../../Guardar_denegado.asp")
            End If
            Me.prcCargarValidacion()
            Me.prcCrearDataset()
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")
            Me.prcCargarOrigen("Evaluacion")
            Me.PrcAcutualizar("grilla_llenar")
            Me.prcCargarOrigen("guardar")
            If Not IsNothing(Request.QueryString("Accion")) Then
                'Llamado a Cargue de datos de la Evaluaci�n
                Me.prcCargarEvaluacion()
            Else
                Me.txtfecha.Text = Now.ToShortDateString
            End If
            Me.PrcCargarDatos("Evaluacion")
            Me.PrcCargarDatos("grilla_llenar")
        End If
        Me.Button1.Style.Add("cursor", "hand")
    End Sub
    Public Sub prcCargarValidacion()
        Me.rvRangoFec.MinimumValue = CDate(DateAdd(DateInterval.Day, -4, CDate(Now.ToShortDateString)))
        Me.rvRangoFec.MaximumValue = Now.ToShortDateString
        Me.rvRangoFec.ErrorMessage = "La Fecha debe estar entre el " & Me.rvRangoFec.MinimumValue & " y el " & Me.rvRangoFec.MaximumValue
    End Sub
    Public Sub prcCrearDataset()
        Dim ds As New DataSet
        Dim dtt_reunuion As New DataTable '---0
        Dim dtt_evaluacion As New DataTable '---1
        Dim dtt_reultado As New DataTable '---2
        Dim dtt_datos As New DataTable '---3
        Dim dtt_calificacion As New DataTable '---4


        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunuion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_evaluacion") ' items de la grilla a evaluar 
        Me.ds_evalua.Tables.Add("dtt_reultado") ' grilla de los resultados obteneidos %
        Me.ds_evalua.Tables.Add("dtt_datos")
        Me.ds_evalua.Tables.Add("dtt_calificacion")


        ' dataset de resultados
        Me.ds_evalua.Tables(2).Columns.Add("itemr", Type.GetType("System.String"))
        Me.ds_evalua.Tables(2).Columns.Add("valorr", Type.GetType("System.String"))

    End Sub
    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "guardar"
                Me.evalu.prcconsultarestructura()
                Me.ds_evalua.Tables(3).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "reuniones"
                Me.evalu.prcconsultarreuniones(Me.evalu.FncRol_Usuario(Session("Usuario")))
                Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "Evaluacion"
                Me.evalu.prcconsultaritemreunion()
                Me.ds_evalua().Tables(1).Merge(Me.evalu.DatasetFill.Tables(0))
                Try
                    Me.ds_evalua.Tables(1).Columns.Add("index", Type.GetType("System.Int32"))
                    Me.ds_evalua.Tables(1).Columns.Add("calif", Type.GetType("System.String"))
                    Me.ds_evalua.Tables(1).Columns.Add("valor", Type.GetType("System.Int32"))
                    Me.ds_evalua.Tables(1).Columns.Add("orden", Type.GetType("System.Int32"))
                    Me.ds_evalua.Tables(1).PrimaryKey = New DataColumn() {Me.ds_evalua.Tables(1).Columns("Id_Item")}
                Catch ex As Exception

                End Try
                For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
                    Me.ds_evalua.Tables(1).Rows(i)("index") = i
                    Me.ds_evalua.Tables(1).Rows(i)("calif") = 0
                    Me.ds_evalua.Tables(1).Rows(i)("valor") = 0
                    Me.ds_evalua.Tables(1).Rows(i)("orden") = i + 1
                Next
                Me.ds_evalua.Tables(1).AcceptChanges()
        End Select
    End Sub


    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Me.DropDownList1.DataSource = Me.ds_evalua.Tables(0).CreateDataReader
                Me.DropDownList1.DataTextField = "Nombre"
                Me.DropDownList1.DataValueField = "Id"
                Me.DropDownList1.DataBind()
            Case "Evaluacion"
                Me.GridView1.DataSource = Me.ds_evalua.Tables(1)
                Me.GridView1.DataBind()
            Case "Resultados"
                
            Case "grilla_llenar"
                Me.GridView2.DataSource = Me.ds_evalua.Tables(2)
                Me.GridView2.DataBind()
        End Select
    End Sub


    Public Sub PrcAcutualizar(ByVal tipo As String)
        Select Case tipo
            Case "grilla_llenar"
                Dim filas As DataRow
                Dim sumapeso As Integer

                For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
                    sumapeso += Me.ds_evalua.Tables(1).Rows(i).Item("Peso")
                Next


                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Puntaje Obtenido"
                filas("valorr") = 0
                Me.ds_evalua.Tables(2).Rows.Add(filas)

                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Efectividad de Esta Reuni�n"
                filas("valorr") = 0
                Me.ds_evalua.Tables(2).Rows.Add(filas)

                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Total Peso"
                filas("valorr") = sumapeso
                Me.ds_evalua.Tables(2).Rows.Add(filas)


                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Maximo Puntaje Posible"
                filas("valorr") = Me.FncMaxPuntaje
                Me.ds_evalua.Tables(2).Rows.Add(filas)

            Case "grilla"
                Dim row As GridViewRow
                Dim total As Integer = 0
                Dim porcentajeo As Integer
                Dim efeti As Integer
                Dim filaa As DataRow
                Dim filab As DataRow

                filaa = Me.ds_evalua.Tables(2).Rows(0)
                filab = Me.ds_evalua.Tables(2).Rows(1)

                For Each row In Me.GridView1.Rows
                    total += CInt(CType(row.FindControl("lbltotal"), Label).Text)
                Next

                efeti = Me.ds_evalua.Tables(2).Rows(3).Item(1)

                porcentajeo = (total / efeti) * 100

                filaa("valorr") = total 'puntos
                filab("valorr") = porcentajeo & "%"

                Me.TextBox3.Text = porcentajeo & "%"

                If porcentajeo > CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString) Then
                    Me.TextBox3.BackColor = Drawing.Color.Green
                    Me.TextBox3.ForeColor = Drawing.Color.White
                Else
                    Me.TextBox3.BackColor = Drawing.Color.Red
                    Me.TextBox3.ForeColor = Drawing.Color.Black
                End If

                Me.GridView2.Rows(1).Cells(1).BackColor = System.Drawing.Color.Red
                Me.GridView2.Rows(3).Cells(1).BackColor = System.Drawing.Color.Blue

            Case "guardar"
                Dim filaa As DataRow
                filaa = Me.ds_evalua.Tables(3).NewRow
                filaa("Id") = Me.DropDownList1.SelectedValue
                filaa("login") = Session("Usuario")
                filaa("lugar") = Me.txtlugar.Text
                filaa("fecha") = Me.txtfecha.Text
                filaa("totalpuntos") = Me.ds_evalua.Tables(2).Rows(0).Item(1)
                filaa("porcentaje") = Replace(Me.ds_evalua.Tables(2).Rows(1).Item(1).ToString, "%", "")
                filaa("comentario") = Me.TextBox1.Text
                Me.ds_evalua.Tables(3).Rows.Add(filaa)

                If IsNothing(Me.Request.QueryString("Accion")) Then
                    'Esta Insertando
                    Me.evalu.prcguardar_encabezado(Me.ds_evalua)
                Else
                    'Est� actualizando
                    Me.evalu.prcupdate_encabezado(Me.ds_evalua, Request.QueryString("Eval"))
                End If


                If Me.evalu.FlagError = False Then
                    limpiar()
                    Me.MsgBox1.ShowMessage("Los Datos Han Sido Guardados")
                Else
                    Me.MsgBox1.ShowMessage("Ha ocurrido un error y los datos no fueron guardados.. Por favor comuniquese con el adminsitrador")
                End If
        End Select
    End Sub
    Public Function FncMaxPuntaje() As Integer

        Dim Total As Integer = 0

        For Each row As DataRow In Me.ds_evalua.Tables(1).Rows
            Total += row("MaxPuntos")
        Next

        Return Total

    End Function
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Cargar �tems de los combos
            Dim com As DropDownList
            Dim id As String = CType(e.Row.FindControl("Lbl_ID"), Label).Text 'Llave del registro de �tems
            Dim item As DataRow = ds_evalua.Tables(1).Rows.Find(id)
            Dim v() As String = Split(item("calificacion").ToString, ",")
            com = CType(e.Row.FindControl("dbcalifica"), DropDownList)
            For s As Integer = 0 To v.Length - 1
                Dim NItem As New ListItem
                NItem.Text = v(s)
                com.Items.Add(NItem)
                NItem = Nothing
            Next
            If Not IsNothing(Request.QueryString("Accion")) Then
                com.SelectedValue = ds_evalua.Tables(1).Rows.Find(id)("calif")
                Dim lbl As Label = CType(e.Row.FindControl("lbltotal"), Label)
                If CInt(lbl.Text) > 0 Then
                    lbl.ForeColor = Drawing.Color.White
                    lbl.BackColor = Drawing.Color.Green
                Else
                    lbl.ForeColor = Drawing.Color.Black
                    lbl.BackColor = Drawing.Color.Crimson
                End If
            End If
            If ds_evalua.Tables(1).Rows.Find(id)("calc") = True Then
                'S� la calificaci�n del �tem es calculada
                If evalu.FncCumplimiento_Compromisos(Me.DropDownList1.SelectedItem.Text) = True Then
                    com.SelectedValue = "3"
                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "3"
                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                    If CType(e.Row.FindControl("lbltotal"), Label).Text > 0 Then
                        'row.Cells(4).BackColor = System.Drawing.Color.Green
                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
                    Else
                        'row.Cells(4).BackColor = System.Drawing.Color.Red
                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
                    End If
                Else
                    com.SelectedValue = "0"
                End If
                com.Enabled = False
            Else
                com.Enabled = True
            End If
        End If
    End Sub
    Public Property str_NmLista() As String
        Get
            Return ViewState("ListaNm")
        End Get
        Set(ByVal value As String)
            ViewState("ListaNm") = value
        End Set
    End Property


    Protected Sub dbcalifica_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cmb As DropDownList = sender
        Dim valor As String = cmb.ToolTip ' la fila en la que estoy 

        Dim row As GridViewRow = GridView1.Rows(valor)
        CType(row.FindControl("lbltotal"), Label).Text = CType(row.FindControl("lblpeso"), Label).Text * CType(row.FindControl("dbcalifica"), DropDownList).Text
        If CType(row.FindControl("lbltotal"), Label).Text > 0 Then
            'row.Cells(4).BackColor = System.Drawing.Color.Green
            CType(row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
            CType(row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
        Else
            'row.Cells(4).BackColor = System.Drawing.Color.Red
            CType(row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
            CType(row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
        End If
        PrcAcutualizar("grilla")
        Me.PrcCargarDatos("grilla_llenar")


        Dim key As String = CType(row.FindControl("Lbl_ID"), Label).Text
        Dim anyrow As DataRow = Me.ds_evalua.Tables(1).Rows.Find(key) ' ver sie sta en lo qeu seleccione
        If Not IsNothing(anyrow) Then
            anyrow("calif") = CType(row.FindControl("dbcalifica"), DropDownList).Text
        End If

    End Sub
    Sub limpiar()
        Me.ds_evalua.Tables(1).Clear()
        Me.ds_evalua.Tables(2).Clear()
        Me.ds_evalua.Tables(3).Clear()
        Me.ds_evalua.Tables(4).Clear()

        ViewState("cont") = 0
        Me.prcCargarOrigen("Evaluacion")
        Me.PrcCargarDatos("Evaluacion")
        Me.PrcAcutualizar("grilla_llenar")
        Me.PrcCargarDatos("grilla_llenar")
        Me.txtfecha.Text = ""
        Me.TextBox3.Text = ""
        Me.txtlugar.Text = ""
        Me.TextBox1.Text = ""
        Me.txtfecha.Text = Now.ToShortDateString
        Me.TextBox3.Text = "0%"
        Me.TextBox3.BackColor = Drawing.Color.Crimson
        Me.TextBox3.ForeColor = Drawing.Color.Black
    End Sub
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        limpiar()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTNGUARDAR.Click
        If Not Me.txtfecha.Text = "" Then
            If IsNothing(Request.QueryString("Accion")) Then
                If evalu.validar_fechas(Me.txtfecha.Text, Me.DropDownList1.SelectedValue) = 0 Then
                    PrcAcutualizar("guardar")
                Else
                    Me.MsgBox1.ShowMessage("Ya se encuentra registrado una evaluaci�n para la reuni�n: " & Chr(10) & Me.DropDownList1.SelectedItem.Text & Chr(10) & " ( No evaluaci�n : " & Me.evalu.str_validar_fecha & ")")
                End If
            Else
                PrcAcutualizar("guardar")
            End If
        End If
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        If evalu.validar_fechas(Me.txtfecha.Text, Me.DropDownList1.SelectedValue) = 0 Then
            Me.MsgBox1.ShowMessage("No se encontraron Registros.. Puede continuar con la Evaluaci�n")
        Else
            Me.MsgBox1.ShowMessage("Ya se encuentra registrado una evaluaci�n para la reuni�n: " & Chr(10) & Me.DropDownList1.SelectedItem.Text & Chr(10) & " ( No evaluaci�n : " & Me.evalu.str_validar_fecha & ")")
        End If
    End Sub
    Public Sub prcCargarEvaluacion()
        '1. Cargar Valores de la Reuni�n
        Dim dtt As DataSet = evalu.prcConsultar_Evaluacion(, , , Request.QueryString("Eval"))
        Dim reunion As DataRow = dtt.Tables(0).Rows(0)
        Me.DropDownList1.SelectedValue = reunion("id")
        Me.txtlugar.Text = reunion("lugar")
        Me.txtfecha.Text = CDate(reunion("fecha")).ToShortDateString
        Me.TextBox3.Text = reunion("porcentaje") & "%"
        If CDbl(reunion("porcentaje")) > CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString) Then
            Me.TextBox3.ForeColor = Drawing.Color.White
            Me.TextBox3.BackColor = Drawing.Color.Green
        Else
            Me.TextBox3.ForeColor = Drawing.Color.Black
            Me.TextBox3.BackColor = Drawing.Color.Crimson
        End If
        Me.TextBox1.Text = reunion("Comentario")

        Dim filaa As DataRow = Me.ds_evalua.Tables(2).Rows(0)
        filaa("valorr") = reunion("totalpuntos")

        Dim filab As DataRow = Me.ds_evalua.Tables(2).Rows(1)
        filab("valorr") = reunion("porcentaje") & "%"

        '2. Cargar Evaluacion
        Dim ds As DataSet = Me.ds_evalua
        Dim dts As DataTable = evalu.FncDetalle_Evaluacion(Request.QueryString("Eval"))
        Dim Item As DataRow
        Dim calif As DataRow
        For i As Integer = 0 To dts.Rows.Count - 1
            calif = dts.Rows(i)
            Item = Me.ds_evalua.Tables(1).Rows.Find(calif("Id_Item"))
            If Not IsNothing(Item) Then
                'Encontr� la fila
                Item("calif") = calif("calificacion")
                Item("valor") = calif("valor")
            End If
        Next
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        limpiar()
        Me.PrcCargarDatos("Evaluacion")
    End Sub
End Class
