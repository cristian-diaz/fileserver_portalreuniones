Imports System.Data
Imports System.Data.OleDb

Partial Class Evaluaciones_Formularios_Evaluacion
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            Me.lblhorafin.Text = Format(Now, "HH:mm:ss")
            Me.prcCargarValidacion()
            Me.prcCrearDataset()

            Me.prcCargarOrigen("reuniones") ' cargar los sombos
            Me.PrcCargarDatos("reuniones") ' enlazar el combo 

            Me.prcCargarOrigen("Evaluacion") ' trae los items a evaluar 
            Me.PrcAcutualizar("grilla_llenar") ' llenar la grilla de la descripcion 
            Me.prcCargarOrigen("guardar") ' estructura para guardar la evaluacion 

            Me.prcCargarOrigen("Frecuencia")
            Me.PrcCargarDatos("Frecuencia")


            str_Automatico = Me.evalu.FncObligatorio(Me.DropDownList1.SelectedValue) ' para validar si la reunion es automatica o no 


            ' verificar si estoy actualizando o no 
            If Not IsNothing(Request.QueryString("Accion")) Then
                Me.DropDownList1.Enabled = False
                Me.prcCargarEvaluacion()
            Else ' estoy creadno una nueva 
                Me.txtfecha.Text = Now.ToShortDateString

                If str_Automatico = True Then
                    Me.evalu.deter_cumplehora(Me.DropDownList1.SelectedValue, Trim(txtfecha.Text))
                    Try
                        If Me.evalu.DatasetFill.Tables(0).Rows.Count > 0 Then
                            Me.ds_evalua.Tables(5).Merge(Me.evalu.DatasetFill.Tables(0))

                            Me.horainicioreun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("realincio")), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadaini")), "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadafin")), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(Now, "HH:mm:ss")
                            Me.lblmenerrror.Text = ""
                            Me.lblmenerrror.Visible = False
                            'Me.BTNGUARDAR.Visible = True
                        Else

                            Me.horainicioreun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblmenerrror.Text = "* No ha dado a�n inicio a la Reuni�n"
                            Me.lblmenerrror.Visible = True
                            'Me.BTNGUARDAR.Visible = False

                        End If
                    Catch ex As Exception
                        Me.lblmenerrror.Text = "Env�e el Error al Adiminstrador, Error: " & ex.Message

                    End Try

                    Me.btnusu.Enabled = True
                    Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")

                Else ' si no es automatico 

                    Me.evalu.Prc_Horas_sin_automatico(Val(Me.DropDownList1.SelectedValue), 1, 1, Trim(txtfecha.Text))

                    Try
                        If Me.evalu.DatasetFill.Tables(0).Rows.Count > 0 Then
                            Me.ds_evalua.Tables(5).Merge(Me.evalu.DatasetFill.Tables(0))

                            Me.horainicioreun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("horaInicio")), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(Now, "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadafin")), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadainicio")), "HH:mm:ss")
                            Me.lblmenerrror.Text = ""
                            Me.lblmenerrror.Visible = False

                        Else

                            Me.horainicioreun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblmenerrror.Text = "* No ha dado a�n inicio a la Reuni�n"
                            Me.lblmenerrror.Visible = True

                        End If
                    Catch ex As Exception

                        Me.lblmenerrror.Text = "Env�e el Error al Adiminstrador, Error: " & ex.Message
                    End Try
                    Me.btnusu.Enabled = False
                    Me.btnusu.BackColor = Drawing.Color.DarkGray
                End If
            End If
            Me.PrcCargarDatos("Evaluacion")
            Me.PrcCargarDatos("grilla_llenar")
        Else
            str_Automatico = Me.evalu.FncObligatorio(Me.DropDownList1.SelectedValue) ' para validar si la reunion es automatica o no 
        End If
        Me.Button1.Style.Add("cursor", "hand")

    End Sub

    Public Sub prcCargarValidacion()
        Me.rvRangoFec.MinimumValue = CDate(DateAdd(DateInterval.Day, -4, CDate(Now.ToShortDateString)))
        Me.rvRangoFec.MaximumValue = Now.ToShortDateString
        Me.rvRangoFec.ErrorMessage = "La Fecha debe estar entre el " & Me.rvRangoFec.MinimumValue & " y el " & Me.rvRangoFec.MaximumValue
    End Sub

    Public Sub prcCrearDataset()
        Dim ds As New DataSet
        Dim dtt_reunuion As New DataTable '---0
        Dim dtt_evaluacion As New DataTable '---1
        Dim dtt_reultado As New DataTable '---2
        Dim dtt_datos As New DataTable '---3
        Dim dtt_calificacion As New DataTable '---4
        Dim dtt_validarfecha As New DataTable '---5
        Dim dtt_comentario As New DataTable '---5


        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunuion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_evaluacion") ' items de la grilla a evaluar 
        Me.ds_evalua.Tables.Add("dtt_reultado") ' grilla de los resultados obteneidos %
        Me.ds_evalua.Tables.Add("dtt_datos")
        Me.ds_evalua.Tables.Add("dtt_calificacion")
        Me.ds_evalua.Tables.Add("dtt_validarfecha")
        Me.ds_evalua.Tables.Add("dtt_comentario")

        ' dataset de resultados
        Me.ds_evalua.Tables(2).Columns.Add("itemr", Type.GetType("System.String"))
        Me.ds_evalua.Tables(2).Columns.Add("valorr", Type.GetType("System.String"))

    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "guardar"
                Me.evalu.prcconsultarestructura()
                Me.ds_evalua.Tables(3).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "reuniones"
                Me.evalu.prcconsultarreuniones(Me.evalu.FncRol_Usuario(Session("Usuario")))
                Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))
            Case "Evaluacion"
                Me.evalu.prcconsultaritemreunion()
                Me.ds_evalua().Tables(1).Merge(Me.evalu.DatasetFill.Tables(0))
                Try
                    Me.ds_evalua.Tables(1).Columns.Add("index", Type.GetType("System.Int32"))
                    Me.ds_evalua.Tables(1).Columns.Add("calif", Type.GetType("System.String"))
                    Me.ds_evalua.Tables(1).Columns.Add("valor", Type.GetType("System.Int32"))
                    Me.ds_evalua.Tables(1).Columns.Add("orden", Type.GetType("System.Int32"))
                    Me.ds_evalua.Tables(1).PrimaryKey = New DataColumn() {Me.ds_evalua.Tables(1).Columns("Id_Item")}
                Catch ex As Exception

                End Try
                For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
                    Me.ds_evalua.Tables(1).Rows(i)("index") = i
                    Me.ds_evalua.Tables(1).Rows(i)("calif") = 0
                    Me.ds_evalua.Tables(1).Rows(i)("valor") = 0
                    Me.ds_evalua.Tables(1).Rows(i)("orden") = i + 1
                Next
                Me.ds_evalua.Tables(1).AcceptChanges()

            Case "Frecuencia"
                Dim v_bf As String = Me.DropDownList1.SelectedValue
                If v_bf = "" Then

                Else

                    Me.ds_evalua.Tables("dtt_comentario").Clear()
                    Me.evalu.consulta_frecuencia(v_bf)
                    Me.ds_evalua().Tables("dtt_comentario").Merge(Me.evalu.DatasetFill.Tables(0))
                End If




        End Select
    End Sub


    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Me.DropDownList1.DataSource = Me.ds_evalua.Tables(0).CreateDataReader
                Me.DropDownList1.DataTextField = "Nombre"
                Me.DropDownList1.DataValueField = "Id"
                Me.DropDownList1.DataBind()
                Me.DropDownList1.Items.Insert(0, New ListItem("--------Seleccione Nombre de Reunion-------", ""))

            Case "Evaluacion"
                Me.GridView1.DataSource = Me.ds_evalua.Tables(1)
                Me.GridView1.DataBind()
            Case "Resultados"
                
            Case "grilla_llenar"
                Me.GridView2.DataSource = Me.ds_evalua.Tables(2)
                Me.GridView2.DataBind()
            Case "validarasistentes"
                Me.ds_evalua.Tables(5).Rows.Clear()

                str_Automatico = Me.evalu.FncObligatorio(Me.DropDownList1.SelectedValue) ' para validar si la reunion es automatica o no 

                If str_Automatico = True Then
                    Me.evalu.deter_cumplehora(Me.DropDownList1.SelectedValue, Trim(txtfecha.Text))

                    Try
                        If Me.evalu.DatasetFill.Tables(0).Rows.Count > 0 Then
                            Me.ds_evalua.Tables(5).Merge(Me.evalu.DatasetFill.Tables(0))

                            Me.horainicioreun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("realincio")), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadaini")), "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadafin")), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(Now, "HH:mm:ss")
                            Me.lblmenerrror.Text = ""
                            Me.lblmenerrror.Visible = False
                            'Me.BTNGUARDAR.Visible = True
                        Else

                            Me.horainicioreun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblmenerrror.Text = "* No ha dado a�n inicio a la Reuni�n"
                            Me.lblmenerrror.Visible = True
                            'Me.BTNGUARDAR.Visible = False

                        End If
                    Catch ex As Exception
                        Me.lblmenerrror.Text = "Env�e el Error al Adiminstrador, Error: " & ex.Message

                    End Try


                    'If Me.evalu.Fnccumplidofecha(Me.DropDownList1.SelectedValue) = False Then
                    '    ' Me.MsgBox1.ShowMessage("NO HA DADO AUN INICIO A LA REUNION") ' ...SE REDIRECCIONARA A LA PAGINA DE REGISTRO DE ASISTENTES")
                    '    Me.lblmenerrror.Visible = True
                    '    If Me.txtlugar.Text = "" Then
                    '        Me.lbllugar.Visible = True
                    '    Else
                    '        Me.lbllugar.Visible = False
                    '    End If
                    'Else

                    '    If Me.txtlugar.Text = "" Then
                    '        Me.lbllugar.Visible = True
                    '    Else
                    '        Me.lbllugar.Visible = False
                    '    End If

                    '    Me.lblmenerrror.Visible = False

                    'End If
                    Me.btnusu.Enabled = True
                    Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
                Else


                    Me.evalu.Prc_Horas_sin_automatico(Val(Me.DropDownList1.SelectedValue), 1, 1, txtfecha.Text)
                    Try
                        If Me.evalu.DatasetFill.Tables(0).Rows.Count > 0 Then
                            Me.ds_evalua.Tables(5).Merge(Me.evalu.DatasetFill.Tables(0))

                            Me.horainicioreun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("horaInicio")), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(Now, "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadafin")), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item("estimadainicio")), "HH:mm:ss")
                            Me.lblmenerrror.Text = ""
                            Me.lblmenerrror.Visible = False
                            'Me.BTNGUARDAR.Visible = True

                        Else

                            Me.horainicioreun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblinicioestimre.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblfinalestimadareun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            Me.lblhorafin.Text = Format(CDate("00:00:00"), "HH:mm:ss")
                            'Me.BTNGUARDAR.Visible = False
                            Me.lblmenerrror.Text = "* No ha dado a�n inicio a la Reuni�n"
                            Me.lblmenerrror.Visible = True

                        End If
                        ' Me.ImageButton1.Visible = False
                    Catch ex As Exception

                        Me.lblmenerrror.Text = "Env�e el Error al Adiminstrador, Error: " & ex.Message
                    End Try

                    Me.btnusu.Enabled = False
                    Me.btnusu.BackColor = Drawing.Color.DarkGray
                    'Me.lblmenerrror.Visible = False
                End If


            Case "Frecuencia"
                Try
                    Me.lbl_frecuencia.Text = Me.ds_evalua.Tables("dtt_comentario").Rows(0).Item(7)

                Catch ex As Exception
                    Me.lbl_frecuencia.Text = "Sin Frecuencia Asignada"

                End Try

        End Select
    End Sub


    Public Sub PrcAcutualizar(ByVal tipo As String)
        Select Case tipo
            Case "grilla_llenar"
                Dim filas As DataRow
                Dim sumapeso As Integer

                For i As Integer = 0 To Me.ds_evalua.Tables(1).Rows.Count - 1
                    sumapeso += Me.ds_evalua.Tables(1).Rows(i).Item("Peso")
                Next


                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Puntaje Obtenido"
                filas("valorr") = 0
                Me.ds_evalua.Tables(2).Rows.Add(filas)

                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Efectividad de Esta Reuni�n"
                filas("valorr") = 0
                Me.ds_evalua.Tables(2).Rows.Add(filas)

                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Total Peso"
                filas("valorr") = sumapeso
                Me.ds_evalua.Tables(2).Rows.Add(filas)


                filas = Me.ds_evalua.Tables(2).NewRow
                filas("itemr") = "Maximo Puntaje Posible"
                filas("valorr") = Me.FncMaxPuntaje
                Me.ds_evalua.Tables(2).Rows.Add(filas)

            Case "grilla"
                Dim row As GridViewRow
                Dim total As Integer = 0
                Dim porcentajeo As Integer
                Dim efeti As Integer
                Dim filaa As DataRow
                Dim filab As DataRow

                filaa = Me.ds_evalua.Tables(2).Rows(0)
                filab = Me.ds_evalua.Tables(2).Rows(1)

                For Each row In Me.GridView1.Rows
                    total += CInt(CType(row.FindControl("lbltotal"), Label).Text)
                Next

                efeti = Me.ds_evalua.Tables(2).Rows(3).Item(1)

                porcentajeo = (total / efeti) * 100

                filaa("valorr") = total 'puntos
                filab("valorr") = porcentajeo & "%"

                Me.TextBox3.Text = porcentajeo & "%"

                If porcentajeo > CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString) Then
                    Me.TextBox3.BackColor = Drawing.Color.Green
                    Me.TextBox3.ForeColor = Drawing.Color.White
                Else
                    Me.TextBox3.BackColor = Drawing.Color.Red
                    Me.TextBox3.ForeColor = Drawing.Color.Black
                End If

                Me.GridView2.Rows(1).Cells(1).BackColor = System.Drawing.Color.Red
                Me.GridView2.Rows(3).Cells(1).BackColor = System.Drawing.Color.Blue

            Case "guardar"
                If Me.DropDownList1.SelectedValue = "" Then
                    Me.MsgBox1.ShowMessage("Elija Una Reunion ha Evaluar")
                Else



                    Dim str_accion As String = Me.Request.QueryString("Accion")
                    Dim nmr_evaluacion As String = Request.QueryString("Eval")

                    Dim filaa As DataRow
                    filaa = Me.ds_evalua.Tables(3).NewRow
                    filaa("Id") = Me.DropDownList1.SelectedValue
                    filaa("login") = Session("Usuario")
                    filaa("lugar") = Me.txtlugar.Text
                    filaa("fecha") = Me.txtfecha.Text
                    Dim ttptos As Integer = Me.ds_evalua.Tables(2).Rows(0).Item(1)
                    filaa("totalpuntos") = Me.ds_evalua.Tables(2).Rows(0).Item(1)
                    filaa("porcentaje") = Replace(Me.ds_evalua.Tables(2).Rows(1).Item(1).ToString, "%", "")
                    filaa("comentario") = Me.TextBox1.Text
                    filaa("hora_estimada_inicio") = lblinicioestimre.Text
                    filaa("hora_estimada_final") = lblfinalestimadareun.Text

                    Me.ds_evalua.Tables(3).Rows.Add(filaa)

                    If ttptos <> 0 Then

                        If IsNothing(Me.Request.QueryString("Accion")) Then
                            'Esta Insertando
                            Me.evalu.prcguardar_encabezado(Me.ds_evalua, Me.horainicioreun.Text, Me.lblhorafin.Text) '  Me.str_horainireal, Me.str_horafinal_real)
                        End If

                        If str_accion = "Update" Then
                            Me.evalu.prcupdate_encabezado(Me.ds_evalua, nmr_evaluacion)
                        End If



                        If Me.evalu.FlagError = False Then
                            limpiar()
                            Me.MsgBox1.ShowMessage("Los Datos Han Sido Guardados")
                            ' Response.Redirect("Evaluacion.aspx")
                            ' Response.Redirect("../Formularios/ver_evaluacion.aspx")
                            Dim nombre_rnion As String = Me.DropDownList1.SelectedItem.Text
                            Response.Redirect("../Formularios/Revisar_evaluacion.aspx?Id_Reunion=" & Me.DropDownList1.SelectedValue & "&fecha=" & Me.txtfecha.Text & "&nombre=" & nombre_rnion)

                        Else
                            Me.MsgBox1.ShowMessage(Me.evalu.ERRORR)
                        End If


                        If str_Automatico = True Then
                            Me.btnusu.Enabled = True
                            Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
                        Else
                            Me.btnusu.Enabled = False
                            Me.btnusu.BackColor = Drawing.Color.DarkGray
                        End If


                    Else
                        Me.MsgBox1.ShowMessage("El resultado de la evaluacion no puede ser igual a 0 Por favor evalue la reunion.")

                    End If

                End If
        End Select
    End Sub

    Public Function FncMaxPuntaje() As Integer

        Dim Total As Integer = 0

        For Each row As DataRow In Me.ds_evalua.Tables(1).Rows
            Total += row("MaxPuntos")
        Next

        Return Total

    End Function
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            'Cargar �tems de los combos
            Dim com As DropDownList
            Dim id As String = CType(e.Row.FindControl("Lbl_ID"), Label).Text 'Llave del registro de �tems
            Dim item As DataRow = ds_evalua.Tables(1).Rows.Find(id)
            Dim v() As String = Split(item("calificacion").ToString, ",")
            com = CType(e.Row.FindControl("dbcalifica"), DropDownList)
            Me.ds_evalua.Tables(5).Rows.Clear()

            For s As Integer = 0 To v.Length - 1
                Dim NItem As New ListItem
                NItem.Text = v(s)
                com.Items.Add(NItem)
                NItem = Nothing
            Next

            ' para verifica si esta actualizando '
            If Not IsNothing(Request.QueryString("Accion")) Then
                com.SelectedValue = ds_evalua.Tables(1).Rows.Find(id)("calif")
                Dim lbl As Label = CType(e.Row.FindControl("lbltotal"), Label)
                If CInt(lbl.Text) > 0 Then
                    lbl.ForeColor = Drawing.Color.White
                    lbl.BackColor = Drawing.Color.Green

                Else
                    lbl.ForeColor = Drawing.Color.Black
                    lbl.BackColor = Drawing.Color.Crimson
                End If

                If str_Automatico = True Then
                    Select Case id
                        Case 1
                            com.Enabled = False
                        Case 2
                            com.Enabled = False
                        Case 5
                            com.Enabled = False
                        Case 12
                            com.Enabled = False
                    End Select
                End If
            Else

                ' cuando esta creando una nueva 

                ' para que no valide lso compromisos cumplidos, los participantes, ni el cumplimiento de inicio y final de la reunion
                If str_Automatico = True Then
                    Dim hora_estima_ini As String
                    Dim hora_estima_fin As String
                    Dim hora_inicio As String
                    Dim hora_final As String


                    Dim duracion_inicio As Integer
                    Dim duracion_final As Integer

                    Dim duracion_ini As Integer
                    Dim duracion_fin As Integer
                    ' Dim bandera_cumpli_ini As Boolean
                    'Dim bandera_cumpli_fin As Boolean

                    If ds_evalua.Tables(1).Rows.Find(id)("calc") = True Then
                        If CInt(id) = 1 Then
                            ''Dim hora_estima_ini As String
                            ''Dim hora_estima_fin As String
                            ''Dim hora_inicio As String
                            ''Dim hora_final As String

                            ''Dim duracion_inicio As Integer
                            ''Dim duracion_final As Integer
                            ''Dim bandera_cumpli_ini As Boolean
                            ''Dim bandera_cumpli_fin As Boolean

                            ' para verificar lo de las horas 
                            Me.evalu.deter_cumplehora(Me.DropDownList1.SelectedValue, Trim(txtfecha.Text))
                            Me.ds_evalua.Tables(5).Merge(Me.evalu.DatasetFill.Tables(0))
                            Try
                                hora_estima_ini = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item(1)), "HH:mm:ss") ' cuando crean la reunion asp
                                hora_estima_fin = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item(2)), "HH:mm:ss") ' cuando crean la reunion asp
                                hora_inicio = Format(CDate(Me.ds_evalua.Tables(5).Rows(0).Item(3)), "HH:mm:ss") ' cuando dan inicio a la reuion modulo inicio reunion asp yd
                                hora_final = Format(CDate(Now), "HH:mm:ss") ' hora final cuando ingresa al modulo de evaluacion .net  

                                Me.str_horafinal_real = CDate(hora_final) ', "hh:mm tt") ' hora final cuando ingresa al modulo de evaluacion .net
                                Me.str_horainireal = CDate(hora_inicio) ', "hh:mm tt") ' cuando dan inicio a la reuion modulo inicio reunion asp yd

                                Dim h1 As Date = hora_estima_ini ' programada
                                Dim h3 As Date = hora_inicio ' reala
                                Dim h2 As Date = hora_estima_fin 'programada
                                Dim h4 As Date

                                Try
                                    h4 = CDate(hora_final) ' real
                                Catch ex As Exception
                                End Try

                                duracion_inicio = DateDiff(DateInterval.Minute, CDate(h3), CDate(h1)) ' encontrar la diferencia 
                                duracion_final = DateDiff(DateInterval.Minute, CDate(h4), CDate(h2)) ' encontrar la diferencia 


                                If duracion_inicio > 0 Then
                                    If duracion_inicio <= CInt(System.Configuration.ConfigurationManager.AppSettings("Interinireunantes").ToString) Then
                                        bandera_cumpli_ini = True
                                    Else
                                        bandera_cumpli_ini = False
                                    End If
                                Else

                                    duracion_ini = duracion_inicio * -1
                                    If duracion_ini <= CInt(System.Configuration.ConfigurationManager.AppSettings("Interinireundes").ToString) Then
                                        bandera_cumpli_ini = True
                                    Else
                                        bandera_cumpli_ini = False
                                    End If
                                End If


                                ' fin de la reunion 
                                If duracion_final > 0 Then
                                    If duracion_final <= CInt(System.Configuration.ConfigurationManager.AppSettings("Interfinreunantes").ToString) Then
                                        bandera_cumpli_fin = True
                                    Else
                                        bandera_cumpli_fin = False
                                    End If
                                Else
                                    duracion_fin = duracion_final * -1
                                    If duracion_fin <= CInt(System.Configuration.ConfigurationManager.AppSettings("Interfinreundes").ToString) Then
                                        bandera_cumpli_fin = True
                                    Else
                                        bandera_cumpli_fin = False
                                    End If
                                End If
                            Catch ex As Exception
                                bandera_cumpli_fin = False
                                bandera_cumpli_ini = False
                            End Try
                        End If

                        Select Case id
                            Case 1
                                If bandera_cumpli_ini = True Then
                                    com.SelectedValue = "Si"
                                    com.Enabled = False
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "3"
                                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "Si"

                                    If CType(e.Row.FindControl("lbltotal"), Label).Text > 0 Then
                                        'row.Cells(4).BackColor = System.Drawing.Color.Green
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
                                    Else
                                        'row.Cells(4).BackColor = System.Drawing.Color.Red
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
                                    End If
                                Else
                                    com.SelectedValue = "No"
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "0"
                                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "No"

                                    com.Enabled = False
                                End If

                            Case 2
                                If str_Automatico = True Then
                                    Dim USUARIOS_REGISTRADOS As DataTable = Me.evalu.Fncpartiregistrados(Me.DropDownList1.SelectedValue)
                                    Dim USUARIOS_PARTICIPANTES As DataTable = Me.evalu.Fncpartiasitieron(Me.DropDownList1.SelectedValue)
                                    If USUARIOS_REGISTRADOS.Rows.Count = USUARIOS_PARTICIPANTES.Rows.Count Then
                                        com.SelectedValue = "Si"
                                        ds_evalua.Tables(1).Rows.Find(id)("calif") = "3"
                                        CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                        ds_evalua.Tables(1).Rows.Find(id)("calif") = "Si"
                                        com.Enabled = False
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
                                    Else
                                        com.SelectedValue = "No"
                                        ds_evalua.Tables(1).Rows.Find(id)("calif") = "0"
                                        CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                        ds_evalua.Tables(1).Rows.Find(id)("calif") = "No"
                                        com.Enabled = False
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
                                    End If
                                End If

                            Case 5

                                If evalu.FncCumplimiento_Compromisos(Me.DropDownList1.SelectedItem.Text) = True Then
                                    com.SelectedValue = "Si"
                                    com.Enabled = False
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "3"

                                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "Si"
                                    If CType(e.Row.FindControl("lbltotal"), Label).Text > 0 Then
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
                                    Else
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
                                    End If
                                Else
                                    com.SelectedValue = "No"
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "0"
                                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "No"
                                    com.Enabled = False
                                End If

                            Case 12
                                If bandera_cumpli_fin = True Then
                                    com.SelectedValue = "Si"
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "3"
                                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "Si"

                                    If CType(e.Row.FindControl("lbltotal"), Label).Text > 0 Then
                                        'row.Cells(4).BackColor = System.Drawing.Color.Green
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
                                    Else
                                        'row.Cells(4).BackColor = System.Drawing.Color.Red
                                        CType(e.Row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
                                        CType(e.Row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
                                    End If
                                    com.Enabled = False
                                Else
                                    com.SelectedValue = "No"
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "0"
                                    CType(e.Row.FindControl("lbltotal"), Label).Text = ds_evalua.Tables(1).Rows.Find(id)("peso") * ds_evalua.Tables(1).Rows.Find(id)("calif")
                                    ds_evalua.Tables(1).Rows.Find(id)("calif") = "No"
                                    com.Enabled = False
                                End If

                        End Select
                    End If
                End If
            End If
        End If
    End Sub
    Public Property str_NmLista() As String
        Get
            Return ViewState("ListaNm")
        End Get
        Set(ByVal value As String)
            ViewState("ListaNm") = value
        End Set
    End Property

    Public Property str_Automatico() As Boolean
        Get
            Return ViewState("str_Automati")
        End Get
        Set(ByVal value As Boolean)
            ViewState("str_Automati") = value
        End Set
    End Property

    Public Property bandera_cumpli_fin() As Boolean
        Get
            Return ViewState("_STbandera_cumpli_fin")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_STbandera_cumpli_fin") = value
        End Set
    End Property

    Public Property bandera_cumpli_ini() As Boolean
        Get
            Return ViewState("_STRbandera_cumpli_ini")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_STRbandera_cumpli_ini") = value
        End Set
    End Property

    Protected Sub dbcalifica_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If Me.txtlugar.Text = "" Then
            Me.lbllugar.Visible = True
        Else
            Me.lbllugar.Visible = False
        End If

        If str_Automatico = True Then
            Me.btnusu.Enabled = True
            Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
        Else
            Me.btnusu.Enabled = False
            Me.btnusu.BackColor = Drawing.Color.DarkGray
        End If


        Dim cmb As DropDownList = sender
        Dim valor As String = cmb.ToolTip ' la fila en la que estoy 

        Dim row As GridViewRow = GridView1.Rows(valor)
        If valor = 6 Then
            If CType(row.FindControl("dbcalifica"), DropDownList).Text = "Unos" Then
                CType(row.FindControl("lbltotal"), Label).Text = CType(row.FindControl("lblpeso"), Label).Text * 1
            Else
                CType(row.FindControl("lbltotal"), Label).Text = CType(row.FindControl("lblpeso"), Label).Text * IIf(CType(row.FindControl("dbcalifica"), DropDownList).Text = "Si", 3, 0)
            End If
        Else
            CType(row.FindControl("lbltotal"), Label).Text = CType(row.FindControl("lblpeso"), Label).Text * IIf(CType(row.FindControl("dbcalifica"), DropDownList).Text = "Si", 3, 0)
        End If

        If CType(row.FindControl("lbltotal"), Label).Text > 0 Then
            'row.Cells(4).BackColor = System.Drawing.Color.Green
            CType(row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Green
            CType(row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.White
        Else
            'row.Cells(4).BackColor = System.Drawing.Color.Red
            CType(row.FindControl("lbltotal"), Label).BackColor = System.Drawing.Color.Crimson
            CType(row.FindControl("lbltotal"), Label).ForeColor = System.Drawing.Color.Black
        End If



        Dim key As String = CType(row.FindControl("Lbl_ID"), Label).Text
        Dim anyrow As DataRow = Me.ds_evalua.Tables(1).Rows.Find(key) ' ver sie sta en lo qeu seleccione
        If Not IsNothing(anyrow) Then
            anyrow("calif") = CType(row.FindControl("dbcalifica"), DropDownList).Text
        End If

        PrcAcutualizar("grilla")
        Me.PrcCargarDatos("grilla_llenar")

    End Sub
    Sub limpiar()
        Me.ds_evalua.Tables(1).Clear()
        Me.ds_evalua.Tables(2).Clear()
        Me.ds_evalua.Tables(3).Clear()
        Me.ds_evalua.Tables(4).Clear()

        ViewState("cont") = 0
        Me.prcCargarOrigen("Evaluacion")
        Me.PrcCargarDatos("Evaluacion")
        Me.PrcAcutualizar("grilla_llenar")
        Me.PrcCargarDatos("grilla_llenar")
        Me.txtfecha.Text = ""
        Me.TextBox3.Text = ""
        Me.txtlugar.Text = ""
        Me.TextBox1.Text = ""
        Me.txtfecha.Text = Now.ToShortDateString
        Me.TextBox3.Text = "0%"
        Me.TextBox3.BackColor = Drawing.Color.Crimson
        Me.TextBox3.ForeColor = Drawing.Color.Black

        'Me.horainicioreun.Text = "00:00"
        'Me.lblinicioestimre.Text = "00:00"
        'Me.lblfinalestimadareun.Text = "00:00"
        'Me.lblhorafin.Text = "00:00"
        str_Automatico = Me.evalu.FncObligatorio(Me.DropDownList1.SelectedValue)

	

        If str_Automatico = True Then
            Me.btnusu.Enabled = True
            Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
        Else
            Me.btnusu.Enabled = False
            Me.btnusu.BackColor = Drawing.Color.DarkGray
        End If
    End Sub
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        limpiar()
    End Sub

    Protected Sub BTNGUARDAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTNGUARDAR.Click
        Dim str_Oopcion As String = Request.QueryString("Accion")

        If IsNothing(Request.QueryString("Accion")) Then

            If str_Automatico = True Then
                If Me.evalu.Fnccumplidofecha(Me.DropDownList1.SelectedValue) = True Then
                    If Not Me.txtfecha.Text = "" Then
                        If evalu.validar_fechas(Me.txtfecha.Text, Me.DropDownList1.SelectedValue) < System.Configuration.ConfigurationManager.AppSettings("maximoevaluaciones").ToString() Then

                            PrcAcutualizar("guardar")
                        Else
                            Me.MsgBox1.ShowMessage("Ya se registraron el m�ximo n�mero de evaluaciones permitidas ( " & System.Configuration.ConfigurationManager.AppSettings("maximoevaluaciones").ToString() & ") para esta reuni�n: " & Chr(10) & Me.DropDownList1.SelectedItem.Text & Chr(10)) '& " ( No evaluaci�n : " & Me.evalu.str_validar_fecha & ")")
                        End If
                    End If
                Else
                    Me.MsgBox1.ShowMessage("No ha dado aun INICIO a la REUNION.... Por favor ingrese al modulo EVALUACION \INICIAR REUNION")
                End If
            Else
                If Not Me.txtfecha.Text = "" Then
                    PrcAcutualizar("guardar")
                End If
            End If


        End If


        If str_Oopcion = "Update" Then
            PrcAcutualizar("guardar")
        End If

        If str_Automatico = True Then
            Me.btnusu.Enabled = True
            Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
        Else
            Me.btnusu.Enabled = False
            Me.btnusu.BackColor = Drawing.Color.DarkGray
        End If

    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        If evalu.validar_fechas(Me.txtfecha.Text, Me.DropDownList1.SelectedValue) < System.Configuration.ConfigurationManager.AppSettings("maximoevaluaciones").ToString() Then
            Me.MsgBox1.ShowMessage("No se encontraron Registros.. Puede continuar con la Evaluaci�n")
        Else
            Me.MsgBox1.ShowMessage("Ya se registraron el m�ximo n�mero de evaluaciones permitidas ( " & System.Configuration.ConfigurationManager.AppSettings("maximoevaluaciones").ToString() & ") para esta reuni�n: " & Chr(10) & Me.DropDownList1.SelectedItem.Text & Chr(10)) '& " ( No evaluaci�n : " & Me.evalu.str_validar_fecha & ")")
        End If
    End Sub

    Public Sub prcCargarEvaluacion()
        '1. Cargar Valores de la Reuni�n
        Dim dtt As DataSet = evalu.prcConsultar_Evaluacion(, , , Request.QueryString("Eval"))
        Dim reunion As DataRow = dtt.Tables(0).Rows(0)
        Me.No_eval_actualizar = reunion("No_Eval")
        Me.horainicioreun.Text = reunion("hora_inici_real") 'evalu.DatasetFill.Tables(0).Rows(0).Item("horaInicio")
        Me.lblhorafin.Text = reunion("hora_final_real") 'evalu.DatasetFill.Tables(0).Rows(0).Item("horaFin")
        'Me.lblhorafin.Text = reunion("hora_final_real")
        'Me.horainicioreun.Text = reunion("hora_inici_real")
        Me.DropDownList1.SelectedValue = reunion("id")
        Me.txtlugar.Text = reunion("lugar")
        Me.txtfecha.Text = CDate(reunion("fecha")).ToShortDateString
        Me.TextBox3.Text = reunion("porcentaje") & "%"
        Try
            Me.lblinicioestimre.Text = reunion("hora_estimada_inicio")
        Catch ex As Exception
            Me.lblinicioestimre.Text = Format(CDate("00:00:00"), "HH:mm:ss")
        End Try
        Try
            Me.lblfinalestimadareun.Text = reunion("hora_estimada_final")
        Catch ex As Exception
            Me.lblfinalestimadareun.Text = Format(CDate("00:00:00"), "HH:mm:ss")
        End Try

        If CDbl(reunion("porcentaje")) > CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString) Then
            Me.TextBox3.ForeColor = Drawing.Color.White
            Me.TextBox3.BackColor = Drawing.Color.Green
        Else
            Me.TextBox3.ForeColor = Drawing.Color.Black
            Me.TextBox3.BackColor = Drawing.Color.Crimson
        End If
        Me.TextBox1.Text = reunion("Comentario")

        Dim filaa As DataRow = Me.ds_evalua.Tables(2).Rows(0)
        filaa("valorr") = reunion("totalpuntos")

        Dim filab As DataRow = Me.ds_evalua.Tables(2).Rows(1)
        filab("valorr") = reunion("porcentaje") & "%"

        '2. Cargar Evaluacion
        Dim ds As DataSet = Me.ds_evalua
        Dim dts As DataTable = evalu.FncDetalle_Evaluacion(Request.QueryString("Eval"))
        Dim Item As DataRow
        Dim calif As DataRow
        For i As Integer = 0 To dts.Rows.Count - 1
            calif = dts.Rows(i)
            Item = Me.ds_evalua.Tables(1).Rows.Find(calif("Id_Item"))
            If Not IsNothing(Item) Then
                'Encontr� la fila
                Item("calif") = calif("calificacion")
                Item("valor") = calif("valor")
            End If
        Next
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        If IsNothing(Request.QueryString("Accion")) Then
            limpiar()
            Me.PrcCargarDatos("validarasistentes")
            Me.prcCargarOrigen("Frecuencia")
            Me.PrcCargarDatos("Frecuencia")

        End If
    End Sub

    Public Property str_horainireal() As String
        Get
            Return ViewState("horarealini")
        End Get
        Set(ByVal value As String)
            ViewState("horarealini") = value
        End Set
    End Property

    Public Property str_horafinal_real() As String
        Get
            Return ViewState("hora_final_real")
        End Get
        Set(ByVal value As String)
            ViewState("hora_final_real") = value
        End Set
    End Property



    ' PARA VERIFICAR QUE LAS REUNION PUEDNA INICAR 10 MINUTOS ANTES O 10 MINUTOS DESPUES APLICA PARA LA FINAL TAMBIEN

    Public Property str_Horainicioantes() As String '10 minutos antes inicio
        Get
            Return ViewState("_strHorainicioantes")
        End Get
        Set(ByVal value As String)
            ViewState("_strHorainicioantes") = value
        End Set
    End Property
    Public Property str_Horainiciodesp() As String ' 10 minutos despues inicio 
        Get
            Return ViewState("_strHorainiciodes")
        End Get
        Set(ByVal value As String)
            ViewState("_strHorainiciodes") = value
        End Set
    End Property
    Public Property str_Horafinoantes() As String '10 minutos antes final 
        Get
            Return ViewState("_strHorafinantes")
        End Get
        Set(ByVal value As String)
            ViewState("_strHorafinantes") = value
        End Set
    End Property
    Public Property str_Horafindesp() As String ' 10 minutos despues final 
        Get
            Return ViewState("_strHorafindes")
        End Get
        Set(ByVal value As String)
            ViewState("_strHorafindes") = value
        End Set
    End Property


    Protected Sub btnusu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnusu.Click

        If IsNothing(Request.QueryString("Accion")) Then
            Dim URL As String = "../../iniciarReunion2.asp?idreunion=" & Me.DropDownList1.SelectedValue

            Dim popupScript As String = "<script language='JavaScript'>" & _
                                                "window.open('" & URL & "', 'CustomPopUp', " & _
                                                "'dependent=yes,toolbar=yes,scrollbars=yes,width=800, height=600,resizable=yes,status=yes,left=120,Top=20')" & _
                                                "</script>"
            Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)
        Else
            Dim URL As String = "../../listarAsistentes.asp?Noevala=" & Me.No_eval_actualizar & "&idreunion=" & Me.DropDownList1.SelectedValue

            Dim popupScript As String = "<script language='JavaScript'>" & _
                                                "window.open('" & URL & "', 'CustomPopUp', " & _
                                                "'dependent=yes,toolbar=yes,scrollbars=yes,resizable=yes,status=yes,left=120,Top=20,width=800, height=600,')" & _
                                                "</script>"
            Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)
        End If

        If str_Automatico = True Then
            Me.btnusu.Enabled = True
            Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
        Else
            Me.btnusu.Enabled = False
            Me.btnusu.BackColor = Drawing.Color.DarkGray
        End If

    End Sub

    Public Property No_eval_actualizar() As String
        Get
            Return ViewState("No_eval_actug")
        End Get
        Set(ByVal value As String)
            ViewState("No_eval_actug") = value
        End Set
    End Property

    Protected Sub txtlugar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtlugar.TextChanged
        'If Me.txtlugar.Text = "" Then
        '    Me.lbllugar.Visible = True
        'Else
        '    Me.lbllugar.Visible = False
        'End If
    End Sub



    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        If IsNothing(Request.QueryString("Accion")) Then
            If str_Automatico = True Then
                If Me.evalu.Fnccumplidofecha(Me.DropDownList1.SelectedValue) = True Then
                    If Not Me.txtfecha.Text = "" Then
                        If evalu.validar_fechas(Me.txtfecha.Text, Me.DropDownList1.SelectedValue) < System.Configuration.ConfigurationManager.AppSettings("maximoevaluaciones").ToString() Then

                            PrcAcutualizar("guardar")
                            'Response.Redirect("../Formularios/ver_evaluacion.aspx")


                            'AddHandler Button4.Click, AddressOf Button4_Click


                        Else
                            Me.MsgBox1.ShowMessage("Ya se registraron el m�ximo n�mero de evaluaciones permitidas ( " & System.Configuration.ConfigurationManager.AppSettings("maximoevaluaciones").ToString() & ") para esta reuni�n: " & Chr(10) & Me.DropDownList1.SelectedItem.Text & Chr(10)) '& " ( No evaluaci�n : " & Me.evalu.str_validar_fecha & ")")
                        End If
                    End If
                Else
                    Me.MsgBox1.ShowMessage("No ha dado aun INICIO a la REUNION.... Por favor ingrese al modulo EVALUACION \INICIAR REUNION")
                End If
            Else
                If Not Me.txtfecha.Text = "" Then
                    PrcAcutualizar("guardar")
                End If
            End If


        End If

        If str_Automatico = True Then
            Me.btnusu.Enabled = True
            Me.btnusu.BackColor = Drawing.Color.FromName("#FFCC00")
        Else
            Me.btnusu.Enabled = False
            Me.btnusu.BackColor = Drawing.Color.DarkGray
        End If

        
    End Sub

End Class
