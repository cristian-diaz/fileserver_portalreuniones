﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="grafica_rangofecha.aspx.vb" Inherits="grafica_rangofecha" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Página sin título</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td style="height: 18px; font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG); color: white; font-family: Verdana,Arial,Helvetica,sans-serif;" colspan="6">
                Estadísticas Reunión<asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 19px">
            </td>
            <td style="width: 20%; height: 19px;">
                </td>
            <td colspan="3" style="height: 19px" align="left">
                </td>
            <td style="width: 10%; height: 19px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%;">
            </td>
            <td colspan="4">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 10%; height: 20px;" valign="middle">
                        </td>
                        <td style="width: 20%; background-color: #d5ddbf; height: 20px;" valign="middle" align="left">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Italic="False" Text="Reunión:" SkinID="TitTabla"></asp:Label></td>
                        <td colspan="3" valign="middle" style="height: 20px">
                <asp:DropDownList ID="DropDownList1" runat="server" Width="502px">
                </asp:DropDownList></td>
                        <td colspan="1" style="width: 10%; height: 20px;" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%; height: 15px">
                        </td>
                        <td align="left" colspan="4" style="width: 20%; height: 15px">
                        </td>
                        <td align="left" colspan="1" style="width: 10%; height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%; height: 20px;" valign="middle">
                        </td>
                        <td align="left" colspan="4" style="background-color: #d5ddbf; height: 20px;" valign="middle">
                            <asp:Label ID="Label10" runat="server" Font-Bold="False" Font-Italic="False" Text="Rango de Fechas por dias" SkinID="TitTabla"></asp:Label></td>
                        <td align="left" colspan="1" style="width: 10%; height: 20px;" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 10%; height: 15px" valign="middle">
                        </td>
                        <td colspan="1" style="width: 10%; height: 15px" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%; height: 15px;" valign="middle" align="right">
                            <asp:RadioButton ID="rdfecha" runat="server" GroupName="tipo" AutoPostBack="True" Width="77px" /></td>
                        <td style="width: 20%; background-color: #d5ddbf; height: 15px;" valign="middle" align="left">
                            <asp:Label ID="Label4" runat="server" Text="Fecha Inicio  :                          " SkinID="TitTabla"></asp:Label></td>
                        <td align="left" style="width: 20%; height: 15px;" valign="middle">
                            <asp:TextBox ID="TxFecIni" runat="server" MaxLength="10" ValidationGroup="VGFecha"
                                Width="107px"></asp:TextBox>
                            <asp:Button ID="Button2" runat="server" Text="..." /></td>
                        <td style="width: 20%; background-color: #d5ddbf; height: 15px;" valign="middle" align="left">
                            <asp:Label ID="Label5" runat="server" Text="    Fecha Final:" SkinID="TitTabla"></asp:Label></td>
                        <td style="width: 20%; height: 15px;" valign="middle" align="left">
                            <asp:TextBox ID="TxFecFin" runat="server" MaxLength="10" ValidationGroup="VGFecha"
                                Width="107px"></asp:TextBox>
                            <asp:Button ID="Button3" runat="server" Text="..." /></td>
                        <td style="width: 10%; height: 15px;" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" style="width: 10%; height: 15px">
                        </td>
                        <td align="left" colspan="1" style="width: 10%; height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 10%; height: 20px;" valign="middle">
                        </td>
                        <td align="left" colspan="4" style="background-color: #d5ddbf; height: 20px;" valign="middle">
                            <asp:Label ID="Label11" runat="server" Font-Bold="False" Font-Italic="False" Text="Consolidado por meses y año" SkinID="TitTabla"></asp:Label></td>
                        <td align="left" colspan="1" style="width: 10%; height: 20px;" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 10%; height: 10px" valign="middle">
                        </td>
                        <td align="left" colspan="4" style="height: 10px" valign="middle">
                        </td>
                        <td align="left" colspan="1" style="width: 10%; height: 10px" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%; height: 20px;" valign="middle" align="right">
                            <asp:RadioButton ID="rdmes" runat="server" GroupName="tipo" /></td>
                        <td style="width: 20%; background-color: #d5ddbf; height: 20px;" valign="middle" align="left">
                            <asp:Label ID="Label2" runat="server" Text="Desde Mes:" SkinID="TitTabla"></asp:Label></td>
                        <td align="left" style="width: 20%; height: 20px;" valign="middle">
                            <asp:DropDownList ID="dbdesde" runat="server">
                                <asp:ListItem Value="01">Enero</asp:ListItem>
                                <asp:ListItem Value="02">Febrero</asp:ListItem>
                                <asp:ListItem Value="03">Marzo</asp:ListItem>
                                <asp:ListItem Value="04">Abril</asp:ListItem>
                                <asp:ListItem Value="05">Mayo</asp:ListItem>
                                <asp:ListItem Value="06">Junio</asp:ListItem>
                                <asp:ListItem Value="07">Julio</asp:ListItem>
                                <asp:ListItem Value="08">Agosto</asp:ListItem>
                                <asp:ListItem Value="09">Septiembre</asp:ListItem>
                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="DropDownList2" runat="server" Width="40px">
                                <asp:ListItem Value="2008">08</asp:ListItem>
                                <asp:ListItem Value="2009">09</asp:ListItem>
                                <asp:ListItem Value="2010">10</asp:ListItem>
                                <asp:ListItem Value="2011">11</asp:ListItem>
                                <asp:ListItem Value="2012">12</asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 20%; background-color: #d5ddbf; height: 20px;" valign="middle" align="left">
                            <asp:Label ID="Label3" runat="server" Text="Hasta el Mes:" SkinID="TitTabla"></asp:Label></td>
                        <td style="width: 20%; height: 20px;" valign="middle" align="left">
                            <asp:DropDownList ID="dbhasta" runat="server">
                                <asp:ListItem Value="01">Enero</asp:ListItem>
                                <asp:ListItem Value="02">Febrero</asp:ListItem>
                                <asp:ListItem Value="03">Marzo</asp:ListItem>
                                <asp:ListItem Value="04">Abril</asp:ListItem>
                                <asp:ListItem Value="05">Mayo</asp:ListItem>
                                <asp:ListItem Value="06">Junio</asp:ListItem>
                                <asp:ListItem Value="07">Julio</asp:ListItem>
                                <asp:ListItem Value="08">Agosto</asp:ListItem>
                                <asp:ListItem Value="09">Septiembre</asp:ListItem>
                                <asp:ListItem Value="10">Octubre</asp:ListItem>
                                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                <asp:ListItem Value="12">Diciembre</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" style="width: 10%; height: 20px;" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" style="width: 10%; height: 15px">
                        </td>
                        <td align="left" colspan="1" style="width: 10%; height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%; height: 20px;" valign="middle">
                        </td>
                        <td style="width: 20%; background-color: #d5ddbf; height: 20px;" valign="middle" align="left">
                            <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Italic="False" Text="Filtro rápido" SkinID="TitTabla"></asp:Label></td>
                        <td align="left" style="width: 20%; height: 20px;" valign="middle">
                        </td>
                        <td style="width: 20%; height: 20px;" valign="middle" align="left">
                        </td>
                        <td style="width: 20%; height: 20px;" valign="middle">
                        </td>
                        <td style="width: 10%; height: 20px;" valign="middle">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;" align="right">
                            &nbsp;<asp:RadioButton ID="rdultim" runat="server" Checked="True" GroupName="tipo" AutoPostBack="True" /></td>
                        <td align="left" colspan="4" style="width: 20%">
                            <asp:Label ID="Label6" runat="server" SkinID="TitTabla"></asp:Label></td>
                        <td align="left" colspan="1" style="width: 10%">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 10%;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 20px">
            </td>
            <td style="width: 20%; height: 20px">
            </td>
            <td colspan="2" style="height: 20px">
            </td>
            <td style="width: 20%; height: 20px">
            </td>
            <td style="width: 10%; height: 20px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 24px;">
            </td>
            <td style="width: 20%; height: 24px;">
                </td>
            <td colspan="2" style="height: 24px">
                &nbsp;<asp:Button ID="Button1" runat="server" Text="Consultar" ValidationGroup="VGFecha" /></td>
            <td style="width: 20%; height: 24px;">
            </td>
            <td style="width: 10%; height: 24px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 20px">
            </td>
            <td style="width: 20%; height: 20px">
            </td>
            <td colspan="2" style="height: 20px">
            </td>
            <td style="width: 20%; height: 20px">
            </td>
            <td style="width: 10%; height: 20px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%;">
            </td>
            <td style="width: 20%;">
            </td>
            <td colspan="2">
                <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px" Visible="False" HorizontalAlign="Center">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="2" style="height: 3px">
                                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Italic="True"
                                    Text="Label" SkinID="TitTabla"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 308px">
                                <br />
                    <chart:webchartviewer id="CDGrafFechas" runat="server" Height="289px" Width="482px"></chart:webchartviewer></td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" Visible="False" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; position: static;
                        height: 100%">
                        <tr>
                            <td align="center" style="height: 71px" valign="middle">
                                <img alt="" src="../../img/Nograficagif.gif" style="vertical-align: middle; text-align: center; width: 44px; height: 45px;" />
                                &nbsp;&nbsp;
                                <asp:Label ID="Label9" runat="server" SkinID="TitTabla" Text="No se encuentran datos para graficar"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td style="width: 20%;">
            </td>
            <td style="width: 10%;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 20%">
            </td>
            <td style="width: 20%">
                <cc1:validatorcalloutextender id="VceFecIni2" runat="server" highlightcssclass="validatorCalloutHighlight"
                    targetcontrolid="RevFecIni">
                        </cc1:validatorcalloutextender>
                <cc1:validatorcalloutextender id="VceFecFin2" runat="server" highlightcssclass="validatorCalloutHighlight"
                    targetcontrolid="RevFecFin">
                        </cc1:validatorcalloutextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" cssclass="MyCalendar"
                    format="dd/MM/yyyy" popupbuttonid="Button3" popupposition="BottomRight" targetcontrolid="TxFecFin">
                        </cc1:calendarextender>
                <asp:RegularExpressionValidator ID="RevFecFin" runat="server" ControlToValidate="TxFecFin"
                    Display="None" ErrorMessage="Digite una Fecha Final Válida" ForeColor="" ValidationExpression="^(0?[1-9]|1[0-9]|2|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(\d{4})$"
                    ValidationGroup="VGFecha"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                        ID="RfvFecFin" runat="server" ControlToValidate="TxFecFin" Display="None" ErrorMessage="Digite Una Fecha Final"
                        ForeColor="" ValidationGroup="VGFecha"></asp:RequiredFieldValidator><asp:CompareValidator
                            ID="cvFecIni" runat="server" ControlToCompare="TxFecIni" ControlToValidate="TxFecFin"
                            Display="None" ErrorMessage="La Fecha Final debe ser Posterior a la Fecha de Inicio"
                            ForeColor="" Operator="GreaterThanEqual" Type="Date" ValidationGroup="VGFecha"></asp:CompareValidator><cc1:validatorcalloutextender
                                id="VceRango" runat="server" highlightcssclass="validatorCalloutHighlight" targetcontrolid="cvFecIni">
                            </cc1:validatorcalloutextender></td>
            <td style="width: 20%">
                <cc1:validatorcalloutextender id="VceFecIni1" runat="server" highlightcssclass="validatorCalloutHighlight"
                    targetcontrolid="RfvFecIni">
                        </cc1:validatorcalloutextender>
                <cc1:calendarextender id="CalendarExtender1" runat="server" cssclass="MyCalendar"
                    format="dd/MM/yyyy" popupbuttonid="Button2" popupposition="BottomRight" targetcontrolid="TxFecIni">
                        </cc1:calendarextender>
                <cc1:validatorcalloutextender id="VceFecFin1" runat="server" highlightcssclass="validatorCalloutHighlight"
                    targetcontrolid="RfvFecFin">
                        </cc1:validatorcalloutextender>
                <asp:RequiredFieldValidator ID="RfvFecIni" runat="server" ControlToValidate="TxFecIni"
                    Display="None" ErrorMessage="Digite Una Fecha de Inicio" ForeColor="" ValidationGroup="VGFecha"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="RevFecIni" runat="server" ControlToValidate="TxFecIni" Display="None" ErrorMessage="Digite una Fecha de Inicio Válida"
                        ForeColor="" ValidationExpression="^(0?[1-9]|1[0-9]|2|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(\d{4})$"
                        ValidationGroup="VGFecha"></asp:RegularExpressionValidator></td>
            <td style="width: 20%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 20%">
            </td>
            <td style="width: 20%">
            </td>
            <td style="width: 20%">
            </td>
            <td style="width: 20%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
