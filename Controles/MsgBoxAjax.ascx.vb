
Partial Class Controles_MsgBoxAjax
    Inherits System.Web.UI.UserControl
    Protected Property Title() As String
        Get
            Return Me.LblTituloMsgBox.Text
        End Get
        Set(ByVal value As String)
            Me.LblTituloMsgBox.Text = value
        End Set
    End Property
    Protected Property Message() As String
        Get
            Return Me.LblMensajeMsgBox.Text
        End Get
        Set(ByVal value As String)
            Me.LblMensajeMsgBox.Text = value
        End Set
    End Property
    Public Sub Show(ByVal Mensaje As String, Optional ByVal Title As String = "")
        Me.Title = IIf(Title = "", "ERROR", Title.ToUpper)
        Me.Message = Mensaje
        Me.MpeMsgBox.Show()
    End Sub
End Class
