﻿Imports AjaxControlToolkit
Imports System.Data
Imports System.Data.OleDb
Partial Class nuevo_nombre
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            lbl_contador.Text = Session("contador")


            Me.prcCrearDataset()
            Me.prcCargarOrigen("reuniones") ' Combos solicitudes
            Me.PrcCargarDatos("reuniones")

            Me.prcCargarOrigen("Moderadores") ' cargar los sombos
            Me.PrcCargarDatos("Moderadores")

            Me.prcCargarOrigen("areass")
            Me.PrcCargarDatos("areass")


            Me.date2.Text = "01/01/2990"
            date1.Text = Format(Now(), "dd/MM/yyyy")

            Me.prcCargarOrigen("grid")
            Me.PrcCargarDatos("grid")

        End If
    End Sub



#Region "PROCEDIMIENTOS"
    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet

        Dim dtt_reunion As New DataTable   '--0
        Dim dtt_Moderador As New DataTable '--1
        Dim dtt_areas As New DataTable   '--2
        Dim dtt_areas_s As New DataTable   '--3
        Dim dtt_Nreunion As New DataTable   '--4

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_Moderador") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_areas")
        Me.ds_evalua.Tables.Add("dtt_areas_s")
        Me.ds_evalua.Tables.Add("dtt_Nreunion")

    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "reuniones"
                Me.evalu.Tp_Reunion()
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "Moderadores"
                Me.evalu.Tp_Moderador()
                Me.ds_evalua.Tables("dtt_Moderador").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "areas"
                Me.ds_evalua.Tables("dtt_areas").Clear()
                Me.evalu.Trae_area(areas.SelectedItem.Text)
                Me.ds_evalua.Tables("dtt_areas").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "areass"
                Me.evalu.Trae_areas()
                Me.ds_evalua.Tables("dtt_areas_s").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "grid"
                Me.evalu.name_Reunion()
                Me.ds_evalua.Tables("dtt_Nreunion").Merge(Me.evalu.DatasetFill.Tables(0))


        End Select
    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Me.tr.DataSource = Me.ds_evalua.Tables(0).CreateDataReader
                Me.tr.DataTextField = "NombreReunion"
                Me.tr.DataValueField = "id"
                Me.tr.DataBind()

            Case "Moderadores"
                Me.dd_moderador.DataSource = Me.ds_evalua.Tables(1).CreateDataReader
                Me.dd_moderador.DataTextField = "Nombreusuario"
                Me.dd_moderador.DataValueField = "Login"
                Me.dd_moderador.DataBind()

            Case "areas"
                Me.dr_areas.DataSource = Me.ds_evalua.Tables(2).CreateDataReader
                Me.dr_areas.DataTextField = "NombreDependencia"
                Me.dr_areas.DataValueField = "id"
                Me.dr_areas.DataBind()

            Case "areass"
                Me.dr_areas.DataSource = Me.ds_evalua.Tables(3).CreateDataReader
                Me.dr_areas.DataTextField = "NombreDependencia"
                Me.dr_areas.DataValueField = "id"
                Me.dr_areas.DataBind()

            Case "grid"
                Me.grid1.DataSource = Me.ds_evalua.Tables(4)
                Me.grid1.DataBind()


        End Select
    End Sub

#End Region


#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region


    Protected Sub areas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles areas.SelectedIndexChanged

        Me.prcCargarOrigen("areas")
        Me.PrcCargarDatos("areas")

    End Sub

    Protected Sub dr_inicio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim inicio As Date
        Dim fin As Date
        Dim horas_dif As Long
        Dim minutos_dif As Long
        Dim minutos_inicio As Long
        Dim minutos_fin As Long
        Dim minutos_total As Long

        inicio = Me.dr_inicio.Text
        fin = Me.dr_fin.Text

        'HORAS Y MINUTOS
        horas_dif = DateDiff(DateInterval.Hour, inicio, fin)
        minutos_dif = DateDiff(DateInterval.Minute, inicio, fin)

        'DIFERENCIA DE MINUTOS
        minutos_inicio = DatePart(DateInterval.Minute, inicio)
        minutos_fin = DatePart(DateInterval.Minute, fin)
        'SUMA TOTAL DE MINUTOS
        minutos_total = minutos_fin - minutos_inicio

        'SI LAS HORAS O LOS MINUTOS SON MENORES QUIERE DECIR QUE ESCOGIO EL FIN MENOR QUE EL INICIO
        If horas_dif >= 0 And minutos_dif >= 0 Then

            Me.txt_duracion.Text = horas_dif & " Horas" & " " & minutos_total & " Minutos"
        Else
            Me.txt_duracion.Text = ""
            Me.MsgBox1.ShowMessage("Hay diferencias En las Horas")
        End If
    End Sub

    Protected Sub dr_fin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim inicio As Date
        Dim fin As Date
        Dim horas_dif As Long
        Dim minutos_dif As Long
        Dim minutos_inicio As Long
        Dim minutos_fin As Long
        Dim minutos_total As Long

        inicio = Me.dr_inicio.Text
        fin = Me.dr_fin.Text

        'HORAS Y MINUTOS
        horas_dif = DateDiff(DateInterval.Hour, inicio, fin)
        minutos_dif = DateDiff(DateInterval.Minute, inicio, fin)

        'DIFERENCIA DE MINUTOS
        minutos_inicio = DatePart(DateInterval.Minute, inicio)
        minutos_fin = DatePart(DateInterval.Minute, fin)
        'SUMA TOTAL DE MINUTOS
        minutos_total = minutos_fin - minutos_inicio

        'SI LAS HORAS O LOS MINUTOS SON MENORES QUIERE DECIR QUE ESCOGIO EL FIN MENOR QUE EL INICIO
        If horas_dif >= 0 And minutos_dif >= 0 Then

            Me.txt_duracion.Text = horas_dif & " Horas" & " " & minutos_total & " Minutos"
        Else
            Me.txt_duracion.Text = ""
            Me.MsgBox1.ShowMessage("Hay diferencias En las Horas")
        End If
    End Sub

   
    Protected Sub rb_finaliza_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb_finaliza.SelectedIndexChanged
        Select Case rb_finaliza.SelectedValue
            Case "si"
                Me.date2.Enabled = True
                date2.Text = Format(Now(), "MM/dd/yyyy")
            Case "no"
                Me.date2.Text = "01/01/2990"
                Me.date2.Enabled = False
        End Select
    End Sub

    Protected Sub b_guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles b_guardar.Click

        Dim nombre As String = Me.txt_nombre.Text
        Dim fec_inicio As String = Me.date1.Text
        Dim fec_fin As String = Me.date2.Text
        Dim areas As String = Me.dr_areas.SelectedItem.Value
        Dim tipo_reunion As String = Me.tr.SelectedItem.Value
        Dim hora_inicio As String = Me.dr_inicio.SelectedValue
        Dim hora_fin As String = Me.dr_fin.SelectedValue
        Dim tiempo_duracion As String = Me.txt_duracion.Text
        Dim frc_reunion As String = Me.frecuencia.SelectedValue
        Dim moderador As String = Me.dd_moderador.SelectedValue
        Dim var As String
        Dim Comentario As String
        Dim var_i As Integer

        If Me.valida.Checked = True Then
            var = 1
        Else
            var = 0
        End If


        ' REVISA SI EL NOMBRE YA EXISTE
        If Me.evalu.nombre_rnion(Me.txt_nombre.Text) = True Then
            'Me.txt_nombre.Focus()
            Me.txt_nombre.Text = ""
            Me.MsgBox1.ShowMessage("Este Nombre de Reunion Ya existe" & evalu.ERRORR)
        Else

            ' evalu.Source_NewReunion(nombre, fec_inicio, fec_fin, areas, tipo_reunion, hora_inicio, hora_fin, tiempo_duracion, frc_reunion, var)
            evalu.Source_NewReunion(nombre, fec_inicio, fec_fin, areas, moderador, tipo_reunion, hora_inicio, hora_fin, tiempo_duracion, frc_reunion, var)
            If evalu.FlagError = True Then
                Me.MsgBox1.ShowMessage("ERROR" & evalu.ERRORR)
            Else
                Me.MsgBox1.ShowMessage("REUNION SE HA GUARDADO...................")
            End If


        End If

        'AQUI GUARDA LA FRECUENCIA
        var_i = Me.frecuencia.SelectedValue


        If Me.frecuencia.SelectedValue = "1" Then
            Comentario = "Esta Frecuencia es Programada Diaria Por Turnos"
            Me.evalu.Finder_Reunion(var_i, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

        End If

        If Me.frecuencia.SelectedValue = "2" Then
            Comentario = "Esta Frecuencia es Programada Diaria"
            Me.evalu.Finder_Reunion(var_i, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

        End If

        If Me.frecuencia.SelectedValue = "3" Then
            Dim d_sem2 As String = Me.txt_sem.Text
            Dim d_semanal As String = Me.opt_semanal.SelectedValue
            Comentario = "Repetir cada " & d_sem2 & " semanas el " & d_semanal
            Me.evalu.Finder_Reunion(var_i, Me.txt_nombre.Text, d_sem2, d_semanal, "", "", "", Comentario)

        End If

        If Me.frecuencia.SelectedValue = "4" Then

            If Me.opt_mensual.SelectedValue = "1" Then
                Dim v_men As String = Me.txt_ms1.Text
                Dim v_men1 As String = Me.txt_Mensual.Text
                Comentario = "El dia " & v_men & " de cada " & v_men1 & " meses"
                Me.evalu.Finder_Reunion(41, Me.txt_nombre.Text, v_men, v_men1, "", "", "", Comentario)
            End If

            If Me.opt_mensual.SelectedValue = "2" Then
                Dim v_men2 As String = Me.dr_ms1.SelectedValue
                Dim v_men3 As String = Me.dr_ms2.SelectedValue
                Dim v_men4 As String = Me.txt_ms3.Text
                Comentario = "El " & v_men2 & " " & v_men3 & " de cada " & v_men4 & " meses"
                Me.evalu.Finder_Reunion(42, Me.txt_nombre.Text, "", "", v_men2, v_men3, v_men4, Comentario)
            End If

        End If

        If Me.frecuencia.SelectedValue = "5" Then

            If Me.opt_an.SelectedValue = "1" Then
                Dim v_an1 As String = Me.txt_anual.Text
                Dim v_an2 As String = Me.dr_anual.SelectedValue
                Comentario = "Cada " & v_an1 & " de " & v_an2
                Me.evalu.Finder_Reunion(41, Me.txt_nombre.Text, v_an1, v_an2, "", "", "", Comentario)
            End If

            If Me.opt_an.SelectedValue = "2" Then
                Dim v_an3 As String = Me.dr_an1.SelectedValue
                Dim v_an4 As String = Me.dr_an2.SelectedValue
                Dim v_an5 As String = Me.dr_an3.SelectedValue
                Comentario = "El " & v_an3 & " " & v_an4 & " de " & v_an5
                Me.evalu.Finder_Reunion(42, Me.txt_nombre.Text, "", "", v_an3, v_an4, v_an5, Comentario)

            End If

        End If



    End Sub

    Protected Sub frecuencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case frecuencia.SelectedValue
            Case "1"
                Me.valida.Checked = False
            Case "2"
                Me.valida.Checked = False
            Case "3"
                Me.MultiView1.ActiveViewIndex = 0
                Me.valida.Checked = True
            Case "4"
                Me.MultiView1.ActiveViewIndex = 1
                Me.valida.Checked = True
            Case "5"
                Me.MultiView1.ActiveViewIndex = 2
                Me.valida.Checked = True
             
        End Select
    End Sub

    Protected Sub rnion_exis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rnion_exis.Click
        Me.prcCargarOrigen("grid")
        Me.PrcCargarDatos("grid")
    End Sub

    Protected Sub grid1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grid1.PageIndexChanging
        Me.grid1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("grid")
    End Sub


End Class
