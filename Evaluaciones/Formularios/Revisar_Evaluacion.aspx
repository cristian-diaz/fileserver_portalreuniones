<%@ Page Language="VB" MasterPageFile="~/Evaluaciones/Plantilla/P_Plantilla_Aux.master" AutoEventWireup="false" CodeFile="Revisar_Evaluacion.aspx.vb" Inherits="Evaluaciones_Formularios_Revisar_Evaluacion" title="Revisar Evaluaciones" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CntCuerpo" Runat="Server">
    <div style="text-align: center">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG);
                    color: white; font-family: Verdana,Arial,Helvetica,sans-serif; height: 18px">
                    Evaluaci�n</td>
            </tr>
        </table>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td style="width: 10%; height: 20px;">
                </td>
            <td align="left" style="width: 20%; height: 20px;">
               </td>
            <td align="left" style="width: 20%; height: 20px;">
                </td>
            <td align="left" style="width: 20%; height: 20px;">
            </td>
            <td align="left" style="width: 20%; height: 20px;">
            </td>
            <td style="width: 10%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 22px;">
            </td>
            <td align="left" style="width: 20%; background-color: #d5ddbf; height: 22px;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Reunion:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" colspan="3" style="height: 22px">
                <asp:Label ID="Label7" runat="server" Text="Label" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" ForeColor="DarkGreen"></asp:Label></td>
            <td style="width: 10%; height: 22px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;"></td>
            <td align="left" style="width: 20%; height: 10px;">
                </td>
            <td align="left" style="width: 20%; height: 10px;"></td>
            <td align="left" style="width: 20%; height: 10px;"></td>
            <td style="width: 10%; height: 10px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td style="width: 10%; height: 10px;">
            </td>
        </tr>
    </table>
    <asp:Panel ID="PnlReuniones" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
            <tr>
                <td style="height: 10px" colspan="6">
                    <asp:GridView ID="GridView3" runat="server" DataKeyNames="No_eval"
                    Width="100%" AutoGenerateColumns="False" AllowPaging="True" SkinID="GvwEvaluacion">
                        <EmptyDataTemplate>
                            <asp:Label ID="lblitem" runat="server" Text='<%# bind("item") %>'></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle BackColor="DarkOliveGreen" ForeColor="White" />
                        <AlternatingRowStyle BackColor="WhiteSmoke" />
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Select"
                                        ImageUrl="~/Evaluaciones/Img/Img_Select.png" Text="Select" ToolTip="Ver Evaluaci�n" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="No_Eval" HeaderText="N&#176;"  Visible="false" />
                            <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="Fecha" HtmlEncode="False" />
                            <asp:BoundField DataField="nombreusuario" HeaderText="Usuario">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemStyle Width="200px"/>                                
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="login" HeaderText="Registro" />
                            
                            <asp:BoundField DataField="porcentaje" HeaderText="Evaluacion" />
                            
                            <asp:BoundField DataField="Comentario" HeaderText="Comentario">
                              <ItemStyle Width="300px"/>
                           </asp:BoundField>
                           
                        </Columns>
                    </asp:GridView>
                <asp:Panel ID="PnlInfo" runat="server" Visible="False" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; position: static;
                        height: 100%">
                        <tr>
                            <td align="center" style="height: 71px" valign="middle">
                                &nbsp;<img alt="" src="../../img/Noreunion.gif" style="vertical-align: middle; text-align: center" width="65" />
                                &nbsp;&nbsp;
                                <asp:Label ID="Label9" runat="server" SkinID="TitTabla" Text="No se ha registrado Evaluaci�n para esta Reuni�n"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                    <asp:Label ID="LblNota" runat="server" SkinID="Nota" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 10%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td style="width: 10%; height: 10px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PnlResultados" runat="server" Visible="False" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
            <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="4" style="width: 20%; background-color: #d5ddbf;">
                <asp:Label ID="LblResultados" runat="server" Font-Bold="True" Font-Italic="True"
                    Font-Size="Small" ForeColor="#004000" Text="Evaluaci�n" SkinID="TitTabla"></asp:Label>
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
                <td style="width: 10%; height: 10px">
                </td>
                <td align="center" colspan="4" style="width: 20%; height: 10px">
                </td>
                <td style="width: 10%; height: 10px">
                </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px; background-color: #d5ddbf;">
                                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                                    ForeColor="#004000" Text="Lugar de Reuni�n:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" style="height: 10px;" colspan="3">
                                <asp:TextBox ID="txtlugar" runat="server" Width="98%" ReadOnly="True"></asp:TextBox></td>
            <td style="width: 10%; height: 10px;">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td style="width: 10%; height: 9px;">
            </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="4">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id_item"
                    Width="100%" SkinID="GvwEvaluacion">
                    <Columns>
                        <asp:BoundField DataField="orden" HeaderText="N&#176;" />
                        <asp:TemplateField HeaderText="No" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# bind("Id_item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item a Evaluar">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblitem" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ayuda" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblmensaje" runat="server" Text='<%# bind("comentario") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Peso">
                            <ItemTemplate>
                                <asp:Label ID="lblpeso" runat="server" Text='<%# bind("Peso") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Calificacion">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# bind("calificacion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total">
                            <ItemTemplate>
                                <asp:Label ID="lbltotal" runat="server" Font-Bold="True" Text='<%# bind("valor") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblitem" runat="server" Text='<%# bind("item") %>'></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle BackColor="DarkOliveGreen" ForeColor="White" />
                    <AlternatingRowStyle BackColor="WhiteSmoke" />
                </asp:GridView>
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td style="width: 10%; height: 10px;">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 20px;">
            </td>
                <td align="left" colspan="2" style="height: 20px; background-color: #d5ddbf">
                <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="#004000"
                    Text="Resultados Obtenidos" Width="151px" SkinID="TitTabla"></asp:Label></td>
                <td align="left" colspan="2" style="height: 20px; background-color: #d5ddbf">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#004000"
                    Text="Comentarios:" SkinID="TitTabla"></asp:Label></td>
            <td style="width: 10%; height: 20px;">
            </td>
            </tr>
            <tr>
                <td style="width: 10%; height: 10px">
                </td>
                <td align="left" colspan="2" style="height: 10px">
                </td>
                <td align="left" colspan="2" style="height: 10px">
                </td>
                <td style="width: 10%; height: 10px">
                </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="2" rowspan="3">
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="260px" SkinID="GvwEvaluacion">
                    <Columns>
                        <asp:TemplateField HeaderText="Item">
                            <ItemStyle BackColor="White" />
                            <ItemTemplate>
                                <asp:Label ID="lblitemr" runat="server" Text='<%# bind("itemr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label ID="lblvalorr" runat="server" Text='<%# bind("valorr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Green" ForeColor="White" />
                </asp:GridView>
            </td>
            <td align="center" colspan="2" rowspan="3">
                <asp:TextBox ID="TextBox1" runat="server" Height="114px" MaxLength="255" Width="335px" ReadOnly="True" TextMode="MultiLine"></asp:TextBox></td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td style="width: 10%; height: 9px;">
            </td>
            </tr>
        </table>
    </asp:Panel>
        <cc2:msgbox id="MsgBox1" runat="server"></cc2:msgbox>
</asp:Content>

