<%@ Language="VBScript" %>
<%    
   Response.buffer = true
   Response.ContentType = "application/vnd.ms-excel"
   Response.AddHeader "Content-Disposition", "filename=Reporte_Reuniones.xls;" 
   Server.ScriptTimeout = 1200    
%>

<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim Sql
Set Conexion = AbrirConexionBD(cadenaConexionDBQ)

Dim horaInicio, horaFin, fecha1, fecha2
 
 'horaInicio	= "08:00:00 AM"
 'horaFin = "09:00:00 AM"
 
 horaInicio = request("horaInicio")&":"&request("minInicio")&" "&request("ampm1")
 horaFin = request("horaFin")&":"&request("minFin")&" "&request("ampm2")
 
 fecha1 = request("anioI")&"/"&request("mesI")&"/"&request("diaI")
 fecha2 = request("anioF")&"/"&request("mesF")&"/"&request("diaF")
 
 Sql = " SELECT DISTINCT t.id, t.nombre, t.tip_asoc, t.horainicio as horaInicioPlaneada, t.horafin as horaFinPlaneada "
 Sql =  Sql & " ,re.no_eval, re.login, re.lugar, re.fecha, re.totalpuntos, re.lugar, re.comentario "
 Sql =  Sql & " ,rc.calificacion, a.horainicio as horaInicioReal, a.horafin as horaFinReal "
 'if request("nombreUsuario") <> "" and request("pr10") <> "" then 
 Sql =  Sql & " ,tl.nombreusuario "
 'end if
 'if request("registro") <> "" and request("pr11") <> "" then 
 Sql =  Sql & " ,tl.login "
 'end if
 Sql =  Sql & " ,rc.calificacion, i.item   " 
 Sql =  Sql & " FROM tiporeunion t, resultados_eval re, asistentesreunion a, resultados_calif rc, item_evaluacion i, t_logins tl, permisos p "
 Sql =  Sql & " WHERE t.tipo_nm = 'REUNION' "
 
 if request("tipoReunion") <> "" and request("pr1") <> "" then
 	Sql =  Sql & " AND t.tip_asoc = '"&request("tipoReunion")&"' "
 end if
 if request("nombreReunion") <> "" and request("pr2") <> "" then 
 Sql =  Sql & " AND ((t.nombre) LIKE '%"&request("nombreReunion")&"%') "
 end if
 Sql =  Sql & " AND re.no_eval = a.no_eval "
 Sql =  Sql & " AND rc.no_eval = a.no_eval "
 Sql =  Sql & " AND rc.id_item = i.id_item "
 Sql =  Sql & " AND p.id_reunion = t.id "
 Sql =  Sql & " AND p.login = tl.login "
 
 if fecha1 <> "" and request("pr3") <> "" then 
 Sql =  Sql & " AND re.fecha >= #"&fecha1&"# " 
 end if
 
 if fecha2 <> "" and request("pr4") <> "" then 
 Sql =  Sql & " AND re.fecha <= #"&fecha2&"# " 
 end if
 
 if horaInicio <> "" and request("pr5") <> "" then
 Sql =  Sql & " AND t.horainicio = #"&horaInicio&"# " 
 end if
 
 if horaFin <> "" and request("pr6") <> "" then
 Sql =  Sql & " AND t.horafin = #"&horaFin&"# " 
 end if
 
 if request("itemEvaluacion") <> "" and request("pr7") <> "" then
 Sql =  Sql & " AND i.id_item = "&request("itemEvaluacion")&" "
 end if
 
 if request("puntaje") <> "" and request("pr8") <> "" then 
 Sql =  Sql & " AND re.totalpuntos >= "&request("puntaje")&" "
 end if
 
 if request("permiso") <> "" and request("pr9") <> "" then 
 Sql =  Sql & " AND p.permiso = '"&request("permiso")&"' "
 end if
 
 if request("nombreUsuario") <> "" and request("pr10") <> "" then 
 Sql =  Sql & " AND ((tl.nombreusuario) like '%"&request("nombreUsuario")&"%') "
 end if
 
 if request("registro") <> "" and request("pr11") <> "" then 
 Sql =  Sql & " AND tl.login = '"&request("registro")&"' "   
 end if

on error resume next
Set ADO_RecordSet = AbrirRecordSet(Conexion, Sql)
'response.Write "Consulta = " & Sql
if err.description <> "" then
	response.Write(err.description)
	response.Write "Consulta = " & Sql

end if


%>
<TABLE width="100%" border=1 cellPadding=0 cellSpacing=0>
	<colgroup>
		<col width="14%">
		<col width="5%">
		<col width="9%">
		<col width="5%">
		<col width="5%">
		<col width="5%">
		<col width="13%">
		<col width="7%">
		<col width="5%">
		<col width="5%">
		<col width="5%">
		<col width="5%">
		<col width="6%">
		<col width="16%">
	</colgroup>
	  <TR>
	    <TH colspan="14"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">LISTADO DE REUNIONES </font></TH>
  	  </TR>
	  <TR> 
		<TH width="14%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Reuni�n</font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tipo de Reuni&oacute;n</font></TH>
		<TH width="9%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Item Evaluado </font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Valor Item</font> </TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Puntaje Evaluaci&oacute;n </font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Registro</font></TH>
		<TH width="13%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Usuario</font></TH>
		<TH width="7%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Lugar</font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora Inicio Planeada </font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora Fin Planeada </font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora Inicio Real </font></TH>
		<TH width="5%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Hora Fin Real </font></TH>
		<TH width="6%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha Evaluaci&oacute;n</font></TH>
		<TH width="16%"><p><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Comentario</font></TH>
	  </TR>
	<%	
		if not ADO_RecordSet.EOF	then
		while not ADO_RecordSet.EOF	
						
		Response.Write("<TR><TD width='14%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("nombre") & "</font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("tip_asoc") & "</font></TD>")
		Response.Write("<TD width='9%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("item") & "</font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> " & ADO_RecordSet("calificacion") & " </font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> " & ADO_RecordSet("totalpuntos") & " </font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("login") & "</font></TD>")
		Response.Write("<TD width='13%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("nombreusuario") & "</font></TD>")
		Response.Write("<TD width='7%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("lugar") & "</font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaInicioPlaneada") & "</font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaFinPlaneada") & "</font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaInicioReal") & "</font></TD>")
		Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaFinReal") & "</font></TD>")
		Response.Write("<TD width='6%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("fecha") & "</font></TD>")
		Response.Write("<TD width='16%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("comentario") & "</font></TD></tr>")
		
			ADO_RecordSet.MoveNext
			wend

		end if	
			ADO_RecordSet.Close
			Set ADO_RecordSet = Nothing
									
	%>
	</TABLE>
