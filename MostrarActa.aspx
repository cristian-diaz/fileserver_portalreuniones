﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MostrarActa.aspx.vb" Inherits="MostrarActa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Página sin título</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="actapdf" runat="server" style="text-align: center">
        <br />
        <div id="prueba" runat="server">
            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="799">

  <tr>
   <td style="height: 20px"><img src="img/spacer.gif" width="134" height="1" border="0" alt="" /></td>
   <td style="height: 20px"><img src="img/spacer.gif" width="133" height="1" border="0" alt="" /></td>
   <td style="height: 20px"><img src="img/spacer.gif" width="133" height="1" border="0" alt="" /></td>
   <td style="height: 20px"><img src="img/spacer.gif" width="50" height="1" border="0" alt="" /></td>
   <td style="height: 20px"><img src="img/spacer.gif" width="173" height="1" border="0" alt="" /></td>
   <td style="height: 20px"><img src="img/spacer.gif" width="176" height="1" border="0" alt="" /></td>
   <td style="height: 20px"><img src="img/spacer.gif" width="1" height="1" border="0" alt="" /></td>
  </tr>

  <tr>
   <td rowspan="4" colspan="2">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/img/logoEcopetrol.gif" /></td>
   <td colspan="4" style="height: 43px">
                    <asp:Label ID="Label15" runat="server" Font-Bold="True" Text="ACTA DE REUNION"></asp:Label></td>
   <td style="height: 43px"><img src="img/spacer.gif" width="1" height="24" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="4">
                    <asp:Label ID="lblgestion_info" runat="server" Font-Bold="True"></asp:Label><br />
                    <asp:Label ID="lbldirtecinfo" runat="server" Font-Bold="True"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="43" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="2">
                                <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Size="Small" Text="CODIGO"></asp:Label><br />
                                <asp:Label ID="lblcodigo_acta" runat="server" Font-Bold="True"></asp:Label></td>
   <td>
                                <asp:Label ID="Label31" runat="server" Font-Bold="True" Font-Size="Small" Text="ELABORADO"></asp:Label><br />
       <asp:Label ID="lblfecha_acta" runat="server" Font-Bold="True"></asp:Label></td>
   <td>
                                <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Size="Small" Text="VERSION"></asp:Label><br />
                                <asp:Label ID="lblvesion" runat="server" Font-Bold="True"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="43" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="4"></td>
   <td><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6"></td>
   <td><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td style="height: 29px; text-align: left">
                    <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Acta No:"></asp:Label></td>
   <td style="height: 29px; text-align: left">
                    <asp:Label ID="lblActaNo" runat="server"></asp:Label></td>
   <td style="height: 29px; text-align: left">
                    <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Tema:"></asp:Label></td>
   <td colspan="3" style="height: 29px; text-align: left">
                    <asp:Label ID="lblTema" runat="server"></asp:Label></td>
   <td style="height: 29px"><img src="img/spacer.gif" width="1" height="29" border="0" alt="" /></td>
  </tr>
  <tr>
   <td style="text-align: left">
                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Fecha:"></asp:Label></td>
   <td style="text-align: left">
                    <asp:Label ID="lblFechaEval" runat="server"></asp:Label></td>
   <td style="text-align: left">
                    <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Ubicacion:"></asp:Label></td>
   <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblUbicacion" runat="server"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="28" border="0" alt="" /></td>
  </tr>
  <tr>
   <td style="text-align: left">
                    <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="Hora Inicio:"></asp:Label></td>
   <td style="text-align: left">
                    <asp:Label ID="lblHoraInicio" runat="server"></asp:Label></td>
   <td style="text-align: left">
                    <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Hora Fin:"></asp:Label></td>
   <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblHoraFin" runat="server"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="27" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="text-align: left">
                    <asp:Label ID="Label16" runat="server" Font-Bold="True" Text="1. ANTES DE LA REUNION"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td style="height: 24px; text-align: left;">
                    <asp:Label ID="Label26" runat="server" Font-Bold="True">Objetivo</asp:Label></td>
   <td colspan="5" style="height: 24px; text-align: left;">
                    <asp:Label ID="lblObjetivo" runat="server" Font-Bold="True">N/A</asp:Label></td>
   <td style="height: 24px"><img src="img/spacer.gif" width="1" height="24" border="0" alt="" /></td>
  </tr>
  <tr>
   <td style="text-align: left; height: 190px;">
                    <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Agenda"></asp:Label></td>
   <td colspan="5" style="text-align: justify; height: 190px;">
                    <br />
       &nbsp;<br />
       <asp:Literal ID="Literal1" runat="server"></asp:Literal></td>
   <td style="height: 190px"><img src="img/spacer.gif" width="1" height="24" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="text-align: left">
                    <asp:Label ID="Label33" runat="server" Font-Bold="True" Text="Participantes"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="21" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="height: 22px">
                    </td>
   <td style="height: 22px"><img src="img/spacer.gif" width="1" height="22" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6">
                    <asp:GridView ID="grdparticipantes" runat="server" AutoGenerateColumns="False" Font-Overline="False"
                        HorizontalAlign="Left" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="width: 100px; height: 21px">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Nombre") %>' Width="380px"></asp:Label></td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Registro">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Login") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#66CC33" />
                    </asp:GridView>
                </td>
   <td><img src="img/spacer.gif" width="1" height="173" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="background-color: green" align="center">
                    <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="2. DESARROLLO DE LA REUNION" ForeColor="White"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="25" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="height: 22px">
                    <asp:Label ID="lbldesarrollo" runat="server"></asp:Label><br />
       <br />
       <asp:GridView ID="grvdesarollo" runat="server" AutoGenerateColumns="False" Width="784px">
           <Columns>
               <asp:TemplateField HeaderText="Puntos tratados en la reuni&#243;n:">
                   <ItemTemplate>
                       <asp:Label ID="Label34" runat="server" Text='<%# Eval("Punto") %>'></asp:Label>
                   </ItemTemplate>
                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
               </asp:TemplateField>
           </Columns>
           <HeaderStyle BackColor="#66CC33" />
       </asp:GridView>
   </td>
   <td style="height: 22px"><img src="img/spacer.gif" width="1" height="22" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="background-color: green" align="center">
                    <asp:Label ID="Label19" runat="server" Font-Bold="True" Text="3. EVALUACION DE LA REUNION" ForeColor="White"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="25" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" align="left"><asp:Label ID="Label20" runat="server" Text="Logramos alcanzar nuestra meta en esta reunion"></asp:Label><asp:Label ID="Label21" runat="server" Text="Si:"></asp:Label>
       <asp:Label
                        ID="lblsi" runat="server" Font-Bold="True"></asp:Label>
       <asp:Label ID="Label22" runat="server" Text="No:"></asp:Label>
       <asp:Label ID="lblno" runat="server" Font-Bold="True"></asp:Label></td>
   <td align="left"><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="background-color: green" align="center">
       <br />
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Text="4. COMPROMISOS" ForeColor="White"></asp:Label></td>
   <td style="background-color: gainsboro" align="center"><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6">
                    <asp:GridView ID="grdcompromisos" runat="server" AutoGenerateColumns="False" Width="100%">
                        <HeaderStyle BackColor="#66CC33" />
                        <Columns>
                            <asp:TemplateField HeaderText="Accion">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Accion") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Compromiso">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Responsable") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Fecha","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
   <td><img src="img/spacer.gif" width="1" height="134" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6"></td>
   <td><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="3" style="text-align: left">
                                <asp:Label ID="Label28" runat="server" Font-Bold="True" Text="Revisó"></asp:Label></td>
   <td colspan="3" style="text-align: left">
                                <asp:Label ID="Label29" runat="server" Font-Bold="True" Text="Aprobó"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="25" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="3">
                                <asp:Label ID="lblReviso" runat="server" Font-Overline="False"></asp:Label></td>
   <td colspan="3">
                                <asp:Label ID="lblAprobo" runat="server"></asp:Label></td>
   <td><img src="img/spacer.gif" width="1" height="23" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="6" style="height: 25px"></td>
   <td style="height: 25px"><img src="img/spacer.gif" width="1" height="25" border="0" alt="" /></td>
  </tr>
</table></div>
    </div>
        <asp:HiddenField ID="Hflid_acta" runat="server" />
        <asp:HiddenField ID="Hflaprobo" runat="server" />
        <asp:Button ID="Button1" runat="server" Text="Button" Visible="False" /><br />
    </form>
</body>
</html>
