<%@ Language="VBScript" %>
<%    
   Response.buffer = true
   Response.ContentType = "application/vnd.ms-excel"
   Response.AddHeader "Content-Disposition", "filename=ComproAsig.xls;"   
   Server.ScriptTimeout = 1200    
%>

<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim objRsTemp, strSQL
Dim objRsUsuarios, objRsReuniones, objRsCompromisos
Set Conexion = AbrirConexionBD(cadenaConexionDBQ)

	strSQL = "SELECT * FROM t_logins WHERE login='" & session("IdUser") & "' AND 1 = 1"
	Set objRsTemp=AbrirRecordset(Conexion,strSQL)
'response.write(strSQL)
'response.end
	If objRsTemp.EOF Then
		objRsTemp.Close 
		Set Conexion = Nothing
		Response.redirect("Presentar_Compromisos.asp")
	End If
	
	Session("Administrador")="SI"

Str_SQL=Request("strSQL")
Set ADO_RecordSet = AbrirRecordSet(Conexion, Str_SQL)
If IsNumeric(ADO_RecordSet) Then
	Response.Write("Error al Ejecutar la consulta, por favor comuniquese con Soporte </p>")
	Response.Write(Str_SQL)
	Response.End()
End If

%>
<TABLE width="100%" border=1 cellPadding=0 cellSpacing=0>
	<colgroup>
		<col width="2%">
		<col width="3%">
		<col width="9%">
		<col width="14%">
		<col width="5%">
		<col width="16%">
		<col width="8%">
		<col width="8%">
		<col width="5%">
		<col width="17%">
		<col width="6%">
		<col width="7%">
	</colgroup>
	  <TR>
	    <TH colspan="12"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">LISTADO DE COMPROMISOS</font></TH>
  </TR>
	  <TR>
	    <TH colspan="12"><div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Reuni&oacute;n: <%=UCase(ADO_RecordSet("TIPO"))%></font></div></TH>
  </TR>
	  <TR> 
		<TH width="2%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Nro</font></TH>
		<TH width="3%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tema</font></TH>
		<TH width="9%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Acci�n 
		  a Realizar</font></TH>
		<TH width="14%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Resultado</font></TH>
		<TH width="16%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Responsable</font></TH>
		<TH width="8%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha 
		  Origen</font></TH>
		<TH width="8%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha 
		  Compromiso</font></TH>
		<TH width="8%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha 
		  Real de Cierre</font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Status</font></TH>
		<TH width="17%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tipo 
		  Reuni�n</font></TH>
		<TH width="6%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Priorizacion</font></TH>
		<TH width="7%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Categor&iacute;a</font></TH>
	  </TR>
	<%
									Do Until ADO_RecordSet.EOF										
									  Response.Write("<TR><TD width='2%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("NoId") & "</font></TD>")
									 Response.Write("<TD width='3%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Tema") & "</font></TD>")
									 Response.Write("<TD width='9%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Accion") & "</font></TD>")
									 Response.Write("<TD width='14%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Solucion") & "&nbsp;</font></TD>")
									 Response.Write("<TD width='16%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Responsable"))
									 Response.Write("</font></TD>")
									 Response.Write("<TD align='center' width='8%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & day(ADO_RecordSet("FechaI"))&"/"&month(ADO_RecordSet("FechaI"))&"/"&year(ADO_RecordSet("FechaI"))& "</font></TD>")
									 Response.Write("<TD align='center' width='8%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & day(ADO_RecordSet("FechaC"))&"/"&month(ADO_RecordSet("FechaC"))&"/"&year(ADO_RecordSet("FechaC")) & "</font></TD>")
									 Response.Write("<TD align='center' width='8%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & day(ADO_RecordSet("FechaRealCierre"))&"/"&month(ADO_RecordSet("FechaRealCierre"))&"/"&year(ADO_RecordSet("FechaRealCierre")) & "</font></TD>")
									 Select Case ADO_RecordSet("Status")
									  Case "SIN COLOR"
											Response.Write("<TD width='5%'>&nbsp;</TD>")
											SinColor = SinColor + 1
									  Case "ROJO"
											If (ADO_RecordSet("VERIFICADO")) Then
											 Response.Write("<TD width='5%' bgcolor=Red><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>Rojo<br>Verificado</font></TD>")
											 Rojoveri = Rojoveri + 1
											Else
											 Response.Write("<TD width='5%' bgcolor=Red><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>Rojo</font></TD>")
											 Rojonove = Rojonove + 1
											End If
									  Case "SIN COLOR-ROJO"
											If (ADO_RecordSet("VERIFICADO")) Then
											 Response.Write("<TD width='5%' bgcolor=Red><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>Rojo<br>Verificado</font></TD>")
											 SinColorRojoveri = SinColorRojoveri + 1
											Else
											 Response.Write("<TD width='5%' bgcolor=Red><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>Rojo</font></TD>")
											 SinColorRojonove = SinColorRojonove + 1
											End If
									  Case "VERDE"
									        '15-Marzo-2006
											'Leyder Correa Romero
											'Modificaci�n del Formato de Despliegue de las Fechas de Cumplimiento
											'de los Compromisos
											If (ADO_RecordSet("VERIFICADO")) Then
											 Response.Write("<TD width='5%' bgcolor=Green><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>Verde<br>Verificado<br>" & day(ADO_RecordSet("FECHAREALCIERRE"))&"/"&month(ADO_RecordSet("FECHAREALCIERRE"))&"/"&year(ADO_RecordSet("FECHAREALCIERRE")) & "</font></TD>")
											 Verdeveri = Verdeveri + 1
											Else
											 Response.Write("<TD width='5%' bgcolor=Green><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>Verde<br>"& day(ADO_RecordSet("FECHAREALCIERRE"))&"/"&month(ADO_RecordSet("FECHAREALCIERRE"))&"/"&year(ADO_RecordSet("FECHAREALCIERRE")) & "</font></TD>")
											 Verdenove = Verdenove + 1
											End If
											
											If ADO_RecordSet("FechaRealCierre") > ADO_RecordSet("FECHAC") Then
												VerdeNoCumplida = VerdeNoCumplida + 1
											Else
												VerdeCumplida = VerdeCumplida + 1
											End If
									  Case Else
									 End Select
											 Response.Write("<TD width='17%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Tipo") & "</font></TD>")
											 Response.Write("<TD width='6%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Priorizacion") & "&nbsp; </font></TD>")
											 Response.Write("<TD width='7%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("Categoria") & " &nbsp;</font></TD></TR>")
									 ADO_RecordSet.MoveNext
									Loop
									ADO_RecordSet.Close
									Set ADO_RecordSet = Nothing
									Session("Rn") = Rojonove
									Session("An") = Amanove
									Session("Sc") = SinColor
									Session("ScRoVe") = SinColorRojoveri
									Session("VC") = VerdeCumplida
									Session("VNoC") = VerdeNoCumplida
	%>
	</TABLE>
