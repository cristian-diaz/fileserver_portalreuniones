<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>Documento sin t&iacute;tulo</title>



<link href="Estilos/GCBstyle.css" rel="stylesheet" type="text/css">
<link href="Estilos/Interno.css" rel="stylesheet" type="text/css">
<link href="Estilos/estprincipal.css" rel="stylesheet" type="text/css">
<link href="incluidos/estprincipal.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {
	font-size: 16;
	font-weight: bold;
}
.Estilo3 {font-size: 16; }
.Estilo4 {
	font-size: 20px;
}
.Estilo5 {font-size: 16pt}
-->
</style>
</head>

<body>
<div align="center">
  <table width="200" border="1" cellpadding="0" cellspacing="0" bordercolor="#D5DDBF">
    <tr>
      <td>

  <table width="701" height="528" border="0">
    <tr>
      <td width="755" class="EncTabla2 Estilo5">REGLAMENTO PARA UNA REUNION EFECTIVA</td>
    </tr>
    <tr>
      <td height="492"><table align="center" cellpadding="0" cellspacing="0" class="TituloContenido">
        <col width="84" />
        <col width="501" />
        <tr height="19">
          <td width="84" height="37" align="right" class="Estilo4"><div align="center" class="Estilo1">1</div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo4">Puntualidad en el inicio de la    reunión</div></td>
        </tr>
        <tr height="38">
          <td height="41" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>2</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Discusión y    análisis con base en hechos y datos</div></td>
        </tr>
        <tr height="19">
          <td height="41" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>3</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Respeto por el    uso de la palabra</div></td>
        </tr>
        <tr height="19">
          <td height="43" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>4</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">No se    realizaran conversaciones paralelas</div></td>
        </tr>
        <tr height="19">
          <td height="33" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>5</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Responder por    los compromisos y tareas asignados </div></td>
        </tr>
        <tr height="38">
          <td height="60" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>6</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Responder por    el rol asignado en la reunión y los compromisos derivados de éste.</div></td>
        </tr>
        <tr height="19">
          <td height="54" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>7</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Teléfonos    móviles y radios avantel apagados o silenciosos.</div></td>
        </tr>
        <tr height="19">
          <td height="44" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>8</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">No se    contestarán teléfonos fijos o móviles.</div></td>
        </tr>
        <tr height="38">
          <td height="54" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>9</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">No habrá    computadores abiertos, a no ser que sea necesario para la reunión.</div></td>
        </tr>
        <tr height="38">
          <td height="53" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>10</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Sólo se podrán    asignar compromisos a los asistentes de la reunión.</div></td>
        </tr>
        <tr height="19">
          <td height="31" align="right" class="Estilo4"><div align="center" class="Estilo3"><strong>11</strong></div></td>
          <td width="579" class="Estilo4"><div align="left" class="Estilo3">Puntualidad en    la terminación de la reunión</div></td>
        </tr>
      </table></td>
    </tr>
  </table>
  
  
</td>
    </tr>
  </table>
  

  </div>
</body>
</html>
