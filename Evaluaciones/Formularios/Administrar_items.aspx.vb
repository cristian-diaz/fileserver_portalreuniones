Imports System.Data

Partial Class Evaluaciones_Formularios_Administrar_items
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Me.prcCargarOrigen("Evaluacion")
            Me.PrcCargarDatos("Evaluacion")
        End If
    End Sub

    Public Sub prcCrearDataset()
        Dim ds As New DataSet
        Dim dtt_evaluacion As New DataTable '---0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_evaluacion") ' items de la grilla a evaluar 
    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "Evaluacion"
                Me.evalu.prcconsultaritemreunion(, True)
                Me.ds_evalua().Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))
        End Select
    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "Evaluacion"
                Me.GridView1.DataSource = Me.ds_evalua.Tables(0)
                Me.GridView1.DataBind()
        End Select
    End Sub

    Public Sub PrcActualizar(ByVal tipo As String)
        Select Case tipo
            Case "Activo"
                For i As Integer = 0 To Me.GridView1.Rows.Count - 1
                    If CType(Me.GridView1.Rows(i).FindControl("CheckBox1"), CheckBox).Checked = True Then
                        Me.ds_evalua.Tables(0).Rows(i).Item(5) = 1
                    Else
                        Me.ds_evalua.Tables(0).Rows(i).Item(5) = 0
                    End If
                Next
                evalu.PrcActualizarEstado(ds_evalua)

        End Select
    End Sub
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Me.GridView1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("Evaluacion")
    End Sub
    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        Response.Redirect("WfrCreaModItems.aspx?Accion=Editar&Id_Eval=" & Me.GridView1.SelectedValue)
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Redirect("WfrCreaModItems.aspx?Accion=Crear")
    End Sub

    Protected Sub ChkPaginar_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkPaginar.CheckedChanged
        Me.GridView1.AllowPaging = Me.ChkPaginar.Checked
        Me.PrcCargarDatos("Evaluacion")
    End Sub
End Class
