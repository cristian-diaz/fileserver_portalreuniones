﻿Imports System.Data
Imports System.Data.OleDb
Partial Class reporte_rpt_asis_reunion
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            Me.prcCrearDataset()
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")

        End If
    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "reuniones"
                Me.evalu.name_Reunion()
                Me.ds_evalua.Tables("nm_reuniones").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "busqueda"
                If Me.c1.Checked = True And Me.c2.Checked = False Then
                    If Me.fecha1.Text = "" Or Me.fecha2.Text = "" Then
                        Me.MsgBox1.ShowMessage("Al Menos un campo de la Busqueda Fecha Esta Vacio.. Complete Los Criterios")

                    Else
                        Me.ds_evalua.Tables("dt_reporte").Clear()
                        Me.evalu.Reporte_asistencias("", 3, Me.dr_nombre.SelectedValue, Me.fecha1.Text, Me.fecha2.Text)
                        Me.ds_evalua.Tables("dt_reporte").Merge(Me.evalu.DatasetFill.Tables(0))
                    End If

                End If


                If Me.c2.Checked = True And Me.c1.Checked = False Then
                    Me.ds_evalua.Tables("dt_reporte").Clear()
                    Me.evalu.Reporte_asistencias(Me.txt_registro.Text, 2, 0, "2999/01/01", "2999/01/01")
                    Me.ds_evalua.Tables("dt_reporte").Merge(Me.evalu.DatasetFill.Tables(0))
                End If


                If Me.c1.Checked = True And Me.c2.Checked = True Then
                    If Me.fecha1.Text = "" Or Me.fecha2.Text = "" Or Me.txt_registro.Text = "" Then
                        Me.MsgBox1.ShowMessage("Al Menos un Criterio de la Busqueda Fecha Esta Vacio.. Complete Los Criterios")

                    Else
                        Me.ds_evalua.Tables("dt_reporte").Clear()
                        Me.evalu.Reporte_asistencias(Me.txt_registro.Text, 1, Me.dr_nombre.SelectedValue, Me.fecha1.Text, Me.fecha2.Text)
                        Me.ds_evalua.Tables("dt_reporte").Merge(Me.evalu.DatasetFill.Tables(0))
                    End If
                End If
        End Select

    End Sub



    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo

            Case "reuniones"
                Me.dr_nombre.DataSource = Me.ds_evalua.Tables("nm_reuniones").CreateDataReader
                Me.dr_nombre.DataTextField = "Nombre"
                Me.dr_nombre.DataValueField = "Id"
                Me.dr_nombre.DataBind()

            Case "busqueda"


                If Me.c1.Checked = True And Me.c2.Checked = False Then
                    Me.MultiView1.ActiveViewIndex = 1
                    Me.grd2.DataSource = Me.ds_evalua.Tables("dt_reporte")
                    Me.grd2.DataBind()

                End If


                If Me.c2.Checked = True And Me.c1.Checked = False Then
                    If Me.ds_evalua.Tables("dt_reporte").Rows.Count > 0 Then
                        Me.MultiView1.ActiveViewIndex = 0
                        Dim reunion As DataRow = Me.ds_evalua.Tables("dt_reporte").Rows(0)
                        Me.Label7.Text = reunion("nombreusuario")
                        Me.gr1.DataSource = Me.ds_evalua.Tables("dt_reporte")
                        Me.gr1.DataBind()
                    Else
                        Me.MsgBox1.ShowMessage("No hay Ningun Registro que cumpla Con este criterio")

                    End If

                End If

                If Me.c1.Checked = True And Me.c2.Checked = True Then
                    If Me.fecha1.Text = "" Or Me.fecha2.Text = "" Or Me.txt_registro.Text = "" Then
                        Me.MsgBox1.ShowMessage("Al Menos un Criterio de la Busqueda Fecha Esta Vacio.. Complete Los Criterios")

                    Else
                        Me.MultiView1.ActiveViewIndex = 2
                        If Me.ds_evalua.Tables("dt_reporte").Rows.Count > 0 Then
                            Dim reunion As DataRow = Me.ds_evalua.Tables("dt_reporte").Rows(0)
                            Me.Label9.Text = reunion("nombreusuario")
                            Me.GRD3.DataSource = Me.ds_evalua.Tables("dt_reporte")
                            Me.GRD3.DataBind()
                        Else
                            Me.MsgBox1.ShowMessage("No hay Ningun Registro que cumpla Con estos criterios")

                        End If



                    End If
                End If



        End Select
    End Sub


    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable   '--0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("tp_reuniones")
        Me.ds_evalua.Tables.Add("nm_reuniones")
        Me.ds_evalua.Tables.Add("dt_reporte")

        Me.ds_evalua.AcceptChanges()


    End Sub


    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.c1.Checked = False And Me.c2.Checked = False Then
            Me.MsgBox1.ShowMessage("Debe elegir una Opción de Busqueda")
        Else
            Me.prcCargarOrigen("busqueda")
            Me.PrcCargarDatos("busqueda")
        End If


    End Sub

    Protected Sub gr1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gr1.RowDataBound
        'Dim gvr As GridViewRow = e.Row
        'Dim porcentaje As String
        'If gvr.RowType = DataControlRowType.DataRow Then
        '    porcentaje = e.Row.Cells(4).Text

        '    If porcentaje = "SI" Then
        '        e.Row.Cells(4).BackColor = Drawing.Color.Green
        '        e.Row.Cells(4).ForeColor = Drawing.Color.White
        '    Else
        '        e.Row.Cells(4).BackColor = Drawing.Color.Crimson
        '        e.Row.Cells(4).ForeColor = Drawing.Color.Black
        '    End If
        '    'e.Row.Cells(5).Text = e.Row.Cells(5).Text & " %"
        'End If
    End Sub

    Protected Sub gr1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gr1.SelectedIndexChanged

    End Sub

    Protected Sub gr1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gr1.PageIndexChanging
        Me.gr1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("busqueda")
    End Sub

    Protected Sub grd2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grd2.RowDataBound
        'Dim gvr As GridViewRow = e.Row
        'Dim porcentaje As String
        'If gvr.RowType = DataControlRowType.DataRow Then
        '    porcentaje = e.Row.Cells(4).Text

        '    If porcentaje = "SI" Then
        '        e.Row.Cells(4).BackColor = Drawing.Color.Green
        '        e.Row.Cells(4).ForeColor = Drawing.Color.White
        '    Else
        '        e.Row.Cells(4).BackColor = Drawing.Color.Crimson
        '        e.Row.Cells(4).ForeColor = Drawing.Color.Black
        '    End If
        '    'e.Row.Cells(5).Text = e.Row.Cells(5).Text & " %"
        'End If
    End Sub

    
    Protected Sub grd2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grd2.SelectedIndexChanged

    End Sub
End Class
