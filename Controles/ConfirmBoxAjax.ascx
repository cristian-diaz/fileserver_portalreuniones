<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfirmBoxAjax.ascx.vb" Inherits="Controles_ConfirmBoxAjax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Panel ID="PnlConfirmBox" runat="server" CssClass="modalPopup2" DefaultButton="BtnAceptar"
    Style="display: none" Width="350px">
    <table width="350">
        <tr>
            <td align="center" class="Heather">
                <b>
                    <asp:Label ID="LblTitle" runat="server"></asp:Label></b></td>
        </tr>
        <tr>
            <td align="center">
                <hr />
                <asp:Label ID="LblMessage" runat="server" SkinID="SubTitulo_Azul_M"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                <hr />
                <div style="text-align: center">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td align="center" style="width: 50%">
                                <asp:Button ID="BtnAceptar" runat="server" Font-Bold="True" Font-Size="X-Small"
                    Text="Aceptar" /></td>
                            <td align="center" style="width: 50%">
                                <asp:Button ID="BtnCancelar" runat="server" Font-Bold="True" Font-Size="X-Small"
                    Text="Cancelar" /></td>
                            <td style="width: 50%">
                            </td>
                            <td style="width: 50%">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<cc1:ModalPopupExtender ID="MpeShow" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="BtnCancelar" DropShadow="True" Enabled="True" PopupControlID="PnlConfirmBox"
    TargetControlID="LblShow">
</cc1:ModalPopupExtender>
<asp:Label ID="LblShow" runat="server"></asp:Label>
