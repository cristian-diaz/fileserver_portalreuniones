Imports System.Data
Partial Class Evaluaciones_Formularios_Revisar_Evaluacion
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Dias As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("diasxmodificar").ToString)
        str_Oopcion = Request.QueryString("Id_Reunion")
        Dim fech_r As String = Request.QueryString("fecha")
        Dim n_reunion As String = Request.QueryString("nombre")

        Me.Label7.Text = n_reunion
        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")
            ' Me.LblNota.Text = "Usted cuenta con " & Dias & " d�as para editar la evaluaci�n de una reuni�n a partir de la fecha de realizaci�n"

            Me.prcCargarOrigen("Evaluacion")
            Me.PrcCargarDatos("Evaluacion")

        End If
    End Sub
    Public Property str_Oopcion() As String
        Get
            Return ViewState("LOopcion")
        End Get
        Set(ByVal value As String)
            ViewState("LOopcion") = value
        End Set
    End Property

    Public Sub prcCrearDataset()
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable '---0
        Dim dtt_evaluacion As New DataTable '---1
        Dim dtt_resultado As New DataTable '---2
        Dim dtt_datos As New DataTable '---3
        Dim dtt_calificacion As New DataTable '---4


        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_evaluacion") ' items de la grilla a evaluar 
        Me.ds_evalua.Tables.Add("dtt_resultado") ' grilla de los resultados obtenidos 
        Me.ds_evalua.Tables.Add("dtt_datos")
        Me.ds_evalua.Tables.Add("dtt_calificacion")


        ' dataset de resultados
        Me.ds_evalua.Tables(2).Columns.Add("itemr", Type.GetType("System.String"))
        Me.ds_evalua.Tables(2).Columns.Add("valorr", Type.GetType("System.String"))
    End Sub
    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "guardar"
                Me.evalu.prcconsultarestructura()
                Me.ds_evalua().Tables(3).Merge(Me.evalu.DatasetFill.Tables(0))
                Me.ds_evalua.Tables(1).Columns.Add("calif", Type.GetType("System.String"))
                Me.ds_evalua.Tables(1).PrimaryKey = New DataColumn() {Me.ds_evalua.Tables(1).Columns("Id_Item")}
                Me.ds_evalua.AcceptChanges()
                'Case "reuniones"
                '    Me.evalu.prcconsultarreuniones(Me.evalu.FncRol_Usuario(Session("Usuario")))
                '    Me.ds_evalua().Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))

            Case "Evaluacion"
                str_Oopcion = Request.QueryString("Id_Reunion")
                Dim fech_r As String = Request.QueryString("fecha")
                Dim fech_f As String = ""
                If Me.ds_evalua.Tables(1).Rows.Count > 0 Then
                    Me.ds_evalua.Tables(1).Clear()
                End If
                '  Dim ds As DataSet = Me.evalu.prcConsultar_Evaluacion(Me.DropDownList1.SelectedValue, Me.txtfecha.Text, Me.TxFecha2.Text)
                Dim ds As DataSet = Me.evalu.prcConsultar_Evaluacion(str_Oopcion, fech_r, fech_f)
                Me.ds_evalua.Tables(1).Merge(ds.Tables(0))
                Me.ds_evalua.Tables(1).PrimaryKey = New DataColumn() {Me.ds_evalua.Tables(1).Columns("No_Eval")}

            Case "Detalle"
                If Me.ds_evalua.Tables(4).Rows.Count > 0 Then
                    Me.ds_evalua.Tables(4).Clear()
                End If
                Me.ds_evalua.Tables(4).Merge(Me.evalu.FncDetalle_Evaluacion(Me.GridView3.SelectedValue))
                Try
                    Me.ds_evalua.Tables(4).Columns.Add("orden", Type.GetType("System.Int32"))
                Catch ex As Exception
                End Try
                For i As Integer = 0 To Me.ds_evalua.Tables(4).Rows.Count - 1
                    Me.ds_evalua.Tables(4).Rows(i)("orden") = i + 1
                Next
        End Select
    End Sub
    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            'Case "reuniones"
            '    Me.DropDownList1.DataSource = Me.ds_evalua.Tables(0).CreateDataReader
            '    Me.DropDownList1.DataTextField = "Nombre"
            '    Me.DropDownList1.DataValueField = "Id"
            '    Me.DropDownList1.DataBind()
            Case "Evaluacion"
                Me.GridView3.DataSource = Me.ds_evalua.Tables(1)
                Me.GridView3.DataBind()
            Case "Detalle"
                Me.GridView1.DataSource = Me.ds_evalua.Tables(4)
                Me.GridView1.DataBind()
            Case "Resultados"
                If Me.ds_evalua.Tables(2).Rows.Count > 0 Then
                    Me.ds_evalua.Tables(2).Clear()
                End If
                Me.prcCargarResultados()
                Me.GridView2.DataSource = Me.ds_evalua.Tables(2)
                Me.GridView2.DataBind()
        End Select
    End Sub
    Public Sub PrcCargarEvaluacion(ByVal No_Eval As String)
        Dim reunion As DataRow = Me.ds_evalua.Tables(1).Rows.Find(No_Eval)
        Me.txtlugar.Text = reunion("lugar")
        Me.TextBox1.Text = reunion("comentario").ToString
        Me.PrcCargarDatos("Resultados")
    End Sub
    Public Sub prcCargarResultados()
        Dim filas As DataRow
        Dim sumapeso As Integer

        For i As Integer = 0 To Me.ds_evalua.Tables(4).Rows.Count - 1
            sumapeso += Me.ds_evalua.Tables(4).Rows(i).Item("Peso")
        Next

        filas = Me.ds_evalua.Tables(2).NewRow
        filas("itemr") = "Puntaje Obtenido"
        filas("valorr") = Me.ds_evalua.Tables(1).Rows.Find(Me.GridView3.SelectedValue)("totalpuntos")
        Me.ds_evalua.Tables(2).Rows.Add(filas)

        filas = Me.ds_evalua.Tables(2).NewRow
        filas("itemr") = "Efectividad de Esta Reuni�n"
        filas("valorr") = Me.ds_evalua.Tables(1).Rows.Find(Me.GridView3.SelectedValue)("porcentaje") & " %"
        Me.ds_evalua.Tables(2).Rows.Add(filas)

        filas = Me.ds_evalua.Tables(2).NewRow
        filas("itemr") = "Total Peso"
        filas("valorr") = sumapeso
        Me.ds_evalua.Tables(2).Rows.Add(filas)


        filas = Me.ds_evalua.Tables(2).NewRow
        filas("itemr") = "Maximo Puntaje Posible"
        filas("valorr") = System.Configuration.ConfigurationManager.AppSettings("efectividadreunion").ToString
        Me.ds_evalua.Tables(2).Rows.Add(filas)

    End Sub
    'Protected Sub BtnConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
    '    Me.prcCargarOrigen("Evaluacion")
    '    Me.PrcCargarDatos("Evaluacion")
    '    Me.PnlResultados.Visible = False
    'End Sub

    Protected Sub GridView3_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView3.DataBound
        If Me.GridView3.Rows.Count > 0 Then
            Me.PnlInfo.Visible = False
            Me.LblNota.Visible = True
        Else
            Me.PnlInfo.Visible = True
            Me.LblNota.Visible = False
        End If
    End Sub

    Protected Sub GridView3_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView3.PageIndexChanging
        Me.GridView3.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("Evaluacion")
    End Sub

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound
        'Pintar de rojo o verde seg�n sea el caso la columna "porcentaje"
        Dim gvr As GridViewRow = e.Row
        Dim porcentaje As Integer
        If gvr.RowType = DataControlRowType.DataRow Then
            porcentaje = e.Row.Cells(5).Text
            If porcentaje > CInt(System.Configuration.ConfigurationManager.AppSettings("promediominimo").ToString) Then
                e.Row.Cells(5).BackColor = Drawing.Color.Green
                e.Row.Cells(5).ForeColor = Drawing.Color.White
            Else
                e.Row.Cells(5).BackColor = Drawing.Color.Crimson
                e.Row.Cells(5).ForeColor = Drawing.Color.Black
            End If
            e.Row.Cells(5).Text = e.Row.Cells(5).Text & " %"
        End If
    End Sub
    Protected Sub GridView3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView3.SelectedIndexChanged
        Me.prcCargarOrigen("Detalle")
        Me.PrcCargarDatos("Detalle")
        Me.PnlResultados.Visible = True
        Me.PrcCargarEvaluacion(Me.GridView3.SelectedValue)
    End Sub

    Protected Sub ImbEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim imb As ImageButton = sender
        Dim No_Eval As String = imb.CommandArgument
        Dim row As DataRow = Me.ds_evalua.Tables(1).Rows.Find(No_Eval)
        'Validar que el usuario consultado sea el evaluador de la reuni�n
        If row("login").ToString.ToUpper = Session("Usuario").ToString.ToUpper Then
            Dim Dias As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("diasxmodificar").ToString)
            Dim Fecha_Min As Date = DateAdd(DateInterval.Day, -Dias, Now)
            If CDate(row("fecha")) >= CDate(Fecha_Min.ToShortDateString) Then
                Response.Redirect("Evaluacion.aspx?Accion=Update&Eval=" & No_Eval)
            Else
                Me.MsgBox1.ShowMessage("La Evaluaci�n no puede ser modificada. Por favor contacte al administrador funcional")
            End If
        Else
            Me.MsgBox1.ShowMessage("La Evaluaci�n solo puede ser modificada por el usuario que la realiz�")
        End If
    End Sub

End Class
