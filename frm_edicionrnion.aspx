﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_edicionrnion.aspx.vb" Inherits="frm_edicionrnion"   Theme="SkinCompromisos"  StylesheetTheme="SkinCompromisos" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PORTAL DE COMPROMISOS:: EDICION REUNION</title>
</head>
<body  style=" margin-bottom:0; margin-left:0; margin-top:0">
    <form id="form1" runat="server">
    <div class="texto">   
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="image2"
            TargetControlID="date1"
             Format="dd/MM/yyyy">          
        </cc1:CalendarExtender>
         
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="image4"
            TargetControlID="date2">
        </cc1:CalendarExtender>
        <cc1:ValidatorCalloutExtender ID="valTxt1" runat="server" TargetControlID="NameRequired">
        </cc1:ValidatorCalloutExtender>      
            
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 100px;background-color:White; height: 13px;" >
        <asp:Image ID="Image3" runat="server" ImageUrl="img/ecoep.gif" /></td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="height: 184px;background-color: #D5DDBF;">
            <tr>
                <td style="width: 118px; height: 40px;">
                    <asp:Label ID="Label2" runat="server" Text=" Nombre:"></asp:Label></td>
                <td colspan="1" style="width: 32px; height: 40px">
                </td>
                <td colspan="1" style="width: 32px; height: 40px;">
                </td>
                <td colspan="2" style="height: 40px; width: 478px;">
                    <asp:TextBox ID="txt_nombre" runat="server" Width="465px" CausesValidation="True" SkinID="txt_INC"></asp:TextBox>
                    
                    <asp:LinkButton ID="rnion_exis" runat="server">Reuniones Existentes.</asp:LinkButton>
                    <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="txt_nombre"
                        Display="None" ErrorMessage="<b>Nombre es Requerido</b><b" ValidationGroup="val"></asp:RequiredFieldValidator>
                    </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 82px">
                  
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="height: 25px" colspan="3">
                                <br />
                                <asp:RadioButtonList ID="reunioneditar" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1">Ver Datos de Reunion</asp:ListItem>
                                    <asp:ListItem Value="2">Editar Datos de Reunion</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:MultiView ID="MultiView2" runat="server">
                                    <asp:View ID="Vista_leer" runat="server" EnableTheming="False">
                                      <table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100px">
                                                
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 100px; height: 16px;">
                                        Tipo de Reunion</td>
                                        <td style="width: 129px; height: 16px;">
                                        </td>
                                        <td style="width: 100px; height: 16px;">
                                        </td>
                                        <td style="width: 100px; height: 16px;">
                                        </td>
                                        <td style="width: 100px; height: 16px;">
                                        </td>
                                        <td style="width: 100px; height: 16px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">                                            
                                            <asp:TextBox ID="text_1" runat="server" Width="260px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            Hora Inicio:</td>
                                        <td style="width: 129px">
                                            Hora Fin:</td>
                                        <td style="width: 100px">
                                            Duracion:</td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:TextBox ID="text_2" runat="server" Width="85px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 129px">
                                            <asp:TextBox ID="text_3" runat="server" Width="85px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 100px">
                                            <asp:TextBox ID="text_4" runat="server" SkinID="txt_INC" Width="316px"></asp:TextBox></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 129px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            Comienzo</td>
                                        <td style="width: 129px">
                                            </td>
                                        <td style="width: 100px">
                                            Finaliza</td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:TextBox ID="text_5" runat="server" Width="93px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 129px">
                                            </td>
                                        <td style="width: 100px">
                                            <asp:TextBox ID="text_6" runat="server" Width="87px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 129px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            Frecuencia</td>
                                        <td style="width: 129px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height: 24px">
                                            <asp:TextBox ID="text_7" runat="server" Width="292px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 100px; height: 24px">
                                        </td>
                                        <td style="width: 100px; height: 24px">
                                        </td>
                                        <td style="width: 100px; height: 24px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 129px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            Moderador</td>
                                        <td style="width: 129px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height: 16px">
                                            <br />
                                            <asp:GridView ID="GridView1" runat="server" SkinID="Gvw_little">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Login">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label19" runat="server" Text='<%# bind("login") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Nombre Moderador">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label20" runat="server" Text='<%# bind("nombreusuario") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 129px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; height: 16px">
                                            Dependencia</td>
                                        <td style="width: 129px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" style="height: 16px">
                                            <asp:TextBox ID="text_9" runat="server" Width="501px" SkinID="txt_INC"></asp:TextBox></td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 129px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                        <td style="width: 100px; height: 16px">
                                        </td>
                                    </tr>
                                </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="Vista_editar" runat="server">
                                  
        <table border="1" cellpadding="0" cellspacing="0" style="width: 100%; text-align: center;">
            <tr>
                <td style="width: 100px">
                                            <asp:Label ID="Label18" runat="server" Text="Item Edicion"></asp:Label></td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center;">
                    <asp:CheckBox ID="chN" runat="server" /></td>
                <td style="width: 100px; text-align: left;">
                    <asp:Label ID="Label3" runat="server" Text="Nombre:"></asp:Label><br />
                    <asp:TextBox ID="txt_nm" runat="server" Width="451px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center;">
                                            <asp:CheckBox ID="ch1" runat="server" /></td>
                <td style="width: 100px; text-align: left;">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 100px; height: 21px;">
                    <asp:Label ID="Label1" runat="server" Text="Tipo de Reunion:" Width="141px"></asp:Label></td>
                                                    <td style="width: 100px; height: 21px;">
                    <asp:DropDownList ID="tr" runat="server">
                    </asp:DropDownList></td>
                                                </tr>
                                            </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center;">
                                            <asp:CheckBox ID="ch2" runat="server" /></td>
                <td style="width: 100px; text-align: left;">
                    <asp:RequiredFieldValidator ID="valduracion" runat="server" ControlToValidate="txt_duracion"
                                    Display="None" ErrorMessage="<b>Duracion es Requerida</b><b" ValidationGroup="val"></asp:RequiredFieldValidator><br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="width: 100px">
                                     <asp:Label ID="Label6" runat="server" Text="Inicio:"></asp:Label></td>
                            <td style="width: 100px">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        
                                <asp:DropDownList ID="dr_inicio" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dr_inicio_SelectedIndexChanged">
                                    <asp:ListItem>12:00 AM</asp:ListItem>
<asp:ListItem>12:15 AM</asp:ListItem>
<asp:ListItem>12:30 AM</asp:ListItem>
<asp:ListItem>12:45 AM</asp:ListItem>
<asp:ListItem>01:00 AM</asp:ListItem>
<asp:ListItem>01:15 AM</asp:ListItem>
<asp:ListItem>01:30 AM</asp:ListItem>
<asp:ListItem>01:45 AM</asp:ListItem>
<asp:ListItem>02:00 AM</asp:ListItem>
<asp:ListItem>02:15 AM</asp:ListItem>
<asp:ListItem>02:30 AM</asp:ListItem>
<asp:ListItem>02:45 AM</asp:ListItem>
<asp:ListItem>03:00 AM</asp:ListItem>
<asp:ListItem>03:15 AM</asp:ListItem>
<asp:ListItem>03:30 AM</asp:ListItem>
<asp:ListItem>03:45 AM</asp:ListItem>
<asp:ListItem>04:00 AM</asp:ListItem>
<asp:ListItem>04:15 AM</asp:ListItem>
<asp:ListItem>04:30 AM</asp:ListItem>
<asp:ListItem>04:45 AM</asp:ListItem>
<asp:ListItem>05:00 AM</asp:ListItem>
<asp:ListItem>05:15 AM</asp:ListItem>
<asp:ListItem>05:30 AM</asp:ListItem>
<asp:ListItem>05:45 AM</asp:ListItem>
<asp:ListItem>06:00 AM</asp:ListItem>
<asp:ListItem>06:15 AM</asp:ListItem>
<asp:ListItem>06:30 AM</asp:ListItem>
<asp:ListItem>06:45 AM</asp:ListItem>
<asp:ListItem>07:00 AM</asp:ListItem>
<asp:ListItem>07:15 AM</asp:ListItem>
<asp:ListItem>07:30 AM</asp:ListItem>
<asp:ListItem>07:45 AM</asp:ListItem>
<asp:ListItem>08:00 AM</asp:ListItem>
<asp:ListItem>08:15 AM</asp:ListItem>
<asp:ListItem>08:30 AM</asp:ListItem>
<asp:ListItem>08:45 AM</asp:ListItem>
<asp:ListItem>09:00 AM</asp:ListItem>
<asp:ListItem>09:15 AM</asp:ListItem>
<asp:ListItem>09:30 AM</asp:ListItem>
<asp:ListItem>09:45 AM</asp:ListItem>
<asp:ListItem>10:00 AM</asp:ListItem>
<asp:ListItem>10:15 AM</asp:ListItem>
<asp:ListItem>10:30 AM</asp:ListItem>
<asp:ListItem>10:45 AM</asp:ListItem>
<asp:ListItem>11:00 AM</asp:ListItem>
<asp:ListItem>11:15 AM</asp:ListItem>
<asp:ListItem>11:30 AM</asp:ListItem>
<asp:ListItem>11:45 AM</asp:ListItem>
<asp:ListItem>12:00 PM</asp:ListItem>
<asp:ListItem>12:15 PM</asp:ListItem>
<asp:ListItem>12:30 PM</asp:ListItem>
<asp:ListItem>12:45 PM</asp:ListItem>
<asp:ListItem>01:00 PM</asp:ListItem>
<asp:ListItem>01:15 PM</asp:ListItem>
<asp:ListItem>01:30 PM</asp:ListItem>
<asp:ListItem>01:45 PM</asp:ListItem>
<asp:ListItem>02:00 PM</asp:ListItem>
<asp:ListItem>02:15 PM</asp:ListItem>
<asp:ListItem>02:30 PM</asp:ListItem>
<asp:ListItem>02:45 PM</asp:ListItem>
<asp:ListItem>03:00 PM</asp:ListItem>
<asp:ListItem>03:15 PM</asp:ListItem>
<asp:ListItem>03:30 PM</asp:ListItem>
<asp:ListItem>03:45 PM</asp:ListItem>
<asp:ListItem>04:00 PM</asp:ListItem>
<asp:ListItem>04:15 PM</asp:ListItem>
<asp:ListItem>04:30 PM</asp:ListItem>
<asp:ListItem>04:45 PM</asp:ListItem>
<asp:ListItem>05:00 PM</asp:ListItem>
<asp:ListItem>05:15 PM</asp:ListItem>
<asp:ListItem>05:30 PM</asp:ListItem>
<asp:ListItem>05:45 PM</asp:ListItem>
<asp:ListItem>06:00 PM</asp:ListItem>
<asp:ListItem>06:15 PM</asp:ListItem>
<asp:ListItem>06:30 PM</asp:ListItem>
<asp:ListItem>06:45 PM</asp:ListItem>
<asp:ListItem>07:00 PM</asp:ListItem>
<asp:ListItem>07:15 PM</asp:ListItem>
<asp:ListItem>07:30 PM</asp:ListItem>
<asp:ListItem>07:45 PM</asp:ListItem>
<asp:ListItem>08:00 PM</asp:ListItem>
<asp:ListItem>08:15 PM</asp:ListItem>
<asp:ListItem>08:30 PM</asp:ListItem>
<asp:ListItem>08:45 PM</asp:ListItem>
<asp:ListItem>09:00 PM</asp:ListItem>
<asp:ListItem>09:15 PM</asp:ListItem>
<asp:ListItem>09:30 PM</asp:ListItem>
<asp:ListItem>09:45 PM</asp:ListItem>
<asp:ListItem>10:00 PM</asp:ListItem>
<asp:ListItem>10:15 PM</asp:ListItem>
<asp:ListItem>10:30 PM</asp:ListItem>
<asp:ListItem>10:45 PM</asp:ListItem>
<asp:ListItem>11:00 PM</asp:ListItem>
<asp:ListItem>11:15 PM</asp:ListItem>
<asp:ListItem>11:30 PM</asp:ListItem>
<asp:ListItem>11:45 PM</asp:ListItem>

                                </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="width: 100px">
                                <asp:Label ID="Label8" runat="server" Text="Fin:"></asp:Label></td>
                            <td style="width: 100px">
                              
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        
                                        <asp:DropDownList ID="dr_fin" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dr_fin_SelectedIndexChanged">
                                            <asp:ListItem>12:00 AM</asp:ListItem>
                                            <asp:ListItem>12:15 AM</asp:ListItem>
                                            <asp:ListItem>12:30 AM</asp:ListItem>
                                            <asp:ListItem>12:45 AM</asp:ListItem>
                                            <asp:ListItem>01:00 AM</asp:ListItem>
                                            <asp:ListItem>01:15 AM</asp:ListItem>
                                            <asp:ListItem>01:30 AM</asp:ListItem>
                                            <asp:ListItem>01:45 AM</asp:ListItem>
                                            <asp:ListItem>02:00 AM</asp:ListItem>
                                            <asp:ListItem>02:15 AM</asp:ListItem>
                                            <asp:ListItem>02:30 AM</asp:ListItem>
                                            <asp:ListItem>02:45 AM</asp:ListItem>
                                            <asp:ListItem>03:00 AM</asp:ListItem>
                                            <asp:ListItem>03:15 AM</asp:ListItem>
                                            <asp:ListItem>03:30 AM</asp:ListItem>
                                            <asp:ListItem>03:45 AM</asp:ListItem>
                                            <asp:ListItem>04:00 AM</asp:ListItem>
                                            <asp:ListItem>04:15 AM</asp:ListItem>
                                            <asp:ListItem>04:30 AM</asp:ListItem>
                                            <asp:ListItem>04:45 AM</asp:ListItem>
                                            <asp:ListItem>05:00 AM</asp:ListItem>
                                            <asp:ListItem>05:15 AM</asp:ListItem>
                                            <asp:ListItem>05:30 AM</asp:ListItem>
                                            <asp:ListItem>05:45 AM</asp:ListItem>
                                            <asp:ListItem>06:00 AM</asp:ListItem>
                                            <asp:ListItem>06:15 AM</asp:ListItem>
                                            <asp:ListItem>06:30 AM</asp:ListItem>
                                            <asp:ListItem>06:45 AM</asp:ListItem>
                                            <asp:ListItem>07:00 AM</asp:ListItem>
                                            <asp:ListItem>07:15 AM</asp:ListItem>
                                            <asp:ListItem>07:30 AM</asp:ListItem>
                                            <asp:ListItem>07:45 AM</asp:ListItem>
                                            <asp:ListItem>08:00 AM</asp:ListItem>
                                            <asp:ListItem>08:15 AM</asp:ListItem>
                                            <asp:ListItem>08:30 AM</asp:ListItem>
                                            <asp:ListItem>08:45 AM</asp:ListItem>
                                            <asp:ListItem>09:00 AM</asp:ListItem>
                                            <asp:ListItem>09:15 AM</asp:ListItem>
                                            <asp:ListItem>09:30 AM</asp:ListItem>
                                            <asp:ListItem>09:45 AM</asp:ListItem>
                                            <asp:ListItem>10:00 AM</asp:ListItem>
                                            <asp:ListItem>10:15 AM</asp:ListItem>
                                            <asp:ListItem>10:30 AM</asp:ListItem>
                                            <asp:ListItem>10:45 AM</asp:ListItem>
                                            <asp:ListItem>11:00 AM</asp:ListItem>
                                            <asp:ListItem>11:15 AM</asp:ListItem>
                                            <asp:ListItem>11:30 AM</asp:ListItem>
                                            <asp:ListItem>11:45 AM</asp:ListItem>
                                            <asp:ListItem>12:00 PM</asp:ListItem>
                                            <asp:ListItem>12:15 PM</asp:ListItem>
                                            <asp:ListItem>12:30 PM</asp:ListItem>
                                            <asp:ListItem>12:45 PM</asp:ListItem>
                                            <asp:ListItem>01:00 PM</asp:ListItem>
                                            <asp:ListItem>01:15 PM</asp:ListItem>
                                            <asp:ListItem>01:30 PM</asp:ListItem>
                                            <asp:ListItem>01:45 PM</asp:ListItem>
                                            <asp:ListItem>02:00 PM</asp:ListItem>
                                            <asp:ListItem>02:15 PM</asp:ListItem>
                                            <asp:ListItem>02:30 PM</asp:ListItem>
                                            <asp:ListItem>02:45 PM</asp:ListItem>
                                            <asp:ListItem>03:00 PM</asp:ListItem>
                                            <asp:ListItem>03:15 PM</asp:ListItem>
                                            <asp:ListItem>03:30 PM</asp:ListItem>
                                            <asp:ListItem>03:45 PM</asp:ListItem>
                                            <asp:ListItem>04:00 PM</asp:ListItem>
                                            <asp:ListItem>04:15 PM</asp:ListItem>
                                            <asp:ListItem>04:30 PM</asp:ListItem>
                                            <asp:ListItem>04:45 PM</asp:ListItem>
                                            <asp:ListItem>05:00 PM</asp:ListItem>
                                            <asp:ListItem>05:15 PM</asp:ListItem>
                                            <asp:ListItem>05:30 PM</asp:ListItem>
                                            <asp:ListItem>05:45 PM</asp:ListItem>
                                            <asp:ListItem>06:00 PM</asp:ListItem>
                                            <asp:ListItem>06:15 PM</asp:ListItem>
                                            <asp:ListItem>06:30 PM</asp:ListItem>
                                            <asp:ListItem>06:45 PM</asp:ListItem>
                                            <asp:ListItem>07:00 PM</asp:ListItem>
                                            <asp:ListItem>07:15 PM</asp:ListItem>
                                            <asp:ListItem>07:30 PM</asp:ListItem>
                                            <asp:ListItem>07:45 PM</asp:ListItem>
                                            <asp:ListItem>08:00 PM</asp:ListItem>
                                            <asp:ListItem>08:15 PM</asp:ListItem>
                                            <asp:ListItem>08:30 PM</asp:ListItem>
                                            <asp:ListItem>08:45 PM</asp:ListItem>
                                            <asp:ListItem>09:00 PM</asp:ListItem>
                                            <asp:ListItem>09:15 PM</asp:ListItem>
                                            <asp:ListItem>09:30 PM</asp:ListItem>
                                            <asp:ListItem>09:45 PM</asp:ListItem>
                                            <asp:ListItem>10:00 PM</asp:ListItem>
                                            <asp:ListItem>10:15 PM</asp:ListItem>
                                            <asp:ListItem>10:30 PM</asp:ListItem>
                                            <asp:ListItem>10:45 PM</asp:ListItem>
                                            <asp:ListItem>11:00 PM</asp:ListItem>
                                            <asp:ListItem>11:15 PM</asp:ListItem>
                                            <asp:ListItem>11:30 PM</asp:ListItem>
                                            <asp:ListItem>11:45 PM</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="width: 100px">
                                <asp:Label ID="Label9" runat="server" Text="Duracion:"></asp:Label></td>
                            <td style="width: 100px">
                                 
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txt_duracion" runat="server" Width="135px"></asp:TextBox>
                                   
                                    </ContentTemplate>
                                </asp:UpdatePanel>                             
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center;">
                    <asp:CheckBox ID="ch3" runat="server" /></td>
                <td style="width: 100px; text-align: left;">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 465px">
                                    <tr>
                                        <td style="width: 100px; height: 66px">
                               <asp:Label ID="Label5" runat="server" Text="Comienzo:"></asp:Label></td>
                                        <td style="width: 100px; height: 66px">
                                <asp:TextBox ID="date1" runat="server" Width="89px"></asp:TextBox></td>
                                        <td style="width: 100px; height: 66px">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                                        <td colspan="2" rowspan="2">
                                            <asp:RadioButtonList ID="rb_finaliza" runat="server" Width="183px" AutoPostBack="True">
                                                <asp:ListItem Value="no" Selected="True">Sin Fecha de Finalizacion</asp:ListItem>
                                                <asp:ListItem Value="si">Finaliza el:</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td style="width: 100px; height: 66px">
                                            <asp:TextBox ID="date2" runat="server" Width="96px" Enabled="False"></asp:TextBox></td>
                                        <td style="width: 100px; height: 66px">
                                            <asp:Image ID="Image4" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                </table>
                                <asp:CompareValidator ID="cfecha" runat="server" ControlToCompare="date1"
                                    ControlToValidate="date2" Display="None" ErrorMessage="La fecha final debe ser superior a la inicial"
                                    Operator="GreaterThan" Type="Date" ValidationGroup="fecha"></asp:CompareValidator></td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center;">
                    <asp:CheckBox ID="ch4" runat="server" />
                    </td>
                <td style="width: 100px; text-align: left;">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label7" runat="server" Text="Frecuencia:"></asp:Label>
                    <asp:RadioButtonList ID="frecuencia" runat="server" OnSelectedIndexChanged="frecuencia_SelectedIndexChanged" Width="487px" AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">Diaria Por turno</asp:ListItem>
                        <asp:ListItem Value="2">Diaria</asp:ListItem>
                        <asp:ListItem Value="3">Semanal</asp:ListItem>
                        <asp:ListItem Value="4">Mensual</asp:ListItem>
                        <asp:ListItem Value="5">Anual</asp:ListItem>
                    </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="FecuenciaValidator" runat="server" ControlToValidate="frecuencia"
                                Display="None" ErrorMessage="Al Menos debe elegir Una frecuencia" ValidationGroup="val"></asp:RequiredFieldValidator>
                            <cc1:ValidatorCalloutExtender ID="valFrecuencia" runat="server" TargetControlID="FecuenciaValidator">
                            </cc1:ValidatorCalloutExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100px">
                                                   
                                            <asp:MultiView ID="MultiView1" runat="server">
                                            
                                                <asp:View ID="View3" runat="server">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100px; height: 25px;">
                                                                
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 275px">
                                                                    <tr>
                                                                        <td style="width: 85px">
                                                                <asp:Label ID="Label16" runat="server" Text="Repetir cada:" Width="92px"></asp:Label></td>
                                                                        <td style="width: 44px">
                                                                <asp:TextBox ID="txt_sem" runat="server" Width="29px"></asp:TextBox></td>
                                                                        <td style="width: 100px">
                                                                <asp:Label ID="Label17" runat="server" Text="Semanas el:" Width="91px"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                   <asp:RadioButtonList ID="opt_semanal" runat="server" RepeatDirection="Horizontal" >
                                                          <asp:ListItem>Lunes</asp:ListItem>
                                                        <asp:ListItem>Martes</asp:ListItem>
                                                        <asp:ListItem>Miercoles</asp:ListItem>
                                                        <asp:ListItem>Jueves</asp:ListItem>
                                                        <asp:ListItem>Viernes</asp:ListItem>
                                                        <asp:ListItem>Sabado</asp:ListItem>
                                                        <asp:ListItem>Domingo</asp:ListItem>
                                                    </asp:RadioButtonList></asp:View>
                                                <asp:View ID="View4" runat="server">
                                                    
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 439px">
                                                        <tr>
                                                            <td rowspan="2" style="width: 95px">
                                                                <asp:RadioButtonList ID="opt_mensual" runat="server">
                                                                    <asp:ListItem Value="1">El dia</asp:ListItem>
                                                                    <asp:ListItem Value="2">El</asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="txt_ms1" runat="server" Width="27px" Height="12px"></asp:TextBox><asp:Label ID="Label13" runat="server" Text="de cada" Width="47px"></asp:Label></td>
                                                            <td colspan="2">
                                                    <asp:TextBox ID="txt_Mensual" runat="server" Width="27px" Height="12px"></asp:TextBox><asp:Label ID="Label11" runat="server" Text="meses"></asp:Label></td>
                                                            <td style="width: 100px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px">
                                                                <asp:DropDownList ID="dr_ms1" runat="server">
                                                                    <asp:ListItem Value="Primer">Primer</asp:ListItem>
                                                                    <asp:ListItem>Segundo</asp:ListItem>
                                                                    <asp:ListItem>Tercer</asp:ListItem>
                                                                    <asp:ListItem>Cuarto</asp:ListItem>
                                                                    <asp:ListItem>último</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                            <td style="width: 100px">
                                                                <asp:DropDownList ID="dr_ms2" runat="server">
                                                                    <asp:ListItem>Lunes</asp:ListItem>
                                                                    <asp:ListItem>Martes</asp:ListItem>
                                                                    <asp:ListItem>Miercoles</asp:ListItem>
                                                                    <asp:ListItem>Jueves</asp:ListItem>
                                                                    <asp:ListItem>Viernes</asp:ListItem>
                                                                    <asp:ListItem>Sabado</asp:ListItem>
                                                                    <asp:ListItem>Domingo</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                            <td style="width: 100px">
                                                                <asp:Label ID="Label12" runat="server" Text="de Cada"></asp:Label></td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="txt_ms3" runat="server" Height="12px" Width="27px"></asp:TextBox><asp:Label ID="txt_ms2" runat="server" Text="Meses"></asp:Label></td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </asp:View>
                                                <asp:View ID="View5" runat="server">
                                                  <table border="0" cellpadding="0" cellspacing="0" style="width: 374px">
                                                        <tr>
                                                            <td style="width: 59px;" rowspan="2">
                                                                <asp:RadioButtonList ID="opt_an" runat="server">
                                                                    <asp:ListItem Value="1">Cada</asp:ListItem>
                                                                    <asp:ListItem Value="2">El</asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                            <td style="width: 102px; height: 20px; text-align: center;">
                                                                <asp:TextBox ID="txt_anual" runat="server" Width="27px" Height="12px"></asp:TextBox>
                                                                <asp:Label ID="Label15" runat="server" Text="de"></asp:Label></td>
                                                            <td style="width: 74px; height: 20px;">
                                                                <asp:DropDownList ID="dr_anual" runat="server">
                                                                    <asp:ListItem>Enero</asp:ListItem>
<asp:ListItem>Febrero</asp:ListItem>
<asp:ListItem>Marzo</asp:ListItem>
<asp:ListItem>Abril</asp:ListItem>
<asp:ListItem>Mayo</asp:ListItem>
<asp:ListItem>Junio</asp:ListItem>
<asp:ListItem>Julio</asp:ListItem>
<asp:ListItem>Agosto</asp:ListItem>
<asp:ListItem>Septiembre</asp:ListItem>
<asp:ListItem>Octubre</asp:ListItem>
<asp:ListItem>Noviembre</asp:ListItem>
<asp:ListItem>Diciembre</asp:ListItem>

                                                                </asp:DropDownList></td>
                                                            <td style="width: 114px; height: 20px">
                                                            </td>
                                                        </tr>
                                                      <tr>
                                                          <td style="width: 102px; height: 20px">
                                                              <asp:DropDownList ID="dr_an1" runat="server">
                                                                  <asp:ListItem Value="Primer">Primer</asp:ListItem>
                                                                  <asp:ListItem>Segundo</asp:ListItem>
                                                                  <asp:ListItem>Tercer</asp:ListItem>
                                                                  <asp:ListItem>Cuarto</asp:ListItem>
                                                                  <asp:ListItem>ultimo</asp:ListItem>
                                                              </asp:DropDownList></td>
                                                          <td style="width: 74px; height: 20px">
                                                              <asp:DropDownList ID="dr_an2" runat="server">
                                                                  <asp:ListItem>Lunes</asp:ListItem>
                                                                  <asp:ListItem>Martes</asp:ListItem>
                                                                  <asp:ListItem>Miercoles</asp:ListItem>
                                                                  <asp:ListItem>Jueves</asp:ListItem>
                                                                  <asp:ListItem>Viernes</asp:ListItem>
                                                                  <asp:ListItem>Sabado</asp:ListItem>
                                                                  <asp:ListItem>Domingo</asp:ListItem>
                                                              </asp:DropDownList></td>
                                                          <td style="width: 114px; height: 20px">
                                                              <asp:Label ID="Label14" runat="server" Text="de"></asp:Label>
                                                              <asp:DropDownList ID="dr_an3" runat="server">
                                                                  <asp:ListItem>Enero</asp:ListItem>
                                                                  <asp:ListItem>Febrero</asp:ListItem>
                                                                  <asp:ListItem>Marzo</asp:ListItem>
                                                                  <asp:ListItem>Abril</asp:ListItem>
                                                                  <asp:ListItem>Mayo</asp:ListItem>
                                                                  <asp:ListItem>Junio</asp:ListItem>
                                                                  <asp:ListItem>Julio</asp:ListItem>
                                                                  <asp:ListItem>Agosto</asp:ListItem>
                                                                  <asp:ListItem>Septiembre</asp:ListItem>
                                                                  <asp:ListItem>Octubre</asp:ListItem>
                                                                  <asp:ListItem>Noviembre</asp:ListItem>
                                                                  <asp:ListItem>Diciembre</asp:ListItem>
                                                              </asp:DropDownList></td>
                                                      </tr>
                                                    </table>
                                                    <br />
                                                </asp:View>
                                                <asp:View ID="View1" runat="server">
                                                </asp:View>
                                                <asp:View ID="View2" runat="server">
                                                </asp:View>
                                            </asp:MultiView></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                            
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                <asp:CheckBox ID="valida" runat="server" Text="Validar Automaticamente" Width="248px" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center;">
                    <asp:CheckBox ID="ch5" runat="server" /></td>
                <td style="width: 100px; text-align: left;">
                    <asp:Label ID="Label4" runat="server" Text="Dependencias"></asp:Label><br />
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                    <asp:RadioButtonList ID="areas" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="Gerencia">Gerencia</asp:ListItem>
                        <asp:ListItem>Departamento</asp:ListItem>
                        <asp:ListItem>Coordinacion</asp:ListItem>
                        <asp:ListItem>Todos</asp:ListItem>
                    </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                    <asp:DropDownList ID="dr_areas" runat="server">
                    </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="arearequerida" runat="server" ControlToValidate="dr_areas"
                        Display="None" ErrorMessage="<b>Area Requerida</b><b" ValidationGroup="val"></asp:RequiredFieldValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" EnableViewState="False" TargetControlID="arearequerida">
        </cc1:ValidatorCalloutExtender>
                </td>
            </tr>
        </table>
                                        <asp:Button ID="btn_actualiza" runat="server" Text="Actualizar" /><br />
               
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/ajax-loader.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                                        &nbsp; &nbsp;<br />
                            
                  
        <asp:Button ID="b_guardar" runat="server" Text="Guardar Reunion" ValidationGroup="val" Visible="False"  /><br />
                    <cc2:msgbox id="MsgBox1" runat="server"></cc2:msgbox>
                                        &nbsp;
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="valduracion">
                    </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="VFecha" runat="server" TargetControlID="cfecha">
                    </cc1:ValidatorCalloutExtender>
                                    </asp:View>
                                </asp:MultiView></td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        
            <tr>
                <td colspan="5" style="height: 16px">             
                    
                </td>
            </tr>
            <tr>
                <td style="height: 28px;" colspan="5">
                    </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 26px;">
                    
                </td>
            </tr>
            <tr>
                <td style="height: 28px; text-align: center;" colspan="5">
                    </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 28px; text-align: center;">
                    </td>
            </tr>
            <tr>
                <td colspan="5" style="height: 28px">
                    
        
       
    </td>
            </tr>
        </table>
     
    
    </div>
        <cc1:ModalPopupExtender 
        ID="ModalPopupExtender1" 
        runat="server" 
        TargetControlID="rnion_exis" 
        PopupControlID="Panel2" 
        BackgroundCssClass="modalBackground" 
        DropShadow="true"
        CancelControlID="btn_cancel"        
        >
        </cc1:ModalPopupExtender>
        
        
     <asp:Panel ID="Panel2" runat="server" Style="display: none" Width="519px"  CssClass="Popups_clases">
            <asp:Panel ID="Panel4" runat="server" Width="100%" Height="25px" CssClass="EncTabla2">
                <div>
                    <p style="text-align: center">
                        <asp:Label ID="Label10" runat="server" Text="Reuniones Existentes"></asp:Label>
                    </p>
                </div>
            </asp:Panel>
       
         <table border="0" cellpadding="0" cellspacing="0" style="width: 117%"  >
             <tr>
                 <td style="width: 100px; text-align: center">
     
         <asp:UpdatePanel ID="UpdatePanel9" runat="server">
             <ContentTemplate>
            
              <p style="text-align: center">
         <asp:GridView ID="grid1" runat="server"  AllowPaging="True" AllowSorting="True"  SkinID="Gvw_little">
             <Columns>
                 <asp:BoundField DataField="Id" HeaderText="Id">
                     <ItemStyle BackColor="#FF8080" />
                 </asp:BoundField>
                 <asp:BoundField DataField="Nombre" HeaderText="Nombre Reunion" />
             </Columns>             
         </asp:GridView>
                </p>  
             </ContentTemplate>
         </asp:UpdatePanel>
                 </td>
             </tr>
             <tr>
                 <td style="width: 100px; text-align: center">
            
                 <asp:Button ID="btn_cancel" runat="server" Text="Cerrar" CssClass="Botones" /></td>
             </tr>
         </table>
         
         
     </asp:Panel>
        
        
       
    </form>
</body>
</html>
