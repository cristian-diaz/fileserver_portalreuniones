﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="parameter_Report.aspx.vb" Inherits="parameter_Report" Theme="SkinCompromisos"  StylesheetTheme="SkinCompromisos" %>

<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>REPORTES</title>

</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager> 
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 100px">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 432px" >
                    <tr>
                        <td style="height: 143px; width: 433px;" >
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="width: 63px; height: 20px; text-align: left">
                                    <asp:CheckBox ID="opt1" runat="server" Text="Tipo de Reunion:" Width="183px" /></td>
                                <td style="width: 100px; height: 20px; text-align: left;">
                                    <asp:DropDownList ID="dr_item" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td style="width: 63px; text-align: left; height: 20px;">
                                    <asp:CheckBox ID="opt2" runat="server" Text="Nombre de Reunion:" Width="213px" /></td>
                                <td style="width: 100px; height: 20px; text-align: left;">
                                    <asp:DropDownList ID="dr_nombre" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td style="width: 63px; height: 20px; text-align: left;">
                                    <asp:CheckBox ID="opt_dp" runat="server" Text="Dependencia:" /></td>
                                <td style="width: 100px; height: 20px; text-align: left;">
                                    <asp:DropDownList ID="dr_dependencia" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td style="width: 63px; text-align: left;">
                                    <asp:CheckBox ID="opt3" runat="server" Text="Hora Inicio Planeada:" Width="195px" /></td>
                                <td style="width: 100px; text-align: left;">
                                    
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:DropDownList ID="i_h" runat="server" Width="41px">
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="width: 100px">
                                                <asp:DropDownList ID="i_m" runat="server">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="width: 100px">
                                                <asp:DropDownList ID="i_hh" runat="server">
                                                    <asp:ListItem>a.m.</asp:ListItem>
                                                    <asp:ListItem>p.m.</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 63px; text-align: left">
                                    <asp:CheckBox ID="opt4" runat="server" Text="Hora fin Planeada:" Width="118px" /></td>
                                <td style="width: 100px; text-align: left">
                                
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:DropDownList ID="f_h" runat="server" Width="41px">
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="width: 100px">
                                                <asp:DropDownList ID="f_m" runat="server">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="width: 100px">
                                                <asp:DropDownList ID="f_hh" runat="server">
                                                    <asp:ListItem>a.m.</asp:ListItem>
                                                    <asp:ListItem>p.m.</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                                    <br />
                            </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
                    </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Button ID="Button1" runat="server" Text="Ver Reporte" /></td>
            </tr>
        </table>
        <br />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 100px">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="50" SkinID="GvwEvaluacion">
                <Columns>
                    <asp:BoundField DataField="nombre" HeaderText="Nombre Reunion">
                     <ItemStyle HorizontalAlign="Left" />
                     <ItemStyle ForeColor="Green" />
                      </asp:BoundField > 
                    <asp:BoundField DataField="NombreReunion" HeaderText="Tipo Reunion">
                      <ItemStyle HorizontalAlign="Left" />                   
                      </asp:BoundField > 
                    <asp:BoundField DataField="horaInicio" HeaderText="Hora Inicio" DataFormatString="{0:hh:mm:ss}" HtmlEncode="False" >
                      <ItemStyle HorizontalAlign="Left" />                 
                      </asp:BoundField > 
                    <asp:BoundField DataField="horaFin" HeaderText="Hora Fin" DataFormatString="{0:hh:mm:ss}" HtmlEncode="False" >
                      <ItemStyle HorizontalAlign="Left" />                   
                      </asp:BoundField > 
                  </Columns>        
               
                </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <uc1:logo ID="Logo1" runat="server" />
        <br />
        &nbsp;<%--   'aqui--%>
   
   
   
        <div style="z-index: 101; left: 504px; width: 100px; position: absolute; top: 153px;
            height: 100px">
        
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                               <table border="0" cellpadding="0" cellspacing="0" style="font-weight: bold; width: 100%;
                    color: green; font-family: Arial; text-align: center; background-color: white;" >
                    <tr>
                        <td style="width: 100px; text-align: center;">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/img/ajax-loader.gif" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; text-align: center; height: 16px;">
                            <asp:Label ID="msg_cargar" runat="server" Text="Cargando Datos ..." Width="181px"></asp:Label></td>
                    </tr>                 
                </table>
            </ProgressTemplate>
        </asp:UpdateProgress>
        </div>
        <cc2:MsgBox ID="MsgBox1" runat="server" />
      
         
        
    </form>
</body>
</html>
