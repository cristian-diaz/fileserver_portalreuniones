﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Admon_frm_doc_Compromisos.aspx.vb" Inherits="Admon_frm_doc_Compromisos" Theme="SkinCompromisos" StylesheetTheme="SkinCompromisos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>

<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >

    <title>Página sin título</title>
</head>
<body>
    <form id="form1" runat="server">
        &nbsp;<asp:Label ID="LBL_USUARIO" runat="server"></asp:Label>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <div style="width: 100%; position: static; height: 48px; text-align: center">
            <span style="font-family: Arial"><strong><span style="color: #cc0000">Solo Adjuntar
                Archivos tipo PDF</span><br />
            </strong></span>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 139px; height: 20px; text-align: left;">
                        <asp:Label ID="Label1" runat="server" Text="Reunion:"></asp:Label></td>
                    <td colspan="2" style="text-align: left; height: 20px;">
        <asp:DropDownList ID="dr_listareuniones" runat="server" Width="544px" AutoPostBack="True">
        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 139px; text-align: left; height: 68px;">
                        <asp:Label ID="Label4" runat="server" Text="Tipo de Documento:"></asp:Label></td>
                    <td style="width: 100px; text-align: left; height: 68px;">
                        <asp:RadioButtonList ID="rb_tpdocumentp" runat="server" RepeatDirection="Horizontal" AppendDataBoundItems="True">
                            <asp:ListItem Value="ACTA">Acta</asp:ListItem>
                            <asp:ListItem Value="REGLAM">Reglamento</asp:ListItem>
                            <asp:ListItem Value="AGEND">Agenda</asp:ListItem>
                        </asp:RadioButtonList>
                        <cc2:ValidatorCalloutExtender ID="val_tpdocumento" runat="server" TargetControlID="RequiredFieldValidator1">
                        </cc2:ValidatorCalloutExtender>
                      
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rb_tpdocumentp"
                            Display="None" ErrorMessage="Se requiere Tipo Documento" ValidationGroup="val"></asp:RequiredFieldValidator></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 139px; height: 19px; text-align: left;">
                        <asp:Label ID="Label2" runat="server" Text="Adjuntar:"></asp:Label></td>
                    <td style="width: 100px; height: 19px;">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="416px" AlternateText="Usted"  /></td>
                    <td style="width: 100px; height: 19px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px">
                        <asp:Label ID="Label3" runat="server" Width="448px"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px; text-align: center">
                        &nbsp;<asp:Button ID="Button1" runat="server" Text="Adjuntar" /></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px; text-align: center">
          
    
     <asp:GridView ID="grVw0" runat="server" SkinID="Gvw_little" AutoGenerateColumns="False" Width="424px" AllowPaging="True" AllowSorting="True" DataKeyNames="nombre" PageSize="20" HorizontalAlign="Center" >
                    <Columns>
                        <asp:TemplateField HeaderText="Documentos Asociados a la Reunion">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("nombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_actividad1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("nombre") %>'
                                    CommandName="Select" Text='<%# Bind("nombre") %>'>
                                </asp:LinkButton>  
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" /> 
                        </asp:TemplateField>                                        
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                              <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Delete" ImageUrl="~/img/delete.png"  ToolTip="Eliminar Archivo" OnClientClick="Javascript:return confirm ( 'Esta seguro que quiere eliminar el Archivo ') " />
                            </ItemTemplate>
                        </asp:TemplateField>
                                         
                        
                    </Columns>
               
                </asp:GridView>
                    </td>
                </tr>
            </table>
          
      
    <uc1:logo ID="Logo1" runat="server" />
            <cc1:msgbox id="MsgBox1" runat="server"></cc1:msgbox>
           
        </div>
      
    </form>
</body>
</html>
