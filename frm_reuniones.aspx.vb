﻿Imports AjaxControlToolkit
Imports System.Data
Imports System.Data.OleDb
Partial Class frm_reuniones
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            prcCrearDataset()
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")
        End If
    End Sub


    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion") 'combo de reuniones 


    End Sub
#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region


    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "reuniones"
                Me.ds_evalua.Tables("dtt_reunion").Clear()
                'Me.evalu.Reuniones_ttal()
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.evalu.DatasetFill.Tables(0))
        End Select

    End Sub
    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Me.grid1.DataSource = Me.ds_evalua.Tables("dtt_reunion")
                Me.grid1.DataBind()
        End Select
    End Sub

    Protected Sub grid1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grid1.PageIndexChanging
        Me.grid1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("reuniones")
    End Sub
End Class
