<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BusyBoxAjax.ascx.vb" Inherits="BusyBoxAjax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Panel ID="PnlProgress" runat="server" Style="display: none" Width="300px">
    <table style="border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid;
        border-bottom: black 1px solid; background-color: white" width="100%">
        <tr>
            <td align="left" style="width: 5px; height: 10px">
            </td>
            <td align="left" style="width: 25px; height: 10px">
            </td>
            <td align="left" style="width: 265px; height: 10px">
            </td>
            <td align="left" style="width: 5px; height: 10px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 5px; height: 30px">
            </td>
            <td align="left" style="width: 25px; height: 30px">
                <asp:Image ID="ImgProgress" runat="server" ImageUrl="~/img/ajax-loader.gif"/></td>
            <td align="left" style="width: 265px; height: 30px">
                &nbsp;<asp:Label ID="LblTituloAccion" runat="server" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="11pt">Ejecutando..</asp:Label></td>
            <td align="left" style="width: 5px; height: 30px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 5px; height: 30px">
            </td>
            <td align="left" style="width: 25px; height: 30px">
            </td>
            <td align="left" style="width: 265px; height: 30px">
                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="8pt">Estamos procesando su solicitud. Por favor espere un momento</asp:Label></td>
            <td align="left" style="width: 5px; height: 30px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 5px; height: 10px">
            </td>
            <td align="left" style="width: 25px; height: 10px">
            </td>
            <td align="left" style="width: 265px; height: 10px">
            </td>
            <td align="left" style="width: 5px; height: 10px">
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Label ID="LblProgress" runat="server"></asp:Label><cc2:modalpopupextender id="MpeProgress"
    runat="server" backgroundcssclass="ProgressBackground" behaviorid="mpp" enabled="True"
    popupcontrolid="PnlProgress" targetcontrolid="LblProgress"> </cc2:modalpopupextender>
