﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Reflection
Partial Class parameter_Report
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            Me.prcCrearDataset()
            'CARGA TIPO DE REUNIONES
            Me.prcCargarOrigen("reunion")
            Me.PrcCargarDatos("reunion")

            ''CARGA NOMBRE DE REUNIONES
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")

            'CARGA NOMBRE DEPENDENCIAS
            Me.prcCargarOrigen("areass")
            Me.PrcCargarDatos("areass")


       



        End If

    End Sub



#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Me.prcCargarOrigen("consulta_reunion")
        Me.PrcCargarDatos("consulta_reunion")


    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Me.GridView1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("consulta_reunion")
    End Sub



#Region "PROCEDIMIENTOS"
    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable   '--0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("tp_reuniones")
        Me.ds_evalua.Tables.Add("nm_reuniones")
        Me.ds_evalua.Tables.Add("nm_dependencias")
        Me.ds_evalua.Tables.Add("c_reunion")

        'EVALUACIONES
        Me.ds_evalua.Tables.Add("e_reunion")
        Me.ds_evalua.Tables.Add("dt_1")
        Me.ds_evalua.Tables.Add("dt_2")


        Me.ds_evalua.AcceptChanges()


    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto

            Case "reunion"
                Try
                    Me.evalu.Tp_Reunion()
                    Me.ds_evalua.Tables("tp_reuniones").Merge(Me.evalu.DatasetFill.Tables(0))
                Catch ex As Exception
                    'Me.TextBox1.Text = Me.evalu.ERRORR
                End Try

            Case "reuniones"
                Me.evalu.name_Reunion()
                Me.ds_evalua.Tables("nm_reuniones").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "consulta_reunion"
                Try
                    Dim v_treunion As String = Me.dr_item.SelectedValue
                    Dim v_nombre As String = Me.dr_nombre.SelectedItem.Text
                    Dim v_inicio As String = Me.i_h.SelectedValue & ":" & Me.i_m.SelectedValue & " " & Me.i_hh.SelectedValue
                    Dim v_fin As String = Me.f_h.SelectedValue & ":" & Me.f_m.SelectedValue & " " & Me.f_hh.SelectedValue
                    Dim v_dependencia As String = Me.dr_dependencia.SelectedValue


                    Me.ds_evalua.Tables("c_reunion").Clear()
                    Me.evalu.Reportes_Reunion(v_treunion, v_nombre, v_dependencia, v_inicio, v_fin, Me.opt1.Checked, Me.opt2.Checked, Me.opt_dp.Checked, Me.opt3.Checked, Me.opt4.Checked)
                    Me.ds_evalua.Tables("c_reunion").Merge(Me.evalu.DatasetFill.Tables(0))

                Catch ex As Exception
                    '  Me.MsgBox1.ShowMessage("ERROR" & ex.Message)
                    Me.MsgBox1.ShowMessage("No hay Datos para este Criterio de Busqueda.")
                Finally
                    '  Me.MsgBox1.ShowMessage("REUNION GUARDANDO")
                End Try
            Case "areass"
                Me.evalu.Trae_areas()
                Me.ds_evalua.Tables("nm_dependencias").Merge(Me.evalu.DatasetFill.Tables(0))

        End Select
    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reunion"
                Me.dr_item.DataSource = Me.ds_evalua.Tables("tp_reuniones").CreateDataReader
                Me.dr_item.DataTextField = "NombreReunion"
                Me.dr_item.DataValueField = "Id"
                Me.dr_item.DataBind()

            Case "reuniones"
                Me.dr_nombre.DataSource = Me.ds_evalua.Tables("nm_reuniones").CreateDataReader
                Me.dr_nombre.DataTextField = "Nombre"
                Me.dr_nombre.DataValueField = "Id"
                Me.dr_nombre.DataBind()


            Case "consulta_reunion"
                Me.GridView1.DataSource = Me.ds_evalua.Tables("c_reunion")
                Me.GridView1.DataBind()

            Case "areass"
                Me.dr_dependencia.DataSource = Me.ds_evalua.Tables("nm_dependencias").CreateDataReader
                Me.dr_dependencia.DataTextField = "NombreDependencia"
                Me.dr_dependencia.DataValueField = "id"
                Me.dr_dependencia.DataBind()


        End Select
    End Sub

#End Region

 
  
End Class
