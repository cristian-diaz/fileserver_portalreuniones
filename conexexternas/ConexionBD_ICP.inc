<% 
Dim PaginasWeb, Gerencia, cadenaConexionDBQ, PathUpFiles

PaginasWeb = "www.gcb/gcbCompromisos"
Gerencia = "GRB"
PathUpFiles = "d:\Intranet\DataBase\Compromisos\Archivos\GCB"
'cadenaConexionDBQ = "driver={Microsoft Access Driver (*.mdb)};dbq=C:\Inetpub\wwwroot\compromisos\BD\Compromisos.mdb;Uid=Administrador;Pwd=Compromisos.gcb;"

cadenaConexionDBQ = "driver={Microsoft Access Driver (*.mdb)};dbq=\\bgaevaapps30p\$ICP\BD\Compromisos.mdb;Uid=Administrador;Pwd=Compromisos.gcb;"

'cadenaConexionDBQ = "driver={Microsoft Access Driver (*.mdb)};dbq=D:\Intranet\DataBase\Compromisos\Compromisos.mdb;Uid=Administrador;Pwd=Compromisos.gcb;"


 Function AbrirConexionBD(strlConexion)
  Dim objlConexion
  If strlConexion="" Then
   Set AbrirConexionBD=Nothing
  Else
   On Error Resume Next
   Set objlConexion=CreateObject("ADODB.Connection")
   objlConexion.Open strlConexion
   If Err Then
    Set AbrirConexionBD=Err
   Else
    Set AbrirConexionBD=objlConexion
   End If  
  End If
 End Function
 
 Function CerrarConexionBD(ObjlConexion)
  On Error Resume Next
  objlConexion.Close
  Set objlConexion=Nothing
  If Err Then
   CerrarConexionBD=False
  Else
   CerrarConexionBD=True
  End If
 End Function

Function AbrirRecordset(objlConexion, strlSQL)
   On Error Resume Next
   Dim objRecordset
   Set objRecordset=CreateObject("ADODB.Recordset")
   objRecordset.Open strlSQL, objlConexion,3,3
   If Err Then
    Set AbrirRecordset=Err
   Else
    Set AbrirRecordset=objRecordset
   End If  
 End Function
 
Function CerrarRecordset(objRecordset)
   On Error Resume Next
   objRecordset.close
   Set objRecordset=Nothing
   If Err Then
    CerrarRecordset=false
   Else
    CerrarRecordset=true
   End If  
 End Function

Function AbrirRecordsetOpSt(objlConexion, strlSQL)
   On Error Resume Next
   Dim objRecordset
   Set objRecordset=CreateObject("ADODB.Recordset")
   objRecordset.Open strlSQL, objlConexion,3,1
   If Err Then
    Set AbrirRecordsetOpSt=Nothing
   Else
    Set AbrirRecordsetOpSt=objRecordset
   End If  
End Function
%>