// ** I18N
Calendar._DN = new Array
("Domingo",
 "Lunes",
 "Martes",
 "Miercoles",
 "Jueves",
 "Viernes",
 "Sabado",
 "Domingo");
Calendar._MN = new Array
("Enero",
 "Febrero",
 "Marzo",
 "Abril",
 "Mayo",
 "Junio",
 "Julio",
 "Agosto",
 "Septiembre",
 "Octubre",
 "Noviembre",
 "Deciembre");

// textos de los tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Cambiar Primer Dia Semana";
Calendar._TT["PREV_YEAR"] = "A�o Anterior (Pulse para ver menu)";
Calendar._TT["PREV_MONTH"] = "Mes Anterior (Pulse para ver menu)";
Calendar._TT["GO_TODAY"] = "Hoy";
Calendar._TT["NEXT_MONTH"] = "Proximo Mes (Pulse para ver menu)";
Calendar._TT["NEXT_YEAR"] = "Proximo A�o (Pulse para ver menu)";
Calendar._TT["SEL_DATE"] = "Elegir Fecha";
Calendar._TT["DRAG_TO_MOVE"] = "Drag para Mover";
Calendar._TT["PART_TODAY"] = " (today)";
Calendar._TT["MON_FIRST"] = "Mostrar Lunes Primero";
Calendar._TT["SUN_FIRST"] = "Mostrar Domingo Primero";
Calendar._TT["CLOSE"] = "Cerrar";
Calendar._TT["TODAY"] = "Hoy";

// formatos de fecha
Calendar._TT["DEF_DATE_FORMAT"] = "dd/MM/y";
Calendar._TT["TT_DATE_FORMAT"] = "D, d de M";

Calendar._TT["WK"] = "wk";
