﻿Imports System.Data
Imports System.Data.OleDb

Imports System.IO
Imports System.Diagnostics
Imports System.Windows
Partial Class frm_doc_Compromisos
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Dim usr As String = HttpContext.Current.User.Identity.Name



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.prcCrearDataset()

            Me.prcCargarOrigen("reuniones") ' cargar los combos
            Me.PrcCargarDatos("reuniones")


            'Me.prcCargarOrigen("archives")
            'Me.PrcCargarDatos("archives")


        End If
    End Sub

    Public Property str_Oopcion() As String
        Get
            Return ViewState("LOopcion")
        End Get
        Set(ByVal value As String)
            ViewState("LOopcion") = value
        End Set
    End Property


    Public Sub prcCrearDataset()
        'TABLAS
        Dim ds As New DataSet
        Dim dtt_archivos As New DataTable '--0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_archivos")
        Me.ds_evalua.Tables.Add("dtt_reuniones")
        Me.ds_evalua.Tables.Add("dtt_rutas")

    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "archives"
                ' str_Oopcion = 493
                Me.ds_evalua.Tables("dtt_archivos").Clear()
                Me.evalu.Trae_archivos(Me.dr_listareuniones.SelectedValue)
                Me.ds_evalua.Tables("dtt_archivos").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "reuniones"
                Dim reg_user As String = Replace(usr, "ECOPETROL\", "")
                Me.ds_evalua.Tables("dtt_reuniones").Clear()
                Me.evalu.reunion_moderador(reg_user)
                Me.ds_evalua.Tables("dtt_reuniones").Merge(Me.evalu.DatasetFill.Tables(0))

        End Select
    End Sub

    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Public Sub PrcCargarArchivos(ByVal n_archive As String)
        Dim link As HyperLink = New HyperLink

        Me.ds_evalua.Tables("dtt_rutas").Clear()
        Me.evalu.Trae_DatosReunionPuntual(n_archive)
        Me.ds_evalua.Tables("dtt_rutas").Merge(Me.evalu.DatasetFill.Tables(0))


        Dim t_archive As DataRow = Me.ds_evalua.Tables("dtt_rutas").Rows(0)
        Dim carpeta As String = Convert.ToString(t_archive("ruta"))

    

        Dim URL As String = "ActasReunion/" & carpeta & "/" & n_archive

        Dim popupScript As String = "<script language='JavaScript'>" & _
                                            "window.open('" & URL & "', 'CustomPopUp', " & _
                                            "'dependent=yes,toolbar=yes,scrollbars=yes,resizable=yes,status=yes,left=120,Top=20')" & _
                                            "</script>"
        Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)





    End Sub

 

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "archives"

                Me.grVw0.DataSource = Me.ds_evalua.Tables("dtt_archivos")
                Me.grVw0.DataBind()

            Case "reuniones"
                Me.dr_listareuniones.DataSource = Me.ds_evalua.Tables("dtt_reuniones").CreateDataReader
                Me.dr_listareuniones.DataTextField = "Nombre"
                Me.dr_listareuniones.DataValueField = "id"
                Me.dr_listareuniones.DataBind()
                Me.dr_listareuniones.Items.Insert(0, New ListItem("--------Seleccione Nombre de Reunion-------", ""))

        End Select

    End Sub

    Protected Sub grVw0_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grVw0.PageIndexChanging
        Me.grVw0.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("archives")
    End Sub

    
    Protected Sub grVw0_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grVw0.SelectedIndexChanged
        Me.PrcCargarArchivos(Me.grVw0.SelectedValue)
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim usuario As String = Replace(usr, "ECOPETROL\", "")
        Dim fecha_actual As String = Format(CDate(Now), "ddMMyyyy")
        Dim id_reunion As String = Me.dr_listareuniones.SelectedValue
        Dim directorio As DirectoryInfo = Directory.CreateDirectory(Server.MapPath("~/ActasReunion/" & fecha_actual & "_" & usuario & "_ReunionNo_" & id_reunion))

        If FileUpload1.HasFile Then
            Try
                Me.FileUpload1.SaveAs(Server.MapPath("~/ActasReunion/" & fecha_actual & "_" & usuario & "_ReunionNo_" & id_reunion & "/" & FileUpload1.FileName))

                'GUARDAR EN LA BASE DE DATOS LA INFORMACION DE LOS ARCHIVOS
                Dim n_archivo As String = FileUpload1.FileName
                Dim ruta As String = fecha_actual & "_" & usuario & "_ReunionNo_" & id_reunion
                Me.evalu.Guardar_Info_archivos(id_reunion, n_archivo, ruta, usuario, "ACT_MODERADOR")

                Me.prcCargarOrigen("archives")
                Me.PrcCargarDatos("archives")


            Catch ex As Exception
                Label3.Text = "Error: " & ex.Message.ToString()
            End Try
        Else
            Me.MsgBox1.ShowMessage("Especifique un Archivo a cargar, por favor.")


        End If



    End Sub
   

    Protected Sub dr_listareuniones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dr_listareuniones.SelectedIndexChanged
        Me.prcCargarOrigen("archives")
        Me.PrcCargarDatos("archives")
    End Sub

  
    Protected Sub grVw0_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles grVw0.SelectedIndexChanging
   
    End Sub
End Class
