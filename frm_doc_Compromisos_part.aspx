﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_doc_Compromisos_part.aspx.vb" Inherits="frm_doc_Compromisos_part" Theme="SkinCompromisos" StylesheetTheme="SkinCompromisos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>
<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>REVISION DE DOCUMENTOS ASOCIADOS LILIA </title>
</head>
<body style="margin-bottom:0; margin-left:0; margin-top:0">
    <form id="form1" runat="server">    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; background-image: url(img/barra2.JPG);">
            <tr>
                                  <td style="color: white; font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG); font-family: Verdana,Arial,Helvetica,sans-serif; height: 18px; text-align: center;" colspan="6">
                Reporte de Documentos Por Reunion</td>    
                        
            </tr>
        </table>
        <div style="width: 100%; position: static; height: 48px; text-align: center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 100px; height: 20px; text-align: left;">
                        </td>
                    <td colspan="2" style="text-align: center; height: 20px;">
                        <br />
                        <asp:Label ID="Label2" runat="server" SkinID="lbl_encabezados" Text="Elija la Reunion para ver Documentos"></asp:Label><br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; text-align: left;">
                        <asp:Label ID="Label1" runat="server" Text="Reunion:"></asp:Label></td>
                    <td colspan="2" style="text-align: left">
        <asp:DropDownList ID="dr_listareuniones" runat="server" Width="544px" AutoPostBack="True">
        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 19px;">
                    </td>
                    <td style="width: 100px; height: 19px;">
                        <asp:Label ID="Label3" runat="server" Width="448px"></asp:Label></td>
                    <td style="width: 100px; height: 19px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px">
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px; text-align: center">
          
    
     <asp:GridView ID="grVw0" runat="server" SkinID="Gvw_little" AutoGenerateColumns="False" Width="656px" AllowPaging="True" AllowSorting="True" DataKeyNames="nombre" PageSize="20" HorizontalAlign="Center" >
                    <Columns>
                        <asp:TemplateField HeaderText="Documentos Asociados a la Reunion">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("nombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_actividad1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("nombre") %>'
                                    CommandName="Select" Text='<%# Bind("nombre") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>                                        
                                         
                        
                    </Columns>
               
                </asp:GridView>
                        <br />
                    </td>
                </tr>
            </table>
            &nbsp;&nbsp;
      
    <uc1:logo ID="Logo1" runat="server" />
        <cc1:MsgBox ID="MsgBox1" runat="server" />
            &nbsp;&nbsp;&nbsp; &nbsp;
        </div>
      
    </form>
</body>
</html>
