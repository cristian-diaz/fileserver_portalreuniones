﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_edicionreuniones.aspx.vb" Inherits="frm_edicionreuniones" StylesheetTheme="SkinCompromisos" Theme="SkinCompromisos" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>EDICION REUNIONES</title>
</head>
<body style="border-top:0px">
    <form id="form1" runat="server">
           <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="8" style="background-color: white">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="img/ecoep.gif" /></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 19px">
                    <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label></td>
                <td colspan="6" style="height: 19px">
                    <asp:TextBox ID="txt_nombre" runat="server" Width="440px" Font-Size="Smaller"></asp:TextBox></td>
                <td style="width: 94px; height: 19px">
                    <asp:HiddenField ID="hf_name" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 19px;">
                    <asp:Label ID="Label2" runat="server" Text="Tipo Reunion:" Width="96px"></asp:Label></td>
                <td colspan="2" style="height: 19px">
                    <asp:DropDownList ID="dr_reunion" runat="server">
                    </asp:DropDownList></td>
                <td style="width: 100px; height: 19px;">
                </td>
                <td style="width: 100px; height: 19px;">
                </td>
                <td style="width: 100px; height: 19px;">
                </td>
                <td style="width: 100px; height: 19px;">
                </td>
                <td style="width: 94px; height: 19px;">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 19px">
                    <asp:Label ID="Label8" runat="server" Text="Validar Asistentes:" Width="128px"></asp:Label>
                    <asp:CheckBox ID="ch_valida" runat="server" /></td>
                <td style="width: 100px; height: 19px">
                </td>
                <td style="width: 100px; height: 19px">
                </td>
                <td style="width: 100px; height: 19px">
                </td>
                <td style="width: 100px; height: 19px">
                </td>
                <td style="width: 94px; height: 19px">
                </td>
            </tr>
            <tr>
                <td style="height: 12px" colspan="8">
                 
                    <table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="width: 100px">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 544px">
                                    <tr>
                                        <td style="width: 100px">
                    <asp:Label ID="Label3" runat="server" Text="Inicio"></asp:Label></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" Text="Fin:"></asp:Label></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                                <asp:DropDownList ID="i_h" runat="server" Width="41px">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="i_m" runat="server" Width="41px">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="i_hh" runat="server" Width="48px">
                                                </asp:DropDownList></td>
                                        <td colspan="4">
                  <asp:DropDownList ID="f_h" runat="server" Width="41px">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="f_m" runat="server" Width="41px">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="f_hh" runat="server" Width="48px">
                                                </asp:DropDownList></td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="Label6" runat="server" Text="Frecuencia:"></asp:Label>
        <asp:LinkButton ID="LinkButton1" runat="server" Font-Names="Arial" Font-Overline="False" Font-Size="Smaller" Font-Underline="False" ForeColor="DarkOliveGreen">.</asp:LinkButton>
                    <asp:RadioButtonList ID="frecuencia" runat="server" AutoPostBack="True" 
                        RepeatDirection="Horizontal" Width="424px" ValidationGroup="val" ToolTip="Escoja Frecuencia">
                        <asp:ListItem Value="1">Diaria Por turno</asp:ListItem>
                        <asp:ListItem Value="2">Diaria</asp:ListItem>
                        <asp:ListItem Value="3">Semanal</asp:ListItem>
                        <asp:ListItem Value="4">Mensual</asp:ListItem>
                        <asp:ListItem Value="5">Anual</asp:ListItem>
                    </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="req_frecuencia" runat="server" ControlToValidate="frecuencia"
                                                Display="None" ErrorMessage="Se Requiere Programar Una Frecuencia" ValidationGroup="val"></asp:RequiredFieldValidator>
                                            <cc1:ValidatorCalloutExtender ID="validafrecuencia" runat="server" TargetControlID="req_frecuencia">
                                            </cc1:ValidatorCalloutExtender>
                    <div style="border-right: activeborder thin solid; border-top: activeborder thin solid;
                        border-left: activeborder thin solid; width: 99%; border-bottom: activeborder thin solid;
                        position: static; height: 100px; background-color: #d0eaae">
 
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="semanal" runat="server" EnableTheming="False">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="width: 100px; height: 25px">
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 275px">
                                            <tr>
                                                <td style="width: 85px; height: 24px;">
                                                    <asp:Label ID="Label16" runat="server" Text="Repetir cada:" Width="92px"></asp:Label></td>
                                                <td style="width: 44px; height: 24px;">
                                                    <asp:TextBox ID="txt_sem" runat="server" Width="29px"></asp:TextBox></td>
                                                <td style="width: 100px; height: 24px;">
                                                    <asp:Label ID="Label17" runat="server" Text="Semanas el:" Width="91px"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <asp:RadioButtonList ID="opt_semanal" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Lunes</asp:ListItem>
                                <asp:ListItem>Martes</asp:ListItem>
                                <asp:ListItem>Miercoles</asp:ListItem>
                                <asp:ListItem>Jueves</asp:ListItem>
                                <asp:ListItem>Viernes</asp:ListItem>
                                <asp:ListItem>Sabado</asp:ListItem>
                                <asp:ListItem>Domingo</asp:ListItem>
                            </asp:RadioButtonList></asp:View>
                        <asp:View ID="mensual" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 712px">
                                <tr>
                                    <td rowspan="2" style="width: 64px">
                                        <asp:RadioButtonList ID="opt_mensual" runat="server" Width="112px">
                                            <asp:ListItem Value="1">El dia</asp:ListItem>
                                            <asp:ListItem Value="2">El</asp:ListItem>
                                            <asp:ListItem Value="3">No determinado</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td rowspan="2" style="width: 100px">
                                        <div style="width: 424px; position: static; height: 16px">
                                        <asp:TextBox ID="txt_ms1" runat="server" Height="12px" Width="27px"></asp:TextBox><asp:Label
                                            ID="Label13" runat="server" Text="de cada" Width="47px"></asp:Label>
                                        <asp:TextBox ID="txt_Mensual" runat="server" Height="12px" Width="27px"></asp:TextBox><asp:Label
                                            ID="Label11" runat="server" Text="meses"></asp:Label></div>
                                        <div style="width: 424px; position: static; height: 16px">
                                        <asp:DropDownList ID="dr_ms1" runat="server">
                                            <asp:ListItem Value="Primer">Primer</asp:ListItem>
                                            <asp:ListItem>Segundo</asp:ListItem>
                                            <asp:ListItem>Tercer</asp:ListItem>
                                            <asp:ListItem>Cuarto</asp:ListItem>
                                            <asp:ListItem>último</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="dr_ms2" runat="server">
                                            <asp:ListItem>Lunes</asp:ListItem>
                                            <asp:ListItem>Martes</asp:ListItem>
                                            <asp:ListItem>Miercoles</asp:ListItem>
                                            <asp:ListItem>Jueves</asp:ListItem>
                                            <asp:ListItem>Viernes</asp:ListItem>
                                            <asp:ListItem>Sabado</asp:ListItem>
                                            <asp:ListItem>Domingo</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="Label12" runat="server" Text="de Cada"></asp:Label>
                                        <asp:TextBox ID="txt_ms3" runat="server" Height="12px" Width="27px"></asp:TextBox><asp:Label
                                            ID="txt_ms2" runat="server" Text="Meses"></asp:Label></div>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td rowspan="1" style="width: 64px">
                                    </td>
                                    <td rowspan="1" style="width: 100px">
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                        </asp:View>
                        <asp:View ID="anual" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 374px">
                                <tr>
                                    <td rowspan="2" style="width: 59px">
                                        <asp:RadioButtonList ID="opt_an" runat="server">
                                            <asp:ListItem Value="1">Cada</asp:ListItem>
                                            <asp:ListItem Value="2">El</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td style="width: 102px; height: 20px; text-align: center">
                                        <asp:TextBox ID="txt_anual" runat="server" Height="12px" Width="27px"></asp:TextBox>
                                        <asp:Label ID="Label15" runat="server" Text="de"></asp:Label></td>
                                    <td style="width: 74px; height: 20px">
                                        <asp:DropDownList ID="dr_anual" runat="server">
                                            <asp:ListItem>Enero</asp:ListItem>
                                            <asp:ListItem>Febrero</asp:ListItem>
                                            <asp:ListItem>Marzo</asp:ListItem>
                                            <asp:ListItem>Abril</asp:ListItem>
                                            <asp:ListItem>Mayo</asp:ListItem>
                                            <asp:ListItem>Junio</asp:ListItem>
                                            <asp:ListItem>Julio</asp:ListItem>
                                            <asp:ListItem>Agosto</asp:ListItem>
                                            <asp:ListItem>Septiembre</asp:ListItem>
                                            <asp:ListItem>Octubre</asp:ListItem>
                                            <asp:ListItem>Noviembre</asp:ListItem>
                                            <asp:ListItem>Diciembre</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="width: 114px; height: 20px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 102px; height: 20px">
                                        <asp:DropDownList ID="dr_an1" runat="server">
                                            <asp:ListItem Value="Primer">Primer</asp:ListItem>
                                            <asp:ListItem>Segundo</asp:ListItem>
                                            <asp:ListItem>Tercer</asp:ListItem>
                                            <asp:ListItem>Cuarto</asp:ListItem>
                                            <asp:ListItem>ultimo</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="width: 74px; height: 20px">
                                        <asp:DropDownList ID="dr_an2" runat="server">
                                            <asp:ListItem>Lunes</asp:ListItem>
                                            <asp:ListItem>Martes</asp:ListItem>
                                            <asp:ListItem>Miercoles</asp:ListItem>
                                            <asp:ListItem>Jueves</asp:ListItem>
                                            <asp:ListItem>Viernes</asp:ListItem>
                                            <asp:ListItem>Sabado</asp:ListItem>
                                            <asp:ListItem>Domingo</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="width: 114px; height: 20px">
                                        <asp:Label ID="Label14" runat="server" Text="de"></asp:Label>
                                        <asp:DropDownList ID="dr_an3" runat="server">
                                            <asp:ListItem>Enero</asp:ListItem>
                                            <asp:ListItem>Febrero</asp:ListItem>
                                            <asp:ListItem>Marzo</asp:ListItem>
                                            <asp:ListItem>Abril</asp:ListItem>
                                            <asp:ListItem>Mayo</asp:ListItem>
                                            <asp:ListItem>Junio</asp:ListItem>
                                            <asp:ListItem>Julio</asp:ListItem>
                                            <asp:ListItem>Agosto</asp:ListItem>
                                            <asp:ListItem>Septiembre</asp:ListItem>
                                            <asp:ListItem>Octubre</asp:ListItem>
                                            <asp:ListItem>Noviembre</asp:ListItem>
                                            <asp:ListItem>Diciembre</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                            </table>
                            <br />
                        </asp:View>
                        <asp:View ID="View1" runat="server">
                            <asp:Label ID="Label7" runat="server" Text="Frecuencia Programacion Diaria" Width="264px"></asp:Label></asp:View>
                   
                    </asp:MultiView></div>
                </td>
            </tr>
            <tr>
                <td style="height: 12px" colspan="8">
                                       <table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="width: 100px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 12px" colspan="8">
                    <table border="1" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="width: 100px">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="width: 100px">
                    <asp:Label ID="Label5" runat="server" Text="Dependencias:"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                    <asp:RadioButtonList ID="opt_dpendencias" runat="server" AppendDataBoundItems="True">
                    </asp:RadioButtonList></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                    <asp:DropDownList ID="dr_dependencia" runat="server">
                    </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 12px">
                </td>
                <td style="width: 100px; height: 12px">
                    </td>
                <td style="width: 100px; height: 12px">
                </td>
                <td style="width: 100px; height: 12px">
                </td>
                <td style="width: 100px; height: 12px">
                </td>
                <td style="width: 100px; height: 12px">
                </td>
                <td style="width: 100px; height: 12px">
                </td>
                <td style="width: 94px; height: 12px">
                </td>
            </tr>
            <tr>
                <td colspan="8" style="height: 12px; text-align: center">
                    <asp:Button ID="guardar" runat="server" Text="Guardar Cambios" /></td>
            </tr>
        </table>
        <br />
 
                    <br />
        <br />
        <br />

        <cc2:MsgBox ID="MsgBox1" runat="server" />   
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <cc1:ModalPopupExtender ID="popup_tx" runat="server"  
         TargetControlID="LinkButton1" 
        PopupControlID="Panel2" 
        BackgroundCssClass="modalBackground" 
        DropShadow="true"
        CancelControlID="btn_cancel"   
        >
        </cc1:ModalPopupExtender>
            
        <asp:Panel ID="Panel2" runat="server" Style="display: none" Width="240px"  CssClass="Popups_clases" BackColor="White" Font-Names="Arial" Font-Size="Smaller" HorizontalAlign="Center">
            <asp:Label ID="Msg_alert" runat="server" SkinID="label_1" Text="Label" ></asp:Label><br />
            <asp:Button ID="btn_cancel" runat="server" Text="Cerrar" /></asp:Panel>
        
    </form>

</body>
</html>
