<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim ObjOptimizacion
Dim ErrorEnviar, CodigoError
Dim ObjL_RecordSet, ObjL2_RecordSet, ObjL3_RecordSet, ObjL4_RecordSet
Dim StrL_SQL, StrL_SQL2,  StrL_Mensaje, Usuarios, Responsables

Set ObjOptimizacion = AbrirConexionBD(cadenaConexionDBQ)

response.write ("Inicia el proceso de envio de correo proximos a vencer.....")

Sub EscribirEnArchivo(strError)
'Procedimiento que Escribe el Log de la Aplicación
Dim strCadena
	On Error Resume Next
	Const ForReading = 1, ForWriting = 2, ForAppending = 8	'Constantes de apertura de archivos
    Const F_FILELOG =  "C:\PortalReunion\LogCorreo\Compromisos2.Log"
	
	Dim fsodata, ffilelog
	
	Set fsodata = CreateObject("Scripting.FileSystemObject")
	If Not fsodata.FileExists(F_FILELOG) Then
		set ffilelog = fsodata.CreateTextFile(F_FILELOG, ForWriting)	
	End If
	
	Select Case strError
		Case "0" : str_Cadena = "Error al Enviar Mail"
		Case "1" : str_Cadena = "Correo Enviado"
		Case "2" : str_Cadena = "No se encontro usuario en el AD"		
		Case Else: str_Cadena = "Verificar si posee Cuenta de Correo"
	End Select
	
	set ffilelog = fsodata.Opentextfile(F_FILELOG,ForAppending, true)
ffilelog.Writeline  Date() & " " & Time() & " " & str_Cadena & " Registro: " & ObjL_RecordSet("LOGIN") &  " Compromiso: " & ObjL_RecordSet("NoID") & ""
End Sub

Function EnviarMail(StrL_Asunto, StrL_Cuerpo, StrL_Destino)

'Función construida para Apiay debido a problemas usando el Componente de CDONTS
Dim Mail, StrL_Cadena, rsObjDirMail
Dim IntL_Posicion, IntL_Sitio, IntL_Longitud, StrL_DestinatarioActual, StrL_CadenaDef
Set Mail = CreateObject("CDO.Message")
	StrL_SQL = "SELECT email, grupo FROM t_logins WHERE Login='" & StrL_Destino & "'"

	set rsObjDirMail = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	If rsObjDirMail.EOF Then
		ErrorEnviar = "2"
		CodigoError= "No se encontro en la tabla 't_logins' "
		exit function 

	End If
	If CInt(rsObjDirMail("grupo"))=0 Then
		StrL_Destino = rsObjDirMail(0) & ""
	Else
		strSQL = "SELECT login_grupo_loginid from login_grupo WHERE " & _
			"login_grupo_grupoid ='" & StrL_Destino & "'"
		Set objRsTemp = AbrirRecordSet(ObjOptimizacion, strSQL)
		While Not objRsTemp.EOF
			SQL = "SELECT email FROM t_logins WHERE Login='" & objRsTemp(0) & "'"
			Set ObjRsGrupos = AbrirRecordSet(ObjOptimizacion, SQL)
			If Not IsNull(ObjRsGrupos(0)) Then
				StrL_Destino = ObjRsGrupos(0)
				'Mail.AddAddress ObjRsGrupos(0) 
			End If
			ObjRsGrupos.Close
			Set ObjRsGrupos = Nothing
			objRsTemp.MoveNext
		WEnd
		objRsTemp.Close
		Set objRsTemp = Nothing
	End If
	set rsObjDirMail = Nothing

	If StrL_Destino <> "" Then
		
'This section provides the configuration information for the remote SMTP server.
'Send the message using the network (SMTP over the network).
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="10.1.141.213"
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
'Use SSL for the connection (True or False)
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False 
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
Mail.Configuration.Fields.Update
'End of remote SMTP server configuration section
		
		Mail.From = "PortalReuniones@ecopetrol.com.co"
		Mail.to = StrL_Destino
		StrL_Cadena = StrL_Destino
		Mail.Subject = StrL_Asunto
		Mail.HTMLBody = StrL_Cuerpo
		ErrorEnviar = ""
		On Error Resume Next 
		Mail.Send
	 End If
	 Set Mail = Nothing
	
		  If Err <> 0 Then 
			ErrorEnviar="0" 
			CodigoError = Err.Description
		  Else
			ErrorEnviar="1"
			CodigoError = "Mensaje Enviado"
		  End If
End Function

Sub Buscar_Moderadores()
'Realiza la consulta del Id de la Reunión
StrL_SQL = "SELECT TipoReunion.Id From TipoReunion " & _
    "WHERE (((TipoReunion.Nombre)='" & ObjL_RecordSet("TIPO") & "')) AND Activa = -1;"

Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL2_RecordSet.EOF Then
    'Realiza la consulta de los Moderadores de la Reunión
    StrL_SQL = "SELECT permisos.login +  ' - '  + t_logins.nombreusuario as Moderador " & _
        "FROM t_logins, permisos WHERE t_logins.Login = permisos.login " & _
        "AND (((permisos.id_reunion) = " & ObjL2_RecordSet("Id") & ") And ((permisos.permiso) = 'M')) " & _
        "ORDER BY t_logins.nombreusuario;"
		

	Set ObjL3_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
    'Construye la cadena que Contiene los moderadores de la reunión
    Responsables = ""
    Do Until ObjL3_RecordSet.EOF
        If Responsables = "" Then
                Responsables = ObjL3_RecordSet("Moderador")
            Else
                Responsables = Responsables & "; " & ObjL3_RecordSet("Moderador")
        End If
        ObjL3_RecordSet.MoveNext
    Loop
    ObjL3_RecordSet.Close
End If
ObjL2_RecordSet.Close
Set ObjL2_RecordSet = Nothing
Set ObjL3_RecordSet = Nothing
End Sub


'Procedimiento para enviar mails a Responsables con Compromisos a vencerse en
'tres días.

StrL_SQL =	"SELECT LOGIN, RESPONSABLE, TIPO, TEMA, Accion, STATUS, FECHAC,NoID, Fecha_EnvioMail " & _
			"From Compromisos " & _
			 "WHERE STATUS <>'VERDE'  AND ((FECHAC)= CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 102), 102)+3) " & _
			 "AND ((Fecha_EnvioMail < CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 102), 102) Or Fecha_EnvioMail is Null))  " & _
		     "ORDER BY LOGIN, FECHAC;"


Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL_RecordSet.EOF Then
Do Until ObjL_RecordSet.EOF
    Buscar_Moderadores
	StrL_Mensaje = "<html><body>"
	StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Señor(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso próximo a vencerse (en tres días):</small></font></p>"
	StrL_Mensaje =  StrL_Mensaje & "<table>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reunión:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
	If Responsables <> "" Then
		StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
	End If
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Recuerde realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
	StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente dirección: " 
	StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
	StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
	StrL_Mensaje = StrL_Mensaje & "</html>" 
    EnviarMail ObjL_RecordSet("TIPO") & " - Compromisos Pendientes por Documentar (A vencerse en 3 dias)", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
	EscribirEnArchivo ErrorEnviar    
	ObjOptimizacion.Execute "UPDATE Compromisos SET Fecha_EnvioMail= getDate() WHERE NoID=" & ObjL_RecordSet("NoID")

    StrL_Mensaje = ""
    ObjL_RecordSet.MoveNext
 Loop
ObjL_RecordSet.Close
End If
Set ObjL_RecordSet = Nothing


'Procedimiento para enviar mails a Responsables con Compromisos a vencerse en
'un día.

StrL_SQL =	"SELECT LOGIN, RESPONSABLE, TIPO, TEMA, Accion, STATUS, FECHAC,NoID, Fecha_EnvioMail " & _
			"From Compromisos " & _
			 "WHERE STATUS <>'VERDE'  AND ((FECHAC)= CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 102), 102)+1) " & _
			 "AND ((Fecha_EnvioMail < CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 102), 102) Or Fecha_EnvioMail is Null))  " & _
		     "ORDER BY LOGIN, FECHAC;"


Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL_RecordSet.EOF Then
Do Until ObjL_RecordSet.EOF
    Buscar_Moderadores
	StrL_Mensaje = "<html><body>"
	StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Señor(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso próximo a vencerse mañana:</small></font></p>"
	StrL_Mensaje =  StrL_Mensaje & "<table>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reunión:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
	If Responsables <> "" Then
		StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
	End If
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Recuerde realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
	StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente dirección: " 
	StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
	StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
	StrL_Mensaje = StrL_Mensaje & "</html>" 
    EnviarMail ObjL_RecordSet("TIPO") & " - Compromiso para cerrar Mañana", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
	EscribirEnArchivo ErrorEnviar    
	ObjOptimizacion.Execute "UPDATE Compromisos SET Fecha_EnvioMail= getDate() WHERE NoID=" & ObjL_RecordSet("NoID")

    StrL_Mensaje = ""
    ObjL_RecordSet.MoveNext
 Loop
ObjL_RecordSet.Close
End If
Set ObjL_RecordSet = Nothing


'Procedimiento para enviar mails a Responsables con Compromisos según periodicidad establecida
Dim Diferencia, VarAux, VarDias
StrL_SQL2 = "SELECT Id_Periodicidad, No_Dias From Periodicidad WHERE Id_Periodicidad > 1"
Set ObjL4_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL2)

'Ciclo para permitir asignar todos los valores posibles existentes en la tabla
'periodicidad. Se Omite el 1 debido a que este no tiene periodicidad
Do Until ObjL4_RecordSet.EOF
	StrL_SQL = "SELECT Compromisos.LOGIN, Compromisos.RESPONSABLE, Compromisos.TIPO, " & _
		"Compromisos.TEMA, Compromisos.Accion, Compromisos.STATUS, Compromisos.NoID, " & _
		"Compromisos.FECHAI, Compromisos.FECHAC, Compromisos.Id_Periodicidad " & _
		"FROM Compromisos WHERE Compromisos.STATUS <>'ROJO' " & _
		"AND ((Compromisos.FECHAC)= CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 102), 102)) " & _
		"AND Compromisos.Id_Periodicidad =" & ObjL4_RecordSet(0) & " " & _
		"AND Compromisos.FechaRealCierre Is Null"
				
	Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	Do Until ObjL_RecordSet.EOF
		Diferencia = ObjL_RecordSet("FECHAC") - ObjL_RecordSet("FECHAI")
		VarAux = CLng(Diferencia / ObjL4_RecordSet(1))
		For i=1 to VarAux
			VarDias = ObjL4_RecordSet(1) * i
			If Date() = (ObjL_RecordSet("FECHAI") + VarDias) Then
				If Date() <= ObjL_RecordSet("FECHAC") Then
					Buscar_Moderadores
					StrL_Mensaje = "<html><body>"
					StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Señor(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso:</small></font></p>"
					StrL_Mensaje =  StrL_Mensaje & "<table>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reunión:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
					If Responsables <> "" Then
						StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
					End If
					StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Recuerde realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
					StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente dirección: " 
					StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
					StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
					StrL_Mensaje = StrL_Mensaje & "</html>" 
		
					EnviarMail ObjL_RecordSet("TIPO") & " - Recordatorio de Compromiso - (" & ObjL4_RecordSet(1) & " Días)", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
					EscribirEnArchivo ErrorEnviar            
					ObjOptimizacion.Execute "UPDATE Compromisos SET Fecha_EnvioMail= getDate() WHERE NoID=" & ObjL_RecordSet("NoID")
					StrL_Mensaje = ""
				End If
			End If
		Next
	ObjL_RecordSet.MoveNext
	Loop
	ObjL_RecordSet.Close
	Set ObjL_RecordSet = Nothing
	ObjL4_RecordSet.MoveNext
 Loop
ObjL4_RecordSet.Close
Set ObjL4_RecordSet = Nothing

response.write("termina el proceso de envio de correo proximos a vencer")
ObjOptimizacion.Close
%>
<script language="JavaScript" type="text/JavaScript">
<!--
window.close();
// -->
</script>