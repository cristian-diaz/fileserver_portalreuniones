﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_indicador.aspx.vb" Inherits="frm_indicador" Theme="SkinCompromisos"  StylesheetTheme="SkinCompromisos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PORTAL DE COMPROMISOS : INDICADOR</title>

</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="im1"
            TargetControlID="date1">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="im2"
            TargetControlID="date2">
        </cc1:CalendarExtender>
        
    </div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td colspan="6" style="font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG);
                    color: white; font-family: Arial; height: 18px; text-align: center;">
                    Indicador de Control de Gestion</td>
            </tr>
            <tr>  
                <td style="width: 20%; height: 16px">
                </td>
                <td style="width: 15%; height: 16px;background-color:#EEF2DD;">
                    <asp:Label ID="Label1" runat="server" Text="Seleccionar Area:"></asp:Label></td>
                <td style="height: 16px;background-color:#EEF2DD;"  colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                    <asp:RadioButtonList ID="areas" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Gerencia">Gerencia</asp:ListItem>
                        <asp:ListItem>Departamento</asp:ListItem>
                        <asp:ListItem>Coordinacion</asp:ListItem>
                    </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="width: 25%; height: 16px">
                </td>
                <td style="width: 10%; height: 16px">
                </td>
            </tr>
            <tr>
                <td style="width: 20%; height: 19px;">
                </td>
                <td align="left" colspan="3" style="background-color:#EEF2DD; height: 19px;">
                                <asp:Label ID="Label2" runat="server" Text="Nombre de Reunion:" Width="241px"></asp:Label></td>
                <td align="left" style="width: 25%; height: 19px;">
                </td>
                <td style="width: 10%; height: 19px;">
                </td>
            </tr>
            <tr>
                <td style="width: 20%; height: 22px">
                </td>
                <td align="left" colspan="3" style="height: 22px; background-color: #eef2dd">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                                <asp:DropDownList ID="dr_nombre" runat="server">
                                </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="left" style="width: 25%; height: 22px">
                    <cc1:ValidatorCalloutExtender ID="vallidafec" runat="server" TargetControlID="valfec">
                    </cc1:ValidatorCalloutExtender>
                </td>
                <td style="width: 10%; height: 22px">
                </td>
            </tr>
            <tr>
                <td style="width: 20%; height: 43px">
                </td>
                <td align="left" colspan="3" style="height: 43px; background-color: #eef2dd">
                    
                    <table border="0" cellpadding="0" cellspacing="0" >
                        <tr >
                            <td style="width: 92px">
                                            <asp:Label ID="Label3" runat="server" Text="Fecha Inicio:"></asp:Label></td>
                            <td style="width: 84px">
                                            <asp:TextBox ID="date1" runat="server" Width="100px"></asp:TextBox></td>
                            <td style="width: 20px">
                                <asp:Image ID="im1" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                            <td style="width: 71px">
                                            <asp:Label ID="Label4" runat="server" Text="Fecha Final:"></asp:Label></td>
                            <td style="width: 100px">
                                            <asp:TextBox ID="date2" runat="server" Width="100px"></asp:TextBox></td>
                            <td style="width: 41px">
                                <asp:Image ID="im2" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                        </tr>
                    </table>
                    <asp:CompareValidator ID="valfec" runat="server" ControlToCompare="date1" ControlToValidate="date2"
                        Display="None" ErrorMessage="La fecha final debe ser superior a la inicial" Operator="GreaterThan"
                        Type="Date" ValidationGroup="fecha"></asp:CompareValidator></td>
                <td align="left" style="width: 25%; height: 43px">
                </td>
                <td style="width: 10%; height: 43px">
                </td>
            </tr>
            <tr>
                <td style="width: 20%; height: 20px">
                </td>
                <td style="width: 15%; height: 20px">
                </td>
                <td style="width: 25%; height: 20px; text-align: center;">
                    <asp:Button ID="Button1" runat="server" BackColor="#E0E0E0" Font-Bold="False" Font-Italic="False"
                        Text="Ver Reporte" Width="146px" /></td>
                <td style="width: 15%; height: 20px">
                </td>
                <td style="width: 25%; height: 20px">
                </td>
                <td style="width: 10%; height: 20px">
                </td>
            </tr>
            <tr>
          
                <td colspan="6" style="width: 20%;background-color:White;">
                   <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="texto" colspan="2">
                                Evaluacion Reuniones Estrategicas</td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
