﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_Visto_buenoActa.aspx.vb" Inherits="frm_Visto_buenoActa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Página sin título</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <table width="100%">
                    <tr>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            &nbsp;<asp:HiddenField ID="Hflid_acta" runat="server" />
                            <asp:HiddenField ID="Hflaprobo" runat="server" />
                            <asp:AccessDataSource ID="AccessDataSource1" runat="server"></asp:AccessDataSource>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 21px">
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <table width="800">
                    <tr>
                        <td colspan="2" rowspan="3">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/logoEcopetrol.gif" /></td>
                        <td colspan="4">
                            <asp:Label ID="Label15" runat="server" Font-Bold="True" Text="ACTA DE REUNION"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height: 40px">
                            <asp:Label ID="lblgestion_info" runat="server" Font-Bold="True"></asp:Label><br />
                            <asp:Label ID="lbldirtecinfo" runat="server" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr>
                                    <td style="width: 100px; height: 21px">
                                        <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Size="Small" Text="CODIGO"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblcodigo_acta" runat="server" Font-Bold="True"></asp:Label></td>
                                    <td style="width: 100px; height: 21px">
                                        <asp:Label ID="Label31" runat="server" Font-Bold="True" Font-Size="Small" Text="ELABORADO"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblfecha_acta" runat="server" Font-Bold="True"></asp:Label></td>
                                    <td style="width: 100px; height: 21px">
                                        <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Size="Small" Text="VERSION"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblvesion" runat="server" Font-Bold="True"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 25px; background-color: #66cc33">
                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Acta No:"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 25px">
                            <asp:Label ID="lblActaNo" runat="server"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 25px; background-color: #66cc33">
                            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Tema:"></asp:Label></td>
                        <td align="left" colspan="3" style="height: 25px">
                            <asp:Label ID="lblTema" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 23px; background-color: #66cc33">
                            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Fecha:"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 23px">
                            <asp:Label ID="lblFechaEval" runat="server"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 23px; background-color: #66cc33">
                            <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Ubicacion:"></asp:Label></td>
                        <td align="left" colspan="3" style="height: 23px">
                            <asp:Label ID="lblUbicacion" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 21px; background-color: #66cc33">
                            <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="Hora Inicio:"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 21px">
                            <asp:Label ID="lblHoraInicio" runat="server"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 23px; background-color: #66cc33">
                            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Hora Fin:"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 21px">
                            <asp:Label ID="lblHoraFin" runat="server"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 21px">
                        </td>
                        <td align="left" style="width: 100px; height: 21px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="height: 21px; text-align: left">
                            <asp:Label ID="Label16" runat="server" Font-Bold="True" Text="1. ANTES DE LA REUNION"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; background-color: #66cc33">
                            <asp:Label ID="Label26" runat="server" Font-Bold="True">Objetivo</asp:Label></td>
                        <td align="left" colspan="5">
                            <asp:Label ID="lblObjetivo" runat="server" Font-Bold="True">N/A</asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; background-color: #66cc33">
                            <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Agenda"></asp:Label></td>
                        <td align="left" colspan="5">
                            <asp:Label ID="lblAgenda" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="width: 100px; background-color: #66cc33; text-align: left">
                            <asp:Label ID="Label33" runat="server" Font-Bold="True" Text="Participantes"></asp:Label>
                            <asp:Label ID="Label8" runat="server" Text="(Personas cuya participacion es impresindible para lograr los objetivos)"
                                Width="583px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="grdparticipantes" runat="server" AutoGenerateColumns="False" Font-Overline="False"
                                HorizontalAlign="Left" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Nombre">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" style="width: 100px; height: 21px">
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Nombre") %>' Width="380px"></asp:Label></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dependencia"></asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#66CC33" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: left">
                            <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="2. DESARROLLO DE LA REUNION"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="6" style="height: 21px; background-color: #66cc33; text-align: left">
                            &nbsp;<asp:Label ID="Label9" runat="server" Text="(Descripcion de los puntos tratados en la reunion)"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: left">
                            <asp:Label ID="lbldesarrollo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="height: 21px; text-align: left">
                            <asp:Label ID="Label19" runat="server" Font-Bold="True" Text="3. EVALUACION DE LA REUNION"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="text-align: left">
                            &nbsp;<asp:Label ID="Label20" runat="server" Text="Logramos alcanzar nuestra meta en esta reunion"></asp:Label>&nbsp;
                            <asp:Label ID="Label21" runat="server" Text="Si:"></asp:Label>&nbsp; &nbsp;<asp:Label
                                ID="lblsi" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                            <asp:Label ID="Label22" runat="server" Text="No:"></asp:Label>&nbsp;
                            <asp:Label ID="lblno" runat="server" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="text-align: left">
                            &nbsp;<asp:Label ID="Label23" runat="server" Text="Si no, como y cuando lo haremos?"></asp:Label>&nbsp;
                            _______________________________________________________</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="text-align: left">
                            <asp:Label ID="Label24" runat="server" Font-Bold="True" Text="4. COMPROMISOS"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="grdcompromisos" runat="server" Width="100%" AutoGenerateColumns="False">
                                <HeaderStyle BackColor="#66CC33" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Accion">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Accion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Compromiso">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Responsable") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Fecha","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 21px; background-color: #66cc33">
                            <asp:Label ID="Label25" runat="server" Font-Bold="True" Text="Asistentes"></asp:Label></td>
                        <td align="left" colspan="5" style="height: 21px">
                            <asp:Label ID="Label27" runat="server" Text="N/A"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="height: 21px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6" style="height: 69px">
                            <table style="text-align: center" width="100%">
                                <tr>
                                    <td style="width: 100px; background-color: #66cc33">
                                        <asp:Label ID="Label28" runat="server" Font-Bold="True" Text="Revisó"></asp:Label></td>
                                    <td style="width: 100px; background-color: #66cc33">
                                        <asp:Label ID="Label29" runat="server" Font-Bold="True" Text="Aprobó"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; text-align: left">
                                        <asp:Label ID="lblReviso" runat="server" Font-Overline="False"></asp:Label></td>
                                    <td style="width: 100px; text-align: left">
                                        <asp:Label ID="lblAprobo" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="BtnVoBo" runat="server" Text="Visto Bueno" /><br />
              
            </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:Label ID="lblMensaje" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"></asp:Label></asp:View>
        </asp:MultiView><br />
        &nbsp;</div>
    </form>
</body>
</html>
