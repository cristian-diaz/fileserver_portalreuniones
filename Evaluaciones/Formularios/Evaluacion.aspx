<%@ Page Language="VB" MasterPageFile="~/Evaluaciones/Plantilla/P_Plantilla_Ajax_Aux.master" AutoEventWireup="false" CodeFile="Evaluacion.aspx.vb" Inherits="Evaluaciones_Formularios_Evaluacion" title="Evaluaci�n de Reuniones" %>

<%@ Register Assembly="BusyBoxDotNet" Namespace="BusyBoxDotNet" TagPrefix="busyboxdotnet" %>
<%@ Register Src="../../Controles/BusyBoxAjax.ascx" TagName="BusyBoxAjax" TagPrefix="uc1" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc3" %>

<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CntCuerpo" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td colspan="6" style="font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG); color: white; font-family: Verdana,Arial,Helvetica,sans-serif; height: 18px" class="titulo">
                M�dulo de Evaluaci�n de Reuniones</td>
        </tr>
        <tr>
            <td style="width: 10%; height: 20px;">
            </td>
            <td style="height: 20px; width: 180px;">
            </td>
            <td style="width: 25%; height: 20px;">
            </td>
            <td style="width: 15%; height: 20px;">
            </td>
            <td style="width: 25%; height: 20px;">
            </td>
            <td style="width: 10%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td>
         
            </td>
            <td align="left" style="background-color: #d5ddbf;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="False" Font-Size="8pt" Text="Reunion:" Font-Names="Verdana,Arial,Helvetica,sans-serif" SkinID="TitTabla"></asp:Label></td>
            <td align="left" colspan="3" >
                  <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>
               <asp:DropDownList id="DropDownList1" runat="server" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="8pt" ForeColor="Black" Width="502px" AutoPostBack="True"></asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td >

                </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 180px">
            </td>
            <td style="width: 25%">
            </td>
            <td style="width: 15%">
            </td>
            <td style="width: 25%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 8px;">
            </td>
            <td align="left" style="background-color: #d5ddbf; height: 8px; width: 180px;">
                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Lugar:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" colspan="3" style="height: 8px">
                <asp:TextBox ID="txtlugar" runat="server" ValidationGroup="Eval" Width="502px"></asp:TextBox></td>
            <td style="width: 10%; height: 8px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="width: 180px">
            </td>
            <td align="left" style="width: 25%">
            </td>
            <td align="left" style="width: 15%">
            </td>
            <td align="left" style="width: 25%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="background-color: #d5ddbf; width: 180px;">
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Fecha:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" style="width: 25%">
                <asp:TextBox ID="txtfecha" runat="server" ValidationGroup="Eval" Width="81px"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="..." />
                <asp:Button ID="Button3" runat="server" Text="Validar" Visible="False" /></td>
            <td align="left" style="width: 15%; background-color: #d5ddbf;">
                <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Evaluaci�n:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" style="width: 25%">
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
<asp:TextBox id="TextBox3" runat="server" Font-Size="Medium" Font-Bold="True" Width="80px" BackColor="Crimson" ReadOnly="True">0%</asp:TextBox> 
</contenttemplate>
                </asp:UpdatePanel></td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 19px;">
            </td>
            <td align="left" style="height: 19px; width: 180px;">
            </td>
            <td align="left" style="width: 25%; height: 19px;">
            </td>
            <td align="left" style="width: 15%; height: 19px;">
            </td>
            <td align="left" style="width: 25%; height: 19px;">
            </td>
            <td style="width: 10%; height: 19px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="background-color: #d5ddbf; width: 180px;">
                <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" SkinID="TitTabla" Text="Hora Inicio Planeada:"></asp:Label></td>
            <td align="left" style="width: 25%">
                <asp:UpdatePanel id="UpdatePanel6" runat="server">
                    <contenttemplate>
<asp:Label id="lblinicioestimre" runat="server" Width="112px" BorderColor="#E0E0E0" BorderStyle="Solid"></asp:Label> 
</contenttemplate>
                </asp:UpdatePanel></td>
            <td align="left" style="width: 15%; height: 20px; background-color: #d5ddbf">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" SkinID="TitTabla" Text="Hora Final Planeada:"></asp:Label></td>
            <td align="left" style="width: 25%; height: 20px">
                <asp:UpdatePanel id="UpdatePanel7" runat="server">
                    <contenttemplate>
<asp:Label id="lblfinalestimadareun" runat="server" Width="112px" BorderColor="#E0E0E0" BorderStyle="Solid"></asp:Label> 
</contenttemplate>
                </asp:UpdatePanel></td>
            <td style="width: 10%; height: 20px">
            </td>
        </tr>
        <tr>
            <td style="height: 8px">
            </td>
            <td align="left" style="height: 8px; width: 180px;">
            </td>
            <td align="left" style="height: 8px">
            </td>
            <td align="left" style="height: 8px">
            </td>
            <td align="left" style="height: 8px">
            </td>
            <td style="height: 8px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="background-color: #d5ddbf; width: 180px;">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" SkinID="TitTabla" Text="Hora Inicio Reuni�n:" Width="104px"></asp:Label></td>
            <td align="left" style="width: 25%">
                <asp:UpdatePanel id="UpdatePanel4" runat="server">
                    <contenttemplate>
<asp:Label id="horainicioreun" runat="server" Text="Label" Width="112px" BorderStyle="Solid" BorderColor="#E0E0E0" ></asp:Label>
</contenttemplate>
                </asp:UpdatePanel></td>
            <td align="left" style="width: 15%; height: 20px; background-color: #d5ddbf;">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" SkinID="TitTabla" Text="Hora Final  Reuni�n:" Width="104px"></asp:Label></td>
            <td align="left" style="width: 25%; height: 20px">
                <asp:UpdatePanel id="UpdatePanel5" runat="server"><contenttemplate>
<asp:Label id="lblhorafin" runat="server" Text="Label" Width="128px" BorderStyle="Solid" BorderColor="#E0E0E0"></asp:Label>
</contenttemplate>
                </asp:UpdatePanel></td>
            <td style="width: 10%; height: 20px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="width: 180px">
            </td>
            <td align="left" style="width: 25%">
            </td>
            <td align="left" style="width: 15%">
            </td>
            <td align="left" style="width: 25%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 114px;">
            </td>
            <td align="left" colspan="2" style="height: 114px">
                <asp:UpdatePanel id="UpdatePanel8" runat="server">
                    <contenttemplate>
<asp:Label id="lblmenerrror" runat="server" Text="* No ha dado a�n inicio a la Reuni�n" Font-Italic="True" Font-Bold="True" ForeColor="Red" Width="280px" Visible="False"></asp:Label><BR /><asp:Label id="lbllugar" runat="server" Text="* Falta digitar  el lugar de la Reuni�n" Font-Italic="True" Font-Bold="True" ForeColor="Red" Width="280px" Visible="False"></asp:Label>
</contenttemplate>
                </asp:UpdatePanel>
            </td>
            <td align="left" style="width: 15%; height: 114px; background-color: #d5ddbf">
                <asp:Label ID="Label99" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" SkinID="TitTabla" Text="Registrar  Usuarios (Tarde): "></asp:Label></td>
            <td align="left" style="width: 25%; height: 114px">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
<asp:Button id="btnusu" runat="server" Text="..." Width="24px"></asp:Button> 
</ContentTemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnusu"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
            <td style="width: 10%; height: 114px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 20px">
            </td>
            <td align="left" colspan="2" style="height: 20px">
                <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" SkinID="TitTabla" Text="La Reunion se debe Realizar: "></asp:Label>
                <asp:UpdatePanel id="UpdatePanel12" runat="server">
                    <contenttemplate>
<asp:Label id="lbl_frecuencia" runat="server" Font-Names="Arial" Font-Size="Small" Font-Bold="True" ForeColor="#C00000"></asp:Label> 
</contenttemplate>
                </asp:UpdatePanel>
                
                 <cc1:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MM/yyyy" PopupButtonID="Button1"  TargetControlID="txtfecha">
                </cc1:CalendarExtender>
                
                </td>
          
            <td align="left" style="width: 15%; height: 20px">
                &nbsp;</td>
            <td align="left" style="width: 25%; height: 20px">
            </td>
            <td style="width: 10%; height: 20px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="4">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id_item" EnableTheming="True" SkinID="GvwEvaluacion">
                    <Columns>
                        <asp:BoundField DataField="orden" HeaderText="N&#176;" />
                        <asp:TemplateField HeaderText="No" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# bind("Id_item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item a Evaluar">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblitem" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Peso">
                            <ItemTemplate>
                                <asp:Label ID="lblpeso" runat="server" Text='<%# bind("Peso") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Calificacion">
                            <ItemTemplate>
                                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                    <contenttemplate>
<asp:DropDownList id="dbcalifica" runat="server" Width="54px" AutoPostBack="True" ToolTip='<%# bind("index") %>' OnSelectedIndexChanged="dbcalifica_SelectedIndexChanged" __designer:wfdid="w4"></asp:DropDownList> 
</contenttemplate>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total">
                            <ItemTemplate>
                                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                    <contenttemplate>
<asp:Label id="lbltotal" runat="server" Text='<%# bind("valor") %>' Font-Bold="True" Width="100%" Height="100%" BackColor="Crimson"></asp:Label> 
</contenttemplate>
                                    <triggers>
<asp:AsyncPostBackTrigger ControlID="DropDownList1" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
</triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ayuda">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbmensa" runat="server" Height="19px" ImageUrl="~/img/ayuda2.gif"
                                    Width="22px" /><br />
                                <asp:Panel ID="pnmen" runat="server" Height="123px" Width="13px">
                                    <table style="border-right: #1c5e55 thin solid; border-top: #1c5e55 thin solid; border-left: #1c5e55 thin solid;
                                        border-bottom: #1c5e55 thin solid; background-color: #c0ffc0">
                                        <tr class="Contenido">
                                            <td align="center" colspan="2">
                                            </td>
                                        </tr>
                                        <tr class="Contenido">
                                            <td colspan="2">
                                                <asp:Label ID="lblmensaje" runat="server" BackColor="White" Height="80px" Text='<%# bind("comentario") %>'
                                                    Width="105px"></asp:Label></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <cc1:HoverMenuExtender ID="HoverMenuExtender1" runat="server" PopupControlID="pnmen"
                                    TargetControlID="imbmensa">
                                </cc1:HoverMenuExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Id_Item" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Id_Item") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Lbl_ID" runat="server" Text='<%# Bind("Id_Item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblitem" runat="server" Text='<%# bind("item") %>'></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="8pt" />
                    <RowStyle Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="8pt" />
                </asp:GridView>
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 20px;">
            </td>
            <td align="left" style="height: 20px; width: 180px;">
            </td>
            <td align="left" style="width: 25%; height: 20px;">
            </td>
            <td align="left" style="width: 15%; height: 20px;">
            </td>
            <td align="left" style="width: 25%; height: 20px;">
            </td>
            <td style="width: 10%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 19px">
            </td>
            <td align="center" colspan="2" style="height: 19px">
                <asp:Label ID="Label4" runat="server" Font-Bold="True"
                    Text="Resultados Obtenidos" Width="151px" SkinID="TitTabla"></asp:Label></td>
            <td align="center" colspan="2" style="height: 19px">
                <asp:Label ID="Label2" runat="server" Font-Bold="True"
                    Text="Comentarios:" SkinID="TitTabla"></asp:Label></td>
            <td style="width: 10%; height: 19px">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="2" rowspan="4">
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<asp:GridView id="GridView2" runat="server" SkinID="GvwEvaluacion" Width="260px" AutoGenerateColumns="False" ><Columns>
<asp:TemplateField HeaderText="Item"><ItemTemplate>
<asp:Label id="lblitemr" runat="server" Text='<%# bind("itemr") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Valor"><ItemTemplate>
<asp:Label id="lblvalorr" runat="server" Text='<%# bind("valorr") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView> 
</contenttemplate>
                </asp:UpdatePanel></td>
            <td align="center" colspan="2" rowspan="4">
                <asp:UpdatePanel id="UpdatePanel11" runat="server"><contenttemplate>
                <asp:TextBox ID="TextBox1" runat="server" Height="114px" MaxLength="3000" Width="335px" TextMode="MultiLine"></asp:TextBox>
</contenttemplate>
                </asp:UpdatePanel></td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 20px;">
            </td>
            <td align="left" style="height: 20px; width: 180px;">
            </td>
            <td align="left" style="width: 25%; height: 20px;">
            </td>
            <td align="left" style="width: 15%; height: 20px;">
            </td>
            <td align="left" style="width: 25%; height: 20px;">
            </td>
            <td style="width: 10%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 24px;">
            </td>
            <td align="center" colspan="2" style="height: 24px">
                <asp:Button ID="BTNGUARDAR" runat="server" Text="ENVIAR" ValidationGroup="Eval" Width="105px" BackColor="#FFCC00" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="8pt" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
                </td>
            <td align="center" colspan="2" style="height: 24px">
                <asp:Button ID="Button2" runat="server" Text="LIMPIAR" Width="105px" /></td>
            <td style="width: 10%; height: 24px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 24px;">
            </td>
            <td align="left" style="width: 180px; height: 24px;">
            </td>
            <td align="left" style="width: 25%; height: 24px;">
                &nbsp;</td>
            <td align="left" style="width: 15%; height: 24px;">
            </td>
            <td align="left" style="width: 25%; height: 24px;">
            </td>
            <td style="width: 10%; height: 24px;">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HiddenField ID="HfSinGuardar" runat="server" Value="False" />
            </td>
            <td align="left" style="width: 25%">
                <cc1:ConfirmButtonExtender ID="CbeGuardar" runat="server" ConfirmText="Guardar Cambios?"
                    Enabled="True" TargetControlID="BTNGUARDAR">
                </cc1:ConfirmButtonExtender>
            </td>
            <td align="left" style="width: 15%">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlugar"
                    Display="None" ErrorMessage="El Campo Lugar No puede estar vacio" ForeColor="#404040"
                    ValidationGroup="Eval" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
            <td align="left" style="width: 25%">
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtfecha"
                    Display="None" ErrorMessage="Digite una Fecha V�lida" ForeColor="" ValidationExpression="^(0?[1-9]|1[0-9]|2|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(\d{4})$"
                    ValidationGroup="Eval"></asp:RegularExpressionValidator></td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" colspan="2">
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" HighlightCssClass="validatorCalloutHighlight"
                    TargetControlID="RequiredFieldValidator1">
                </cc1:ValidatorCalloutExtender>
            </td>
            <td align="left" colspan="2">
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" HighlightCssClass="validatorCalloutHighlight"
                    TargetControlID="RequiredFieldValidator2">
                </cc1:ValidatorCalloutExtender>
            </td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="width: 180px">
                <asp:RangeValidator ID="rvRangoFec" runat="server" ControlToValidate="txtfecha" Display="None"
                    ErrorMessage="RangeValidator" ForeColor="" SetFocusOnError="True" Type="Date"
                    ValidationGroup="Eval"></asp:RangeValidator></td>
            <td align="left" style="width: 25%">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtfecha"
                    Display="None" ErrorMessage="Falta Selecionar la Fecha" ForeColor="Black" ValidationGroup="Eval" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
            <td align="left" style="width: 15%">
                <cc3:MsgBox ID="MsgBox1" runat="server" />
            </td>
            <td colspan="2">
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" HighlightCssClass="validatorCalloutHighlight"
                    TargetControlID="RegularExpressionValidator1">
                </cc1:ValidatorCalloutExtender>
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="rvRangoFec">
                </cc1:ValidatorCalloutExtender>
            </td>
        </tr>
    </table>
            <asp:Panel ID="Panel1" runat="server" Style="display: none" CssClass="modalPopup" Width="387px">
                <asp:LinkButton ID="LinkButton1" runat="server">Desea Guardar Datos de Evaluacion</asp:LinkButton><br />
                <br />
                <div>
                    <p style="text-align: center">
                        <asp:Button ID="OkButton" runat="server" Text="OK" />
                        <asp:Button ID="CancelButton" runat="server" Text="Cancel" />
                    </p>
                </div>
        </asp:Panel>
    
 
    <cc1:ModalPopupExtender ID="ModalPopupExtender" runat="server" 
            TargetControlID="LinkButton2"
            PopupControlID="Panel1" 
            BackgroundCssClass="modalBackground" 
            OkControlID="OkButton"
            OnOkScript="onOk()" 
            CancelControlID="CancelButton" 
            DropShadow="true"
            PopupDragHandleControlID="Panel3" />
    <asp:LinkButton ID="LinkButton2" runat="server" Visible="False">LinkButton</asp:LinkButton>
    <asp:HiddenField ID="hfl_str_Automatico" runat="server" />
    
</asp:Content>

