Imports System.Data
Partial Class Evaluaciones_Formularios_WfrCreaModItems
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub BTNGUARDAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTNGUARDAR.Click
        PrcAcutualizar(str_Oopcion)
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Response.Redirect("Administrar_items.aspx")
    End Sub

    Private Function Cadena() As String
        Dim t As String = ""

        For i As Integer = 0 To CblCalificaion.Items.Count - 1
            If CblCalificaion.Items(i).Selected = True Then
                If t = "" Then
                    t = CblCalificaion.Items(i).Value
                Else
                    t &= "," & CblCalificaion.Items(i).Value 'i
                End If
            End If
        Next
        Return t
    End Function
    Public Sub prcActualizar(ByVal Id_Item As String)
        Me.evalu.prcconsultaritemreunion(Id_Item)
        Me.ds_evalua = Me.evalu.DatasetFill

        Dim Item As DataRow = Me.ds_evalua.Tables(0).Rows(0)

        Me.TxtItemEval.Text = Item("Item")
        Me.TxtComentario.Text = Item("comentario")
        Me.TxtPeso.Text = Item("Peso")
        Me.CkbActivo.Checked = Item("activo")

        Dim arr() As String = Split(Item("calificacion"), ",")

        For i As Integer = 0 To arr.Length - 1
            Try
                Me.CblCalificaion.Items.FindByText(arr(i)).Selected = True
            Catch ex As Exception

            End Try
        Next

    End Sub
    Public Sub PrcAcutualizar(ByVal tipo As String)
        Select Case tipo
            Case "Crear"
                evalu.PrcInbserItems(Me.TxtItemEval.Text, Me.TxtPeso.Text, Me.TxtComentario.Text, Cadena, Me.CkbActivo.Checked, Max(CInt(Me.TxtPeso.Text), Cadena))
            Case "Editar"
                evalu.PrcActualizarItems(Me.TxtItemEval.Text, Me.TxtPeso.Text, Me.TxtComentario.Text, Cadena, Me.CkbActivo.Checked, Me.Request.QueryString("Id_Eval"), Max(CInt(Me.TxtPeso.Text), Cadena))
        End Select

        If Me.evalu.FlagError = False Then
            limpiar()
            Me.MsgBox1.ShowMessage("Los Datos Han Sido Guardado")
        Else
            Me.MsgBox1.ShowMessage("Ha ocurrido un error y los datos no fueron guardados.. Por favor comuniquese con el adminsitrador")
        End If

    End Sub
    Public Function Max(ByVal Peso As Integer, ByVal Cadena As String) As Integer
        Dim Calif() As String = Split(Cadena, ",")
        Dim MaxCalif As Integer = 0
        For i As Integer = 0 To Calif.Length - 1
            If CInt(Calif(i)) > MaxCalif Then
                MaxCalif = CInt(Calif(i))
            End If
        Next
        Return MaxCalif * Peso
    End Function
    Sub limpiar()
        Me.TxtPeso.Text = ""
        Me.TxtComentario.Text = ""
        Me.TxtItemEval.Text = ""
        Me.CkbActivo.Checked = True
        Me.CblCalificaion.ClearSelection()
        Me.str_Oopcion = "Crear"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            str_Oopcion = Request.QueryString("Accion")
            If Me.str_Oopcion = "Editar" Then
                Me.prcActualizar(Me.Request.QueryString("Id_Eval"))
            End If
        End If
        If Me.str_Oopcion = "Crear" Then
            Me.BTNGUARDAR.Text = "Grabar"
        Else
            Me.BTNGUARDAR.Text = "Actualizar"
        End If
    End Sub

    Public Property str_Oopcion() As String
        Get
            Return ViewState("LOopcion")
        End Get
        Set(ByVal value As String)
            ViewState("LOopcion") = value
        End Set
    End Property
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
End Class
