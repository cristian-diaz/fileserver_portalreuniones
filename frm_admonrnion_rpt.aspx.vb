﻿Imports System.Data
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web.CrystalReportSource
Imports CrystalDecisions.Web.CrystalReportViewer
Imports CrystalDecisions.CrystalReports.Engine
Partial Class frm_admonrnion_rpt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Me.IsPostBack Then
            ' Me.prcCargarOrigen()
            Me.prcCargarDatos()
        End If


    End Sub
    Public Sub prcCargarDatos()


        Dim RptPrincipalDoc As New ReportDocument

        RptPrincipalDoc.Load(Server.MapPath("~/reporte/reporternion.rpt"))

        Try
            RptPrincipalDoc.SetDataSource(Session("ds_Reporte"))
        Catch ex As Exception

        End Try

        Me.CrystalReportViewer1.ReportSource = RptPrincipalDoc
        Me.CrystalReportViewer1.DataBind()


    End Sub

End Class
