
Partial Class Controles_ConfirmBoxAjax
    Inherits System.Web.UI.UserControl
    Protected Property Title() As String
        Get
            Return Me.LblTitle.Text
        End Get
        Set(ByVal value As String)
            Me.LblTitle.Text = value
        End Set
    End Property
    Protected Property Message() As String
        Get
            Return Me.LblMessage.Text
        End Get
        Set(ByVal value As String)
            Me.LblMessage.Text = value
        End Set
    End Property
    Public Property Btn_Confirm() As Button
        Get
            Return Me.BtnAceptar
        End Get
        Set(ByVal value As Button)
            Me.BtnAceptar = value
        End Set
    End Property

    Public Sub Show(ByVal Mensaje As String, Optional ByVal Title As String = "")
        Me.Title = IIf(Title = "", "CONFIRMACIÓN", Title.ToUpper)
        Me.Message = Mensaje
        Me.MpeShow.Show()
    End Sub
End Class
