Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb

Public Class Conexion
    Private _strUser As String
    Private _BolPage As String
    Private _StrConexion As String
    Private _StrMensaje As String
    Private _VbError As Boolean

    Public Property Usuario() As String
        Get
            Return _strUser
        End Get
        Set(ByVal value As String)
            _strUser = value
        End Set
    End Property

    Public Sub prcArmarCadena()
        _StrConexion = System.Configuration.ConfigurationManager.AppSettings("Conexioncompromiso").ToString
    End Sub

    Public Function Consultar(ByVal strcon As String, Optional ByVal StrConex As String = "") As DataSet

        If StrConex = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = StrConex
        End If

        Dim Conect As New OleDb.OleDbConnection(_StrConexion)
        Dim comand As New OleDb.OleDbCommand(strcon, Conect)

        Dim adap As New OleDb.OleDbDataAdapter(comand)
        Dim ds As New DataSet

        Try
            Conect.Open()
            adap.Fill(ds)
        Catch ex As Exception
            Me._StrMensaje = ex.Message
            Me._VbError = True
        Finally
            Conect.Close()
        End Try
        Return ds
    End Function
    Public Sub GuardarBD(ByVal SQL As String, Optional ByVal Cadena As String = "")

        If Cadena = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = Cadena
        End If

        Dim Conect As New OleDb.OleDbConnection(_StrConexion)

        Dim comand As New OleDb.OleDbCommand(SQL, Conect)

        Try
            Conect.Open()
            comand.ExecuteNonQuery()
            Me._VbError = False ' se agrego esta mejora pues quedaba en la cache
            Me._StrMensaje = ""
        Catch ex As Exception
            Me._StrMensaje = ex.Message & " CONSULTA QUE GUARDA " & SQL
            Me._VbError = True
        Finally
            Conect.Close()
        End Try

    End Sub
    Public Function ConsultaDato(ByVal strcon As String, Optional ByVal Cadena As String = "")
        'Esta funci�n devuelve le resultado de una consulta que devuelve un solo campo 

        If Cadena = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = Cadena
        End If

        Dim cone As New OleDbConnection(_StrConexion)
        Dim cmd As New OleDbCommand(strcon, cone)

        cone.Open()

        ConsultaDato = cmd.ExecuteScalar()

        cone.Close()

    End Function

    Public Function ConsultaDato2(ByVal strcon As String, Optional ByVal Cadena As String = "") As String
        'Esta funci�n devuelve le resultado de una consulta que devuelve un solo campo 
        Dim valor As String
        Dim cnn As OleDbConnection = Nothing
        Dim cmd As OleDbCommand = Nothing

        If Cadena = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = Cadena
        End If

        cnn = New OleDbConnection(_StrConexion)
        cmd = New OleDbCommand(strcon, cnn)

        cmd.CommandType = CommandType.Text

        cnn.Open()
        valor = cmd.ExecuteScalar
        Return valor
    
        cnn.Close()


    End Function

    Public Function Consultar_nbre_tbl(ByVal strcon As String, Optional ByVal StrConex As String = "", Optional ByVal nombretabla As String = "") As DataSet

        If StrConex = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = StrConex
        End If

        Dim Conect As New OleDb.OleDbConnection(_StrConexion)
        Dim comand As New OleDb.OleDbCommand(strcon, Conect)

        Dim adap As New OleDb.OleDbDataAdapter(comand)
        Dim ds As New DataSet

        Try
            Conect.Open()
            adap.Fill(ds, nombretabla)
        Catch ex As Exception
            Me._StrMensaje = ex.Message
            Me._VbError = True
        Finally
            Conect.Close()
        End Try
        Return ds
    End Function

    Public ReadOnly Property FlagError() As Boolean
        Get
            Return _VbError
        End Get
    End Property

    Public ReadOnly Property ERRORR() As String
        Get
            Return Me._StrMensaje
        End Get
    End Property

End Class
