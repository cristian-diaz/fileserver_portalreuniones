//Seccion de Variables
var MenuContextual =document.oncontextmenu;
var ClickMenu = document.onclick;
//var isIE5=document.all&&document.getElementById
//var isNS6=document.getElementById&&!document.all
var blnMenuContextVisible = false;

var objMenuContext;
var varEstiloMenuContext = 'MenuCon';
var varEstiloItemMenuContext = 'menuitems';
var varFondoEstiloResalteContext = 'highlight';
var varColorEstiloResalteContext = '#ffffff';
var varFondoEstiloNormalContext = '';
var varColorEstiloNormalContext = '#000000';

var varTituloColExplorar = '';

isNS4 = (document.layers) ? true : false;
isIE4 = (document.all && !document.getElementById) ? true : false;
isIE5 = (document.all && document.getElementById) ? true : false;
isNS6 = (!document.all && document.getElementById) ? true : false;

//funciones Posicionamiento
function getAbsX(elt) { return (elt.x) ? elt.x : getAbsPos(elt,"Left"); }
function getAbsY(elt) { return (elt.y) ? elt.y : getAbsPos(elt,"Top"); }
function getAbsW(elt) { return (elt.y) ? elt.y : getAbsPos(elt,"Width"); }

function getAbsPos(elt,which) {
 iPos = 0;
 while (elt != null) {
  iPos += elt["offset" + which];
  elt = elt.offsetParent;
 }
 return iPos;
}


function getElementoXId(tag)
{
if (isNS4) {
  obj = document.layers[tag];
} else if (isIE4) {
  obj = document.all[tag];
} else if (isIE5 || isNS6) {
  obj = document.getElementById(tag);
}
return obj;
}


function getElementosXTag(tag)
{
var arrObj = document.getElementsByTagName(tag);
return arrObj;
}

//funcion de Ubicacion de Elements

function UbicarElemento(idbase,id,posx,posy)
{
base = getElementoXId(idbase);
ele = getElementoXId(id);
ele.style.visibility = 'visible';
ele.style.top = getAbsY(base) +posy;
ele.style.left = getAbsX(base) +posx ;
}

// Funciones d Manejo Menu Contextual

function showmenuContext(e){
blnMenuContextVisible = true;
//Find out how close the mouse is to the corner of the window
var rightedge=isIE5? document.body.clientWidth-event.clientX : window.innerWidth-e.clientX
var bottomedge=isIE5? document.body.clientHeight-event.clientY : window.innerHeight-e.clientY

//if the horizontal distance isn't enough to accomodate the width of the context menu
if (rightedge<objMenuContext.offsetWidth)
//move the horizontal position of the menu to the left by it's width
objMenuContext.style.left=isIE5? document.body.scrollLeft+event.clientX-objMenuContext.offsetWidth : window.pageXOffset+e.clientX-objMenuContext.offsetWidth
else
//position the horizontal position of the menu where the mouse was clicked
objMenuContext.style.left=isIE5? document.body.scrollLeft+event.clientX : window.pageXOffset+e.clientX

//same concept with the vertical position
if (bottomedge<objMenuContext.offsetHeight)
objMenuContext.style.top=isIE5? document.body.scrollTop+event.clientY-objMenuContext.offsetHeight : window.pageYOffset+e.clientY-objMenuContext.offsetHeight
else
objMenuContext.style.top=isIE5? document.body.scrollTop+event.clientY : window.pageYOffset+e.clientY

objMenuContext.style.visibility="visible"
return false
}

function hidemenuContext(e){
objMenuContext.style.visibility="hidden"
if (blnMenuContextVisible) {
document.oncontextmenu=MenuContextual
document.onclick=ClickMenu
}
blnMenuContextVisible = false;
}

function highlightContext(e){
var firingobj=isIE5? event.srcElement : e.target
if (firingobj.className==varEstiloItemMenuContext||isNS6&&firingobj.parentNode.className==varEstiloItemMenuContext){
if (isNS6&&firingobj.parentNode.className==varEstiloItemMenuContext) firingobj=firingobj.parentNode //up one node
firingobj.style.backgroundColor=varFondoEstiloResalteContext;
firingobj.style.color=varColorEstiloResalteContext;
//if (display_url==1) window.status=event.srcElement.url
}
}

function lowlightContext(e){
var firingobj=isIE5? event.srcElement : e.target
if (firingobj.className==varEstiloItemMenuContext||isNS6&&firingobj.parentNode.className==varEstiloItemMenuContext){
if (isNS6&&firingobj.parentNode.className==varEstiloItemMenuContext) firingobj=firingobj.parentNode //up one node
firingobj.style.backgroundColor=varFondoEstiloNormalContext;
firingobj.style.color=varColorEstiloNormalContext;
window.status=''
}
}

function jumptoContext(e){
var firingobj=isIE5? event.srcElement : e.target
if (firingobj.className==varEstiloItemMenuContext||isNS6&&firingobj.parentNode.className==varEstiloItemMenuContext){
if (isNS6&&firingobj.parentNode.className==varEstiloItemMenuContext) firingobj=firingobj.parentNode
if (firingobj.getAttribute("target"))
window.open(firingobj.getAttribute("url"),firingobj.getAttribute("target"))
else
window.location=firingobj.getAttribute("url")
}
}


function ActivarMenuContextual(id)
{
DesactivarMenusContextuales(varEstiloMenuContext);
if (isIE5||isNS6){
objMenuContext=getElementoXId(id)
objMenuContext.style.display=''
document.oncontextmenu=showmenuContext
document.onclick=hidemenuContext
}
}


function DesactivarMenusContextuales(clase)
{
var menus = document.getElementsByTagName('DIV');
var i;
for (i=0; i < menus.length; i++)
{
	if (menus[i].className == "MenuCon")
	{	
	menus[i].style.visibility = 'hidden';
	}
}
}

function ResaltarOpcionMenu(idOpcion,estilo){
opcion = getElementoXId(idOpcion);
opcion.className = estilo;
}

//----------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
var blnMenuNodo = false;
var blnMenuValor = false;

function ActivarMenuVariable(idvar,hsel,intval,blnAnalisis)
{
blnMenuNodo=false;
blnMenuValor=false;
ActivarItemMenu(idvar,hsel,intval,blnAnalisis,'','','');
}

function ActivarMenuVariablePD(idvar,hsel,intval,blnAnalisis)
{
blnMenuNodo=false;
blnMenuValor=false;
ActivarItemMenu(idvar,hsel,intval,blnAnalisis,'','','');
}
//Menu de variable en el Diagrama Causal
function ActivarMenuVariableDC(idvar,hsel,intval,blnAnalisis,valValor)
{
blnMenuNodo=false;
blnMenuValor=true;
ActivarItemMenu(idvar,hsel,intval,blnAnalisis,'',valValor,'');
//alert(valValor);
}


function ActivarMenuNodo(idvar,hsel,intval,blnAnalisis,lstCmp,lstCmpH)
{
blnMenuNodo=true;
blnMenuValor=false;
ActivarItemMenu(idvar,hsel,intval,blnAnalisis,lstCmp,'',lstCmpH);
}

function AsignarValorColExp(strCol)
{
varTituloColExplorar = strCol;
}
function ActivarItemMenu(idvar,hsel,intval,blnAnalisis,IdCmp,strValor,lstCmpH)
{
//alert(strValor);
//-- El orden del Menu sera:
/*
(expl)	1. Explorar. explorar.asp?h_lngIdVariable=&h_intValorVigente=&h_strFechaConsulta=
(dep)	2. Variables Dependientes. varDepInd.asp?h_lngIdVariable=&h_intValorVigente=&h_strFechaConsulta=&h_intTipo=2
(ind)	3. Variables Inductoras. varDepInd.asp?h_lngIdVariable=&h_intValorVigente=&h_strFechaConsulta=&h_intTipo=1
(cmp)	5. Componentes, adiciona unas filas al Arbol Dupont . ActivarMenuNodo
(val)	6. Valor, aplica para el Diagrama Causal de una Presentacion
	-------------------------------------------------------------------------------
	LA SUITE
	-------------------------------------------------------------------------------
(pln)	7. Modulo de Planes. ../EnlaceVE.asp?stridVariableVE='+idvar+'&strFechaDato=&strOpcion=PLANES'
(doc)	8. Modulo de Planes. ../EnlaceVE.asp?stridVariableVE='+idvar+'&strFechaDato=&strOpcion=DOCUMENTOS'
(olap)	9. Modulo de Planes. ../EnlaceVE.asp?stridVariableVE='+idvar+'&strFechaDato=&strOpcion=OLAP'
	-------------------------------------------------------------------------------
	EL ANALISIS
	-------------------------------------------------------------------------------
(est)	10. Estadisticas. 'varEstadisticas.asp?IdVar='+idvar+'&Tipo=1&strFechaBase='+hsel;

(anl)	11. Analisis. 
	-------------------------------------------------------------------------------
	LAS PROPIEDADES
	-------------------------------------------------------------------------------
(pro)	12. Propiedades.  propiedades.asp?idVariable=

*/
/*
	Los Parametros de la Funcion.
	idvar : Id de la Variable a la q se muestra el menu ctxtual.
	hsel  : Fecha de Consulta
	intVal: Valores Vigentes (0,1) 
	blnAnalisis : 1, si tiene analsisis, 0 si no
	IdCmp: Id de la variable q hay q traer la componente.
	strValor: Al Valor a Mostrar en el Item Valor del Menu
	lstCmpH: las componentes de la Variable.	
*/
var opciones = '';
var idtag = "mcontent1"; //el id del elemento q contendra el menu contextual
//var tmp = ',\''+hsel+'\',\''+intval+'\''
var hvinculo ;

if ( (lstCmpH != '') && (idvar != '0') )
{
//Estadisticas del GRupo de Vars
opciones = opciones + '<div class="menuitems" url="javascript:EstadisticasConjunto(' + '\'' + lstCmpH+'\'' + ');">Estadisticas Conjunto</div>';
// Separador
opciones = opciones + '<div class="menuitems" url="javascript:return false;" style="height:1px;" ><img src="imagenes/pixel.gif" width="100%" height="1" border=0></div>';
}

// Explorar
hvinculo = 'explorar.asp?h_intIdVariable='+idvar+'&h_intIndicador='+intval+'&h_datFechaHora='+hsel+'&h_strTituloPrimeraColumna='+escape(varTituloColExplorar);
// Las opciones :
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'expl\',' + '\'' + hvinculo+'\'' + ');">Explorar</div>';
// Vars Dependientes
hvinculo = 'varDepInd.asp?h_lngIdVariable='+idvar+'&h_intValorVigente='+intval+'&h_strFechaConsulta='+hsel+'&h_intTipo=2'
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'dep\',' + '\'' + hvinculo+'\'' + ');">Variables Dependientes</div>';
// Vars Inductoras
hvinculo = 'varDepInd.asp?h_lngIdVariable='+idvar+'&h_intValorVigente='+intval+'&h_strFechaConsulta='+hsel+'&h_intTipo=1'
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'dep\',' + '\'' + hvinculo+'\'' + ');">Variables Inductoras</div>';
if (blnMenuNodo ) {
	if (IdCmp != '')
	{
		opciones = opciones + '<div class="menuitems" url="javascript:BuscarComponentes(' + IdCmp +');">Cargar Componentes</div>';
	}
}
if (blnMenuValor) {
	if (strValor != '')
	{
		opciones = opciones + '<div class="menuitems" url="javascript:alert(' + '\''+strValor +'\''+');">Valor</div>';
	}
}

// Separador
opciones = opciones + '<div class="menuitems" url="javascript:return false;" style="height:1px;" ><img src="imagenes/pixel.gif" width="100%" height="1" border=0></div>';

// Suite 
//Documentos
hvinculo = '../EnlaceVE.asp?stridVariableVE='+idvar+'&strFechaDato='+hsel+'&strOpcion=DOCUMENTOS'
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'doc\',' + '\'' + hvinculo+'\'' + ');">Documentos</div>';
//Planes
hvinculo = '../EnlaceVE.asp?stridVariableVE='+idvar+'&strFechaDato='+hsel+'&strOpcion=PLANES'
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'pln\',' + '\'' + hvinculo+'\'' + ');">Planes</div>';
//OLAP
hvinculo = '../EnlaceVE.asp?stridVariableVE='+idvar+'&strFechaDato='+hsel+'&strOpcion=OLAP'
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'olap\',' + '\'' + hvinculo+'\'' + ');">OLAP</div>';
// Separador
opciones = opciones + '<div class="menuitems" url="javascript:return false;" style="height:1px;" ><img src="imagenes/pixel.gif" width="100%" height="1" border=0></div>';

// Analisis y Estadistica
hvinculo = 'varEstadisticas.asp?IdVar='+idvar+'&Tipo=1&strFechaBase='+hsel;
opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'est\',' + '\'' + hvinculo+'\'' + ');">Estadisticas</div>';
if (blnAnalisis == '1' ) {
	hvinculo = 'analisis.asp?idVariable='+idvar+'&ValorVigente='+intval+'&FechaHoraValor='+hsel;
	opciones = opciones + '<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'anl\',' + '\'' + hvinculo+'\'' + ');">Analisis</div>';
}
// Separador
opciones = opciones + '<div class="menuitems" url="javascript:return false;" style="height:1px;" ><img src="imagenes/pixel.gif" width="100%" height="1" border=0></div>';

// Propiedades
hvinculo = 'propiedades.asp?idVariable='+idvar+'&ValorVigente='+intval+'FechaHoraValor='+hsel;
opciones = opciones+'<div class="menuitems" url="javascript:DestinoOpcionMenuCtxtual(\'pro\',' + '\'' + hvinculo+'\'' + ');">Propiedades</div>';
/*
*/
idmenu = getElementoXId(idtag);
if (idvar == '0') 
{
opciones = '';
//Estadisticas del GRupo de Vars
	if (lstCmpH != '')
	{
		opciones =  '<div class="menuitems" url="javascript:EstadisticasConjunto(' + '\'' + lstCmpH+'\'' + ');">Estadisticas Conjunto</div>';
	}
}

idmenu.innerHTML = opciones;

ActivarMenuContextual(idtag);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------




// Destinos de Opciones del Menu Contextual de Variables
function  DestinoOpcionMenuCtxtual(tipo,param)
{
//alert(horaseleccionada);
	switch (tipo)
	{
		case 'expl':
			//	alert(param);
			//	explorar
				window.open(param,'','left=50,top=50,width=750,height=500');
				break;
		case 'dep':
			//	dependientes
				window.open(param,'','left=50,top=50,width=585,height=335');
				break;
		case 'ind':
			//	Inductoras
				window.open(param,'','left=50,top=50,width=585,height=335');
				break;
		// Suite
		case 'pln': // Planes
		case 'doc': //documentos
		case 'olap': //olap
				var ventana;
				ventana = window.open(param,'URL'); 
				break;
		// Analisis
		case 'est': // Estadisticas
					window.open(param,'','left=50,top=50,width=550,height=320,resizable=yes');
				break;
		case 'anl': // ANalisis
					window.open(param,'','left=50,top=50,width=550,height=280');
				break;
		// Propiedades
		case 'pro': // Analisis
				window.open(param,'','left=50,top=50,width=550,height=280');
				break;
		default:
				//Se alerta
				alert(param + ":" + tipo);
				//se carga sobre el opener de parent
				break;
	}
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//-----------------------------
