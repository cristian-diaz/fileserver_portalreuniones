﻿Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Microsoft.Reporting.WebForms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO

Partial Class frm_Visto_buenoActa

    Inherits System.Web.UI.Page
    Dim gral As New Consultas
    Dim ds As New DataSet
    Dim dt As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            Hflid_acta.Value = Request("Id_Acta")
            gral.id_acta = Hflid_acta.Value
            Try
                gral.Acta("buscar", 2, , Hflid_acta.Value)
                If Not gral.DatasetFill.Tables("Acta") Is Nothing And gral.DatasetFill.Tables("Acta").Rows.Count > 0 Then
                    dt = gral.DatasetFill.Tables("Acta")

                    ds.Merge(dt)
                    'gral.id_acta = Hflid_acta.Value
                    gral.buscaagenda()
                    If Not gral.DatasetFill.Tables("Agenda") Is Nothing And gral.DatasetFill.Tables("Agenda").Rows.Count > 0 Then
                        dt.Clear()
                        dt = gral.DatasetFill.Tables("Agenda")
                        ds.Merge(dt)
                    End If
                    gral.Acta_Participante("buscar")
                    If Not gral.DatasetFill.Tables("Participante") Is Nothing And gral.DatasetFill.Tables("Participante").Rows.Count > 0 Then
                        dt.Clear()
                        dt = gral.DatasetFill.Tables("Participante")
                        ds.Merge(dt)
                    End If
                    gral.Desarrollo()
                    If Not gral.DatasetFill.Tables("Desarrollo") Is Nothing And gral.DatasetFill.Tables("Desarrollo").Rows.Count > 0 Then
                        dt.Clear()
                        dt = gral.DatasetFill.Tables("Desarrollo")
                        ds.Merge(dt)
                    End If

                    gral.Compromisos(3)
                    If Not gral.DatasetFill2.Tables("Compromisos") Is Nothing And gral.DatasetFill2.Tables("Compromisos").Rows.Count > 0 Then
                        dt.Clear()
                        dt = gral.DatasetFill2.Tables("Compromisos")
                        ds.Merge(dt)

                    End If
                    MultiView1.ActiveViewIndex = 1

                Else
                    lblMensaje.Text = "HASTA EL MOMENTO NO HAY ACTAS ASIGNADAS PARA DAR VISTO BUENO...."
                    MultiView1.ActiveViewIndex = 2
                    Exit Sub
                End If
                CargarActa()
            Catch ex As Exception
                lblMensaje.Text = "Error: " & ex.Message & " Al generar el acta favor comunicarse con el encargado de soporte."
                MultiView1.ActiveViewIndex = 2
            End Try
        End If
    End Sub

    Public Sub CargarActa()

        lblgestion_info.Text = ds.Tables("Acta").Rows(0).Item("nombre_acta").ToString
        lbldirtecinfo.Text = ds.Tables("Acta").Rows(0).Item("Departamento_acta").ToString
        lblcodigo_acta.Text = ds.Tables("Acta").Rows(0).Item("codigo_formato").ToString
        lblfecha_acta.Text = Format(CDate(ds.Tables("Acta").Rows(0).Item("fecha_acta")), "dd/MM/yyyy")
        lblvesion.Text = ds.Tables("Acta").Rows(0).Item("version").ToString
        lblActaNo.Text = ds.Tables("Acta").Rows(0).Item("id_acta").ToString
        lblTema.Text = ds.Tables("Acta").Rows(0).Item("tema_acta").ToString
        lblFechaEval.Text = Format(CDate(ds.Tables("Acta").Rows(0).Item("Fecha_eval")), "dd/MM/yyyy")
        lblUbicacion.Text = ds.Tables("Acta").Rows(0).Item("ubicacion").ToString
        lblHoraInicio.Text = Format(CDate(ds.Tables("Acta").Rows(0).Item("hora_inicio")), "HH:mm:ss")
        lblHoraFin.Text = Format(CDate(ds.Tables("Acta").Rows(0).Item("hora_fin")), "HH:mm:ss")
        If ds.Tables("Agenda") Is Nothing Then
            lblAgenda.Text = ""
        Else
            lblAgenda.Text = ds.Tables("Agenda").Rows(0).Item("agenda").ToString
        End If
        grdparticipantes.DataSource = ds.Tables("Participante")
        grdparticipantes.DataBind()

        If ds.Tables("Desarrollo") Is Nothing Then
            lbldesarrollo.Text = ""
        Else

            If ds.Tables("Desarrollo").Rows.Count > 0 Then
                For indi As Integer = 0 To ds.Tables("Desarrollo").Rows.Count - 1
                    'se trae el nombre con el numero del registro almacenado en los comentarios
                    gral.admon_usuarios(2, Mid(ds.Tables("Desarrollo").Rows(indi).Item("Responsable"), 1))
                    'concatena todas los desarrollos redactados por los participantes de la reunion
                    lbldesarrollo.Text = lbldesarrollo.Text & "&nbsp;&nbsp;" & ds.Tables("Desarrollo").Rows(indi).Item("punto").ToString & " por :" & gral.DatasetFill2.Tables(0).Rows(0).Item(1).ToString & ""
                    'separador de Desarrollo
                    If indi = ds.Tables("Desarrollo").Rows.Count - 1 Then
                        lbldesarrollo.Text = lbldesarrollo.Text & "."
                    Else
                        lbldesarrollo.Text = lbldesarrollo.Text & ";"
                    End If
                Next
            End If
        End If


        If ds.Tables("Acta").Rows(0).Item("Eval_reunion") > 85 Then
            lblno.Text = ""
            lblsi.Text = "X"
        Else
            lblsi.Text = ""
            lblno.Text = "X"
        End If

        grdcompromisos.DataSource = ds.Tables("Compromisos")
        grdcompromisos.DataBind()
        lblReviso.Text = ds.Tables("Acta").Rows(0).Item("reviso").ToString
        lblAprobo.Text = ds.Tables("Acta").Rows(0).Item("Aprobo").ToString

    End Sub

    Protected Sub BtnVoBo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnVoBo.Click
        gral.Acta("VoBo", , , Hflid_acta.Value)
        'exportar a pdf
    End Sub
End Class
