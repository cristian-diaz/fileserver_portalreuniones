<%@ Language="VBScript" %>
<%    
   Response.buffer = true
   Response.ContentType = "application/vnd.ms-excel"
   Response.AddHeader "Content-Disposition", "filename=Reporte_Reuniones.xls;" 
   'Server.ScriptTimeout = 1200    
%>

<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim Sql
Set Conexion = AbrirConexionBD(cadenaConexionDBQ)

Dim horaInicio, horaFin, fecha1, fecha2, valueOpc
 
 valueOpc = request("valueOpc")
  
 horaInicio = request("horaInicio")&":"&request("minInicio")&":00 "&request("ampm1")
 horaFin = request("horaFin")&":"&request("minFin")&":00 "&request("ampm2")

 horaInicioe = request("horaInicioe")&":"&request("minInicioe")&":00 "&request("ampm1e")
 horaFine = request("horaFine")&":"&request("minFine")&":00 "&request("ampm2e")
 
 fecha1 = request("anioI")&"/"&request("mesI")&"/"&request("diaI")
 fecha2 = request("anioF")&"/"&request("mesF")&"/"&request("diaF")
 
 if valueOpc = "R" then 
 
	 Sql = " SELECT * FROM tiporeunion WHERE tipo_nm = 'REUNION' "
	 if request("pr1") <> "" then
	 Sql =  Sql & " AND tip_asoc = '"&request("tipoReunion")&"' "
	 end if
	 if request("pr2") <> "" then
	 Sql =  Sql & " AND ((nombre) LIKE '%"&request("nombreReunion")&"%') "
	 end if
	 if request("pr5") <> "" then
	 Sql =  Sql & " AND horainicio = #"&horaInicio&"# " 
	 end if
	 if request("pr6") <> "" then
	 Sql =  Sql & " AND horafin = #"&horaFin&"# " 
	 end if
	 
	on error resume next
	Set ADO_RecordSet = AbrirRecordSet(Conexion, Sql)
	'response.Write Sql
	
 %>
 
<TABLE width="82%" border=1 cellPadding=0 cellSpacing=0>
	<colgroup>
		<col width="34%">
		<col width="13%">
		<col width="9%">
		<col width="10%">
		<col width="13%">
		<col width="10%">
		<col width="11%">
	</colgroup>
	  <TR>
	    <TH colspan="7"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">LISTADO DE REUNIONES </font></TH>
  	  </TR>
	  <TR> 
		<TH width="34%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Reuni�n</font></TH>
		<TH width="13%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tipo de Reuni&oacute;n</font></TH>
		<TH width="9%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Activa </font></TH>
		<TH width="10%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora de Inicio </font></TH>
		<TH width="13%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora de Fin  </font></TH>
		<TH width="10%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Duraci&oacute;n</font></TH>
		<TH width="11%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Autom&aacute;tica</font></TH>
	  </TR>
	<%	if not ADO_RecordSet.EOF then
			while not ADO_RecordSet.EOF	
							
				Response.Write("<TR><TD width='34%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("nombre") & "</font></TD>")
				Response.Write("<TD width='13%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("tip_asoc") & "</font></TD>")
				if ADO_RecordSet("activa") = -1 then
				Response.Write("<TD width='9%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> SI </font></TD>")
				else
				Response.Write("<TD width='9%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> NO </font></TD>")
				end if
				Response.Write("<TD width='10%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> " & ADO_RecordSet("horainicio") & " </font></TD>")
				Response.Write("<TD width='13%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> " & ADO_RecordSet("horafin") & " </font></TD>")
				Response.Write("<TD width='10%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("duracion") & "</font></TD>")
				if ADO_RecordSet("validarasistentes") = -1 then
				Response.Write("<TD width='9%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> SI </font></TD>")
				else
				Response.Write("<TD width='9%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> NO </font></TD>")
				end if
				ADO_RecordSet.MoveNext
			wend
		end if
	%>
	</TABLE>
 <%
 else if valueOpc = "C" then
 
 	 Sql = " SELECT c.*, tl.nombreusuario "
	 Sql =  Sql & " FROM t_logins tl, compromisos c "
	 Sql =  Sql & " WHERE c.delegado = tl.login "
	 
	 if request("pr9") <> "" then
	 Sql =  Sql & " AND ((c.tema) LIKE '%"&request("tema")&"%') "
	 end if
	 if request("pr10") <> "" then 
	 Sql =  Sql & " AND ((c.accion) LIKE '%"&request("actividad")&"%') "
	 end if
	 if request("pr11") <> "" then
	 Sql =  Sql & " AND ((c.tipo) LIKE '%"&request("nomReunion")&"%') "
	 end if
	 if request("pr12") <> "" then
	 Sql =  Sql & " AND ((c.responsable) LIKE '%"&request("responsable")&"%') "
	 end if
 	 
	on error resume next
	Set ADO_RecordSet = AbrirRecordSet(Conexion, Sql)
	'response.Write Sql
	
 %>
 
<TABLE width="82%" border=1 cellPadding=0 cellSpacing=0>
	<colgroup>
		<col width="20%">
		<col width="14%">
		<col width="14%">
		<col width="16%">
		<col width="17%">
		<col width="7%">
		<col width="7%">
		<col width="5%">
	</colgroup>
	  <TR>
	    <TH colspan="8"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">LISTADO DE COMPROMISOS </font></TH>
  	  </TR>
	  <TR> 
		<TH width="20%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Reuni&oacute;n</font></TH>
		<TH width="14%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tema</font></TH>
		<TH width="14%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Acci&oacute;n</font></TH>
		<TH width="16%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Responsable</font></TH>
		<TH width="17%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Soluci�n </font></TH>
		<TH width="7%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha Compromiso </font></TH>
		<TH width="7%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Status</font></TH>
		<TH width="5%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> Verificado</font></TH>
	  </TR>
	<%	if not ADO_RecordSet.EOF then
			while not ADO_RecordSet.EOF	
							
				Response.Write("<TR><TD width='20%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("tipo") & "</font></TD>")
				Response.Write("<TD width='14%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("tema") & "</font></TD>")
				Response.Write("<TD width='14%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("accion") & "</font></TD>")				
				Response.Write("<TD width='16%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("responsable") & "</font></TD>")				
				Response.Write("<TD width='17%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("solucion") & "</font></TD>")				
				Response.Write("<TD width='7%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("fechac") & "</font></TD>")				
				Response.Write("<TD width='7%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("status") & "</font></TD>")				
				if ADO_RecordSet("verificado") = -1 then
				Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> SI </font></TD>")
				else
				Response.Write("<TD width='5%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> NO </font></TD>")
				end if
				
				ADO_RecordSet.MoveNext
			wend
		end if
	%>
	</TABLE>
 <%
 
 else if valueOpc = "E" then

	 Sql = " SELECT DISTINCT t.nombre, t.tip_asoc, i.item "
	 Sql =  Sql & " ,rc.calificacion, re.totalpuntos "
	 Sql =  Sql & " ,tl.login, tl.nombreusuario "
	 Sql =  Sql & " ,re.fecha "
	 Sql =  Sql & " ,re.lugar "
	 Sql =  Sql & " ,re.comentario "
	 Sql =  Sql & " ,re.hora_inici_real as horaInicioReal "
	 Sql =  Sql & " ,re.hora_final_real as horaFinReal "
	 Sql =  Sql & " ,re.hora_estimada_inicio as horaInicioPlaneada "
	 Sql =  Sql & " ,re.hora_estimada_final as horaFinPlaneada "
	 Sql =  Sql & " FROM tiporeunion t, resultados_eval re, asistentesreunion a, resultados_calif rc, item_evaluacion i, t_logins tl, permisos p "
	 Sql =  Sql & " WHERE t.tipo_nm = 'REUNION' "
	 Sql =  Sql & " AND t.id = a.idreunion "
	 Sql =  Sql & " AND p.id_reunion = t.id "
	 Sql =  Sql & " AND re.no_eval = a.no_eval " 
	 Sql =  Sql & " AND rc.no_eval = a.no_eval "
	 Sql =  Sql & " AND rc.id_item = i.id_item "
	 Sql =  Sql & " AND p.id_reunion = t.id "
	 Sql =  Sql & " AND p.login = tl.login AND p.permiso = 'M' "
	
	 if request("pr15") <> "" then		
	 Sql =  Sql & " AND ((t.nombre) LIKE '%"&request("nomReu")&"%') "
	 end if
	 
	 if request("pr7") <> "" then
	 Sql =  Sql & " AND i.id_item = "&request("itemEvaluacion")&" "
	 end if
	 
	 if request("pr3") <> "" then 
	 Sql =  Sql & " AND re.fecha >= #"&fecha1&"# " 
	 end if
	 
	 if request("pr4") <> "" then 
	 Sql =  Sql & " AND re.fecha <= #"&fecha2&"# " 
	 end if
	 
	 if request("pr13") <> "" then
	 Sql =  Sql & " AND re.hora_inici_real = #"&horaInicioe&"# " 
	 end if
	 
	 if request("pr14") <> "" then
	 Sql =  Sql & " AND re.hora_final_real = #"&horaFine&"# " 
	 end if
	 
	 if request("pr8") <> "" then 
	 Sql =  Sql & " AND re.totalpuntos >= "&request("puntaje")&" "
	 end if
	 
	 'Sql =  Sql & " GROUP BY t.nombre "
	 	 
	on error resume next
	Set ADO_RecordSet = AbrirRecordSet(Conexion, Sql)
	'response.Write Sql
	'response.End
	
 %>
 
<TABLE width="126%" border=1 cellPadding=0 cellSpacing=0>
	<colgroup>
		<col width="256%">
		<col width="203%">
		<col width="76%">
		<col width="69%">
		<col width="128%">
		<col width="95%">
		<col width="95%">
		<col width="95%">
		<col width="95%">
		<col width="95%">
		<col width="155%">
		<col width="163%">
	</colgroup>
	  <TR>
	    <TH colspan="12"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">LISTADO DE EVALUACIONES </font></TH>
  	  </TR>
	  <TR> 
		<TH width="256"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Reuni�n</font></TH>
		<TH width="203"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Item Evaluado </font></TH>
		<TH width="76"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Puntaje Item</font> </TH>
		<TH width="69"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Puntaje Evaluaci&oacute;n </font></TH>
		<TH width="128"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Evaluador</font></TH>
		<TH width="95"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora Inicio Planeada </font></TH>
		<TH width="95"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora Fin Planeada </font></TH>
		<TH width="95"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Hora Inicio Real </font></TH>
		<TH width="95"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Hora Fin Real </font></TH>
		<TH width="95"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha Evaluaci&oacute;n</font></TH>
		<TH width="155"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Lugar</font></TH>
		<TH width="163"><p><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Comentario</font></TH>
	  </TR>
	<%	
		if not ADO_RecordSet.EOF	then
		while not ADO_RecordSet.EOF	
						
		Response.Write("<TR><TD width='256%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("nombre") & "</font></TD>")
		Response.Write("<TD width='203%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("item") & "</font></TD>")
		Response.Write("<TD width='76%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> " & ADO_RecordSet("calificacion") & " </font></TD>")
		Response.Write("<TD width='69%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'> " & ADO_RecordSet("totalpuntos") & " </font></TD>")
		Response.Write("<TD width='128%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("nombreusuario") & "</font></TD>")
		Response.Write("<TD width='95%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaInicioPlaneada") & "</font></TD>")
		Response.Write("<TD width='95%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaFinPlaneada") & "</font></TD>")
		Response.Write("<TD width='95%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaInicioReal") & "</font></TD>")
		Response.Write("<TD width='95%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("horaFinReal") & "</font></TD>")
		Response.Write("<TD width='95%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("fecha") & "</font></TD>")
		Response.Write("<TD width='155%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("lugar") & "</font></TD>")
		Response.Write("<TD width='163%'><font size='1' face='Verdana, Arial, Helvetica, sans-serif'>" & ADO_RecordSet("comentario") & "</font></TD></tr>")
		
			ADO_RecordSet.MoveNext
			wend

		end if	
			ADO_RecordSet.Close
			Set ADO_RecordSet = Nothing
									
	%>
</TABLE>
 <%
 
 end if 'Fin if valueOpc = E
 end if 'Fin if valueOpc = C
 end if 'Fin if valueOpc = R
 
 	if err.description <> "" then
		'response.Write(err.description)
		'response.Write " --> " & Sql
	end if
	
	ADO_RecordSet.Close
	Set ADO_RecordSet = Nothing
 %>
