﻿Imports System.Data
Imports System.Data.OleDb
Partial Class frm_docAsociados
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Dim idrnion As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            str_Oopcion = Request.QueryString("idReunion")


            Dim ds As New DataSet
            Dim dtt_archivos As New DataTable   '--0

            Me.ds_evalua = ds
            Me.ds_evalua.Tables.Add("dtt_archivos")



            Me.evalu.Trae_archivos(str_Oopcion)
            Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))
            Me.grd1.DataSource = Me.ds_evalua.Tables(0)
            Me.grd1.DataBind()

        End If
    End Sub

    Public Property str_Oopcion() As String
        Get
            Return ViewState("LOopcion")
        End Get
        Set(ByVal value As String)
            ViewState("LOopcion") = value
        End Set
    End Property


    Public Sub prcCrearDataset()
        'TABLAS
        Dim ds As New DataSet
        Dim dtt_archivos As New DataTable   '--0

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_archivos")

    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "archives"
                Me.evalu.Trae_archivos(idrnion)
                Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))

        End Select
    End Sub




#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region



End Class
