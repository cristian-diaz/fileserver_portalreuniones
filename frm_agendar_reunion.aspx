<%@ Page Language="VB"  ValidateRequest="false" MasterPageFile="~/Evaluaciones/Plantilla/P_Plantilla_Ajax_Aux.master" AutoEventWireup="false" CodeFile="frm_agendar_reunion.aspx.vb" Inherits="Evaluaciones_Plantilla_Default" title="Untitled Page" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>
 

 
<asp:Content ID="Content1" ContentPlaceHolderID="CntCuerpo" Runat="Server">
   
   <div id="prueba" runat="server">
       &nbsp;</div>
   
   
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 513px; text-align: left; height: 19px;">
                <asp:Label ID="Label1" runat="server" Text="Escoja la reuni�n:" SkinID="TitTabla"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 513px; height: 22px;" >
                                    <asp:DropDownList ID="drlist11" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlist11_SelectedIndexChanged" Width="512px">
                                    </asp:DropDownList></td>
                            </tr>
        <tr>
            <td style="width: 513px; height: 22px; text-align: left">
                <asp:Label ID="Label2" runat="server" Width="144px" SkinID="TitTabla" Visible="False">Fechas Reuniones:</asp:Label></td>
        </tr>
                        </table>
    <asp:GridView ID="grid1" runat="server" AutoGenerateColumns="False" Font-Names="Arial"
                    Font-Size="X-Small" Width="320px" SkinID="GvwEvaluacion">
                    <Columns>
                        <asp:TemplateField HeaderText="Fecha">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_fecha" runat="server" Text='<%# Bind("fecha", "{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Hora">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="80%" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_hora" runat="server" Text='<%# Bind("hora", "{0:hh:mm:ss}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Porcentaje Reunion">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("porcentaje") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("porcentaje") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Agenda">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImgbtnAgenda" runat="server" CausesValidation="False" CommandName="select" ImageUrl="~/img/agenda_2.gif" OnClick="ImgbtnAgenda_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="temas tratados en la reuni&#243;n">
                            <EditItemTemplate>                          
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                               <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/img/Desarrollo.gif" CausesValidation="False" CommandName="Update"/>
                                <asp:HiddenField ID="hdd_idreunion" runat="server" Value='<%# Bind("idreunion") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Generar acta autom&#225;tica">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="BtnActas" runat="server" ImageUrl="~/img/Actas_1.gif" CausesValidation="False" CommandName="select" OnClick="BtnActas_Click" />&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            &nbsp; &nbsp;
            &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
            <cc2:ModalPopupExtender ID="MppeGeneraracta" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="BtnCerrar" DropShadow="True"  PopupControlID="Pnlgeneraracta"
                TargetControlID="Lblgeneraracta">
            </cc2:ModalPopupExtender>
            <cc2:ModalPopupExtender ID="MppeGenerarAgenda" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="Button6" DropShadow="True" Enabled="True" PopupControlID="PnlGenerarAgenda"
                TargetControlID="lblgeneraragenda">
            </cc2:ModalPopupExtender>
            <asp:HiddenField ID="HflIdActa" runat="server" />
            <asp:HiddenField ID="HflAccion" runat="server" Value="0" />
    
                        <asp:HiddenField ID="hd_hora" runat="server" />
                        <asp:HiddenField ID="hd_fecha" runat="server" />   
                         <asp:HiddenField ID="hd_reunion" runat="server" />
 
                                    
                 <asp:HiddenField ID="hd_punto" runat="server" />
            &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp;&nbsp; &nbsp;<br /><asp:Panel style="display: none" BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Height="200px" ID="Pnlgeneraracta" runat="server" Width="412px">
        <br />
                <table style="width: 403px">
                    <tr>
                        <td colspan="4" style="width: 400px">
        &nbsp;<asp:Label ID="Lblgeneraracta" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 400px">
                            <asp:Label ID="lblquienaprueba" runat="server" Text="Quien aprueba el acta?"></asp:Label>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 400px">
                            <asp:DropDownList ID="DdlParticipantes" runat="server" Width="100%">
                            </asp:DropDownList><br />
                            <br />
                <asp:Label ID="lbldetall" runat="server" Text="Se le enviara un correo electronico al encargado de aprobar el acta y no sepodra realizar cambios mientras se le da el visto bueno"></asp:Label></td>
                    </tr>
                </table>
                <asp:Button ID="BtnGeneraracta" runat="server" Text="Agregar" style="position: static" />
        <asp:Button ID="BtnCerrar" runat="server" CssClass="Botones" Text="Cerrar" style="position: static" />&nbsp;<br>
        &nbsp;&nbsp;
        <br>
    </asp:Panel><asp:Panel style="display: none" BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Height="200px" ID="PnlGenerarAgenda" runat="server" Width="412px">
        <br />
        <asp:Label ID="lblgeneraragenda" runat="server" ForeColor="Red"></asp:Label>
        &nbsp;&nbsp;<br />
        <br />
        <br />
        <br>
        <br>
        <asp:Button ID="BtnGenerarAgenda" runat="server" Text="Agregar" style="position: static" />
        <asp:Button ID="Button6" runat="server" CssClass="Botones" Text="Cerrar" style="position: static" /><br />
        <br />
        <br />
    </asp:Panel>
        </asp:View>
        <asp:View ID="View2" runat="server">
            &nbsp;<table border="0" cellpadding="0" cellspacing="0" style="width: 492px; height: 588px;">
                                <tr>
                                    <td style="height: 13px; background-color: #99cc66; text-transform: uppercase; font-family: Arial;" colspan="3">
                                        <strong><span style="color: white">
                                            <asp:Label ID="Nom_PanelReunion" runat="server"></asp:Label></span></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 18px">
                                        <asp:Label ID="lblPorcentajeReunion" runat="server"></asp:Label>
                                        % Reuni�n
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left">
                                        <table style="width: 614px; height: 218px">
                                            <tr>
                                                <td colspan="2" rowspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Panel ID="pnlDescripcion" runat="server" Height="200px" ScrollBars="Vertical" Width="100%">
                                                        <asp:Label ID="lblDescipcionanterior" runat="server" Width="100%"></asp:Label></asp:Panel>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    Desarrollo de la reunion:</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 376px">
                                        <asp:TextBox ID="txt_tema" runat="server" Height="374px" TextMode="MultiLine" Width="870px"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texto" style="text-align: left" colspan="3" rowspan="2">
                                        </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td class="texto" style="width: 100px; height: 19px; text-align: left">
                                        Responsable</td>
                                    <td style="width: 100px; height: 19px">
                                    </td>
                                    <td style="width: 100px; height: 19px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 22px; text-align: left">
                                        <asp:Label ID="lblResponsable" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                         
                            <asp:HiddenField ID="hd_punto_agendar" runat="server" />
                            <asp:HiddenField ID="hflcomentario" runat="server" />
                            <asp:HiddenField ID="hflid_puntos_reunion" runat="server" />
                            &nbsp; &nbsp;
                            &nbsp;&nbsp;&nbsp; &nbsp;<asp:HiddenField ID="HfNoEval" runat="server" />
                            &nbsp;<br />
                            <br />
                            <asp:Button ID="Button1" runat="server" Text="Agregar" />
                                        <asp:Button ID="btn_cancel" runat="server" CssClass="Botones" Text="Cerrar" /></asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <div id="actapdf" runat="server">
            <table width="800">
                <tr>
                    <td colspan="2" rowspan="3">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/img/logoEcopetrol.gif" /></td>
                    <td colspan="4">
                        <asp:Label ID="Label15" runat="server" Text="ACTA DE REUNION" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="4" style="height: 40px">
                        <asp:Label ID="lblgestion_info" runat="server" Font-Bold="True"></asp:Label><br />
                        <asp:Label ID="lbldirtecinfo" runat="server" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table width="100%">
                            <tr>
                                <td style="width: 100px; height: 21px">
                                    <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Size="Small" Text="CODIGO"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblcodigo_acta" runat="server" Font-Bold="True"></asp:Label></td>
                                <td style="width: 100px; height: 21px">
                                    <asp:Label ID="Label31" runat="server" Font-Bold="True" Font-Size="Small" Text="ELABORADO"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblfecha_acta" runat="server" Font-Bold="True"></asp:Label></td>
                                <td style="width: 100px; height: 21px">
                                    <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Size="Small" Text="VERSION"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblvesion" runat="server" Font-Bold="True"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="6">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 25px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label7" runat="server" Text="Acta No:" Font-Bold="True"></asp:Label></td>
                    <td style="width: 100px; height: 25px;" align="left">
                        <asp:Label ID="lblActaNo" runat="server"></asp:Label></td>
                    <td style="width: 100px; height: 25px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label12" runat="server" Text="Tema:" Font-Bold="True"></asp:Label></td>
                    <td style="height: 25px;" colspan="3" align="left">
                        <asp:Label ID="lblTema" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 23px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label10" runat="server" Text="Fecha:" Font-Bold="True"></asp:Label></td>
                    <td style="width: 100px; height: 23px;" align="left">
                        <asp:Label ID="lblFechaEval" runat="server"></asp:Label></td>
                    <td style="width: 100px; height: 23px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label13" runat="server" Text="Ubicacion:" Font-Bold="True"></asp:Label></td>
                    <td style="height: 23px;" colspan="3" align="left">
                        <asp:Label ID="lblUbicacion" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 21px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label11" runat="server" Text="Hora Inicio:" Font-Bold="True"></asp:Label></td>
                    <td style="width: 100px; height: 21px;" align="left">
                        <asp:Label ID="lblHoraInicio" runat="server"></asp:Label></td>
                    <td style="width: 100px; height: 23px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label14" runat="server" Text="Hora Fin:" Font-Bold="True"></asp:Label></td>
                    <td style="width: 100px; height: 21px;" align="left">
                        <asp:Label ID="lblHoraFin" runat="server"></asp:Label></td>
                    <td style="width: 100px; height: 21px;" align="left">
                    </td>
                    <td style="width: 100px; height: 21px;" align="left">
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px; text-align: left;" colspan="6">
                        <asp:Label ID="Label16" runat="server" Text="1. ANTES DE LA REUNION" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 100px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label26" runat="server" Font-Bold="True">Objetivo</asp:Label></td>
                    <td colspan="5" align="left">
                        <asp:Label ID="lblObjetivo" runat="server" Font-Bold="True">N/A</asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 100px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label17" runat="server" Text="Agenda" Font-Bold="True"></asp:Label></td>
                    <td colspan="5" align="left">
                        <asp:Label ID="lblAgenda" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; background-color: #66cc33; text-align: left;" colspan="6" align="left">
                        <asp:Label ID="Label33" runat="server" Font-Bold="True" Text="Participantes"></asp:Label>
                        <asp:Label ID="Label8" runat="server" Text="(Personas cuya participacion es impresindible para lograr los objetivos)"
                            Width="583px"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:GridView ID="grdparticipantes" runat="server" AutoGenerateColumns="False" Font-Overline="False"
                            HorizontalAlign="Left" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Nombre">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" style="width: 100px; height: 21px">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Nombre") %>' Width="380px"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dependencia"></asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CC33" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="6">
                        <asp:Label ID="Label18" runat="server" Text="2. DESARROLLO DE LA REUNION" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td style="background-color: #66cc33; text-align: left; height: 21px;" colspan="6">
                        &nbsp;<asp:Label ID="Label9" runat="server" Text="(Descripcion de los puntos tratados en la reunion)"></asp:Label></td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="6"><asp:Label ID="lbldesarrollo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px; text-align: left;" colspan="6" align="left">
                        <asp:Label ID="Label19" runat="server" Text="3. EVALUACION DE LA REUNION" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="6" align="left">
                        &nbsp;<asp:Label ID="Label20" runat="server" Text="Logramos alcanzar nuestra meta en esta reunion"></asp:Label>&nbsp;
                        <asp:Label ID="Label21" runat="server" Text="Si:"></asp:Label>&nbsp; &nbsp;<asp:Label ID="lblsi" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                        <asp:Label ID="Label22" runat="server" Text="No:"></asp:Label>&nbsp;
                        <asp:Label ID="lblno" runat="server" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="6" align="left">
                        &nbsp;<asp:Label ID="Label23" runat="server" Text="Si no, como y cuando lo haremos?"></asp:Label>&nbsp;
                        _______________________________________________________</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="6" align="left">
                        <asp:Label ID="Label24" runat="server" Text="4. COMPROMISOS" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:GridView ID="grdcompromisos" runat="server" Width="100%">
                            <HeaderStyle BackColor="#66CC33" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 21px; background-color: #66cc33;" align="left">
                        <asp:Label ID="Label25" runat="server" Text="Asistentes" Font-Bold="True"></asp:Label></td>
                    <td style="height: 21px;" colspan="5" align="left">
                        <asp:Label ID="Label27" runat="server" Text="N/A"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6" style="height: 21px" align="left">
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="left" style="height: 69px">
                        <table style="text-align: center" width="100%">
                            <tr>
                                <td style="width: 100px; background-color: #66cc33">
                                    <asp:Label ID="Label28" runat="server" Text="Revis�" Font-Bold="True"></asp:Label></td>
                                <td style="width: 100px; background-color: #66cc33">
                                    <asp:Label ID="Label29" runat="server" Text="Aprob�" Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 100px; text-align: left;">
                                    <asp:Label ID="lblReviso" runat="server" Font-Overline="False"></asp:Label></td>
                                <td style="width: 100px; text-align: left;">
                                    <asp:Label ID="lblAprobo" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
            <br />
            
                &nbsp;</div>
            &nbsp;&nbsp;<br />
            <asp:Button ID="Button3" runat="server" Text="Generar PDF" /><br />
        </asp:View>
        <asp:View ID="View4" runat="server">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 492px; height: 588px;">
                <tr>
                    <td style="height: 13px; background-color: #99cc66; text-transform: uppercase; font-family: Arial;" colspan="3">
                        <strong><span style="color: white">
                            <asp:Label ID="Nom_PanelReunion2" runat="server"></asp:Label></span></strong></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 18px">
                        <asp:Label ID="lblPorcentajeReunion2" runat="server"></asp:Label>
                        % Reuni�n
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: left">
                        <table style="width: 614px; height: 218px">
                            <tr>
                                <td colspan="2">
                                                    <asp:Label ID="Label35" runat="server" Font-Bold="True" Font-Size="Small" Text="Agenda Almacenada hasta el momento"
                                                        Width="280px"></asp:Label>
                                                    <asp:Panel ID="Panel3" runat="server" Height="200px" ScrollBars="Vertical" Width="100%">
                                                        <asp:Label ID="lblagendaAnterior" runat="server" Width="100%"></asp:Label></asp:Panel>
                                                </td>
                                            </tr><tr>
                                                <td colspan="2">
                                                    Agenda de la reunion:</td>
                                            </tr>
                            <tr>
                                <td colspan="2" style="height: 376px">
                                    <asp:TextBox ID="txtAgenda" runat="server" Height="374px" TextMode="MultiLine" Width="870px"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="texto" style="text-align: left" colspan="3" rowspan="2">
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td class="texto" style="width: 100px; height: 19px; text-align: left">
                        Responsable</td>
                    <td style="width: 100px; height: 19px">
                    </td>
                    <td style="width: 100px; height: 19px">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 22px; text-align: left">
                        <asp:Label ID="lblResponsable2" runat="server"></asp:Label></td>
                </tr>
            </table>
            <asp:Button ID="Button2" runat="server" Text="Agregar" /><asp:Button ID="Button4" runat="server" CssClass="Botones" Text="Cerrar" /></asp:View>
        &nbsp;
        <asp:View ID="Impresion" runat="server">
            
            <asp:Label ID="lblinnerhtml" runat="server" Text="Label"></asp:Label>
        </asp:View>
    </asp:MultiView>&nbsp;<br />

    &nbsp;<br />
    <cc1:MsgBox ID="MsgBox1" runat="server" />
</asp:Content>

