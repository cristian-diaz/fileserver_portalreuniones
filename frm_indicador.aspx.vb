﻿Imports System.Data
Imports System.Data.OleDb
Partial Class frm_indicador
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable   '--0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_Nreunion") 'combo de reuniones


        'CARGA NOMBRE DE REUNIONES
        Me.evalu.name_Reunion()
        Me.ds_evalua.Tables("dtt_Nreunion").Merge(Me.evalu.DatasetFill.Tables(0))
        Me.dr_nombre.DataSource = Me.ds_evalua.Tables(1).CreateDataReader
        Me.dr_nombre.DataTextField = "Nombre"
        Me.dr_nombre.DataValueField = "Id"
        Me.dr_nombre.DataBind()

    End Sub

#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region

   
    Protected Sub areas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles areas.SelectedIndexChanged
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable   '--0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion") 'combo de reuniones 
        Me.ds_evalua.Tables.Add("dtt_Nreunion") 'combo de reuniones


        'CARGA NOMBRE DE REUNIONES
        Me.evalu.name_Reunion_Indicador(Me.areas.SelectedValue)
        Me.ds_evalua.Tables("dtt_Nreunion").Merge(Me.evalu.DatasetFill.Tables(0))
        Me.dr_nombre.DataSource = Me.ds_evalua.Tables(1).CreateDataReader
        Me.dr_nombre.DataTextField = "Nombre"
        Me.dr_nombre.DataValueField = "Id"
        Me.dr_nombre.DataBind()
    End Sub
End Class
