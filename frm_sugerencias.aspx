﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_sugerencias.aspx.vb" Inherits="frm_sugerencias" StylesheetTheme="SkinCompromisos" Theme="SkinCompromisos" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>BUZON DE SUGERENCIAS. PORTAL DE COMPROMISOS</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 100px; background-color: white">
                    <span style="font-size: 9pt; font-family: Arial">&nbsp;
                        <asp:Image ID="Image1" runat="server" ImageUrl="img/ecoep.gif" /></span></td>
            </tr>
            <tr>
                <td style="width: 100px; background-color: white; text-align: justify">
                    <span style="font-size: 9pt; font-family: Arial"></span><span style="font-size: 9pt;
                            font-family: Arial">
                        <div style="width: 544px; position: static; height: 64px">
                            Estimado Usuario&nbsp; &nbsp;<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><span style="font-family: Arial"> , con el Objeto de satisfacer sus necesidades de acuerdo al
                            portal
                            le sugerimos nos escriba corto y detalladamente para poder brindarle una solucion
                            a su problema
                            particular.</span></div>
                    </span>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 560px">
            <tr>
                <td style="width: 100px; height: 19px;">
                    &nbsp;<asp:Label ID="Label2" runat="server" Text="OBSERVACION:"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:DropDownList ID="c_observacion" runat="server">
                        <asp:ListItem>Queja</asp:ListItem>
                        <asp:ListItem>Observacion</asp:ListItem>
                        <asp:ListItem>Sugerencia</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" Text="Correo mail:"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 24px;">
                    <asp:TextBox ID="c_mail" runat="server" Width="416px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: left">
                    <asp:Label ID="Label3" runat="server" Text="COMENTARIOS:"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center">
                    <asp:TextBox ID="c_comentario" runat="server" Height="208px" Width="520px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center">
                    <asp:Button ID="Button1" runat="server" Text="Enviar" /></td>
            </tr>
            <tr>
                <td style="width: 100px; text-align: center">
                    <cc1:msgbox id="MsgBox1" runat="server"></cc1:msgbox>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
