<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim ObjOptimizacion
Dim ErrorEnviar, CodigoError
Dim ObjL_RecordSet, ObjL2_RecordSet, ObjL3_RecordSet, ObjL4_RecordSet
Dim StrL_SQL, StrL_SQL2,  StrL_Mensaje, Usuarios, Responsables

Set ObjOptimizacion = AbrirConexionBD(cadenaConexionDBQ)

response.write ("Inicia el proceso de envio de correo compromisos vencidos.....")
'response.end()

Sub EscribirEnArchivo(strError)
'Procedimiento que Escribe el Log de la Aplicaci�n
Dim strCadena
	On Error Resume Next
	Const ForReading = 1, ForWriting = 2, ForAppending = 8	'Constantes de apertura de archivos
	'Const F_FILELOG =  "E:\Inetpub\wwwroot\morichalgll\database\Compromisos\Logs\CompromisosSOA.Log" 'Archivo de log del script
'	Const F_FILELOG =  "D:\Intranet\DataBase\Compromisos\Logs\Compromisos.Log"
    Const F_FILELOG =  "C:\PortalReunion\LogCorreo\Compromisos.Log"
	
	Dim fsodata, ffilelog
	
	Set fsodata = CreateObject("Scripting.FileSystemObject")
	If Not fsodata.FileExists(F_FILELOG) Then
		set ffilelog = fsodata.CreateTextFile(F_FILELOG, ForWriting)	
	End If
	
	Select Case strError
		Case "0" : str_Cadena = "Error al Enviar Mail"
		Case "1" : str_Cadena = "Correo Enviado"
		Case "2" : str_Cadena = "No se encontro usuario en el AD"		
		Case Else: str_Cadena = "Verificar si posee Cuenta de Correo"
	End Select
	
	set ffilelog = fsodata.Opentextfile(F_FILELOG,ForAppending, true)
	ffilelog.Writeline  Date() & " " & Time() & " " & str_Cadena & " Registro: " & ObjL_RecordSet("LOGIN") &  " Compromiso: " & ObjL_RecordSet("NoID") & ""
End Sub

Function EnviarMail(StrL_Asunto, StrL_Cuerpo, StrL_Destino)
'15-Febrero-2006
'Leyder Correa Romero
Dim Mail, StrL_Cadena, rsObjDirMail
Dim IntL_Posicion, IntL_Sitio, IntL_Longitud, StrL_DestinatarioActual, StrL_CadenaDef
Set Mail = CreateObject("CDO.Message")
	StrL_SQL = "SELECT email, grupo FROM t_logins WHERE Login='" & StrL_Destino & "'"

	set rsObjDirMail = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	If rsObjDirMail.EOF Then
		ErrorEnviar = "2"
		CodigoError= "No se encontro en la tabla 't_logins' "
		exit function 
'		response.Write(StrL_SQL)
'		response.End()
	End If
	If CInt(rsObjDirMail("grupo"))=0 Then
		StrL_Destino = rsObjDirMail(0) & ""
		'Mail.AddAddress StrL_Destino Desarrollo
	Else
		strSQL = "SELECT login_grupo_loginid from login_grupo WHERE " & _
			"login_grupo_grupoid ='" & StrL_Destino & "'"
		Set objRsTemp = AbrirRecordSet(ObjOptimizacion, strSQL)
		While Not objRsTemp.EOF
			SQL = "SELECT email FROM t_logins WHERE Login='" & objRsTemp(0) & "'"
			Set ObjRsGrupos = AbrirRecordSet(ObjOptimizacion, SQL)
			If Not IsNull(ObjRsGrupos(0)) Then
				StrL_Destino = ObjRsGrupos(0)
				'Mail.AddAddress ObjRsGrupos(0) 
			End If
			ObjRsGrupos.Close
			Set ObjRsGrupos = Nothing
			objRsTemp.MoveNext
		WEnd
		objRsTemp.Close
		Set objRsTemp = Nothing
	End If
	set rsObjDirMail = Nothing

	If StrL_Destino <> "" Then
		
'Configuracion para envio de correos.  DBC 17-7-15
'This section provides the configuration information for the remote SMTP server.
'Send the message using the network (SMTP over the network).
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="10.1.141.213"
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
'Use SSL for the connection (True or False)
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False 
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
Mail.Configuration.Fields.Update
'End of remote SMTP server configuration section
		'Set Mail = Server.CreateObject("Persits.MailSender")
		'Configurar las opciones elementales como el servidor SMTP y Puerto
		Mail.From = "PortalReuniones@ecopetrol.com.co"
		Mail.to = StrL_Destino
		StrL_Cadena = StrL_Destino
		Mail.Subject = StrL_Asunto
		Mail.HTMLBody = StrL_Cuerpo
		ErrorEnviar = ""
		On Error Resume Next 
		Mail.Send
	 End If
	 Set Mail = Nothing
	
		  If Err <> 0 Then 
			ErrorEnviar="0" 
			CodigoError = Err.Description
		  Else
			ErrorEnviar="1"
			CodigoError = "Mensaje Enviado"
		  End If
End Function

Sub Buscar_Moderadores()
'Realiza la consulta del Id de la Reuni�n
StrL_SQL = "SELECT TipoReunion.Id From TipoReunion " & _
    "WHERE (((TipoReunion.Nombre)='" & ObjL_RecordSet("TIPO") & "')) AND Activa = -1;"
Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL2_RecordSet.EOF Then
    'Realiza la consulta de los Moderadores de la Reuni�n
    StrL_SQL = "SELECT permisos.login +  ' - '  + t_logins.nombreusuario as Moderador " & _
        "FROM t_logins, permisos WHERE t_logins.Login = permisos.login " & _
        "AND (((permisos.id_reunion) = " & ObjL2_RecordSet("Id") & ") And ((permisos.permiso) = 'M')) " & _
        "ORDER BY t_logins.nombreusuario;"
	Set ObjL3_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
    'Construye la cadena que Contiene los moderadores de la reuni�n
    Responsables = ""
    Do Until ObjL3_RecordSet.EOF
        If Responsables = "" Then
                Responsables = ObjL3_RecordSet("Moderador")
            Else
                Responsables = Responsables & "; " & ObjL3_RecordSet("Moderador")
        End If
        ObjL3_RecordSet.MoveNext
    Loop
    ObjL3_RecordSet.Close
End If
ObjL2_RecordSet.Close
Set ObjL2_RecordSet = Nothing
Set ObjL3_RecordSet = Nothing
End Sub

'Consulta de los ccompromisos vencidos para enviar a cada uno de sus responsables
StrL_SQL = "SELECT Compromisos.LOGIN, Compromisos.RESPONSABLE, Compromisos.TIPO," & _
	" Compromisos.TEMA, Compromisos.ACCION, Compromisos.STATUS, Compromisos.FECHAC, " & _
	" Compromisos.NoID FROM Compromisos  WHERE (((Compromisos.STATUS)<>'VERDE')" & _
	" AND ((Compromisos.FECHAC) <= getdate()) AND  ((Compromisos.FECHAC) >= '01/01/2012'))" & _
	" ORDER BY  LOGIN, Compromisos.FECHAC;"
  
  Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
  If Not ObjL_RecordSet.EOF Then
    Do Until ObjL_RecordSet.EOF
      Buscar_Moderadores
	  StrL_Mensaje = "<html><body>"
	  StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Cordial Saludo, " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso Vencido:</small></font></p>"
	  StrL_Mensaje =  StrL_Mensaje & "<table>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reuni�n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
	  If Responsables <> "" Then
		 StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
	  End If
	  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Recuerde realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
	  StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente direcci�n: " 
	  StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
	  StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
	  StrL_Mensaje = StrL_Mensaje & "</html>" 
      EnviarMail ObjL_RecordSet("TIPO") & " - Compromisos Vencidos", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
	  EscribirEnArchivo ErrorEnviar
      StrL_Mensaje = ""
      ObjL_RecordSet.MoveNext
    Loop
  End If
  ObjL_RecordSet.Close
  Set ObjL_RecordSet = Nothing

response.write("termina el proceso de envio de correo compromisos vencidos")
ObjOptimizacion.Close
%>
<script language="JavaScript" type="text/JavaScript">
<!--
window.close();
// -->
</script>