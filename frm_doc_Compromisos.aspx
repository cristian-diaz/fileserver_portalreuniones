﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_doc_Compromisos.aspx.vb" Inherits="frm_doc_Compromisos" Theme="SkinCompromisos" StylesheetTheme="SkinCompromisos" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>

<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=6"/>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Página sin título</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:Label ID="LBL_USUARIO" runat="server"></asp:Label><br />
        <div style="width: 100%; position: static; height: 48px; text-align: center">
            <span style="font-family: Arial"><strong><span style="color: #cc0000">Solo Adjuntar
                Archivos tipo PDF</span><br />
            </strong></span>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 100px; height: 20px; text-align: left;">
                        <asp:Label ID="Label1" runat="server" Text="Reunion:"></asp:Label></td>
                    <td colspan="2" style="text-align: left; height: 20px;">
        <asp:DropDownList ID="dr_listareuniones" runat="server" Width="544px" AutoPostBack="True">
        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 100px; text-align: left;">
                        <asp:Label ID="Label2" runat="server" Text="Adjuntar:"></asp:Label></td>
                    <td style="width: 100px; text-align: left;">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="416px" AlternateText="Usted"  /></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 19px;">
                    </td>
                    <td style="width: 100px; height: 19px;">
                        <asp:Label ID="Label3" runat="server" Width="448px"></asp:Label></td>
                    <td style="width: 100px; height: 19px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px; text-align: center;">
                        <asp:Button ID="Button1" runat="server" Text="Adjuntar" /><br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 21px; text-align: center">
          
    
     <asp:GridView ID="grVw0" runat="server" SkinID="Gvw_little" AutoGenerateColumns="False" Width="424px" AllowPaging="True" AllowSorting="True" DataKeyNames="nombre" PageSize="20" HorizontalAlign="Center" >
                    <Columns>
                        <asp:TemplateField HeaderText="Documentos Asociados a la Reunion">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("nombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_actividad1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("nombre") %>'
                                    CommandName="Select" Text='<%# Bind("nombre") %>'>
                                </asp:LinkButton>  
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" /> 
                        </asp:TemplateField>                                        
                                         
                        
                    </Columns>
               
                </asp:GridView>
                        <br />
                    </td>
                </tr>
            </table>
          
      
    <uc1:logo ID="Logo1" runat="server" />
            <cc1:msgbox id="MsgBox1" runat="server"></cc1:msgbox>
           
        </div>
      
    </form>
</body>
</html>
