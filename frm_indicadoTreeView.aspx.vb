﻿Imports System.Data
Imports System.Data.OleDb
Imports System.web.UI.WebControls
Partial Class frm_indicadoTreeView
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            prcCrearDataset()
            Me.prcCargarOrigen("reunion")
            Me.PrcCargarDatos("reunion")


            prcCargarOrigen("Jerarquia")
            PrcCargarDatos("Jerarquia")

        End If

    End Sub

#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Public Property ds_Jerarquias() As DataSet
        Get
            Return ViewState("dsJerarquias")
        End Get
        Set(ByVal value As DataSet)
            ViewState("dsJerarquias") = value
        End Set
    End Property


#End Region


    Public Sub prcCrearDataset()
        Dim ds As New DataSet

        Dim dtt_evaluaciones As New DataTable
        Dim dtt_indi As New DataTable

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_Nreunion")
        Me.ds_evalua.Tables.Add("dtt_indi")
        Me.ds_evalua.Tables.Add("dtt_re")
        Me.ds_evalua.Tables.Add("dtt_jerarquia")


    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "reunion"
                Me.evalu.name_Reunion()
                Me.ds_evalua.Tables("dtt_Nreunion").Merge(Me.evalu.DatasetFill.Tables(0))


            Case "evaluaciones"
                Me.ds_evalua.Tables("dtt_indi").Clear()
                Me.evalu.indicador_cumplimiento_total(Me.date1.Text, Me.date2.Text)
                Me.ds_evalua.Tables("dtt_indi").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "Jerarquia"

                Me.evalu.pro_jerarquico()
                Me.ds_evalua.Tables("dtt_jerarquia").Merge(Me.evalu.DatasetFill.Tables(0))
                Me.ds_evalua.Tables("dtt_jerarquia").PrimaryKey = New DataColumn() {Me.ds_evalua.Tables("dtt_jerarquia").Columns("IDRECURSO")}



        End Select
    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo

            Case "evaluaciones"
                Me.grid1.DataSource = Me.ds_evalua.Tables("dtt_indi")
                Me.grid1.DataBind()

           
            Case "ere_dep" 'Evaluacion reuniones estrategicas por dependencias
                Me.txt_indi1.Text = Me.evalu.indicador_cumplimiento_dep(Me.txt_titulo.Text, Me.date1.Text, Me.date2.Text) & " %"
                Me.txt_indi1.Style("text-align") = "center"
            Case "cc_dep" 'caso cumplimiento de compromisos
                Me.txt_comp.Text = Me.evalu.cumplimiento_compromisos_dep(Me.txt_titulo.Text, Me.date1.Text, Me.date2.Text) & " %"
                Me.txt_comp.Style("text-align") = "center"
            Case "ca_dep" ' Cumplimiento de Agendas
                Me.txt_cag.Text = Me.evalu.cumplimiento_agendas_dep(Me.txt_titulo.Text, Me.date1.Text, Me.date2.Text) & " %"
                Me.txt_cag.Style("text-align") = "center"
            Case "implementacion_dependencias"
                Dim ttal As Integer
                Dim val_1 As Integer = Me.evalu.indicador_cumplimiento_dep(Me.txt_titulo.Text, Me.date1.Text, Me.date2.Text)
                Dim val_2 As Integer = Me.evalu.cumplimiento_agendas_dep(Me.txt_titulo.Text, Me.date1.Text, Me.date2.Text)
                Dim val_3 As Integer = Me.evalu.cumplimiento_compromisos_dep(Me.txt_titulo.Text, Me.date1.Text, Me.date2.Text)

                ttal = (val_1 * 0.4) + (val_2 * 0.4) + (val_3 * 0.2)
                Me.txt_TTAL.Text = ttal & " %"
                Me.txt_TTAL.Style("text-align") = "center"

                If ttal >= 85 Then
                    Me.txt_TTAL.BackColor = Drawing.Color.LimeGreen
                ElseIf ttal < 85 Then
                    Me.txt_TTAL.BackColor = Drawing.Color.Red
                End If


                If val_1 >= 80 Then
                    Me.txt_indi1.BackColor = Drawing.Color.LimeGreen

                ElseIf val_1 < 80 Then
                    Me.txt_indi1.BackColor = Drawing.Color.Red
                End If

                If val_2 >= 85 Then
                    Me.txt_cag.BackColor = Drawing.Color.LimeGreen
                ElseIf val_2 < 85 Then
                    Me.txt_cag.BackColor = Drawing.Color.Red
                End If
              
                If val_3 >= 85 Then
                    Me.txt_comp.BackColor = Drawing.Color.LimeGreen
                ElseIf val_3 < 85 Then
                    Me.txt_comp.BackColor = Drawing.Color.Red
                End If

            
            Case "Jerarquia"
                Me.prcCargarArbolJerarquias(Me.ds_evalua.Tables("dtt_jerarquia").DefaultView(0), Nothing)

        End Select

    End Sub
    Private Sub prcCargarArbolJerarquias(ByVal Recurso As DataRowView, ByVal nodePadre As TreeNode)

        Dim dataViewHijos As DataView = ds_evalua.Tables("dtt_jerarquia").DefaultView 'Recurso("Tipo_Recurso")).DefaultView

        Dim SubTitulo As String = ""
        Dim NombreNodo As String = ""
        Dim ValueNodo As String = ""

        If IsNothing(nodePadre) Then
            Dim locationitem_id As String = ConfigurationManager.AppSettings("locationitem_id")
            dataViewHijos = New DataView(Me.ds_evalua.Tables("dtt_jerarquia"), "locationitem_tp='VICEPRESIDENCIA'", "", DataViewRowState.CurrentRows)
        Else
            dataViewHijos = New DataView(Me.ds_evalua.Tables("dtt_jerarquia"), "locationitem_id=" & Recurso("IDRECURSO"), "order_nb ASC", DataViewRowState.CurrentRows)
        End If

        'Agregar al TreeView los nodos Hijos que se han obtenido en el DataView.
        For Each dataRowCurrent As DataRowView In dataViewHijos
            Dim nuevoNodo As TreeNode = New TreeNode()

            nuevoNodo.Text = dataRowCurrent("HIJO_TX").ToString().Trim()
            nuevoNodo.Value = dataRowCurrent("IDRECURSO")
            nuevoNodo.ToolTip = dataRowCurrent("attachitem_tp").ToString
            nuevoNodo.Target = dataRowCurrent("IDRECURSO") & "|" & dataRowCurrent("HIJO") & "|" & "|" & dataRowCurrent("HIJO_tx") & "|" & dataRowCurrent("attachitem_tp") & "|" & dataRowCurrent("attachment_tp") & "|" & dataRowCurrent("locationitem_id") & "|" & dataRowCurrent("PADRE") & "|" & dataRowCurrent("PADRE_tx") & "|" & dataRowCurrent("locationitem_tp") & "|" & dataRowCurrent("order_nb")

            Select Case dataRowCurrent("attachitem_tp")

                Case "GERENCIA", "GERENCIA OPERATIVA"
                    nuevoNodo.ImageUrl = "img/GERENCIA.gif"
                Case "AREA ADMINISTRATIVA"
                    nuevoNodo.ImageUrl = "Img/ImgAreasAdministrativas.gif"
                Case "DEPARTAMENTO"
                    nuevoNodo.ImageUrl = "Img/DEPARTAMENTO.gif"
                Case "COORDINACION"
                    nuevoNodo.ImageUrl = "Img/COORDINACION.gif"
                Case "AREA OPERATIVA"
                    nuevoNodo.ImageUrl = "Img/ImgAreaOperativa.gif"
                Case "REGIONAL"
                    nuevoNodo.ImageUrl = "Img/ImgRegional.gif"
            End Select

            'Si el parámetro nodoPadre es nulo es porque es la primera llamada, son los Nodos
            'del primer nivel que no dependen de otro nodo.
            If IsNothing(nodePadre) Then
                tree_jerarquia.Nodes.Add(nuevoNodo)
                'se añade el nuevo nodo al nodo padre.
            Else
                nodePadre.ChildNodes.Add(nuevoNodo)
            End If
            ' Llamada recurrente al mismo método para agregar los Hijos del Nodo recién agregado.
            prcCargarArbolJerarquias(dataRowCurrent, nuevoNodo)
        Next
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        'Evaluacion reuniones estrategicas por dependencias
        Me.PrcCargarDatos("ere_dep")

        Me.PrcCargarDatos("cc_dep")

        Me.PrcCargarDatos("ca_dep")

        Me.PrcCargarDatos("implementacion_dependencias")


        ''Evaluacion reuniones estrategicas.
        'Me.PrcCargarDatos("ere")
        ''caso cumplimiento de compromisos
        'Me.PrcCargarDatos("cc")
        ''Cumplimiento de Agendas
        '


        'Me.prcCargarOrigen("evaluaciones")
        'Me.PrcCargarDatos("evaluaciones")
        'Me.PrcCargarDatos("implementacion")



    End Sub

    Protected Sub grid1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grid1.PageIndexChanging
        Me.grid1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("evaluaciones")
    End Sub

    Sub PrcActivarSubTitulos()
        blancos()
        Me.txt_titulo.Style("text-align") = "center"
        Me.txt_titulo.Visible = True
        Dim cadena() As String = Split(Me.tree_jerarquia.SelectedNode.Target.Trim, "|")
        Me.txt_titulo.Text = cadena(3)

    End Sub
    
    Protected Sub tree_jerarquia_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tree_jerarquia.SelectedNodeChanged

        PrcActivarSubTitulos()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Me.PrcCargarDatos("ere_dep")

        Me.PrcCargarDatos("cc_dep")

        Me.PrcCargarDatos("ca_dep")

        Me.PrcCargarDatos("implementacion_dependencias")

    End Sub

    Public Sub blancos()
        Me.txt_TTAL.Text = ""
        Me.txt_indi1.Text = ""
        Me.txt_cag.Text = ""
        Me.txt_comp.Text = ""
    End Sub
End Class
