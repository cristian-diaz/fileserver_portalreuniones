<%@ Language="VBScript" %>
<%    
   Response.buffer = true
   Response.ContentType = "application/vnd.ms-excel"
   Response.AddHeader "Content-Disposition", "filename=ComproAsig.xls;"   
   Server.ScriptTimeout = 1200    
%>

<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim objRsTemp, strSQL
Dim objRsUsuarios, objRsReuniones, objRsCompromisos
Set Conexion = AbrirConexionBD(cadenaConexionDBQ)

	strSQL = "SELECT * FROM t_logins WHERE login='" & session("IdUser") & "' AND administrador = 1"
	Set objRsTemp=AbrirRecordset(Conexion,strSQL)

	If objRsTemp.EOF Then
		objRsTemp.Close 
		Set Conexion = Nothing
		Response.redirect("Presentar_Compromisos.asp")
	End If
	
	Session("Administrador")="SI"

'Selecciono Usuarios Activos para Realizar Consulta
strSQL="SELECT Login, nombreusuario FROM t_logins WHERE Activo=1 " & _
	"And Upper(Login) In (" & Request("UsuariosReporte") & ")"

Set objRsUsuarios=AbrirRecordset(Conexion,strSQL)
%>
<table width="90%" border="1">
  <tr>
  <td height="49" colspan="7" align="center" valign="middle"><font size="5">LISTADO DE COMPROMISOS ASIGNADOS POR USUARIO</font></td>
  </tr>
<%
Do Until objRsUsuarios.EOF
	'Se selecciona las Reuniones en las que Participa para Ordenar Compromisos
	strSQL = "SELECT DISTINCT TIPO FROM Compromisos, TipoReunion  " & _
		"WHERE LOGIN='" & objRsUsuarios("Login") & "' " & _
		"AND Compromisos.TIPO = TipoReunion.Nombre " & _
		"AND TipoReunion.Activa = 1"
	
	Set objRsReuniones=AbrirRecordset(Conexion,strSQL)
	If Not objRsReuniones.EOF Then
	%>
	  <tr>
		<td bgcolor="#BFEBFF">Usuario:</td>
	    <td bgcolor="#BFEBFF"><%=Ucase(objRsUsuarios("Login"))%></td>
        <td bgcolor="#BFEBFF" colspan="5"><%=Ucase(objRsUsuarios("nombreusuario"))%></td>
      </tr>
	<%
	Else
	%>
	  <tr>
		<td bgcolor="#BFEBFF">Usuario:</td>
		<td bgcolor="#BFEBFF"><%=UCase(objRsUsuarios("Login"))%></td>
		<td bgcolor="#BFEBFF" colspan="5"><%=Ucase(objRsUsuarios("nombreusuario"))%></td>
	  </tr>
	  <tr>
	  <td align="center" colspan="7" align="center" valign="middle" bgcolor="#FFFFCC">EL USUARIO NO POSEE COMPROMISOS / NO EST� INSCRITO EN REUNIONES</td>
	  </tr>
	<%
	End If
	Do Until objRsReuniones.EOF
		'Generaci�n de Compromisos Ordenados por Reuni�n
		strSQL = "SELECT NoID, TEMA, Accion, FECHAI, FECHAC, STATUS, VERIFICADO " & _
			"FROM Compromisos WHERE TIPO='" & objRsReuniones("TIPO") & "'"
		If Request("txtFechaI") <> "" And Request("txtFechaC") <> "" Then
			If IsDate(Request("txtFechaI"))  And IsDate(Request("txtFechaC"))  Then
				strSQL = strSQL & " AND FECHAI >= '" & Request("txtFechaI") & _
					"' And FECHAC <= '" & Request("txtFechaC") & "'"
			End If
		Else
			If Request("txtFechaI") <> "" And IsDate(Request("txtFechaI")) Then
				strSQL = strSQL & " AND FECHAI >= '" & Request("txtFechaI") & "'"
			Else
				If Trim(Request("txtFechaC")) <> "" And IsDate(Request("txtFechaC")) Then
					strSQL = strSQL & " AND FECHAC >= '" & Request("txtFechaC") & "'"
				End If
			End If
		End If
		strSQL = strSQL & " AND LOGIN='" & objRsUsuarios("Login") & "'"
		
		Select Case Request("GrupoOpciones1")
			Case 1: strSQL = strSQL & " AND VERIFICADO=1"
			Case 2: strSQL = strSQL & " AND VERIFICADO=0"
		End Select
		
		strSQL = strSQL & " ORDER BY NoID"
			response.write(SrtSQL)
	'response.End()

		Set objRsCompromisos=AbrirRecordset(Conexion,strSQL)
		If Not objRsCompromisos.EOF Then
		%>
		  <tr>
			<td bgcolor="#66CCFF">Reuni&oacute;n:&nbsp;</td>
		    <td bgcolor="#66CCFF" colspan="6"><%=Ucase(objRsReuniones("TIPO"))%></td>
	      </tr>
		  <tr align="center">
			<td>No.</td>
			<td>Tema</td>
			<td>Compromiso</td>
			<td>Fecha Inicio </td>
			<td>Fecha Compromiso </td>
			<td>Status</td>
			<td>Verificado?</td>
		  </tr>
		<%
		Do Until objRsCompromisos.EOF
		%>
		  <tr>
			<td><div align="center"><%=objRsCompromisos("NoID")%></div></td>
			<td><%=objRsCompromisos("TEMA")%></td>
			<td><%=objRsCompromisos("Accion")%></td>
			<td><div align="center"><%=objRsCompromisos("FECHAI")%></div></td>
			<td><div align="center"><%=objRsCompromisos("FECHAC")%></div></td>
			<td><div align="center"><%=objRsCompromisos("STATUS")%></div></td>
			<td><div align="center">
		      <%if objRsCompromisos("VERIFICADO")=False Then%>
		      No
		      <%Else%>
		      Si
		      <%End If%>
			</div></td>
		  </tr>
		<%
			objRsCompromisos.MoveNext
			Acertados = Acertados + 1
		Loop
		End If
		objRsCompromisos.Close
		Set objRsCompromisos = Nothing
		objRsReuniones.MoveNext
	Loop
	objRsReuniones.Close
	Set objRsReuniones = Nothing
	objRsUsuarios.MoveNext
	%>
	  <tr><td colspan="7">&nbsp;</td></tr>
	<%
Loop
objRsUsuarios.Close
Set objRsUsuarios = Nothing
%>
</table>
