<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>COMPROMISOS ..INICIO</title>



<link href="Estilos/GCBstyle.css" rel="stylesheet" type="text/css">
<link href="Estilos/Interno.css" rel="stylesheet" type="text/css">
<link href="Estilos/estprincipal.css" rel="stylesheet" type="text/css">
<link href="incluidos/estprincipal.css" rel="stylesheet" type="text/css">

 <script type="text/javascript">
        var GB_ROOT_DIR = "./greybox/";
    </script>

<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
    <link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" />



<style type="text/css">
<!--
.Estilo1 {
	color: #003300;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.Estilo2 {
	color: #009900;
	font-weight: bold;
}
.Estilo3 {color: #009900}
.Estilo5 {font-family: Verdana, Arial, Helvetica, sans-serif}
.Estilo6 {color: #009900; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif; }
.Estilo7 {color: #009900; font-family: Verdana, Arial, Helvetica, sans-serif; }
-->
</style>
</head>

<body>
<div align="center">

  <table width="666" height="482" border="0">
    <tr>
      <th width="11"height="52" scope="col"><span class="Estilo5"></span></th>
      <th width="585" scope="col"><p class="Estilo1"><img src="img/bienv.gif" width="313" height="67" /></p>
<p class="Estilo1">GUIA PARA UNA REUNION EFECTIVA</p></th>
      <th width="48" scope="col"><span class="Estilo5"></span></th>
    </tr>
    <tr>
      <th scope="row"><span class="Estilo5"><strong>1</strong></span></th>
      <td><p align="left" class="Estilo6"><span class="Estilo3">Nombrar el reglamento para una reunion efectiva</span></p>      </td>
      <th width="48" scope="col">
      <a href="Reglamen.asp" title="REGLAMENTO" rel="gb_page_center[750, 600]">
      <img src="img/reglamen.gif" width="45" height="43" border="0" /></a></th>
    </tr>
    <tr>
      <th scope="row"><span class="Estilo5"><strong>2</strong></span></th>
      <td><div align="left" class="Estilo5"><span class="Estilo2">Todas las reuniones deben INICIAR y EVALUAR REUNIóN</span></div></td>
      <th scope="col"><span class="Estilo5"><a href="iniciarReunion.asp" target="mainFrame"><img src="img/iniciaReunion.gif" alt="Iniciar Reuniòn" width="42" height="41" border="0" /></a></span></th>
    </tr>
    <tr>
      <th height="56" scope="col"><span class="Estilo5">3</span></th>
      <th scope="col"><div align="left" class="Estilo7">De a conocer la agenda de la reunión.</div></th>
      <th scope="col"><span class="Estilo5"><img src="img/agenda.gif" width="42" height="41" border="0" /><a href="Presentar_Compromisos.asp"></a></span></th>
    </tr>
    <tr>
      <th scope="col"><span class="Estilo5">4</span></th>
      <th scope="col"><div align="left" class="Estilo7"><span class="Estilo2">Una vez Iniciada la reunión, se procede a Consultar los compromisos</span>.</div></th>
      <th scope="col"><span class="Estilo5"><a href="Presentar_Compromisos.asp"><img src="img/menu/Consultar.gif" alt="Consultar Compromiso" width="47" height="38" border="0" /></a></span></th>
    </tr>
    <tr>
      <th height="57" scope="col"><span class="Estilo5">5</span></th>
      <th scope="col"><div align="left" class="Estilo7">Al terminar el desarrollo de la reunion, EVALUELA.</div></th>
      <th scope="col"><span class="Estilo5"><a href="Evaluaciones/Formularios/Evaluacion.aspx?Evaluacion=True" target="mainFrame"><img src="img/menu/Evalar.gif" alt="Iniciar Reuniòn" width="42" height="41" border="0" /></a></span></th>
    </tr>
    <tr>
      <th height="39" scope="col"><span class="Estilo5">6</span></th>
      <th scope="col"><div align="left" class="Estilo7">Revise las estadisticas de efectividad de la reunion.</div></th>
      <th scope="col"><a href="Evaluaciones/Formularios/grafica_rangofecha.aspx"><img src="img/kchart.png" alt="Consultar Compromiso" width="48" height="48" border="0" /></a></th>
    </tr>
    <tr>
      <th height="45" scope="col"><span class="Estilo5">7</span></th>
      <th scope="col"><div align="left" class="Estilo7">Como vamos segun los indicadores de desempeño.</div></th>
      <th scope="col"><a href="#" onclick="abrirNoBordes('../frm_indicadoTreeView.aspx');"><img src="img/menu/Grafica2.gif"  width="42" height="41" border="0" /></a></th>
    </tr>
    <tr>
      <th scope="col"><span class="Estilo5"></span></th>
      <th scope="col">&nbsp;</th>
      <th scope="col"><span class="Estilo5"></span></th>
    </tr>
    <tr>
      <th scope="row">&nbsp;</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th scope="row">&nbsp;</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <!-- #include file="incluidos/CopyRight.asp" -->
</div>
</body>
</html>
