﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_admonrnion.aspx.vb" Inherits="frm_admonrnion" StylesheetTheme="SkinCompromisos" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>
<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ADMINISTRACION DE REUNIONES</title>
    <link href="App_Themes/SkinCompromisos/stilo.css" rel="stylesheet" type="text/css" />
          
   <SCRIPT LANGUAGE="JavaScript">
function url(direccion) {
hidden = open(direccion ,'NewWindow','top=200,left=250,width=800, height=600,status=yes,resizable=no,scrollbars=no');
}
</SCRIPT>        
</head>
<body style="text-align: center" >
    <form id="form1" runat="server">
  
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div style="width: 528px; position: static; height: 64px">
                    <asp:ImageButton ID="picbtn1" runat="server" ImageUrl="~/img/nuevo.jpg" />
                    <asp:ImageButton ID="picbtn2" runat="server" ImageUrl="~/img/ntipo.jpg" />
                    <asp:ImageButton ID="picbtn3" runat="server" ImageUrl="~/img/ntema.jpg" />
            <asp:ImageButton ID="picbtn4" runat="server" ImageUrl="~/img/borrarr.jpg" /></div>
                    <asp:ImageButton ID="btn_report" runat="server" ImageUrl="~/img/ms_excel.png" OnClick="ImageButton3_Click" />&nbsp;<br />
        <asp:CheckBox ID="chkAllReuniones" runat="server" Text="Todas Las Reuniones" /><br />
        <asp:Label ID="lblExpExcel" runat="server"></asp:Label><br />
        <table border="0" cellpadding="0" cellspacing="0" style="background-color: #ffffff" >
            <tr>
                <td style="width: 100px; text-align: left;">
                    <asp:Label ID="Label1" runat="server" Text="Filtros:"></asp:Label></td>
                <td style="width: 69px; text-align: left;">
                    <asp:Label ID="Label2" runat="server" Text="Nombre:"></asp:Label></td>
                <td style="width: 100px; text-align: left;">
                    <asp:Label ID="Label3" runat="server" Text="Tipo:"></asp:Label></td>
                <td style="width: 100px; text-align: left;">
                    <asp:Label ID="Label4" runat="server" Text="Estado:"></asp:Label></td>
                <td style="width: 74px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 24px;">
                </td>
                <td style="width: 69px; height: 24px;">
                    <asp:TextBox ID="txt_nombre" runat="server" Width="288px"></asp:TextBox></td>
                <td style="width: 100px; height: 24px;">
                    <asp:DropDownList ID="dr_tipo" runat="server">
                        <asp:ListItem Selected="True">---Seleccione Tipo---</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 100px; height: 24px;">
                    <asp:DropDownList ID="dr_estado" runat="server">
                        <asp:ListItem Value="---Seleccione Estado---" Selected="True">---Seleccione Estado---</asp:ListItem>
                        <asp:ListItem Value="-1">Activa</asp:ListItem>
                        <asp:ListItem Value="0">Inactiva</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 74px; height: 24px;">
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/cambiar.gif" /></td>
                <td style="width: 100px; height: 24px">
                    </td>
                <td style="width: 100px; height: 24px;">
                    </td>
            </tr>
            <tr>
                <td style="height: 24px" colspan="7">
                    <asp:Button ID="Button1" runat="server" Text="Buscar" /></td>
            </tr>
        </table>
        <asp:Label ID="lbl_count" runat="server" Text="Numero de Registros:"></asp:Label><asp:GridView ID="grd_rniones" runat="server" AutoGenerateColumns="False" SkinID="Grid_3" Width="100%" AllowPaging="True" AllowSorting="True" PageSize="20" DataKeyNames="Id" >
            <Columns>
                <asp:TemplateField HeaderText="Id">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>                                          
                   <asp:LinkButton ID="lnk_actividad1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("Id") %>'
                    CommandName="Select" Text='<%# Bind("Id") %>'></asp:LinkButton>                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nombre de Reunion">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25%" />
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="X-Small" ForeColor="#C04000"
                            Text='<%# Bind("Nombre") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="NombreDependencia" HeaderText="Dependencia">
                   <ItemStyle Width="20%" HorizontalAlign="Left"/>
                </asp:BoundField>                
                
                <asp:BoundField DataField="horainicio" HeaderText="Hora Inicio" DataFormatString="{0:hh:mm:ss tt}" HtmlEncode="False">
                   <ItemStyle Width="100px" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="horafin" HeaderText="Hora Fin" DataFormatString="{0:hh:mm:ss tt}" HtmlEncode="False">
                   <ItemStyle Width="100px" HorizontalAlign="Center"/>
                </asp:BoundField>
                <asp:BoundField HeaderText="Frecuencia" DataField="comentario">
                   <ItemStyle Width="20%" HorizontalAlign="Left"/>
                </asp:BoundField>
                
                <asp:BoundField DataField="nombrereunion" HeaderText="Tipo de Reunion">
                 <ItemStyle Width="10%" HorizontalAlign="Center"/>
                </asp:BoundField>
                
                <asp:TemplateField HeaderText="Estado">
                  <ItemStyle Width="5%" HorizontalAlign="Center"/>
                    <EditItemTemplate>
                      
                    </EditItemTemplate>
                    <ItemTemplate>
                      <asp:ImageButton ID="img_button" runat="server" CommandName="Delete" />                         
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="activa">
                    <ItemStyle Width="0%" HorizontalAlign="Left"/>
                </asp:BoundField>
            </Columns>
            <PagerSettings Position="TopAndBottom" />
        </asp:GridView>
        &nbsp;<br />
        &nbsp;
        <cc2:MsgBox ID="MsgBox1" runat="server" />
        <br />
        <asp:HiddenField ID="hfield" runat="server" />
        <br />
        <br />
        <uc1:logo ID="Logo1" runat="server" />
    </form>
</body>
</html>
