Imports System
Imports System.IO
Imports System.Globalization
Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Data
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data.OleDb


Partial Class _Default
    Inherits System.Web.UI.Page
    Private _StrConexion As String
    Private _strUser As String
    Private _BolPage As String
    Private _StrMensaje As String
    Private _VbError As Boolean

    Private conjDatos As DataSet

    Public Sub prcArmarCadena()
        _StrConexion = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & Server.MapPath("counter.mdb") & "';Jet OLEDB:Database Password="

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cnn As OleDbConnection = Nothing
        cnn = New OleDbConnection(_StrConexion)
        cnn.Close()

        Me.Label4.Text = ""
        Dim remoteUser As String = System.Web.HttpContext.Current.Request.ServerVariables("LOGON_USER")
        Dim reg_user As String = Replace(remoteUser, "ECOPETROL\", "")

        Me.lb_user.Text = reg_user

        Me.guardar_usuario(reg_user)

        Try
            Dim Data As New DataSet
            Data.ReadXml(Server.MapPath("contador.xml"))
            Dim cont As Integer = Data.Tables(0).Rows.Count - 1
            Me.Label1.Text = cont
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick

    End Sub

    Public Sub guardar_usuario(ByVal registro As String)
        Dim sql, strcon As String
        Dim dts As Integer

        Try
            strcon = "SELECT Count(*) FROM users WHERE usuarios='" & registro & "'"
            dts = Me.ConsultaDato(strcon)

            If dts = 0 Then
                sql = "Insert Into users values ('" & registro & "')"
                Me.GuardarBD(sql)
            End If
        Catch ex As Exception

        End Try



        Dim i As Integer

        Dim path As String = Server.MapPath("contador.xml")
        Me.prcCrearDataset()
        Me.trae_usuarios()
        Me.ds_evalua.Tables("dtt_s").Merge(Me.DatasetFill.Tables(0))
        Dim t_archive As DataRow = Me.ds_evalua.Tables(0).Rows(0)

        Using writer As XmlTextWriter = New XmlTextWriter(path, Encoding.UTF8)
            writer.Formatting = Formatting.Indented
            writer.WriteStartElement("usuarios")
            Try
                For i = 0 To Me.ds_evalua.Tables(0).Rows.Count - 1
                    Dim codigo As String = ds_evalua.Tables(0).Rows(i).Item(0)
                    writer.WriteStartElement("usuario")
                    writer.WriteElementString("nombre", codigo)
                    writer.WriteEndElement()
                Next
            Catch ex As Exception

            End Try
            writer.WriteEndElement()
            writer.Close()
        End Using
    End Sub

    Public Sub trae_usuarios()
        Dim sqlx As String
        sqlx = "SELECT * FROM users"
        conjDatos = Me.Consultar(sqlx)
    End Sub


    Public Sub prcCrearDataset()
        'TABLAS
        Dim ds As New DataSet
        Dim dtt_archivos As New DataTable '--0
        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_s")
    End Sub
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Public Function ConsultaDato(ByVal strcon As String, Optional ByVal Cadena As String = "") As String
        'Esta funci�n devuelve le resultado de una consulta que devuelve un solo campo 

        Dim valor As String
        Dim cnn As OleDbConnection = Nothing
        Dim cmd As OleDbCommand = Nothing

        If Cadena = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = Cadena
        End If

        cnn = New OleDbConnection(_StrConexion)
        cmd = New OleDbCommand(strcon, cnn)

        cmd.CommandType = CommandType.Text

        cnn.Open()
        valor = cmd.ExecuteScalar
        Return valor

        cnn.Close()


    End Function

    Public Sub GuardarBD(ByVal SQL As String, Optional ByVal Cadena As String = "")

        If Cadena = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = Cadena
        End If

        Dim Conect As New OleDb.OleDbConnection(_StrConexion)

        Dim comand As New OleDb.OleDbCommand(SQL, Conect)

        Try
            Conect.Open()
            comand.ExecuteNonQuery()
            Me._VbError = False ' se agrego esta mejora pues quedaba en la cache
            Me._StrMensaje = ""
        Catch ex As Exception
            Me._StrMensaje = ex.Message & " CONSULTA QUE GUARDA " & SQL
            Me._VbError = True
        Finally
            Conect.Close()
        End Try

    End Sub

    Public Function Consultar(ByVal strcon As String, Optional ByVal StrConex As String = "") As DataSet

        If StrConex = "" Then
            Me.prcArmarCadena() 'Este procedimiento Arma cadena de conexi�n 
        Else
            _StrConexion = StrConex
        End If

        Dim Conect As New OleDb.OleDbConnection(_StrConexion)
        Dim comand As New OleDb.OleDbCommand(strcon, Conect)

        Dim adap As New OleDb.OleDbDataAdapter(comand)
        Dim ds As New DataSet

        Try
            Conect.Open()
            adap.Fill(ds)
        Catch ex As Exception
            Me._StrMensaje = ex.Message
            Me._VbError = True
        Finally
            Conect.Close()
        End Try
        Return ds
    End Function

    Public Property DatasetFill() As DataSet
        Get
            Return conjDatos
        End Get
        Set(ByVal value As DataSet)
            conjDatos = value
        End Set
    End Property


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        '    Dim remoteUser As String = System.Web.HttpContext.Current.Request.ServerVariables("LOGON_USER")
        '    Dim reg_user As String = Replace(remoteUser, "ECOPETROL\", "")


        '    Dim sql As String

        '    sql = "delete from users where usuarios='" & registro & "'"
        '    Me.GuardarBD(sql)
    End Sub
End Class