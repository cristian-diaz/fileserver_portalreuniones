﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frm_calendar.aspx.vb" Inherits="frm_calendar" %>

<%@ Register Assembly="DataCalendar" Namespace="DataControls" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CALENDARIO DE EVENTOS</title>
</head>
<body>
    <form id="form1" runat="server">
   
        <br />
        <cc1:DataCalendar ID="cal1" runat="server" Height="352px" Width="416px" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" SelectionMode="None">
      <OtherMonthDayStyle ForeColor="#808080" />
        <DayStyle HorizontalAlign="Left" VerticalAlign="Top" Font-Size="8" Font-Name="Arial" />  
         <OtherMonthDayStyle BackColor="#BAD405" ForeColor="White" Font-Bold="True"/>                       
            <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
            <TodayDayStyle BackColor="#BAD405" ForeColor="Black" Font-Bold="True"/>
            <SelectorStyle BackColor="#CCCCCC" />
            <WeekendDayStyle BackColor="#669900" />
            <NextPrevStyle VerticalAlign="Bottom" />
            <DayHeaderStyle BackColor="#5f9b51" Font-Bold="True" ForeColor="White" Font-Size="7pt"  Font-Italic="true"/>
            <TitleStyle BackColor="activeborder" BorderColor="Black" Font-Bold="True" />            
              <ItemTemplate>
                    <br />                    
                          <font color="red">
                            <%# Container.DataItem("EventTitle") %>
                        </font>                                           
               </ItemTemplate>
        </cc1:DataCalendar>
        &nbsp;
       
    
 
    </form>
</body>
</html>
