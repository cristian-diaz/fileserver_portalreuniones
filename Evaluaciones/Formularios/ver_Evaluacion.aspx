<%@ Page Language="VB" MasterPageFile="~/Evaluaciones/Plantilla/P_Plantilla_Aux.master" AutoEventWireup="false" CodeFile="ver_Evaluacion.aspx.vb" Inherits="Evaluaciones_Formularios_ver_Evaluacion" title="Ver Evaluaciones" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CntCuerpo" Runat="Server">
    <div style="text-align: center">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG);
                    color: white; font-family: Verdana,Arial,Helvetica,sans-serif; height: 18px">
                    Consultar Resultados de Evaluaci�n</td>
            </tr>
        </table>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td style="width: 10%; height: 20px;">
                </td>
            <td align="left" style="width: 20%; height: 20px;">
               </td>
            <td align="left" style="width: 20%; height: 20px;">
            </td>
            <td align="left" style="width: 20%; height: 20px;">
            </td>
            <td align="left" style="width: 20%; height: 20px;">
            </td>
            <td style="width: 10%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
            </td>
            <td align="left" style="width: 20%; background-color: #d5ddbf;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Reunion:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" colspan="3">
                <asp:DropDownList ID="DropDownList1" runat="server" Width="100%">
                </asp:DropDownList></td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;"></td>
            <td align="left" style="width: 20%; height: 10px;">
                <asp:RegularExpressionValidator ID="revFecIni" runat="server" ControlToValidate="txtfecha"
                    Display="None" ErrorMessage="Digite una fecha de Inicio v�lida" ForeColor=""
                    ValidationExpression="^(0?[1-9]|1[0-9]|2|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(\d{2}|\d{4})$"
                    ValidationGroup="Reporte"></asp:RegularExpressionValidator></td>
            <td align="left" style="width: 20%; height: 10px;"><asp:CompareValidator ControlToCompare="txtfecha" ControlToValidate="TxFecha2" Display="None" ErrorMessage="La Fecha Final debe ser Posterior a la Fecha de Inicio" ForeColor="" ID="cvFecIni" Operator="GreaterThanEqual" runat="server" Type="Date" ValidationGroup="Reporte"></asp:CompareValidator></td>
            <td align="left" style="width: 20%; height: 10px;"><asp:RegularExpressionValidator ControlToValidate="TxFecha2" Display="None" ErrorMessage="Digite una fecha Final v�lida" ForeColor="" ID="revFecFin" runat="server" ValidationExpression="^(0?[1-9]|1[0-9]|2|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(\d{2}|\d{4})$" ValidationGroup="Reporte"></asp:RegularExpressionValidator></td>
            <td style="width: 10%; height: 10px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 25px;">
            </td>
            <td align="left" style="width: 20%; background-color: #d5ddbf; height: 25px;">
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Fecha Desde:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" style="width: 20%; height: 25px;">
                <asp:TextBox ID="txtfecha" runat="server" Width="81px"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="..." />
                </td>
            <td align="left" style="width: 20%; background-color: #d5ddbf; height: 25px;">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                    ForeColor="#004000" Text="Fecha Hasta:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" style="width: 20%; height: 25px;">
                <asp:TextBox ID="TxFecha2" runat="server" Width="81px"></asp:TextBox>
                <asp:Button ID="Button2" runat="server" Text="..." />
                </td>
            <td style="width: 10%; height: 25px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="center" colspan="2" style="height: 10px">
<cc1:calendarextender id="CalendarExtender1" runat="server" cssclass="MyCalendar"
                    format="dd/MM/yyyy" popupbuttonid="Button1" targetcontrolid="txtfecha">
                </cc1:calendarextender>
                <cc1:ValidatorCalloutExtender HighlightCssClass="validatorCalloutHighlight" ID="vceFecIni" runat="server" TargetControlID="revFecIni">
                </cc1:ValidatorCalloutExtender>
              
            </td>
            <td align="center" colspan="2" style="height: 10px">
  <cc1:calendarextender id="Calendarextender2" runat="server" cssclass="MyCalendar" format="dd/MM/yyyy" popupbuttonid="Button2"  targetcontrolid="TxFecha2">
                </cc1:CalendarExtender>
                
                <cc1:ValidatorCalloutExtender HighlightCssClass="validatorCalloutHighlight" ID="vceFecFin" runat="server" TargetControlID="revFecFin">
                </cc1:ValidatorCalloutExtender>
                <cc1:ValidatorCalloutExtender ID="VceRango" runat="server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="cvFecIni">
                </cc1:ValidatorCalloutExtender>
            </td>
            <td style="width: 10%; height: 10px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 24px;">
            </td>
            <td align="left" style="width: 20%; height: 24px;">
            </td>
            <td align="center" colspan="2" style="height: 24px">
                <asp:Button ID="BtnConsultar" runat="server" Text="Consultar" /></td>
            <td align="left" style="width: 20%; height: 24px;">
            </td>
            <td style="width: 10%; height: 24px;">
            </td>
        </tr>
        <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td style="width: 10%; height: 10px;">
            </td>
        </tr>
    </table>
    <asp:Panel ID="PnlReuniones" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
            <tr>
                <td style="height: 10px" colspan="6">
                    <asp:GridView ID="GridView3" runat="server" DataKeyNames="No_eval"
                    Width="100%" AutoGenerateColumns="False" AllowPaging="True" SkinID="GvwEvaluacion">
                        <EmptyDataTemplate>
                            <asp:Label ID="lblitem" runat="server" Text='<%# bind("item") %>'></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle BackColor="DarkOliveGreen" ForeColor="White" />
                        <AlternatingRowStyle BackColor="WhiteSmoke" />
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Select"
                                        ImageUrl="~/Evaluaciones/Img/Img_Select.png" Text="Select" ToolTip="Ver Evaluaci�n" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="No_Eval" HeaderText="N&#176;"  Visible="False" />
                            <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="Fecha" HtmlEncode="False" />
                            <asp:TemplateField HeaderText="Hora Inicio">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("hora_inici_real") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("hora_inici_real", "{0:hh:mm:ss tt}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Hora Final">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("hora_final_real", "{0:hh:mm:ss tt}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="nombreusuario" HeaderText="Usuario">
                                <ItemStyle Width="200px" HorizontalAlign="Left"/>                                
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="login" HeaderText="Registro" />
                            
                            <asp:BoundField DataField="porcentaje" HeaderText="Evaluacion" />
                            
                            <asp:BoundField DataField="Comentario" HeaderText="Comentario">
                              <ItemStyle Width="300px"/>
                           </asp:BoundField>
                           
                        </Columns>
                    </asp:GridView>
                <asp:Panel ID="PnlInfo" runat="server" Visible="False" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; position: static;
                        height: 100%">
                        <tr>
                            <td align="center" style="height: 71px" valign="middle">
                                <img alt="" src="../../img/Noreunion.gif" style="vertical-align: middle; text-align: center" width="65" />
                                
                                <asp:Label ID="Label9" runat="server" SkinID="TitTabla" Text="No se ha registrado Evaluaci�n para esta Reuni�n"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                    <asp:Label ID="LblNota" runat="server" SkinID="Nota" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 10%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td align="left" style="width: 20%; height: 10px">
                </td>
                <td style="width: 10%; height: 10px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PnlResultados" runat="server" Visible="False" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
            <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="4" style="width: 20%; background-color: #d5ddbf;">
                <asp:Label ID="LblResultados" runat="server" Font-Bold="True" Font-Italic="True"
                    Font-Size="Small" ForeColor="#004000" Text="Evaluaci�n" SkinID="TitTabla"></asp:Label>
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
                <td style="width: 10%; height: 10px">
                </td>
                <td align="center" colspan="4" style="width: 20%; height: 10px">
                </td>
                <td style="width: 10%; height: 10px">
                </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px; background-color: #d5ddbf;">
                                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Small"
                                    ForeColor="#004000" Text="Lugar de Reuni�n:" SkinID="TitTabla"></asp:Label></td>
            <td align="left" style="height: 10px;" colspan="3">
                                <asp:TextBox ID="txtlugar" runat="server" Width="98%" ReadOnly="True"></asp:TextBox></td>
            <td style="width: 10%; height: 10px;">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td style="width: 10%; height: 9px;">
            </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="4">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id_item"
                    Width="100%" SkinID="GvwEvaluacion">
                    <Columns>
                        <asp:BoundField DataField="orden" HeaderText="N&#176;" />
                        <asp:TemplateField HeaderText="No" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# bind("Id_item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item a Evaluar">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblitem" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ayuda" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblmensaje" runat="server" Text='<%# bind("comentario") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Peso">
                            <ItemTemplate>
                                <asp:Label ID="lblpeso" runat="server" Text='<%# bind("Peso") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Calificacion">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# bind("calificacion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total">
                            <ItemTemplate>
                                <asp:Label ID="lbltotal" runat="server" Font-Bold="True" Text='<%# bind("valor") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="lblitem" runat="server" Text='<%# bind("item") %>'></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle BackColor="DarkOliveGreen" ForeColor="White" />
                    <AlternatingRowStyle BackColor="WhiteSmoke" />
                </asp:GridView>
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td align="left" style="width: 20%; height: 10px;">
            </td>
            <td style="width: 10%; height: 10px;">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 20px;">
            </td>
                <td align="left" colspan="2" style="height: 20px; background-color: #d5ddbf">
                <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="#004000"
                    Text="Resultados Obtenidos" Width="151px" SkinID="TitTabla"></asp:Label></td>
                <td align="left" colspan="2" style="height: 20px; background-color: #d5ddbf">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#004000"
                    Text="Comentarios:" SkinID="TitTabla"></asp:Label></td>
            <td style="width: 10%; height: 20px;">
            </td>
            </tr>
            <tr>
                <td style="width: 10%; height: 10px">
                </td>
                <td align="left" colspan="2" style="height: 10px">
                </td>
                <td align="left" colspan="2" style="height: 10px">
                </td>
                <td style="width: 10%; height: 10px">
                </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td align="center" colspan="2" rowspan="3">
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" Width="260px" SkinID="GvwEvaluacion">
                    <Columns>
                        <asp:TemplateField HeaderText="Item">
                            <ItemStyle BackColor="White" />
                            <ItemTemplate>
                                <asp:Label ID="lblitemr" runat="server" Text='<%# bind("itemr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label ID="lblvalorr" runat="server" Text='<%# bind("valorr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Green" ForeColor="White" />
                </asp:GridView>
            </td>
            <td align="center" colspan="2" rowspan="3">
                <asp:TextBox ID="TextBox1" runat="server" Height="114px" MaxLength="255" Width="335px" ReadOnly="True" TextMode="MultiLine"></asp:TextBox></td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%">
            </td>
            <td style="width: 10%">
            </td>
            </tr>
            <tr>
            <td style="width: 10%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td align="left" style="width: 20%; height: 9px;">
            </td>
            <td style="width: 10%; height: 9px;">
            </td>
            </tr>
        </table>
    </asp:Panel>
        <cc2:msgbox id="MsgBox1" runat="server"></cc2:msgbox>
</asp:Content>

