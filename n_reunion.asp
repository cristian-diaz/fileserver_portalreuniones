<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>NoGray Calendar Component</title>

<script src="mootools.v1.11.js" type="text/javascript"></script>
<script src="nogray_date_calendar_vs1_min.js" type="text/javascript"></script>

<script language="javascript">
	window.addEvent("domready", function(){
		var today = new Date();
		
		var calender1 = new Calendar("calendar1", "cal1_toggler", {inputField:'date1'});
		
		var td_events = [];
		var d_16 = (today.getMonth()+1)+'-16-'+today.getFullYear();
		td_events[d_16] = {'click':function(td, date_str){alert(new Date().fromString(date_str));}};
		
		
		var dateon = [];
		var d_14 = (today.getMonth()+1)+'-14-'+today.getFullYear();
		dateon[d_14] = function(td, date_str){
			td.setStyle('background-color', '#aad7ea');
		};
		
		var calender2 = new Calendar("calendar2", "cal2_toggler", {inputField:{date:'date',
																	month:'month',
																	year:'year'},
																	inputType:'select',
																	idPrefix:'cal2',
																	numMonths:4,
																	tdEvents:td_events,
																	dateOnAvailable:dateon
																});
		
		var us_holidays = [{date:1,
					month:0},
					{date:"3rd Monday",
					month:0},
					{date:"3rd Monday",
					month:1},
					{date:"last Monday",
					month:4},
					{date:4,
					month:6},
					{date:"1st Monday",
					month:8},
					{date:"2nd Monday",
					month:9},
					{date:11,
					month:10},
					{date:"4th Thursday",
					month:10},
					{date:25,
					month:11}];
																		
		var calender3 = new Calendar("calendar3", "cal3_toggler", {inputField:'date3',
																	datesOff:us_holidays,
																	numMonths:3,
																	multiSelection:true,
																	maxSelection:5,
																	forceSelections:[
																			{date:'last Saturday'}
																		],
																	dateFormat:'D, d M Y ::  ',
																	idPrefix:'cal3'});
																	
		var calender4 = new Calendar("calendar4", null, {visible:true,
														allowSelection:false,
														datesOff:us_holidays,
														startDate:new Date(today.getFullYear(), 0, 1),
														endDate:new Date(today.getFullYear(), 11, 31),
														idPrefix:'cal4',
														inputType:'none',
														onSelect: function(){
															alert(this.options.selectedDate);
														}});
	});
</script>
<link href="nogray_calendar_vs1.css" rel="stylesheet" type="text/css" />
<style>
	* {font-family:Arial, Helvetica, sans-serif;
		font-size:9pt;}
		
	/* table list */
	.table_list {border-collapse:collapse;
		border:solid #cccccc 1px;
		width:100%;}
	
	.table_list td {padding:5px;
		border:solid #efefef 1px;}
	
	.table_list th {background:#75b2d1;
		padding:5px;
		color:#ffffff;}
	
	.table_list tr.odd {background:#e1eff5;}
	
	
	/* calendar styles */
	#calendar1 {border:solid #666666 1px;
		background:#ffffff;
		padding-bottom:5px;
		padding-top:5px;}
	
	#calendar1 {width:183px;}
	#calendar1 .ng-cal-header-table {width:173px;}
	
	
</style>
</head>
<body>


Simple Calendar with a text field input:<br />
<input type="text" name="date1" id="date1" /> 
<a href="#" id="cal1_toggler">Open Calendar</a>
</center>
<div id="calendar1"></div>



</body>
</html>