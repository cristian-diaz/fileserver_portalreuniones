﻿Imports System.Web
Imports System.ComponentModel
Imports System.Security.Principal
Imports System.Data
Imports System.Data.OleDb

Partial Class frm_sugerencias
    Inherits System.Web.UI.Page

    Dim evalu As New Consultas
    Dim usr As String = HttpContext.Current.User.Identity.Name

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            prcCrearDataset()
            prcCargarOrigen("edicionR")

            '  Me.Label1.Text = Replace(usr, "ECOPETROL\", "")

        End If




    End Sub
    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "edicionR"

                Try
                    Dim usuario As String = Replace(usr, "ECOPETROL\", "")
                    Me.ds_evalua.Tables(0).Clear()
                    Me.evalu.Usuario_correo(usuario)
                    Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))

                    Dim filaa As DataRow
                    filaa = Me.ds_evalua.Tables(0).Rows(0)
                    Me.Label1.Text = filaa("nombreusuario")


                Catch ex As Exception
                    Me.Label1.Text = "Visitante"

                End Try




        End Select


    End Sub

    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_users As New DataTable   '--0
        Dim dtt_reunion As New DataTable

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_users")



    End Sub

#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region




    Private Sub enviar_correo()
        Dim HTML As String
        Dim correo As New System.Net.Mail.MailMessage()
        correo.From = New System.Net.Mail.MailAddress("compromisos@ecopetrol.com.co")
        correo.To.Add("david.badillo@ecopetrol.com.co")
        correo.Subject = "CORREO PORTAL COMPROMISOS:  " & Me.c_observacion.Text



        correo.IsBodyHtml = False
        correo.Priority = System.Net.Mail.MailPriority.Normal


        HTML = "Correo Electronico:" & Me.c_mail.Text & Chr(13)
        HTML = HTML & "Comentarios:" & Me.c_comentario.Text & Chr(13)
        HTML = HTML & "Usuario:" & Me.Label1.Text

        correo.Body = HTML


        'Me.c_comentario.Text
        Dim smtp As New System.Net.Mail.SmtpClient
        smtp.Host = "10.1.141.213"


        Try
            smtp.Send(correo)
            Me.MsgBox1.ShowMessage("Mensaje enviado satisfactoriamente")
        Catch ex As Exception
            Me.MsgBox1.ShowMessage("Problemas en el Envio" & ex.Message)

        End Try




    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        enviar_correo()
    End Sub
End Class
