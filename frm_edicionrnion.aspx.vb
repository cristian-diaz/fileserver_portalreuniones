﻿Imports AjaxControlToolkit
Imports System
Imports System.Data
Imports System.Data.OleDb
Partial Class frm_edicionrnion
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Dim idrnion As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")

            Me.prcCargarOrigen("frecuencia")
            Me.PrcCargarDatos("frecuencia")

            Me.prcCargarOrigen("tp_reuniones")
            Me.PrcCargarDatos("tp_reuniones")

            Me.date2.Text = "01/01/2990"
            date1.Text = Format(Now(), "dd/MM/yyyy")



        End If
    End Sub



#Region "PROCEDIMIENTOS"
    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet

        Dim dtt_reunion As New DataTable   '--0
        Dim dtt_frecuencia As New DataTable '--1

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion")
        Me.ds_evalua.Tables.Add("dtt_frecuencia")
        Me.ds_evalua.Tables.Add("dtt_moderador")
        Me.ds_evalua.Tables.Add("dtt_tprnion")
        Me.ds_evalua.Tables.Add("dtt_areas")


    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        'idrnion = 88
        idrnion = Request.QueryString("idReunion")

        Select Case Objeto
            Case "reuniones"

                Me.ds_evalua.Tables("dtt_reunion").Clear()
                Me.evalu.R_edicion(idrnion)
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "frecuencia"
                Me.ds_evalua.Tables("dtt_frecuencia").Clear()
                Me.evalu.R_Frecuencia(idrnion)
                Me.ds_evalua.Tables("dtt_frecuencia").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "tp_reuniones"
                Me.evalu.Tp_Reunion()
                Me.ds_evalua.Tables("dtt_tprnion").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "areas"
                Me.ds_evalua.Tables("dtt_areas").Clear()
                Me.evalu.Trae_area(areas.SelectedItem.Text)
                Me.ds_evalua.Tables("dtt_areas").Merge(Me.evalu.DatasetFill.Tables(0))

        End Select
    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Dim reunion As DataRow = ds_evalua.Tables("dtt_reunion").Rows(0)

                Dim name As String = Convert.ToString(reunion("nombre"))
                Dim tp_rnion As String = Convert.ToString(reunion("NombreReunion"))
                Dim hinicio As String = Convert.ToString(reunion("horaInicio"))
                Dim hfin As String = Convert.ToString(reunion("horaFin"))
                Dim Hdura As String = Convert.ToString(reunion("duracion"))
                Dim fecInicio As String = Convert.ToString(reunion("fechaInicio"))
                Dim fecFin As String = Convert.ToString(reunion("fechaFinaliza"))
                Dim v_dependencia As String = Convert.ToString(reunion("NombreDependencia"))

                Me.txt_nombre.Text = name
                Me.txt_nm.Text = name
                Me.text_1.Text = tp_rnion
                Me.text_2.Text = Mid(hinicio, 11, 20)
                Me.text_3.Text = Mid(hfin, 11, 20)
                Me.text_4.Text = Hdura
                Me.text_5.Text = Mid(fecInicio, 1, 11)
                Me.text_6.Text = Mid(fecFin, 1, 11)
                Me.text_9.Text = v_dependencia

                'EN LA DE EDITAR
                'Me.tr.SelectedValue = tp_rnion

                'TRAE EL MODERADOR DE LA REUNION 
                Try
                    Me.ds_evalua.Tables("dtt_moderador").Clear()
                    Me.evalu.R_Moderador(idrnion)
                    Me.ds_evalua.Tables("dtt_moderador").Merge(Me.evalu.DatasetFill.Tables(0))
                    Me.GridView1.DataSource = Me.ds_evalua.Tables("dtt_moderador")
                    Me.GridView1.DataBind()
                Catch ex As Exception

                End Try

            Case "frecuencia"
                Try
                    Dim frecuencia As DataRow = ds_evalua.Tables("dtt_frecuencia").Rows(0)
                    Me.text_7.Text = frecuencia("comentario")
                Catch ex As Exception
                    Me.text_7.Text = "Sin Frecuencia Asignada"
                End Try

            Case "tp_reuniones"
                Me.tr.DataSource = Me.ds_evalua.Tables("dtt_tprnion").CreateDataReader
                Me.tr.DataTextField = "NombreReunion"
                Me.tr.DataValueField = "id"
                Me.tr.DataBind()


            Case "areas"
                Me.dr_areas.DataSource = Me.ds_evalua.Tables("dtt_areas").CreateDataReader
                Me.dr_areas.DataTextField = "NombreDependencia"
                Me.dr_areas.DataValueField = "id"
                Me.dr_areas.DataBind()

        End Select
    End Sub

#End Region


#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region


    Protected Sub areas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles areas.SelectedIndexChanged

        Me.prcCargarOrigen("areas")
        Me.PrcCargarDatos("areas")

    End Sub

    Protected Sub dr_inicio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim inicio As Date
        Dim fin As Date
        Dim horas_dif As Long
        Dim minutos_dif As Long
        Dim minutos_inicio As Long
        Dim minutos_fin As Long
        Dim minutos_total As Long

        inicio = Me.dr_inicio.Text
        fin = Me.dr_fin.Text

        'HORAS Y MINUTOS
        horas_dif = DateDiff(DateInterval.Hour, inicio, fin)
        minutos_dif = DateDiff(DateInterval.Minute, inicio, fin)

        'DIFERENCIA DE MINUTOS
        minutos_inicio = DatePart(DateInterval.Minute, inicio)
        minutos_fin = DatePart(DateInterval.Minute, fin)
        'SUMA TOTAL DE MINUTOS
        minutos_total = minutos_fin - minutos_inicio

        'SI LAS HORAS O LOS MINUTOS SON MENORES QUIERE DECIR QUE ESCOGIO EL FIN MENOR QUE EL INICIO
        If horas_dif >= 0 And minutos_dif >= 0 Then

            Me.txt_duracion.Text = horas_dif & " Horas" & " " & minutos_total & " Minutos"
        Else
            Me.txt_duracion.Text = ""
            Me.MsgBox1.ShowMessage("Hay diferencias En las Horas")
        End If
    End Sub

    Protected Sub dr_fin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim inicio As Date
        Dim fin As Date
        Dim horas_dif As Long
        Dim minutos_dif As Long
        Dim minutos_inicio As Long
        Dim minutos_fin As Long
        Dim minutos_total As Long

        inicio = Me.dr_inicio.Text
        fin = Me.dr_fin.Text

        'HORAS Y MINUTOS
        horas_dif = DateDiff(DateInterval.Hour, inicio, fin)
        minutos_dif = DateDiff(DateInterval.Minute, inicio, fin)

        'DIFERENCIA DE MINUTOS
        minutos_inicio = DatePart(DateInterval.Minute, inicio)
        minutos_fin = DatePart(DateInterval.Minute, fin)
        'SUMA TOTAL DE MINUTOS
        minutos_total = minutos_fin - minutos_inicio

        'SI LAS HORAS O LOS MINUTOS SON MENORES QUIERE DECIR QUE ESCOGIO EL FIN MENOR QUE EL INICIO
        If horas_dif >= 0 And minutos_dif >= 0 Then

            Me.txt_duracion.Text = horas_dif & " Horas" & " " & minutos_total & " Minutos"
        Else
            Me.txt_duracion.Text = ""
            Me.MsgBox1.ShowMessage("Hay diferencias En las Horas")
        End If
    End Sub


    Protected Sub rb_finaliza_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb_finaliza.SelectedIndexChanged
        Select Case rb_finaliza.SelectedValue
            Case "si"
                Me.date2.Enabled = True
                date2.Text = Format(Now(), "dd/MM/yyyy")
            Case "no"
                Me.date2.Text = "01/01/2990"
                Me.date2.Enabled = False
        End Select
    End Sub

    Protected Sub b_guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles b_guardar.Click

        Dim nombre As String = Me.txt_nombre.Text
        Dim fec_inicio As String = Me.date1.Text
        Dim fec_fin As String = Me.date2.Text
        Dim areas As String = Me.dr_areas.SelectedItem.Value
        Dim tipo_reunion As String = Me.tr.SelectedItem.Value
        Dim hora_inicio As String = Me.dr_inicio.SelectedValue
        Dim hora_fin As String = Me.dr_fin.SelectedValue
        Dim tiempo_duracion As String = Me.txt_duracion.Text
        Dim frc_reunion As String = Me.frecuencia.SelectedValue
        'Dim moderador As String = Me.dd_moderador.SelectedValue
        Dim var As String
        Dim Comentario As String
        Dim var_i As Integer

        If Me.valida.Checked = True Then
            var = 1
        Else
            var = 0
        End If


        ' REVISA SI EL NOMBRE YA EXISTE
        If Me.evalu.nombre_rnion(Me.txt_nombre.Text) = True Then
            'Me.txt_nombre.Focus()
            Me.txt_nombre.Text = ""
            Me.MsgBox1.ShowMessage("Este Nombre de Reunion Ya existe" & evalu.ERRORR)
        Else

            ' evalu.Source_NewReunion(nombre, fec_inicio, fec_fin, areas, tipo_reunion, hora_inicio, hora_fin, tiempo_duracion, frc_reunion, var)
            'evalu.Source_NewReunion(nombre, fec_inicio, fec_fin, areas, moderador, tipo_reunion, hora_inicio, hora_fin, tiempo_duracion, frc_reunion, var)
            If evalu.FlagError = True Then
                Me.MsgBox1.ShowMessage("ERROR" & evalu.ERRORR)
            Else
                Me.MsgBox1.ShowMessage("REUNION SE HA GUARDADO...................")
            End If


        End If

        'AQUI GUARDA LA FRECUENCIA
        var_i = Me.frecuencia.SelectedValue


        If Me.frecuencia.SelectedValue = "1" Then
            Comentario = "Esta Frecuencia es Programada Diaria Por Turnos"
            Me.evalu.Finder_Reunion(var_i, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

        End If

        If Me.frecuencia.SelectedValue = "2" Then
            Comentario = "Esta Frecuencia es Programada Diaria"
            Me.evalu.Finder_Reunion(var_i, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

        End If

        If Me.frecuencia.SelectedValue = "3" Then
            Dim d_sem2 As String = Me.txt_sem.Text
            Dim d_semanal As String = Me.opt_semanal.SelectedValue
            Comentario = "Repetir cada " & d_sem2 & " semanas el " & d_semanal
            Me.evalu.Finder_Reunion(var_i, Me.txt_nombre.Text, d_sem2, d_semanal, "", "", "", Comentario)

        End If

        If Me.frecuencia.SelectedValue = "4" Then

            If Me.opt_mensual.SelectedValue = "1" Then
                Dim v_men As String = Me.txt_ms1.Text
                Dim v_men1 As String = Me.txt_Mensual.Text
                Comentario = "El dia " & v_men & " de cada " & v_men1 & " meses"
                Me.evalu.Finder_Reunion(41, Me.txt_nombre.Text, v_men, v_men1, "", "", "", Comentario)
            End If

            If Me.opt_mensual.SelectedValue = "2" Then
                Dim v_men2 As String = Me.dr_ms1.SelectedValue
                Dim v_men3 As String = Me.dr_ms2.SelectedValue
                Dim v_men4 As String = Me.txt_ms3.Text
                Comentario = "El " & v_men2 & " " & v_men3 & " de cada " & v_men4 & " meses"
                Me.evalu.Finder_Reunion(42, Me.txt_nombre.Text, "", "", v_men2, v_men3, v_men4, Comentario)
            End If

        End If

        If Me.frecuencia.SelectedValue = "5" Then

            If Me.opt_an.SelectedValue = "1" Then
                Dim v_an1 As String = Me.txt_anual.Text
                Dim v_an2 As String = Me.dr_anual.SelectedValue
                Comentario = "Cada " & v_an1 & " de " & v_an2
                Me.evalu.Finder_Reunion(41, Me.txt_nombre.Text, v_an1, v_an2, "", "", "", Comentario)
            End If

            If Me.opt_an.SelectedValue = "2" Then
                Dim v_an3 As String = Me.dr_an1.SelectedValue
                Dim v_an4 As String = Me.dr_an2.SelectedValue
                Dim v_an5 As String = Me.dr_an3.SelectedValue
                Comentario = "El " & v_an3 & " " & v_an4 & " de " & v_an5
                Me.evalu.Finder_Reunion(42, Me.txt_nombre.Text, "", "", v_an3, v_an4, v_an5, Comentario)

            End If

        End If



    End Sub

    Protected Sub frecuencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case frecuencia.SelectedValue
            Case "1"
                Me.valida.Checked = False
                Me.MultiView1.ActiveViewIndex = 3
            Case "2"
                Me.valida.Checked = False
                Me.MultiView1.ActiveViewIndex = 4
            Case "3"
                Me.MultiView1.ActiveViewIndex = 0
                Me.valida.Checked = True
            Case "4"
                Me.MultiView1.ActiveViewIndex = 1
                Me.valida.Checked = True
            Case "5"
                Me.MultiView1.ActiveViewIndex = 2
                Me.valida.Checked = True

        End Select
    End Sub

    Protected Sub rnion_exis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rnion_exis.Click
        Me.prcCargarOrigen("grid")
        Me.PrcCargarDatos("grid")
    End Sub

    Protected Sub grid1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grid1.PageIndexChanging
        Me.grid1.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("grid")
    End Sub


    Protected Sub reunioneditar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reunioneditar.SelectedIndexChanged
        Select Case reunioneditar.SelectedValue
            Case "1"
                Me.MultiView2.ActiveViewIndex = 0
                Me.prcCargarOrigen("reuniones")
                Me.PrcCargarDatos("reuniones")

            Case "2"
                Me.MultiView2.ActiveViewIndex = 1



        End Select
    End Sub


    Protected Sub btn_actualiza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_actualiza.Click
        'idrnion = 88
        idrnion = Request.QueryString("idReunion")


        'VARIABLES OPCION 1
        Dim V_nombre As String = Me.txt_nm.Text

        'VARIABLES OPCION 2
        Dim V_tpRN As String = Me.tr.SelectedItem.Value

        'VARIABLES OPCION 3
        Dim v_inicio As String = Me.dr_inicio.SelectedValue
        Dim v_fin As String = Me.dr_fin.SelectedValue
        Dim v_duracion As String = Me.txt_duracion.Text

        'VARIABLES OPCION 4
        Dim v_fechaInicio As String = Me.date1.Text
        Dim v_fechafin As String = Me.date2.Text

        'VARIABLES DEPENDENCIA
        Dim v_area As String = Me.dr_areas.SelectedValue


        'If Me.evalu.nombre_rnion(Me.txt_nm.Text) = True Then
        '    Me.txt_nm.Text = ""
        '    Me.MsgBox1.ShowMessage("Este Nombre de Reunion Ya existe" & evalu.ERRORR)
        'Else
        Me.evalu.rnion_Update_gral(idrnion, V_nombre, V_tpRN, v_inicio, v_fin, v_duracion, v_fechaInicio, v_fechafin, v_area, Me.chN.Checked, Me.ch1.Checked, Me.ch2.Checked, Me.ch3.Checked, Me.ch5.Checked)
        Me.evalu.compromisos_update(Me.txt_nombre.Text, V_nombre)

        If evalu.FlagError = True Then
            Me.MsgBox1.ShowMessage("ERROR" & evalu.ERRORR)
        Else
            Me.MsgBox1.ShowMessage("REUNION SE HA GUARDADO...................")
        End If
        'End If

        If Me.ch4.Checked = True Then


            'ACTUALIZA FRECUENCIA EN LAS REUNIONES
            Dim var As String
            Dim Comentario As String
            Dim var_i As Integer

            If Me.valida.Checked = True Then
                var = 1
            Else
                var = 0
            End If

            var_i = Me.frecuencia.SelectedValue


            If Me.frecuencia.SelectedValue = "1" Then
                Comentario = "Esta Frecuencia es Programada Diaria Por Turnos"
                Me.evalu.Finder_Update(var_i, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

            End If

            If Me.frecuencia.SelectedValue = "2" Then
                Comentario = "Esta Frecuencia es Programada Diaria"
                Me.evalu.Finder_Update(var_i, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

            End If

            If Me.frecuencia.SelectedValue = "3" Then
                Dim d_sem2 As String = Me.txt_sem.Text
                Dim d_semanal As String = Me.opt_semanal.SelectedValue
                Comentario = "Repetir cada " & d_sem2 & " semanas el " & d_semanal
                Me.evalu.Finder_Update(var_i, Me.txt_nombre.Text, d_sem2, d_semanal, "", "", "", Comentario)

            End If

            If Me.frecuencia.SelectedValue = "4" Then

                If Me.opt_mensual.SelectedValue = "1" Then
                    Dim v_men As String = Me.txt_ms1.Text
                    Dim v_men1 As String = Me.txt_Mensual.Text
                    Comentario = "El dia " & v_men & " de cada " & v_men1 & " meses"
                    Me.evalu.Finder_Update(41, Me.txt_nombre.Text, v_men, v_men1, "", "", "", Comentario)
                End If

                If Me.opt_mensual.SelectedValue = "2" Then
                    Dim v_men2 As String = Me.dr_ms1.SelectedValue
                    Dim v_men3 As String = Me.dr_ms2.SelectedValue
                    Dim v_men4 As String = Me.txt_ms3.Text
                    Comentario = "El " & v_men2 & " " & v_men3 & " de cada " & v_men4 & " meses"
                    Me.evalu.Finder_Update(42, Me.txt_nombre.Text, "", "", v_men2, v_men3, v_men4, Comentario)
                End If

            End If

            If Me.frecuencia.SelectedValue = "5" Then

                If Me.opt_an.SelectedValue = "1" Then
                    Dim v_an1 As String = Me.txt_anual.Text
                    Dim v_an2 As String = Me.dr_anual.SelectedValue
                    Comentario = "Cada " & v_an1 & " de " & v_an2
                    Me.evalu.Finder_Update(41, Me.txt_nombre.Text, v_an1, v_an2, "", "", "", Comentario)
                End If

                If Me.opt_an.SelectedValue = "2" Then
                    Dim v_an3 As String = Me.dr_an1.SelectedValue
                    Dim v_an4 As String = Me.dr_an2.SelectedValue
                    Dim v_an5 As String = Me.dr_an3.SelectedValue
                    Comentario = "El " & v_an3 & " " & v_an4 & " de " & v_an5
                    Me.evalu.Finder_Update(42, Me.txt_nombre.Text, "", "", v_an3, v_an4, v_an5, Comentario)

                End If

            End If


        End If 'fin de checked frecuencia


    End Sub


End Class
