<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim ObjOptimizacion
Dim ErrorEnviar, CodigoError
Dim ObjL_RecordSet, ObjL2_RecordSet, ObjL3_RecordSet, ObjL4_RecordSet
Dim StrL_SQL, StrL_SQL2,  StrL_Mensaje, Usuarios, Responsables

Set ObjOptimizacion = AbrirConexionBD(cadenaConexionDBQ)

response.write ("Inicia el proceso de envio de correo con estadisticas de reuniones .....")

Sub EscribirEnArchivo(strError)
'Procedimiento que Escribe el Log de la Aplicación
Dim strCadena
	On Error Resume Next
	Const ForReading = 1, ForWriting = 2, ForAppending = 8	'Constantes de apertura de archivos
    Const F_FILELOG =  "C:\PortalReunion\LogCorreo\Compromisos3.Log"
	
	Dim fsodata, ffilelog
	
	Set fsodata = CreateObject("Scripting.FileSystemObject")
	If Not fsodata.FileExists(F_FILELOG) Then
		set ffilelog = fsodata.CreateTextFile(F_FILELOG, ForWriting)	
	End If
	
	Select Case strError
		Case "0" : str_Cadena = "Error al Enviar Mail"
		Case "1" : str_Cadena = "Correo Enviado"
		Case "2" : str_Cadena = "No se encontro usuario en el AD"		
		Case Else: str_Cadena = "Verificar si posee Cuenta de Correo"
	End Select
	
	set ffilelog = fsodata.Opentextfile(F_FILELOG,ForAppending, true)
	ffilelog.Writeline  Date() & " " & Time() & " " & str_Cadena & " Registro: " & ObjL_RecordSet("LOGIN") & ""
End Sub

Function EnviarMail(StrL_Asunto, StrL_Cuerpo, StrL_Destino)

'Función construida para Apiay debido a problemas usando el Componente de CDONTS
Dim Mail, StrL_Cadena, rsObjDirMail
Dim IntL_Posicion, IntL_Sitio, IntL_Longitud, StrL_DestinatarioActual, StrL_CadenaDef
Set Mail = CreateObject("CDO.Message")
	StrL_SQL = "SELECT email, grupo FROM t_logins WHERE Login='" & StrL_Destino & "'"
response.write(StrL_SQL)
response.end()
	set rsObjDirMail = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	If rsObjDirMail.EOF Then
		ErrorEnviar = "2"
		CodigoError= "No se encontro en la tabla 't_logins' "
		exit function 

	End If
	If CInt(rsObjDirMail("grupo"))=0 Then
		StrL_Destino = rsObjDirMail(0) & ""
	Else
		strSQL = "SELECT login_grupo_loginid from login_grupo WHERE " & _
			"login_grupo_grupoid ='" & StrL_Destino & "'"
		Set objRsTemp = AbrirRecordSet(ObjOptimizacion, strSQL)
		While Not objRsTemp.EOF
			SQL = "SELECT email FROM t_logins WHERE Login='" & objRsTemp(0) & "'"
			Set ObjRsGrupos = AbrirRecordSet(ObjOptimizacion, SQL)
			If Not IsNull(ObjRsGrupos(0)) Then
				StrL_Destino = ObjRsGrupos(0)
				'Mail.AddAddress ObjRsGrupos(0) 
			End If
			ObjRsGrupos.Close
			Set ObjRsGrupos = Nothing
			objRsTemp.MoveNext
		WEnd
		objRsTemp.Close
		Set objRsTemp = Nothing
	End If
	set rsObjDirMail = Nothing

	If StrL_Destino <> "" Then
		
'This section provides the configuration information for the remote SMTP server.
'Send the message using the network (SMTP over the network).
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="10.1.141.213"
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
'Use SSL for the connection (True or False)
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False 
Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
Mail.Configuration.Fields.Update
'End of remote SMTP server configuration section
		
		Mail.From = "PortalReuniones@ecopetrol.com.co"
		Mail.to = StrL_Destino
		StrL_Cadena = StrL_Destino
		Mail.Subject = StrL_Asunto
		Mail.HTMLBody = StrL_Cuerpo
		ErrorEnviar = ""
		On Error Resume Next 
		Mail.Send
	 End If
	 Set Mail = Nothing
	
		  If Err <> 0 Then 
			ErrorEnviar="0" 
			CodigoError = Err.Description
		  Else
			ErrorEnviar="1"
			CodigoError = "Mensaje Enviado"
		  End If
End Function

Sub Buscar_Moderadores()
'Realiza la consulta del Id de la Reunión
StrL_SQL = "SELECT TipoReunion.Id From TipoReunion " & _
    "WHERE (((TipoReunion.Nombre)='" & ObjL_RecordSet("TIPO") & "')) AND Activa = -1;"
Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL2_RecordSet.EOF Then
    'Realiza la consulta de los Moderadores de la Reunión
    StrL_SQL = "SELECT permisos.login +  ' - '  + t_logins.nombreusuario as Moderador " & _
        "FROM t_logins, permisos WHERE t_logins.Login = permisos.login " & _
        "AND (((permisos.id_reunion) = " & ObjL2_RecordSet("Id") & ") And ((permisos.permiso) = 'M')) " & _
        "ORDER BY t_logins.nombreusuario;"
	Set ObjL3_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
    'Construye la cadena que Contiene los moderadores de la reunión
    Responsables = ""
    Do Until ObjL3_RecordSet.EOF
        If Responsables = "" Then
                Responsables = ObjL3_RecordSet("Moderador")
            Else
                Responsables = Responsables & "; " & ObjL3_RecordSet("Moderador")
        End If
        ObjL3_RecordSet.MoveNext
    Loop
    ObjL3_RecordSet.Close
End If
ObjL2_RecordSet.Close
Set ObjL2_RecordSet = Nothing
Set ObjL3_RecordSet = Nothing
End Sub


'Procedimiento para el envío de mail con estadísticas a cada moderador
'de las reuniones. Estos contienen un hipervínculo que les permite
'ver el detalle de la información enviada. Este mensaje se envía cada
'quince días

	StrL_SQL = "SELECT permisos.login, permisos.permiso, t_logins.nombreusuario FROM permisos, t_logins " & _
		"WHERE permisos.login = t_logins.Login GROUP BY permisos.login, permisos.permiso, " & _
		"t_logins.nombreusuario HAVING (((permisos.permiso)='M')) ORDER BY permisos.login"

	Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	Do Until ObjL2_RecordSet.EOF '1
		StrL_Mensaje = ""
		StrL_Mensaje2 = "<html><body>" & _
			"<font face=Verdana color=#000000><small>Señor(a), " & UCase(ObjL2_RecordSet("nombreusuario")) & ", a la fecha de Hoy " & Date & _
			" este es el estadístico de la(s) siguiente(s) reunión(es) de la cual usted es moderador:</small></font>" 
		StrL_SQL = "SELECT permisos.id_reunion, TipoReunion.Nombre FROM permisos, TipoReunion " & _
			"WHERE (((permisos.login)='" & ObjL2_RecordSet("login") & "') AND ((permisos.permiso)='M') " & _
			"AND ((TipoReunion.Id)=Permisos.Id_Reunion)) AND TipoReunion.Activa = -1 " & _
			"ORDER BY permisos.id_reunion, permisos.login;"
	    Set ObjL3_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
		Do Until ObjL3_RecordSet.EOF '2
			StrL_SQL = "SELECT Compromisos.STATUS, Count(Compromisos.STATUS) AS Cantidad FROM Compromisos " & _
				"GROUP BY Compromisos.TIPO, Compromisos.STATUS, Compromisos.VERIFICADO " & _
				"HAVING (((Compromisos.TIPO)='" & ObjL3_RecordSet("Nombre") & "') AND " & _
				"((Compromisos.VERIFICADO)=0));"
			  Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
			  If Not ObjL_RecordSet.EOF Then
				  StrL_Mensaje = StrL_Mensaje & "<p><font face=Verdana color=#000000><small> Reunión:&nbsp;&nbsp;" & ObjL3_RecordSet("Nombre") & "</small></font><p>"
				  StrL_Mensaje = StrL_Mensaje & "<table width='25%'  border='1' cellspacing='0' cellpadding='0'>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td width='50%'><div align='center'><font face=Verdana color=#000080><small> Estado </small></font></div></td><td width='50%'><div align='center'><font face=Verdana color=#000000><small>Cantidad</small></font></div></td></tr>"
					  Do Until  ObjL_RecordSet.EOF '3
					   Select Case ObjL_RecordSet("STATUS")
						   Case "SIN COLOR"
								StrL_Mensaje = StrL_Mensaje & "<tr><td>"
						   Case "ROJO"
								StrL_Mensaje = StrL_Mensaje & "<tr><td bgcolor=Red>"
						   Case "VERDE"
								StrL_Mensaje = StrL_Mensaje & "<tr><td bgcolor=Green>"
					   End Select
					   StrL_Mensaje = StrL_Mensaje & "<font face=Verdana color=#000080><small><a href='http://bogevaprp/portaldereuniones/Presentar_Compromisos.asp?txtReunion=" & ObjL3_RecordSet("Nombre") & "&txtStatus=" & ObjL_RecordSet("STATUS") & "&Consultar=SI'>" & ObjL_RecordSet("STATUS") & "</small></font></td><td><div align='center'><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Cantidad") & "</small></font></div></td></tr>"
					   ObjL_RecordSet.MoveNext
					  Loop 'De la Reunion '3
					  StrL_Mensaje = StrL_Mensaje & "</table><p>"					  
			  End If
			  Set ObjL_RecordSet = Nothing
			  ObjL3_RecordSet.MoveNext
		Loop 'Del Usuario '2
		Set ObjL3_RecordSet = Nothing
		If StrL_Mensaje <> "" Then
			StrL_Mensaje = StrL_Mensaje2  & StrL_Mensaje 
			StrL_Mensaje = StrL_Mensaje & "<font face=Verdana color=#00000><small>Recuerde realizar las Acciones para que los Responsables, " 
			StrL_Mensaje = StrL_Mensaje & "cumplan con los compromisos asignados y Verificar los Cumplidos</small></font>" 
			StrL_Mensaje = StrL_Mensaje & "</html>" 
			EnviarMail "Estadístico de Compromisos para Moderadores", StrL_Mensaje, ObjL2_RecordSet("login")
			EscribirEnArchivo ErrorEnviar
		End If
		ObjL2_RecordSet.MoveNext
	Loop 'De la lista de Moderadores '1
	Set ObjL_RecordSet = Nothing

response.write("termina el proceso de envio de correo con estadisticas de reuniones ")
ObjOptimizacion.Close
%>
<script language="JavaScript" type="text/JavaScript">
<!--
window.close();
// -->
</script>