<%@ Language="VBScript" %>
<!--#include file="incluidos/ConexionBD.inc" -->
<!--#include file="Incluidos/libfunciones1.inc" -->
<!--#include file="Incluidos/libFuncValidacion.inc" -->

<%
if session("DerechoAInsertarReunion") <> "SI" then
    Response.redirect("Presentar_Compromisos.asp")
end if

 Dim ADO_Conexion,Str_SQL,rs
 Set ADO_Conexion = AbrirConexionBD(cadenaConexionDBQ)

remoto = request("REMOTE_USER")
if remoto <> "" and not IsNull(remoto) then
 remoto = Trim(remoto)
 posicion_div = Instr(1, remoto, "\", 1)
  if posicion_div <> "" and not IsNull(posicion_div) then 
    posicion_inicio = posicion_div + 1
    Login = mid(remoto, posicion_inicio)
  else
    Login = "visitante"
  end if
else
 Login= "visitante"
end if
   
Dim strIdUsuario, strNombreUsuario, strMail, blnAdministrador, blnActivo, blnNoexiste

strIdUsuario = request("strIdUsuario")
If request("bandera")="B" Then
	Str_SQL="SELECT * FROM t_logins WHERE login = '" & strIdUsuario & "'"
	Set ADO_Recordset=AbrirRecordset(ADO_Conexion,Str_SQL)
		If NOT ADO_RecordSet.EOF Then
			  strNombreUsuario = ADO_RecordSet("nombreusuario")
			  strMail = ADO_RecordSet("EMail")
			  blnActivo = ADO_RecordSet("Activo")
			  blnAdministrador = ADO_RecordSet("administrador")
		Else      
			  blnNoexiste = true
		End If
	ADO_RecordSet.Close 
	set ADO_RecordSet = Nothing
End If

If request("bandera")="G" Then
	strNombreUsuario = request("strNombreUsuario")
	strMail = request("strMail")
	If request("ChkAdministrador") = "A" Then
		blnAdministrador = "-1"
	Else
		blnAdministrador = "0"
	End If

	If request("ChkActivo") = "A" Then
		blnActivo = "-1"
	Else
		blnActivo = "0"
	End If

	Str_SQL="UPDATE t_logins Set nombreusuario = '" & strNombreUsuario & _
		"', EMail ='" & strMail & _
		"', Activo = " & blnActivo & _
		", administrador = " & blnAdministrador & _
		" WHERE login = '" & strIdUsuario & "'"
	ADO_Conexion.execute Str_SQL
	Set objRsTemp = Nothing
End If
%>
<script language="javascript">
function editar_Usuario()
{
	if (document.frmEdicion.strNombreUsuario.value == "")
	{
	alert("El nombre del usuario no puede ir en blanco, por favor escriba un nombre de usuario.");
	return false;
	}
document.frmEdicion.bandera.value = "G"
document.frmEdicion.submit();
return false;
}

</script>

<HTML>
	<HEAD>
		<TITLE>Nuevo Usuario</TITLE>
	    <link href="Estilos/Interno.css" rel="stylesheet" type="text/css">
	    <link href="Estilos/GCBstyle.css" rel="stylesheet" type="text/css">
	    <link href="Estilos/estprincipal.css" rel="stylesheet" type="text/css">
		     <link rel="stylesheet" type="text/css" href="codebase/dhtmlxtabbar.css">
	     <script  src="codebase/dhtmlxcommon.js"></script>
    	 <script  src="codebase/dhtmlxtabbar.js"></script>
		 <script  src="codebase/dhtmlxtabbar_start.js"></script>
	    <style type="text/css">
<!--
.Estilo1 {color: #336600}
-->
        </style>
</HEAD>
<BODY>
	<!--include file="Incluidos/menux.asp" -->
		<table width="98%" border="0" cellspacing="0" cellpadding="0" ID="Table1">
  <tr>
    <td background="img/Barra2.JPG"><div align="center" ><strong><font color="#FFFFFF"  face="Verdana">Modulo de Editar Usuario</font> </strong></div></td>
  </tr>
		</table>
		<center>
		<form Name="frmEdicion" method="POST">
			
    <table width="51%" border="1" cellspacing="0" cellpadding="3">
	<% if blnNoexiste then %>
      <tr> 
        <td colspan="2" align="center" class="TituloCelda"><font color="#FF0000">No 
          existen usuario con el registro consultado, intente nuevamente</font>&nbsp;</td>
      </tr>
	<% end if %>
      <tr> 
        <td width="34%" align="right" class="EncTabla2"> <input type="hidden" name="bandera" value="B">
          Registro del usuario: 
          <center>
        </center></td>
        <td class="DetTablaNE" width="66%"><input type="text" name="strIdUsuario" size="15" maxlength="12" value="<%=strIdUsuario%>"> 
        <input name="Submit" type="submit" class="Boton" value="     Buscar      "></td>
      </tr>
      <tr> 
        <td align="right" class="EncTabla2">Nombre del usuario: </td>
        <td class="DetTablaNE"><input type="text" name="strNombreUsuario" size="37" maxlength="50" value="<%=strNombreUsuario%>"></td>
      </tr>
      <tr> 
        <td align="right" class="EncTabla2">Correo Electr&oacute;nico: </td>
        <td class="DetTablaNE"><input name="strMail" type="text" id="strMail" value="<%=strMail%>" size="37" maxlength="50"></td>
      </tr>
      <tr>
        <td align="right" class="EncTabla2">Activo?:</td>
        <td class="DetTablaNE"><input name="ChkActivo" type="Checkbox" id="ChkActivo" value="A"<% if blnActivo then response.Write(" checked") %>></td>
      </tr>
      <tr>
        <td align="right" class="EncTabla2">Administrador</td>
        <td class="DetTablaNE"><input name="ChkAdministrador" type="Checkbox" value="A"<% if blnAdministrador then response.Write(" checked") %>></td>
      </tr>
      <tr> 
        <td colspan="2" align="right" class="TituloCelda"><div align="center" class="Estilo1">
            <input type="button" class="Boton" value="Guardar Modificaciones" name="btnGuardar" onClick="JavaScript:editar_Usuario()">          
            <input type="reset" class="Boton" value="         Restablecer         " name="B2" id="B2">
        </div></td>
      </tr>
    </table>
		</form>
		</center>
		  <!-- #include file="incluidos/CopyRight.asp" -->
</BODY>
</HTML>
