﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rpt_asis_reunion.aspx.vb" Inherits="reporte_rpt_asis_reunion" StylesheetTheme="SkinCompromisos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>
<%@ Register Src="../logo.ascx" TagName="logo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>REPORTE ASISTENCIA</title>
	
<style type="text/css">
  .hiddencol
  {
    display: none;
  }
</style>
	
</head>
<body style="text-align: center">
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
      
  
        <div style="width: 100%;   text-align: center">
            <asp:Label ID="Label5" runat="server" Text="Consulta Asistentes a Reuniones:"></asp:Label><br />
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 64px; text-align: left">
                        <asp:CheckBox ID="c1" runat="server" Text=" " /></td>
                    <td style="width: 100px; text-align: left">
                        <asp:Label ID="Label1" runat="server" Text="Reunion:"></asp:Label></td>
                    <td style="width: 74px">&nbsp;
                        </td>
                    <td style="width: 29px">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 64px; height: 19px">
                    </td>
                    <td colspan="5" style="height: 19px; text-align: left">
                        <asp:DropDownList ID="dr_nombre" runat="server" SkinID="dropListObjetivos" Width="496px">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 64px">
                    </td>
                    <td colspan="6" style="text-align: left">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 100px; height: 24px;">
                        <asp:Label ID="Label2" runat="server" Text="Fecha Inicio:"></asp:Label></td>
                                <td style="width: 100px; height: 24px;">
                        <asp:TextBox ID="fecha1" runat="server" Width="104px"></asp:TextBox></td>
                                <td style="width: 24px; height: 24px;">
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                                <td style="width: 100px; height: 24px;">
                        <asp:Label ID="Label3" runat="server" Text="Fecha Final:"></asp:Label></td>
                                <td style="width: 100px; height: 24px;">
                        <asp:TextBox ID="fecha2" runat="server" Width="104px"></asp:TextBox></td>
                                <td style="width: 25px; height: 24px;">
                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/view_calendar_timeline.png" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 64px; height: 21px">
                    </td>
                    <td style="width: 100px; height: 21px; text-align: left">
                    </td>
                    <td style="width: 74px; height: 21px">
                    </td>
                    <td style="width: 29px; height: 21px">
                    </td>
                    <td style="width: 100px; height: 21px; text-align: left">
                    </td>
                    <td style="width: 100px; height: 21px">
                    </td>
                    <td style="width: 28px; height: 21px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 64px; text-align: left">
                        <asp:CheckBox ID="c2" runat="server" Text=" " /></td>
                    <td style="width: 100px; text-align: left">
                        <asp:Label ID="Label4" runat="server" Text="Registro Usuario:"></asp:Label></td>
                    <td style="width: 74px; text-align: left">
                        <asp:TextBox ID="txt_registro" runat="server"></asp:TextBox></td>
                    <td style="width: 29px">
                    </td>
                    <td style="width: 100px; text-align: left">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 28px">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left">
                    </td>
                    <td style="width: 74px">
                    </td>
                    <td style="width: 29px">
                    </td>
                    <td style="width: 100px; text-align: left">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 28px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; text-align: center" colspan="7">
			
			  <asp:Button ID="Button1" runat="server" Text="Buscar" />
			
                        </td>
                </tr>
            </table>
            <cc2:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                Format="dd/MM/yyyy" PopupButtonID="ImageButton1" TargetControlID="fecha1">
            </cc2:CalendarExtender>
          
            <cc2:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                Format="dd/MM/yyyy" PopupButtonID="ImageButton2" TargetControlID="fecha2">
            </cc2:CalendarExtender>
            <br />
			
    </div>
	
	<div style="width: 100%;  text-align: center">
        <asp:MultiView ID="MultiView1" runat="server">
           <asp:View ID="View1" runat="server" >
               <br />
               <asp:Label ID="Label8" runat="server" Text="Asistencia Usuario:"></asp:Label>
               <asp:Label ID="Label7" runat="server" SkinID="lbl_encabezados" Text="Label"></asp:Label><br />
               <br />
               <asp:GridView ID="gr1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    PageSize="20" SkinID="Grid_3" Width="1040px">
                   <Columns>
                       <asp:BoundField DataField="Id" HeaderText="Reunion Id">
                           <ItemStyle HorizontalAlign="Left"  Width="5%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="nombre" HeaderText="Nombre">
                           <ItemStyle HorizontalAlign="Left" Width="30%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                           <ItemStyle HorizontalAlign="Left" Width="5%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="horainicio" HeaderText="Hora Inicio" DataFormatString="{0:hh:mm:ss tt}" HtmlEncode="False">
                           <ItemStyle HorizontalAlign="Left" Width="7%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="puntual" HeaderText="Estado">
                           <ItemStyle HorizontalAlign="Left" Width="7%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="suplente" HeaderText="Suplente" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                         <ItemStyle HorizontalAlign="Left" Width="5%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="excusa" HeaderText="Excusa" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                         <ItemStyle HorizontalAlign="Left" Width="5%"/>
                       </asp:BoundField>
                       <asp:BoundField DataField="comentario" HeaderText="Comentarios" >
                         <ItemStyle HorizontalAlign="Left" Width="30%"/>
                       </asp:BoundField>
                   </Columns>
               </asp:GridView>
           </asp:View>
            <asp:View ID="View2" runat="server"><asp:GridView ID="grd2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    PageSize="20" SkinID="Grid_3" Width="1040px">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Reunion Id">
                        <ItemStyle HorizontalAlign="Left"  Width="5%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="nombre" HeaderText="Nombre">
                        <ItemStyle HorizontalAlign="Left" Width="30%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="nombreusuario" HeaderText="Usuario">
                    <ItemStyle HorizontalAlign="Left" Width="20%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                        <ItemStyle HorizontalAlign="Left" Width="5%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="horainicio" HeaderText="Hora Inicio" DataFormatString="{0:hh:mm:ss tt}" HtmlEncode="False">
                        <ItemStyle HorizontalAlign="Left" Width="7%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="puntual" HeaderText="Estado">
                        <ItemStyle HorizontalAlign="Left" Width="7%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="suplente" HeaderText="Suplente" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                        <ItemStyle HorizontalAlign="Left" Width="5%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="excusa" HeaderText="Excusa" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                        <ItemStyle HorizontalAlign="Left" Width="5%"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="comentario" HeaderText="Comentarios" >
                        <ItemStyle HorizontalAlign="Left" Width="30%"/>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
                </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:Label ID="Label9" runat="server" Text="Asistencia Usuario:"></asp:Label>
                <asp:Label ID="Label6" runat="server" SkinID="lbl_encabezados"></asp:Label><br />
                <asp:GridView ID="GRD3" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    PageSize="20" SkinID="Grid_3" Width="1040px">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Reunion Id">
                            <ItemStyle HorizontalAlign="Left"  Width="5%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="nombre" HeaderText="Nombre">
                            <ItemStyle HorizontalAlign="Left" Width="30%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Left" Width="5%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="horainicio" HeaderText="Hora Inicio" DataFormatString="{0:hh:mm:ss tt}" HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Left" Width="7%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="puntual" HeaderText="Estado">
                            <ItemStyle HorizontalAlign="Left" Width="7%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="suplente" HeaderText="Suplente" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                            <ItemStyle HorizontalAlign="Left" Width="5%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="excusa" HeaderText="Excusa" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                            <ItemStyle HorizontalAlign="Left" Width="5%"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="comentario" HeaderText="Comentarios" >
                            <ItemStyle HorizontalAlign="Left" Width="30%"/>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </asp:View>
        </asp:MultiView>   
    </div>
   
        <cc1:MsgBox ID="MsgBox1" runat="server" />
        <br />
        <br />	
			
        <uc1:logo ID="Logo1" runat="server" />
	
		
    </form>
</body>
</html>
