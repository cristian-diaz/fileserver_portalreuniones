﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="adm_usr.aspx.vb" Inherits="adm_usr" StylesheetTheme="SkinCompromisos" %>

<%@ Register Src="logo.ascx" TagName="logo" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="ControlesWeb" Namespace="ControlesWeb" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ADMINISTRACION DE USUARIOS</title>    
    <link rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript">
function url(direccion) {
hidden = open(direccion ,'NewWindow','top=200,left=250,width=800, height=600,status=yes,resizable=no,scrollbars=no');
}
</SCRIPT>      
</head>
<body style="text-align: center">
    <form id="form1" runat="server">
        &nbsp;

        <asp:Button ID="Button1" runat="server" Text="Nuevo Usuario" />
        <asp:Button ID="Button2" runat="server" Text="Grupos" />&nbsp;<br />
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        &nbsp;
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 100px; height: 19px">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100px; height: 24px;">
                                            <asp:Label ID="Label5" runat="server" Text="Registro Usuario:" Width="120px"></asp:Label></td>
                                                <td style="width: 87px; height: 24px;">
                                            <asp:TextBox ID="txt_registro" runat="server"></asp:TextBox></td>
                                                <td style="width: 100px; height: 24px;">
                                                    <asp:Label ID="Label6" runat="server" Text="Nombre Usuario:" Width="192px"></asp:Label></td>
                                                <td style="width: 100px; height: 24px;">
                                                    <asp:TextBox ID="txt_nombre" runat="server" Width="376px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="height: 24px">
                                            <asp:Button ID="btn_search" runat="server" Text="Buscar" /><asp:Button ID="btn_todos"
                                                runat="server" Text="Ver Todos" /></td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table border="1" cellpadding="0" cellspacing="0" style="background-color: white">
            <tr>
                <td style="width: 100px">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 100px; text-align: left">
        <asp:GridView ID="grd2" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
            SkinID="GvwEvaluacion" PageSize="20">
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                       
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 40px">
                            <tr>
                                <td style="width: 100px; text-align: left">
                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/update.png" CommandName="Update"/>
                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/img/cancel.png" CommandName="Cancel"/></td>
                            </tr>
                        </table>
                    </EditItemTemplate>
                    <ItemTemplate>
                       <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/img/page_white_edit.png" CommandName="Edit"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/img/delete.png"  CommandName="Delete" OnClientClick="Javascript:return confirm ( 'Esta seguro que quiere eliminar el usuario ') " ToolTip="Eliminar Registro"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Login">
                    <EditItemTemplate>
                        <asp:Label ID="lbl_login" runat="server" BorderColor="White" ForeColor="#FF8000"
                            Text='<%# Bind("Login") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Login") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nombre Usuario">
                    <EditItemTemplate>
                        <asp:TextBox ID="txt_nombre" runat="server" Text='<%# Bind("nombreusuario") %>' Width="317px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("nombreusuario") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rol">
                    <EditItemTemplate>
                       <asp:DropDownList ID="dl1" runat="server">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("rol") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Correo Electronico">
                    <EditItemTemplate>
                        
                        <asp:TextBox ID="txt_mail" runat="server" Text='<%# Bind("EMail") %>' Width="367px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("EMail") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Activo">
                    <ItemTemplate>
                        <asp:CheckBox ID="act1" runat="server" Checked='<%# Bind("Activo") %>' Enabled="False" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="act2" runat="server" Checked='<%# Bind("Activo") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
                </td>
            </tr>
        </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        &nbsp;
        <br />
        <uc1:logo ID="Logo1" runat="server" />
    
      
        
        <cc1:MsgBox ID="MsgBox1" runat="server" />
        <br />
        &nbsp;&nbsp;<br />
        <div style="z-index: 101; left: 448px; width: 304px; position: absolute; top: 320px;
            height: 104px; font: caption;">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <table border="1" cellpadding="0" cellspacing="0" style="border-left-color: yellowgreen;
                        border-bottom-color: yellowgreen; width: 248px; border-top-style: solid; border-top-color: yellowgreen;
                        border-right-style: solid; border-left-style: solid; height: 56px; background-color: white;
                        border-right-color: yellowgreen; border-bottom-style: solid">
                        <tr>
                            <td style="width: 100px; text-align: center;">
                                .<asp:Image ID="Image1" runat="server" ImageUrl="~/img/ajax-loader.gif" /></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; text-align: center">
                                &nbsp;En proceso.....Espere</td>
                        </tr>
                    </table>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    

      
   
    </form>
</body>
</html>
