﻿Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.HtmlControls.HtmlContainerControl
Imports Microsoft.Reporting.WebForms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO

Partial Class Evaluaciones_Plantilla_Default

    Inherits System.Web.UI.Page
    Dim agendar As New Consultas
    Dim item As Integer = 0
    Dim dtcomentarios As DataTable

    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Me.prcCargarOrigen("treunion")
            Me.PrcCargarDatos("treunion")
            Me.Label2.Visible = True
            Me.prcCargarOrigen("horario")
            Me.PrcCargarDatos("horario")
            Me.MultiView1.ActiveViewIndex = 0
        End If
        BtnGeneraracta.Enabled = True
    End Sub

    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable   '--0

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_agendar")
        Me.ds_evalua.Tables.Add("dtt_horario")
        Me.ds_evalua.Tables.Add("dtt_buscar")
        Me.ds_evalua.Tables.Add("dtt_buscarcmt")
        Me.ds_evalua.Tables.Add("dtt_responsable")

    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String, Optional ByVal item As Integer = 0)
        Dim usr As String = HttpContext.Current.User.Identity.Name
        Dim reg_user As String = Replace(usr, "ECOPETROL\", "")

        Select Case Objeto
            Case "treunion"
                Me.ds_evalua.Tables("dtt_agendar").Clear()
                Me.agendar.reunion_moderador(reg_user)
                Me.ds_evalua.Tables("dtt_agendar").Merge(Me.agendar.DatasetFill.Tables(0))

            Case "horario"

                Me.ds_evalua.Tables("dtt_horario").Clear()
                Me.agendar.pro_buscar_horas(Me.drlist11.SelectedValue)
                Me.ds_evalua.Tables("dtt_horario").Merge(Me.agendar.DatasetFill.Tables(0))
                Dim cantidad As Integer = Me.ds_evalua.Tables("dtt_horario").Rows.Count
                If cantidad = 0 Then
                    Me.MsgBox1.ShowMessage("No Hay Reuniones Programadas")
                End If
                Me.Nom_PanelReunion.Text = "REUNION " & Me.drlist11.SelectedItem.ToString
                Nom_PanelReunion2.Text = "REUNION " & Me.drlist11.SelectedItem.ToString

            Case "buscar"
                Me.ds_evalua.Tables("dtt_buscar").Clear()
                Me.agendar.puntoexiste(Me.hd_reunion.Value, Me.hd_fecha.Value, Me.hd_hora.Value)
                Me.ds_evalua.Tables("dtt_buscar").Merge(Me.agendar.DatasetFill.Tables(0))

            Case "buscar_comentario"
                Me.ds_evalua.Tables("dtt_buscarcmt").Clear()
                Me.agendar.busqueda_comentario(Me.hd_punto.Value)

                If item = 0 Then
                    Me.ds_evalua.Tables("dtt_buscarcmt").Clear()
                    Me.ds_evalua.Tables("dtt_buscarcmt").Merge(Me.agendar.DatasetFill.Tables(0))
                Else
                    Dim newRow As DataRow = dtcomentarios.NewRow()
                    ' se envian los valores a las columnas:
                    newRow(0) = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
                    newRow(1) = agendar.DatasetFill.Tables(0).Rows(0).Item(1)
                    newRow(2) = agendar.DatasetFill.Tables(0).Rows(0).Item(2)
                    newRow(3) = agendar.DatasetFill.Tables(0).Rows(0).Item(3)
                    newRow(4) = agendar.DatasetFill.Tables(0).Rows(0).Item(4)

                    ' agrega la fila nueva a la tabla dtcomentarios.
                    dtcomentarios.Rows.Add(newRow)

                End If

            Case "Responsables"
                Me.ds_evalua.Tables("dtt_responsable").Clear()
                Me.agendar.busqueda_moderadoresporreunion(Me.hd_reunion.Value)
                Me.ds_evalua.Tables("dtt_responsable").Merge(Me.agendar.DatasetFill.Tables(0))

        End Select

    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "treunion"

                Me.drlist11.DataSource = Me.ds_evalua.Tables("dtt_agendar").CreateDataReader
                Me.drlist11.DataTextField = "Nombre"
                Me.drlist11.DataValueField = "id"
                Me.drlist11.DataBind()

            Case "horario"

                Me.grid1.DataSource = Me.ds_evalua.Tables("dtt_horario")
                Me.grid1.DataBind()

            Case "buscar_comentario"

            Case "Responsables"

            Case "UsuarioActual"

                agendar.admon_usuarios(2, Mid(User.Identity.Name, 11), 0)
                lblResponsable.Text = agendar.DatasetFill2.Tables(0).Rows(0).Item(1).ToString

        End Select

    End Sub

    Protected Sub grid1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grid1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "javascript:setMouseOverColor(this);")
            e.Row.Attributes.Add("onmouseout", "javascript:setMouseOutColor(this);")
        End If
    End Sub

    Protected Sub grid1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grid1.RowUpdating

        Try
            Dim i, j As Integer
            Dim Fila As GridViewRow = Nothing
            Dim index As Integer


            lblagendaAnterior.Text = ""
            lblDescipcionanterior.Text = ""

            'Carga el nombre del usuario actual 
            PrcCargarDatos("UsuarioActual")

            i = e.RowIndex + 1
            For j = i To Me.ds_evalua.Tables("dtt_horario").Rows.Count - 1
                Dim ae As Integer = Me.ds_evalua.Tables("dtt_horario").Rows(j).Item(0)
                Me.ds_evalua.Tables("dtt_horario").Rows(j).Item(0) = ae - 1
            Next

            index = i - 1
            Fila = Me.grid1.Rows(index)

            Me.hd_reunion.Value = CType(Fila.FindControl("hdd_idreunion"), HiddenField).Value
            Me.hd_fecha.Value = CType(Fila.FindControl("lbl_fecha"), Label).Text
            Me.hd_hora.Value = Format(CDate(ds_evalua.Tables("dtt_horario").Rows(index).Item(2)), "HH:mm:ss")

            'consulto el numero de evaluacion de la reunion seleccionada
            agendar.BucarEval_reunion(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 0)
            If agendar.DatasetFill.Tables(0).Rows.Count = 0 Then

                MsgBox1.ShowMessage("La reunion seleccionada no la han evaluado")

            Else
                Me.HfNoEval.Value = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
                agendar.id_acta = Me.HfNoEval.Value
                'consulto si el acta de la reunion seleccionada esta generada
                agendar.Acta("buscar", 1, , HfNoEval.Value)
                If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                    If agendar.DatasetFill.Tables(0).Rows(0).Item("estado") = 3 Or agendar.DatasetFill.Tables(0).Rows(0).Item("estado") = 2 Then
                        Me.MsgBox1.ShowMessage("El acta de esta reunion ya fue generada y/o en proceso de revision, solo podra ver su contenido")
                        'llenaFormato()
                        Response.Write("<script language='JavaScript'>window.open('http://bogevaprp/portaldereuniones/MostrarActa.aspx?Id_Acta=" + HfNoEval.Value + "')</script>") '
                    Else
                        Dim coment As Integer
                        cargaActa()
                        agendar.Desarrollo_acta(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 1)
                        If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                            For coment = 0 To agendar.DatasetFill.Tables(0).Rows.Count - 1
                                If Mid(User.Identity.Name, 11) = agendar.DatasetFill.Tables(0).Rows(coment).Item("Responsable") Then
                                    hflcomentario.Value = "Actualizar"
                                    txt_tema.Text = agendar.DatasetFill.Tables(0).Rows(coment).Item("punto")
                                    Exit For
                                Else
                                    hflcomentario.Value = "Insert2"
                                    txt_tema.Text = "Usted aun no ha agregado desarrollo de la reunion"
                                    Me.hd_punto_agendar.Value = agendar.DatasetFill.Tables(0).Rows(0).Item(3)
                                End If
                            Next
                            MultiView1.ActiveViewIndex = 1
                        Else
                            MultiView1.ActiveViewIndex = 1

                        End If
                    End If
                Else
                    Dim coment As Integer
                    cargaActa()
                    agendar.Desarrollo_acta(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 1)
                    If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                        For coment = 0 To agendar.DatasetFill.Tables(0).Rows.Count - 1
                            If Mid(User.Identity.Name, 11) = agendar.DatasetFill.Tables(0).Rows(coment).Item("Responsable") Then
                                hflcomentario.Value = "Actualizar"
                                txt_tema.Text = agendar.DatasetFill.Tables(0).Rows(coment).Item("punto")
                                Exit For
                            Else
                                hflcomentario.Value = "Insert2"
                                txt_tema.Text = "Usted aun no ha agregado desarrollo de la reunion"
                                Me.hd_punto_agendar.Value = agendar.DatasetFill.Tables(0).Rows(0).Item(3)
                            End If
                        Next
                        MultiView1.ActiveViewIndex = 1
                    Else
                        agendar.puntoexiste(hd_reunion.Value, hd_fecha.Value, hd_hora.Value)
                        If agendar.DatasetFill.Tables.Count > 0 And agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                            hflcomentario.Value = "Insert2" 'si existe la agenda para esa reunion seleccionada actualiza
                            txt_tema.Text = "Usted aun no ha agregado desarrollo de la reunion"
                            MultiView1.ActiveViewIndex = 1
                        Else
                            hflcomentario.Value = "Insert" 'si existe la agenda para esa reunion seleccionada actualiza
                            txt_tema.Text = "Usted aun no ha agregado desarrollo de la reunion"
                            MultiView1.ActiveViewIndex = 1

                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox1.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub cargaActa()
        Dim comentarios As Integer = 0

        'se consulta el porcentaje de la reunion seleccionada
        agendar.Resul_eval_porcentaje(HfNoEval.Value)
        Me.lblPorcentajeReunion.Text = agendar.DatasetFill.Tables(0).Rows(0).Item(3).ToString

        'busca los puntos tratados en la reunion seleccionada
        Me.prcCargarOrigen("buscar")
        Dim encontrado As Integer = Me.ds_evalua.Tables("dtt_buscar").Rows.Count
        pnlDescripcion.Visible = False
        Me.lblDescipcionanterior.Visible = False
        If encontrado > 0 Then
            'se carga la tabla dtcomentarios con los comentarios
            dtcomentarios = Me.ds_evalua.Tables("dtt_buscarcmt")

            'se recorre todos los comentarios de la reunion seleccionada
            For comentarios = 0 To Me.ds_evalua.Tables("dtt_buscar").Rows.Count - 1

                Dim row As DataRow = Me.ds_evalua.Tables("dtt_buscar").Rows(comentarios)
                Dim idptoreunion As String = row.Item(0)
                Dim horario As String = row.Item(3)

                Me.hd_punto.Value = idptoreunion
                Me.hd_punto_agendar.Value = idptoreunion

                Me.prcCargarOrigen("buscar_comentario", item)
                item += 1

            Next

            Dim numerocomentarios As Integer = Me.ds_evalua.Tables("dtt_buscarcmt").Rows.Count
            'si no existe comentarios de la reunion seleccionada manda un mensaje y/o luego carga el nombre del usuario actual como responsable del comentario nuevo
            If numerocomentarios = 0 Then
                Me.MsgBox1.ShowMessage("No existe desarrollo de la Reunion para el Horario " & Me.hd_hora.Value)
                Me.PrcCargarDatos("UsuarioActual")

            Else
                Me.PrcCargarDatos("UsuarioActual")
                'se consulta el desarrollo  de la reunion de los participantes de la reunion
                agendar.Desarrollo_acta(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 1)

                If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                    pnlDescripcion.Visible = True
                    Me.lblDescipcionanterior.Visible = True
                    hflid_puntos_reunion.Value = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
                    For indi As Integer = 0 To agendar.DatasetFill.Tables(0).Rows.Count - 1
                        'se trae el nombre con el numero del registro almacenado en los comentarios
                        agendar.admon_usuarios(2, agendar.DatasetFill.Tables(0).Rows(indi).Item("responsable"), 0)
                        'concatena todas los desarrollos redactados por de los participantes de la reunion
                        Me.lblDescipcionanterior.Text = lblDescipcionanterior.Text & "&nbsp;&nbsp;" & agendar.DatasetFill.Tables(0).Rows(indi).Item("punto") & " por :" & agendar.DatasetFill2.Tables(0).Rows(0).Item(1).ToString & ""
                        'separador de Desarrollo
                        If indi = agendar.DatasetFill.Tables(0).Rows.Count - 1 Then
                            Me.lblDescipcionanterior.Text = lblDescipcionanterior.Text & "."
                        Else
                            lblagendaAnterior.Text = lblagendaAnterior.Text & ";"
                        End If
                    Next

                End If
            End If

        Else

        End If

    End Sub

    Protected Sub drlist11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drlist11.SelectedIndexChanged
        Me.Label2.Visible = True

        Me.prcCargarOrigen("horario")
        Me.PrcCargarDatos("horario")

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        grabar_Punto()
        Me.MsgBox1.ShowMessage("Guardado")
        Me.ds_evalua.Tables("dtt_buscarcmt").Clear()
        item = 0
    End Sub

    Public Sub grabar_Punto()

        Select Case hflcomentario.Value
            Case "Insert"
                Me.hd_punto_agendar.Value = Me.agendar.guarda_punto(Me.hd_reunion.Value, Me.hd_fecha.Value, Me.hd_hora.Value, "", "Guardar")
                Me.agendar.Desarrollo(CType(Me.hd_punto_agendar.Value, Integer), Me.txt_tema.Text & Now, Mid(User.Identity.Name, 11).ToString, hflcomentario.Value)
            Case "Insert2"
                Me.agendar.Desarrollo(CType(Me.hd_punto_agendar.Value, Integer), Me.txt_tema.Text & Now, Mid(User.Identity.Name, 11).ToString, "Insert")

            Case "Actualizar"
                Me.agendar.Desarrollo(CType(Me.hd_punto_agendar.Value, Integer), Me.txt_tema.Text & Now, Mid(User.Identity.Name, 11).ToString, hflcomentario.Value, hflid_puntos_reunion.Value)
        End Select
    End Sub

    Public Sub grabar_agenda()

        Select Case hflcomentario.Value
            Case "Insert"
                Me.agendar.guarda_punto(Me.hd_reunion.Value, Me.hd_fecha.Value, Me.hd_hora.Value, txtAgenda.Text & "Autor: " & lblResponsable2.Text & " Fecha: " & Now, "Guardar")
            Case "Actualizar"
                Me.agendar.guarda_punto(Me.hd_reunion.Value, Me.hd_fecha.Value, Me.hd_hora.Value, txtAgenda.Text & "Autor: " & lblResponsable2.Text & " Fecha: " & Now, "Actualizar")
        End Select
    End Sub

    Public Sub InfoActa(ByVal orden As Integer)
        Try


            Select Case orden
                Case 1

                    'agendar.estado = 0
                    agendar.codigo_formato = "ECP-DTI-F-045"
                    agendar.version = "3"
                    'consulto por la informacion de la evaluacion de la reunion con el NoEval o Id_acta
                    agendar.Resultados_Eval(agendar.id_acta)
                    'asignamos el nombre del acta 
                    agendar.nombre_acta = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("nombre_acta")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("nombre_acta"))
                    'dependencia acta
                    agendar.Departamento_acta = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("Departamento_acta")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("Departamento_acta"))
                    'tema de la reunion
                    agendar.tema_acta = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("tema_acta")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("tema_acta"))
                    agendar.ubicacion = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("ubicacion")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("ubicacion"))
                    agendar.Fecha_eval = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("Fecha_eval")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("Fecha_eval"))
                    agendar.hora_inicio = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("hora_inicio")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("hora_inicio"))
                    agendar.hora_fin = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("hora_fin")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("hora_fin"))
                    agendar.Eval_reunion = IIf(IsDBNull(agendar.DatasetFill2.Tables(0).Rows(0).Item("Eval_Reunion")), DBNull.Value, agendar.DatasetFill2.Tables(0).Rows(0).Item("Eval_Reunion"))
                    agendar.Objetivo = "N/A"
                    agendar.asistentes = "N/A"
                    agendar.Aprobo = DdlParticipantes.SelectedItem.Text
                    agendar.correo_aprobo = DdlParticipantes.SelectedValue
                    'asigno el usuario actual
                    agendar.admon_usuarios(2, Mid(User.Identity.Name, 11), 0)
                    agendar.reviso = agendar.DatasetFill2.Tables(0).Rows(0).Item(1).ToString()
                    'activa el boton de generar acta solo si es moderador el usuario en sesion

                Case 2

                    enviar_correo(agendar.DatasetFill.Tables(0).Rows(0).Item("correo_aprobo"), "Se requiere su aprobación de acta ", agendar.DatasetFill.Tables(0).Rows(0).Item("correo_aprobo"), "Se requiere su aprobación para el acta de la reunión " & agendar.DatasetFill.Tables(0).Rows(0).Item("nombre_acta") & " de la fecha " & agendar.DatasetFill.Tables(0).Rows(0).Item("Fecha_eval") & ", consulte el acta de la reunion ingresando a: http://bogevaprp/portaldereuniones/frm_Visto_buenoActa.aspx?Id_Acta=" & agendar.id_acta & "", agendar.DatasetFill.Tables(0).Rows(0).Item("aprobo"))
                    ' enviar_correo("victor.fuentes@ecopetrol.com.co", "Se requiere su aprobación de acta ", "victor.fuentes@ecopetrol.com.co", "Se requiere su aprobación para el acta de la reunión " & agendar.DatasetFill.Tables(0).Rows(0).Item("nombre_acta") & " de la fecha " & agendar.DatasetFill.Tables(0).Rows(0).Item("Fecha_eval") & ", consulte el acta de la reunion ingresando a:", agendar.DatasetFill.Tables(0).Rows(0).Item("aprobo"))

                    'se consulta la agenda

                    agendar.puntoexiste(hd_reunion.Value, hd_fecha.Value, hd_hora.Value)
                    If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                        agendar.id_punto = agendar.DatasetFill.Tables(0).Rows(0).Item("id_puntos")
                    End If

                    'se consulta el desarrollo de los participantes de la reunion
                    agendar.Desarrollo_acta(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 1)

                    If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then

                        For indi As Integer = 0 To agendar.DatasetFill.Tables(0).Rows.Count - 1
                            'se trae el nombre con el numero del registro almacenado en los comentarios
                            agendar.admon_usuarios(2, Mid(agendar.DatasetFill.Tables(0).Rows(indi).Item("responsable"), 1))
                            'concatena todas los desarrollos redactados por los participantes de la reunion
                            agendar.id_puntos_reunion = agendar.DatasetFill.Tables(0).Rows(indi).Item("id_puntos_reunion")
                            agendar.Acta_Punto("crear")
                        Next
                    End If

                    'busca los participantes de la reunion seleccionada
                    agendar.BucarEval_reunion(, , , 1)
                    If agendar.DatasetFill2.Tables(0).Rows.Count > 0 Then
                        Dim indi As Integer = 0
                        For indi = 0 To agendar.DatasetFill2.Tables(0).Rows.Count - 1
                            agendar.login = agendar.DatasetFill2.Tables(0).Rows(indi).Item("login").ToString
                            agendar.Acta_Participante("crear")
                        Next
                    End If

                    'se cargan los compromisos generados para la reunion seleccionada
                    agendar.Compromisos(2)
                    If agendar.DatasetFill2.Tables(0).Rows.Count > 0 Then
                        Dim indi As Integer = 0

                        For indi = 0 To agendar.DatasetFill2.Tables(0).Rows.Count - 1
                            agendar.id_compromisos = agendar.DatasetFill2.Tables(0).Rows(indi).Item("NoID")
                            agendar.Acta_Compromiso("crear")
                        Next
                    End If

                    Response.Write("<script language='JavaScript'>window.open('http://bogevaprp/portaldereuniones/MostrarActa.aspx?Id_Acta=" + agendar.id_acta.ToString + "')</script>") '

            End Select



        Catch ex As Exception
            MsgBox1.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub enviar_correo(ByVal para As String, ByVal asunto As String, ByVal correodestino As String, ByVal contenido As String, ByVal usuario As String)

        Dim HTML As String
        Dim correo As New System.Net.Mail.MailMessage()
        correo.From = New System.Net.Mail.MailAddress("compromisos@ecopetrol.com.co")
        correo.To.Add(para)
        correo.Subject = "CORREO PORTAL REUNIONES:  " & asunto

        correo.IsBodyHtml = True
        correo.Priority = System.Net.Mail.MailPriority.Normal


        HTML = "Correo Electronico:" & correodestino & Chr(13)
        HTML = HTML & "Comentarios:" & contenido & Chr(13)
        HTML = HTML & "Usuario:" & usuario

        correo.Body = HTML


        Dim smtp As New System.Net.Mail.SmtpClient
        'smtp.Host = "bjaesicms1p.red.ecopetrol.com.co"
		'smtp.Host = "10.6.125.56"
		 smtp.Host = "10.1.141.213"


        Try
            smtp.Send(correo)
            Me.MsgBox1.ShowMessage("Mensaje enviado satisfactoriamente")
        Catch ex As Exception
            Me.MsgBox1.ShowMessage("Problemas en el Envio" & ex.Message)

        End Try
    End Sub

    'Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
    '    'MultiView1.ActiveViewIndex = 0
    '    Me.crsBase.ReportDocument.Load(Server.MapPath("~/Reportes/ActaAuto.rpt"), CrystalDecisions.Shared.OpenReportMethod.OpenReportByDefault)


    'agendar.num_acta = lblActaNo.Text
    'agendar.Acta("buscar", 0)

    '    'datos de la consulta
    '    Dim ds As New DsActa
    '    Dim dt As DataTable


    '    dt = agendar.DatasetFill.Tables("Acta")
    '    ds.Merge(dt)

    '    If Not ds Is Nothing Then
    '        dt.Clear()
    '        agendar.id_acta = ds.Tables("Acta").Rows(0).Item("id_acta")
    '        agendar.Acta("Acta_Compromisos", , , ds.Tables("Acta").Rows(0).Item("id_acta"))
    '        dt = agendar.DatasetFill.Tables("Compromiso")
    '        ds.Merge(dt)
    '        dt.Clear()
    '        agendar.Acta_Participante("buscar")
    '        dt = agendar.DatasetFill.Tables("Participante")
    '        ds.Merge(dt)
    '        dt.Clear()
    '        agendar.buscaagenda()
    '        dt = agendar.DatasetFill.Tables("agenda")
    '        ds.Merge(dt)
    '        If Not ds Is Nothing Then
    '            'Me.crsBase.ReportDocument.Subreports.Item(0).SetDataSource(ds.Tables("Participante"))
    '            Me.crsBase.ReportDocument.SetDataSource(ds)

    '        Else
    '            'Me.MultiView1.ActiveViewIndex = 1
    '            Exit Sub
    '        End If
    '    End If

    '    'Me.crsBase.ReportDocument.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLetter
    '    'Me.crsBase.ReportDocument.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait

    '    'Dim Reporte As ReportDocument = New ReportDocument
    '    'Reporte = Me.crsBase.ReportDocument

    '    'Dim oStream As New MemoryStream
    '    'oStream = Reporte.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)

    '    'Response.Clear()
    '    'Response.Buffer = True
    '    'Response.ContentType = "application/pdf"
    '    'Response.BinaryWrite(oStream.ToArray())
    '    'Response.End()
    '    Me.MultiView1.ActiveViewIndex = 4
    'End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub grid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grid1.SelectedIndexChanged
        Guardar_Acta()
    End Sub

    Public Sub Guardar_Acta()
        Try
            hd_reunion.Value = CType(Me.grid1.SelectedRow.FindControl("hdd_idreunion"), HiddenField).Value
            hd_fecha.Value = CType(Me.grid1.SelectedRow.FindControl("lbl_fecha"), Label).Text
            hd_hora.Value = CType(Me.grid1.SelectedRow.FindControl("lbl_hora"), Label).Text

            Select Case HflAccion.Value

                Case "Acta"
                    agendar.Return_NoEval(hd_reunion.Value, Me.hd_fecha.Value, Me.hd_hora.Value)
                    If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                        HfNoEval.Value = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
                        agendar.id_acta = HfNoEval.Value
                        agendar.Acta("buscar", 1, , HfNoEval.Value)
                        Dim indi As Integer = 0
                        'If agendar.DatasetFill.Tables.Count > 0 Then
                        If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                            Response.Write("<script language='JavaScript'>window.open('http://bogevaprp/portaldereuniones/MostrarActa.aspx?Id_Acta=" + HfNoEval.Value + "')</script>") '
                        Else
                            'falta preguntar que si an evaluado la reunion.
                            agendar.BucarEval_reunion(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 2)

                            If agendar.DatasetFill2.Tables(0).Rows.Count > 0 Then
                                DdlParticipantes.DataSource = agendar.DatasetFill2.Tables(0)
                                DdlParticipantes.DataTextField = "Nombre"
                                DdlParticipantes.DataValueField = "EMail"
                                DdlParticipantes.DataBind()
                            End If


                            Me.Lblgeneraracta.Text = " El acta no existe desea generar el acta?"
                            BtnGeneraracta.Enabled = True

                            MppeGeneraracta.Show()

                        End If

                    Else
                        Me.Lblgeneraracta.Text = " No se a generado la evaluacion"

                        BtnGeneraracta.Enabled = False
                        MppeGeneraracta.Show()
                    End If

                Case "Agenda"
                    'consulto el numero de evaluacion de la reunion seleccionada
                    agendar.BucarEval_reunion(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 0)
                    If agendar.DatasetFill.Tables(0).Rows.Count = 0 Then

                        MsgBox1.ShowMessage("La reunion seleccionada no la han evaluado")

                    Else
                        Me.HfNoEval.Value = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
                        agendar.id_acta = Me.HfNoEval.Value

                        'se consulta el porcentaje de la reunion seleccionada
                        agendar.Resul_eval_porcentaje(HfNoEval.Value)
                        Me.lblPorcentajeReunion2.Text = agendar.DatasetFill.Tables(0).Rows(0).Item(3).ToString
                        'asigno el usuario actual
                        agendar.admon_usuarios(2, Mid(User.Identity.Name, 11), 0)
                        lblResponsable2.Text = agendar.DatasetFill2.Tables(0).Rows(0).Item(1).ToString

                        'consulto si el acta de la reunion seleccionada esta generada y es oficial
                        agendar.Acta("buscar", 1, , Me.HfNoEval.Value)
                        If agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                            If agendar.DatasetFill.Tables(0).Rows(0).Item("estado") = 3 Or agendar.DatasetFill.Tables(0).Rows(0).Item("estado") = 2 Then
                                'se visualiza el acta con todo su contenido
                                MsgBox1.ShowMessage(" El acta de esta reunion ya fue generada y/o en proceso de revision, solo podra ver el contenido de la agenda en el acta generada")
                                ' llenaFormato()
                                Response.Write("<script language='JavaScript'>window.open('http://bogevaprp/portaldereuniones/MostrarActa.aspx?Id_Acta=" + HfNoEval.Value + "')</script>") '

                            Else
                                agendar.puntoexiste(hd_reunion.Value, hd_fecha.Value, hd_hora.Value)
                                If agendar.DatasetFill.Tables.Count > 0 And agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                                    lblagendaAnterior.Text = ""
                                    txtAgenda.Text = ""
                                    'se visualiza el contenido de la agenda y se prepara para ser actualizada
                                    hflcomentario.Value = "Actualizar"
                                    lblagendaAnterior.Text = agendar.DatasetFill.Tables(0).Rows(0).Item("Agenda").ToString
                                    txtAgenda.Text = agendar.DatasetFill.Tables(0).Rows(0).Item("Agenda").ToString
                                    BtnGenerarAgenda.Enabled = True

                                    'MppeGenerarAgenda.Show()
                                    MultiView1.ActiveViewIndex = 3
                                Else
                                    lblagendaAnterior.Text = ""
                                    txtAgenda.Text = ""
                                    'se crea un nueva agenda nueva eso implica que el punto tambien
                                    hflcomentario.Value = "Insert"
                                    'Me.lblgeneraragenda.Text = "No existe agenda en la reunion seleccionada desea Generarla?"
                                    BtnGenerarAgenda.Enabled = True
                                    ' MppeGenerarAgenda.Show()
                                    MultiView1.ActiveViewIndex = 3
                                End If
                            End If
                        Else
                            agendar.puntoexiste(hd_reunion.Value, hd_fecha.Value, hd_hora.Value)
                            If agendar.DatasetFill.Tables.Count > 0 And agendar.DatasetFill.Tables(0).Rows.Count > 0 Then
                                lblagendaAnterior.Text = ""
                                txtAgenda.Text = ""
                                'se visualiza el contenido de la agenda y se prepara para ser actualizada
                                hflcomentario.Value = "Actualizar"
                                lblagendaAnterior.Text = agendar.DatasetFill.Tables(0).Rows(0).Item("Agenda").ToString
                                txtAgenda.Text = agendar.DatasetFill.Tables(0).Rows(0).Item("Agenda").ToString
                                BtnGenerarAgenda.Enabled = True
                                'Me.lblgeneraragenda.Text = "No existe agenda en la reunion seleccionada desea Generarla?"
                                'MppeGenerarAgenda.Show()
                                MultiView1.ActiveViewIndex = 3
                            Else
                                lblagendaAnterior.Text = ""
                                txtAgenda.Text = ""
                                'se crea un nueva agenda nueva eso implica que el punto tambien
                                hflcomentario.Value = "Insert"
                                'Me.lblgeneraragenda.Text = "No existe agenda en la reunion seleccionada desea Generarla?"
                                BtnGenerarAgenda.Enabled = True
                                'MppeGenerarAgenda.Show()
                                MultiView1.ActiveViewIndex = 3

                            End If
                        End If
                        ' MultiView1.ActiveViewIndex = 3
                    End If
            End Select

        Catch ex As Exception
            MsgBox1.ShowMessage("Error: " & ex.Message.ToString)
        End Try
    End Sub

    Public Sub ejecutar_acta()
        Try
            'consulta el consecutivo del acta segun el tipo de reunion y actualiza el consecutivo
            agendar.Acta("Sig_Num_acta", , hd_reunion.Value)
            'asigno el valor del numero de acta 
            agendar.num_acta = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
            'consulto el numero de evaluacion de la reunion seleccionada
            agendar.BucarEval_reunion(hd_reunion.Value, hd_fecha.Value, hd_hora.Value, 0)
            'creo el acta
            agendar.Acta("CrearActaini", , , agendar.DatasetFill.Tables(0).Rows(0).Item(0))
            'asigno el id_acta
            agendar.id_acta = agendar.DatasetFill.Tables(0).Rows(0).Item(0)
            HflIdActa.Value = agendar.id_acta
            'fecha de la creacion del acta
            agendar.fecha_acta = Now
            'busca la informacion del acta que se creo
            'cargaActa()
            InfoActa(1)
            'atualiza el acta con los parametros asignados por medio de la clase
            agendar.Acta("Actualizar", 1, , HflIdActa.Value)
            agendar.Acta("buscar", 1, , HflIdActa.Value)
            InfoActa(2)
        Catch ex As Exception
            MsgBox1.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub BtnGeneraracta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGeneraracta.Click
        ejecutar_acta()
    End Sub

    Protected Sub ImgbtnAgenda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        HflAccion.Value = "Agenda"
    End Sub

    Protected Sub BtnActas_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        HflAccion.Value = "Acta"
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        'agendar.guarda_punto(Me.hd_reunion.Value, Me.hd_fecha.Value, Me.hd_hora.Value, txtAgenda.Text, "Actualizar")
        grabar_agenda()
        Me.MsgBox1.ShowMessage("Guardado agenda o actualiza")
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        MultiView1.ActiveViewIndex = 0
    End Sub



    Protected Sub BtnGenerarAgenda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGenerarAgenda.Click
        grabar_agenda()
    End Sub


End Class
