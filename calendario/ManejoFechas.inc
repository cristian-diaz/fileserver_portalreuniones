<%
 Function FormatearFechaBD(strlFecha,strlFormato)
  Dim strlTemporal
  Select Case LCase(strlFormato)
   Case "dd/mmm/yyyy","dd/mmm/yyyy hh:mm"
    Select Case LCase(mid(strlFecha,4,3))
     Case "ene","jan"
      strlTemporal="01"
     Case "feb"
      strlTemporal="02"
     Case "mar"
      strlTemporal="03"
     Case "abr","apr"
      strlTemporal="04"
     Case "may"
      strlTemporal="05"
     Case "jun"
      strlTemporal="06"
     Case "jul"
      strlTemporal="07"
     Case "ago","aug"
      strlTemporal="08"
     Case "sep"
      strlTemporal="09"
     Case "oct"
      strlTemporal="10"
     Case "nov"
      strlTemporal="11"
     Case "dic","dec"
      strlTemporal="12"
    End Select
    strlTemporal=strlTemporal & "/" & mid(strlFecha,1,3) & mid(strlFecha,8)
   Case "mmm/dd/yyyy","mmm/dd/yyyy hh:mm"
    Select Case LCase(mid(strlFecha,1,3))
     Case "ene","jan"
      strlTemporal="01"
     Case "feb"
      strlTemporal="02"
     Case "mar"
      strlTemporal="03"
     Case "abr","apr"
      strlTemporal="04"
     Case "may"
      strlTemporal="05"
     Case "jun"
      strlTemporal="06"
     Case "jul"
      strlTemporal="07"
     Case "ago","aug"
      strlTemporal="08"
     Case "sep"
      strlTemporal="09"
     Case "oct"
      strlTemporal="10"
     Case "nov"
      strlTemporal="11"
     Case "dic","dec"
      strlTemporal="12"
    End Select
    strlTemporal=strlTemporal & mid(strlFecha,4)
   Case "dd/mm/yyyy","dd/mm/yyyy hh:mm"
    strlTemporal=mid(strlFecha,4,3) & mid(strlFecha,1,3) & mid(strlFecha,8)
   Case "mm/dd/yyyy","mm/dd/yyyy hh:mm"
    strlTemporal=strlFecha
   Case Else
    strlTemporal=""
  End Select
  FormatearFechaBD=strlTemporal
 End Function

 Function FormatearFechaPrg(strlFecha)
  Dim blnlNoEncontrado
  If IsDate(strlFecha) Then
   FormatearFechaPrg=CDate(strlFecha)
  Else 
   blnlNoEncontrado=True
   If blnlNoEncontrado AND InStr(1,strlFecha,"ene",1) Then
    strlFecha=Replace(strlFecha,"ene","jan",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"abr",1) Then
    strlFecha=Replace(strlFecha,"abr","apr",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"ago",1) Then
    strlFecha=Replace(strlFecha,"ago","aug",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"dic",1) Then
    strlFecha=Replace(strlFecha,"dic","dec",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"jan",1) Then
    strlFecha=Replace(strlFecha,"jan","ene",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"apr",1) Then
    strlFecha=Replace(strlFecha,"apr","abr",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"aug",1) Then
    strlFecha=Replace(strlFecha,"aug","ago",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado AND InStr(1,strlFecha,"dec",1) Then
    strlFecha=Replace(strlFecha,"dec","dic",1,-1,1)
    blnlNoEncontrado=False
   End If
   If blnlNoEncontrado Then
    FormatearFechaPrg=""
   Else
    If IsDate(strlFecha) Then
     FormatearFechaPrg=CDate(strlFecha)
    Else
     FormatearFechaPrg=""
    End If
   End If
  End If  
 End Function

 Function FormatearFechaVer(datlFecha)
  Dim strlDia,strlMes,strlAno,strlHora,strlMinuto
 	session.LCID=2058
  If IsDate(datlFecha) Then
   strlDia=Day(datlFecha)
   If Len(strlDia)=1 Then strlDia="0" & strlDia
   strlMes=MonthName(Month(datlFecha),True)
   If Len(strlMes)=1 Then strlMes="0" & strlMes
   strlAno=Year(datlFecha)
   strlHora=Hour(datlFecha)
   If Len(strlHora)=1 Then strlHora="0" & strlHora
   strlMinuto=Minute(datlFecha)
   If Len(strlMinuto)=1 Then strlMinuto="0" & strlMinuto
   FormatearFechaVer=strlDia & "/" & strlMes & "/" & strlAno & " " & strlHora & ":" & strlMinuto
  Else 
   FormatearFechaVer=""
  End If
   session.LCID=1033 '2058
 End Function
%>