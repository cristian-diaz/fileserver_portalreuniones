<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="incluidos/ConexionBD.inc" -->
<%
Dim ObjOptimizacion
Dim ErrorEnviar, CodigoError
Dim ObjL_RecordSet, ObjL2_RecordSet, ObjL3_RecordSet, ObjL4_RecordSet
Dim StrL_SQL, StrL_SQL2,  StrL_Mensaje, Usuarios, Responsables

Set ObjOptimizacion = AbrirConexionBD(cadenaConexionDBQ)

Sub EscribirEnArchivo(strError)
'Procedimiento que Escribe el Log de la Aplicaci�n
Dim strCadena
	On Error Resume Next
	Const ForReading = 1, ForWriting = 2, ForAppending = 8	'Constantes de apertura de archivos
	'Const F_FILELOG =  "E:\Inetpub\wwwroot\morichalgll\database\Compromisos\Logs\CompromisosSOA.Log" 'Archivo de log del script
'	Const F_FILELOG =  "D:\Intranet\DataBase\Compromisos\Logs\Compromisos.Log"
    Const F_FILELOG =  "C:\PortalReunion\LogCorreo\Compromisos.Log"
	
	Dim fsodata, ffilelog
	
	Set fsodata = CreateObject("Scripting.FileSystemObject")
	If Not fsodata.FileExists(F_FILELOG) Then
		set ffilelog = fsodata.CreateTextFile(F_FILELOG, ForWriting)	
	End If
	
	Select Case strError
		Case "0" : str_Cadena = "Error al Enviar Mail"
		Case "1" : str_Cadena = "Correo Enviado"
		Case "2" : str_Cadena = "No se encontro usuario en el AD"		
		Case Else: str_Cadena = "Verificar si posee Cuenta de Correo"
	End Select
	
	set ffilelog = fsodata.Opentextfile(F_FILELOG,ForAppending, true)
	ffilelog.Writeline  Date() & " " & Time() & " " & str_Cadena & " Registro: " & ObjL_RecordSet("LOGIN") & ""
End Sub

Function EnviarMail(StrL_Asunto, StrL_Cuerpo, StrL_Destino)
'15-Febrero-2006
'Leyder Correa Romero
'Funci�n construida para Apiay debido a problemas usando el Componente de CDONTS
Dim Mail, StrL_Cadena, rsObjDirMail
Dim IntL_Posicion, IntL_Sitio, IntL_Longitud, StrL_DestinatarioActual, StrL_CadenaDef
Set Mail = Server.CreateObject("Persits.MailSender")
	StrL_SQL = "SELECT email, grupo FROM t_logins WHERE Login='" & StrL_Destino & "'"
	set rsObjDirMail = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	If rsObjDirMail.EOF Then
		ErrorEnviar = "2"
		CodigoError= "No se encontro en la tabla 't_logins' "
		exit function 
'		response.Write(StrL_SQL)
'		response.End()
	End If
	If CInt(rsObjDirMail("grupo"))=0 Then
		StrL_Destino = rsObjDirMail(0) & ""
		'Mail.AddAddress StrL_Destino Desarrollo
	Else
		strSQL = "SELECT login_grupo_loginid from login_grupo WHERE " & _
			"login_grupo_grupoid ='" & StrL_Destino & "'"
		Set objRsTemp = AbrirRecordSet(ObjOptimizacion, strSQL)
		While Not objRsTemp.EOF
			SQL = "SELECT email FROM t_logins WHERE Login='" & objRsTemp(0) & "'"
			Set ObjRsGrupos = AbrirRecordSet(ObjOptimizacion, SQL)
			If Not IsNull(ObjRsGrupos(0)) Then
				StrL_Destino = ObjRsGrupos(0)
				'Mail.AddAddress ObjRsGrupos(0) 
			End If
			ObjRsGrupos.Close
			Set ObjRsGrupos = Nothing
			objRsTemp.MoveNext
		WEnd
		objRsTemp.Close
		Set objRsTemp = Nothing
	End If
	set rsObjDirMail = Nothing

	If StrL_Destino <> "" Then
		'Configurar las opciones elementales como el servidor SMTP y Puerto
		Mail.Host = "10.1.141.213"
		Mail.Port = 25
		Mail.From = "ringftp@ecopetrol.com.co"
		Mail.FromName = "Revisor de Tareas"
		'Mail.AddAddress "Leyder.Correa@Ecopetrol.com.co"
		Mail.AddAddress StrL_Destino
		StrL_Cadena = StrL_Destino
	
		Mail.Subject = StrL_Asunto
		Mail.Body = StrL_Cuerpo
		Mail.IsHTML = True
		ErrorEnviar = ""
		On Error Resume Next 
		Mail.Send
	 End If
	 Set Mail = Nothing
	
		  If Err <> 0 Then 
			ErrorEnviar="0" 
			CodigoError = Err.Description
		  Else
			ErrorEnviar="1"
			CodigoError = "Mensaje Enviado"
		  End If
End Function

Sub Buscar_Moderadores()
'Realiza la consulta del Id de la Reuni�n
StrL_SQL = "SELECT TipoReunion.Id From TipoReunion " & _
    "WHERE (((TipoReunion.Nombre)='" & ObjL_RecordSet("TIPO") & "')) AND Activa = -1;"
Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL2_RecordSet.EOF Then
    'Realiza la consulta de los Moderadores de la Reuni�n
    StrL_SQL = "SELECT permisos.login &  ' - '  & t_logins.nombreusuario as Moderador " & _
        "FROM t_logins, permisos WHERE t_logins.Login = permisos.login " & _
        "AND (((permisos.id_reunion) = " & ObjL2_RecordSet("Id") & ") And ((permisos.permiso) = 'M')) " & _
        "ORDER BY t_logins.nombreusuario;"
	Set ObjL3_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
    'Construye la cadena que Contiene los moderadores de la reuni�n
    Responsables = ""
    Do Until ObjL3_RecordSet.EOF
        If Responsables = "" Then
                Responsables = ObjL3_RecordSet("Moderador")
            Else
                Responsables = Responsables & "; " & ObjL3_RecordSet("Moderador")
        End If
        ObjL3_RecordSet.MoveNext
    Loop
    ObjL3_RecordSet.Close
End If
ObjL2_RecordSet.Close
Set ObjL2_RecordSet = Nothing
Set ObjL3_RecordSet = Nothing
End Sub

'10-Enero-2006
'Leyder Correa Romero
'Cambio de Visual Basic a C�digo ASP.
'Consulta modificada para Unificar los monitores por Gerencia
'Se incluye adem�s los Responsables de Cada Reuni�n con el Fin de Informarle al usuario
StrL_SQL = "UPDATE Compromisos SET Compromisos.STATUS = 'ROJO' " & _
	 "WHERE (((Compromisos.STATUS)<>'VERDE' and (Compromisos.STATUS)<>'ROJO') " & _
	 "AND  ((Compromisos.FECHAC) >= '01/01/2003')) " & _
	 "AND ((Compromisos.FECHAC) < date());"
ObjOptimizacion.Execute StrL_SQL

StrL_SQL = "SELECT Compromisos.LOGIN, Compromisos.RESPONSABLE, Compromisos.TIPO," & _
	" Compromisos.TEMA, Compromisos.ACCION, Compromisos.STATUS, Compromisos.FECHAC, " & _
	" Compromisos.NoID FROM Compromisos  WHERE (((Compromisos.STATUS)<>'VERDE')" & _
	" AND ((Compromisos.FECHAC) <= date()) AND  ((Compromisos.FECHAC) >= '01/01/2003'))" & _
	" ORDER BY  LOGIN, Compromisos.FECHAC;"
  
  Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
  If Not ObjL_RecordSet.EOF Then
    Do Until ObjL_RecordSet.EOF
      Buscar_Moderadores
	  StrL_Mensaje = "<html><body>"
	  StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Se�or(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso Vencido:</small></font></p>"
	  StrL_Mensaje =  StrL_Mensaje & "<table>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reuni�n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
	  If Responsables <> "" Then
		 StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
	  End If
	  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
	  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Por Favor NO Olvide realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
	  StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente direcci�n: " 
	  StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
	  StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
	  StrL_Mensaje = StrL_Mensaje & "</html>" 
      EnviarMail ObjL_RecordSet("TEMA") & " - Compromisos Vencidos", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
	  EscribirEnArchivo ErrorEnviar
      StrL_Mensaje = ""
      ObjL_RecordSet.MoveNext
    Loop
  End If
  ObjL_RecordSet.Close
  Set ObjL_RecordSet = Nothing

'1-Dic-2005
'Leyder Correa Romero
'Procedimiento para enviar mails a Responsables con Compromisos a vencerse en
'tres d�as.

'Realiza la consulta de los compromisos a vencerse en tres d�as
StrL_SQL = "SELECT Compromisos.LOGIN, Compromisos.RESPONSABLE, Compromisos.TIPO, " & _
    "Compromisos.TEMA, Compromisos.Accion, Compromisos.STATUS, Compromisos.FECHAC, " & _
    "Compromisos.NoID, Compromisos.Fecha_EnvioMail From Compromisos " & _
    "WHERE (((Compromisos.STATUS)<>'VERDE') AND ((Compromisos.FECHAC)=Date()+3) AND " & _
    "((Compromisos.Fecha_EnvioMail)<Date() Or (Compromisos.Fecha_EnvioMail) Is Null)) " & _
    "ORDER BY Compromisos.LOGIN, Compromisos.FECHAC;"

Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL_RecordSet.EOF Then
Do Until ObjL_RecordSet.EOF
    Buscar_Moderadores
	StrL_Mensaje = "<html><body>"
	StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Se�or(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso pr�ximo a vencerse (en tres d�as):</small></font></p>"
	StrL_Mensaje =  StrL_Mensaje & "<table>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reuni�n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
	If Responsables <> "" Then
		StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
	End If
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Por Favor NO Olvide realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
	StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente direcci�n: " 
	StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
	StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
	StrL_Mensaje = StrL_Mensaje & "</html>" 
    EnviarMail ObjL_RecordSet("TEMA") & " - Compromisos Pendientes por Ejecuci�n (A Vencerse - 3 D�as)", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
	EscribirEnArchivo ErrorEnviar    
	ObjOptimizacion.Execute "UPDATE Compromisos SET Fecha_EnvioMail=Date() WHERE NoID=" & ObjL_RecordSet("NoID")
    StrL_Mensaje = ""
    ObjL_RecordSet.MoveNext
 Loop
ObjL_RecordSet.Close
End If
Set ObjL_RecordSet = Nothing

'1-Dic-2005
'Leyder Correa Romero
'Procedimiento para enviar mails a Responsables con Compromisos a vencerse en m�s de 30
'd�as. El mail se envia en la mitad del tiempo del Compromiso
'Los parametros se encuentran definidos en la consulta ComproMayores30Dias

StrL_SQL = "SELECT ComproMayores30Dias.LOGIN, ComproMayores30Dias.RESPONSABLE, " & _
    "ComproMayores30Dias.TIPO, ComproMayores30Dias.TEMA, ComproMayores30Dias.Accion, " & _
    "ComproMayores30Dias.STATUS, ComproMayores30Dias.NoID, " & _
    "ComproMayores30Dias.Fecha_EnvioMail, ComproMayores30Dias.FECHAC, " & _
    "ComproMayores30Dias.FechaEnviar From ComproMayores30Dias " & _
    "ORDER BY ComproMayores30Dias.RESPONSABLE;"

Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
If Not ObjL_RecordSet.EOF Then
Do Until ObjL_RecordSet.EOF
    Buscar_Moderadores
	StrL_Mensaje = "<html><body>"
	StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Se�or(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso:</small></font></p>"
	StrL_Mensaje =  StrL_Mensaje & "<table>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reuni�n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
	If Responsables <> "" Then
		StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
	End If
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
	StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Por Favor NO Olvide realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
	StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente direcci�n: " 
	StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
	StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
	StrL_Mensaje = StrL_Mensaje & "</html>" 
	EnviarMail ObjL_RecordSet("TEMA") & " - Compromisos Pendientes por Ejecuci�n (Est� a Mitad de Tiempo)", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
	EscribirEnArchivo ErrorEnviar
	If ErrorEnviar <> 0 Then
		EscribirEnArchivo "Error enviando mail " & Err.Number & " - " & Err.Description
		EnviarMail = "algo mal enviando mail " & Err.Number & " - " & Err.Description
	End If
    ObjOptimizacion.Execute "UPDATE Compromisos SET Fecha_EnvioMail=Date() WHERE NoID=" & ObjL_RecordSet("NoID")
    StrL_Mensaje = ""
    ObjL_RecordSet.MoveNext
Loop
ObjL_RecordSet.Close
End If
Set ObjL_RecordSet = Nothing

'1-Dic-2005
'Leyder Correa Romero
'Procedimiento para enviar mails a Responsables con Compromisos seg�n periodicidad establecida
Dim Diferencia, VarAux, VarDias
StrL_SQL2 = "SELECT Id_Periodicidad, No_Dias From Periodicidad WHERE Id_Periodicidad > 1"
Set ObjL4_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL2)

'Ciclo para permitir asignar todos los valores posibles existentes en la tabla
'periodicidad. Se Omite el 1 debido a que este no tiene periodicidad
Do Until ObjL4_RecordSet.EOF
	StrL_SQL = "SELECT Compromisos.LOGIN, Compromisos.RESPONSABLE, Compromisos.TIPO, " & _
		"Compromisos.TEMA, Compromisos.Accion, Compromisos.STATUS, Compromisos.NoID, " & _
		"Compromisos.FECHAI, Compromisos.FECHAC, Compromisos.Id_Periodicidad " & _
		"FROM Compromisos WHERE (((Compromisos.STATUS)<>'ROJO') " & _
		"AND ((Compromisos.FECHAC)>=Date()) " & _
		"AND ((Compromisos.Id_Periodicidad)=" & ObjL4_RecordSet(0) & ")) " & _
		"AND ((Compromisos.FechaRealCierre) Is Null)"
		
	Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	Do Until ObjL_RecordSet.EOF
		Diferencia = ObjL_RecordSet("FECHAC") - ObjL_RecordSet("FECHAI")
		VarAux = CLng(Diferencia / ObjL4_RecordSet(1))
		For i=1 to VarAux
			VarDias = ObjL4_RecordSet(1) * i
			If Date() = (ObjL_RecordSet("FECHAI") + VarDias) Then
				If Date() <= ObjL_RecordSet("FECHAC") Then
					Buscar_Moderadores
					StrL_Mensaje = "<html><body>"
					StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Se�or(a), " & UCase(ObjL_RecordSet("RESPONSABLE")) & ", a la fecha de Hoy " & Date & " tiene el siguiente Compromiso:</small></font></p>"
					StrL_Mensaje =  StrL_Mensaje & "<table>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small>Reuni�n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
					If Responsables <> "" Then
						StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Moderadores: </small></font></td><td><font face=Verdana color=#000000><small>" & Responsables & "</small></font></td></tr>"
					End If
					StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
					StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Por Favor NO Olvide realizar las Acciones para cumplir con el Compromiso y Cerrarlo, " 
					StrL_Mensaje = StrL_Mensaje & "para actualizar el estado puede consultar en cualquier momento el Portal Web en la siguiente direcci�n: " 
					StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
					StrL_Mensaje = StrL_Mensaje & "</td></tr></table></body></p>" 
					StrL_Mensaje = StrL_Mensaje & "</html>" 
		
					EnviarMail ObjL_RecordSet("TIPO") & " - Recordatorio de Compromiso - (" & ObjL4_RecordSet(1) & " D�as)", StrL_Mensaje, (ObjL_RecordSet("LOGIN"))
					EscribirEnArchivo ErrorEnviar            
					ObjOptimizacion.Execute "UPDATE Compromisos SET Fecha_EnvioMail=Date() WHERE NoID=" & ObjL_RecordSet("NoID")
					StrL_Mensaje = ""
				End If
			End If
		Next
	ObjL_RecordSet.MoveNext
	Loop
	ObjL_RecordSet.Close
	Set ObjL_RecordSet = Nothing
	ObjL4_RecordSet.MoveNext
 Loop
ObjL4_RecordSet.Close
Set ObjL4_RecordSet = Nothing

'17-Enero-2006
'Leyder Correa Romero
'Consulta que permite seleccionar los delegados de cada uno de los compromisos
'para enviarles mail recordandoles.
'El mail debe enviarse el Primero y el Quince de Cada Mes
If CInt(Day(Now)) =1 Or CInt(Day(Now)) =16 Then
	StrL_SQL = "SELECT Delegado FROM SubLista_Delegados_Asignados GROUP BY Delegado " & _
		"ORDER BY Delegado"
	Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	
	Do Until ObjL2_RecordSet.EOF
		StrL_SQL = "SELECT Compromisos.LOGIN, Compromisos.RESPONSABLE, Compromisos.TIPO, Compromisos.TEMA" & _
			"Compromisos.Accion, Compromisos.STATUS, Compromisos.FECHAC, Compromisos.Delegado, " & _
			"t_logins.nombreusuario, Compromisos.NoID FROM Compromisos, t_logins " & _
			"WHERE (((Compromisos.Delegado)<>Compromisos.Login And (Compromisos.Delegado)='" & ObjL2_RecordSet(0) & "') " & _
			"AND ((Compromisos.STATUS)<>'VERDE') AND ((t_logins.Login)=Compromisos.Delegado)) " & _
			"ORDER BY Compromisos.TIPO;"
		  Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
		  If Not ObjL_RecordSet.EOF Then
			  If ObjL_RecordSet("LOGIN") <> ObjL_RecordSet("Delegado") Then
			  StrL_Mensaje = "<html><body>"
			  StrL_Mensaje =  StrL_Mensaje & "<font face=Verdana color=#000000><small>Se�or(a), " & UCase(ObjL_RecordSet("nombreusuario")) & ", a la fecha de Hoy " & Date & " usted es delegado para seguimiento de los siguientes Compromisos y est�n pendientes por verificaci�n:</small></font></p>"
			  StrL_Mensaje =  StrL_Mensaje & "<table>"
				Do Until ObjL_RecordSet.EOF
				  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Reuni�n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TIPO") & "</small></font></td></tr>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Tema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("TEMA") & "</small></font></td></tr>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Responsable:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Login") & " - " & ObjL_RecordSet("RESPONSABLE") & "</small></font></td></tr>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Compromiso:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Accion") & "</small></font></td></tr>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Fecha Vencimiento:</small></font></td><td><font face=Verdana color=#000000><small>" & ObjL_RecordSet("FECHAC") & "</small></font></td></tr>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td><font face=Verdana color=#000080><small> Estado:</small></font></td><td><font face=Verdana color=#000000><small>" & UCase(ObjL_RecordSet("STATUS")) & "</small></font></td></tr>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Los Detalles del Compromiso puede verificarlos en la siguiente direcci�n Web: " 
				  StrL_Mensaje = StrL_Mensaje & "<a href='http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "'>http://bogevaprp/portaldereuniones/EditarCompromiso.asp?txtNro=" & ObjL_RecordSet("NoID") & "</a></small></font>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p>&nbsp;</td></tr>"
				  ObjL_RecordSet.MoveNext
				Loop
				  StrL_Mensaje = StrL_Mensaje & "<tr><td colspan=2></p><font face=Verdana color=#00000><small>Por Favor NO Olvide realizar las Acciones para que los Responsables, " 
				  StrL_Mensaje = StrL_Mensaje & "cumplan con los compromisos asignados y los Moderadores los verifiquen</small></font>" 
				  StrL_Mensaje = StrL_Mensaje & "</html>" 
				  EnviarMail ObjL_RecordSet("TEMA") & " - Compromisos Asignado como Delegado", StrL_Mensaje, ObjL2_RecordSet(0)
				  EscribirEnArchivo ErrorEnviar
				  StrL_Mensaje = ""
			  End If
			  ObjL_RecordSet.Close
		  End If
		Set ObjL_RecordSet = Nothing
		ObjL2_RecordSet.MoveNext
	Loop
	Set ObjL2_RecordSet = Nothing
End If

'17-Enero-2006
'Leyder Correa Romero
'Procedimiento para el env�o de mail con estad�sticas a cada moderador
'de las reuniones. Estos contienen un hiperv�nculo que les permite
'ver el detalle de la informaci�n enviada. Este mensaje se env�a cada
'quince d�as
If CInt(Day(Now)) =1 Or CInt(Day(Now)) =16 Then
	StrL_SQL = "SELECT permisos.login, permisos.permiso, t_logins.nombreusuario FROM permisos, t_logins " & _
		"WHERE permisos.login = t_logins.Login GROUP BY permisos.login, permisos.permiso, " & _
		"t_logins.nombreusuario HAVING (((permisos.permiso)='M')) ORDER BY permisos.login"

	Set ObjL2_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
	Do Until ObjL2_RecordSet.EOF '1
		StrL_Mensaje = ""
		StrL_Mensaje2 = "<html><body>" & _
			"<font face=Verdana color=#000000><small>Se�or(a), " & UCase(ObjL2_RecordSet("nombreusuario")) & ", a la fecha de Hoy " & Date & _
			" este es el estad�stico de la(s) siguiente(s) reuni�n(es) de la cual usted es moderador:</small></font>" 
		StrL_SQL = "SELECT permisos.id_reunion, TipoReunion.Nombre FROM permisos, TipoReunion " & _
			"WHERE (((permisos.login)='" & ObjL2_RecordSet("login") & "') AND ((permisos.permiso)='M') " & _
			"AND ((TipoReunion.Id)=Permisos.Id_Reunion)) AND TipoReunion.Activa = -1 " & _
			"ORDER BY permisos.id_reunion, permisos.login;"
	    Set ObjL3_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
		Do Until ObjL3_RecordSet.EOF '2
			StrL_SQL = "SELECT Compromisos.STATUS, Count(Compromisos.STATUS) AS Cantidad FROM Compromisos " & _
				"GROUP BY Compromisos.TIPO, Compromisos.STATUS, Compromisos.VERIFICADO " & _
				"HAVING (((Compromisos.TIPO)='" & ObjL3_RecordSet("Nombre") & "') AND " & _
				"((Compromisos.VERIFICADO)=0));"
			  Set ObjL_RecordSet = AbrirRecordSet(ObjOptimizacion, StrL_SQL)
			  If Not ObjL_RecordSet.EOF Then
				  StrL_Mensaje = StrL_Mensaje & "<p><font face=Verdana color=#000000><small> Reuni�n:&nbsp;&nbsp;" & ObjL3_RecordSet("Nombre") & "</small></font><p>"
				  StrL_Mensaje = StrL_Mensaje & "<table width='25%'  border='1' cellspacing='0' cellpadding='0'>"
				  StrL_Mensaje = StrL_Mensaje & "<tr><td width='50%'><div align='center'><font face=Verdana color=#000080><small> Estado </small></font></div></td><td width='50%'><div align='center'><font face=Verdana color=#000000><small>Cantidad</small></font></div></td></tr>"
					  Do Until  ObjL_RecordSet.EOF '3
					   Select Case ObjL_RecordSet("STATUS")
						   Case "SIN COLOR"
								StrL_Mensaje = StrL_Mensaje & "<tr><td>"
						   Case "ROJO"
								StrL_Mensaje = StrL_Mensaje & "<tr><td bgcolor=Red>"
						   Case "VERDE"
								StrL_Mensaje = StrL_Mensaje & "<tr><td bgcolor=Green>"
					   End Select
					   StrL_Mensaje = StrL_Mensaje & "<font face=Verdana color=#000080><small><a href='http://bogevaprp/portaldereuniones/Presentar_Compromisos.asp?txtReunion=" & ObjL3_RecordSet("Nombre") & "&txtStatus=" & ObjL_RecordSet("STATUS") & "&Consultar=SI'>" & ObjL_RecordSet("STATUS") & "</small></font></td><td><div align='center'><font face=Verdana color=#000000><small>" & ObjL_RecordSet("Cantidad") & "</small></font></div></td></tr>"
					   ObjL_RecordSet.MoveNext
					  Loop 'De la Reunion '3
					  StrL_Mensaje = StrL_Mensaje & "</table><p>"					  
			  End If
			  Set ObjL_RecordSet = Nothing
			  ObjL3_RecordSet.MoveNext
		Loop 'Del Usuario '2
		Set ObjL3_RecordSet = Nothing
		If StrL_Mensaje <> "" Then
			StrL_Mensaje = StrL_Mensaje2  & StrL_Mensaje 
			StrL_Mensaje = StrL_Mensaje & "<font face=Verdana color=#00000><small>Por Favor NO Olvide realizar las Acciones para que los Responsables, " 
			StrL_Mensaje = StrL_Mensaje & "cumplan con los compromisos asignados y Verificar los Cumplidos</small></font>" 
			StrL_Mensaje = StrL_Mensaje & "</html>" 
			EnviarMail "Estad�stico de Compromisos para Moderadores", StrL_Mensaje, ObjL2_RecordSet("login")
			EscribirEnArchivo ErrorEnviar
		End If
		ObjL2_RecordSet.MoveNext
	Loop 'De la lista de Moderadores '1
	Set ObjL_RecordSet = Nothing
End If
ObjOptimizacion.Close
%>
<script language="JavaScript" type="text/JavaScript">
<!--
window.close();
// -->
</script>