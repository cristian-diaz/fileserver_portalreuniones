<html>
<head>
<% 'Función que pasa el , a . para ser visible en pantalla
   Function FixNumber(num)
   Dim tmp
   Dim i  
   tmp = ""
   For i = 1 To Len(CStr(num))
    Select Case Mid(num,i,1)
     Case ",":
      tmp = tmp & "."
     Case Else
      tmp = tmp & Mid(num,i,1)
    End Select
   Next
   FixNumber = tmp
  End Function%>
   
  <%Sub truncar(num)
     Response.write(FixNumber(FormatNumber(num,0,,,-1)) & " %")
    End Sub%>
    
<title>Estadística de Compromisos por Reunión</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="Estilos/estprincipal.css" rel="stylesheet" type="text/css">
<link href="Estilos/GCBstyle.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {
	font-size: 12px;
	color: #000000;
}
.Estilo2 {
	font-size: 12px;
	font-weight: bold;
}
.Estilo8 {font-size: 10px; font-weight: bold; color: #000000; }
.Estilo9 {font-size: 10px}
.Estilo10 {color: #000000}
.Estilo11 {color: #FFFFFF}
.Estilo12 {
	font-size: 10px;
	color: #FFFFFF;
	font-weight: bold;
}
body {
	background-color: #EDF3E2;
}
-->
</style>
</head>

<body text="#000000">
<form name="form1" method="post" action="">
  <Br>
  <table width="100%" border="0">
    <tr class="EncTabla2"> 
      <td>
        <h1 align="center" class="EncTabla2"><b>ESTADISTICAS</b></h1>
      </td>
    </tr>
    <tr>
      <td height="4">&nbsp;</td>
    </tr>
    <tr> 
      <td> 
        <ul>
          <li>
            <div align="left"><font face="Arial, Helvetica, sans-serif" size="2"><b>Status 
              de Compromisos</b></font></div>
          </li>
        </ul>
      </td>
    </tr>
  </table>
  <div align="center">    <table width="94%" border="1" cellpadding="0" cellspacing="0">
      <tr class="EncTabla"> 
        <td width="71%" class="EncTabla2"> 
          <div align="center" class="Estilo2 Estilo1 Estilo9"><font face="Arial, Helvetica, sans-serif">Status</font></div>
        </td>
        <td width="10%" class="EncTabla2"> 
          <div align="center" class="Estilo8 Estilo9"><font face="Arial, Helvetica, sans-serif">Cant.</font></div>
        </td>
        <td width="19%" class="EncTabla2"> 
          <div align="center" class="Estilo8"><font face="Arial, Helvetica, sans-serif">% 
            Cumpli.</font></div>
        </td>
      </tr>
      <tr class="DetTabla"> 
        <td bgcolor=Green width="71%"><div align="center" class="Estilo9 Estilo11"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Verde (Cumplidos en la Fecha) </font></strong></div></td>
        <td width="10%" class="DetTablaNE">          <div align="center" class="Estilo9"><%=Session("VC")%></div></td>
        <td width="19%" class="DetTablaNE">          <span class="Estilo9">
          <%
          if   Session("VC")= 0 then
          Vnum=0
          else
          Vnum=(Session("VC")/(Session("Rn")+Session("VNoC")+Session("VC")+Session("Sc")))*100
          end if   
		 'Vnum=(Session("VC")/(Session("Rn")+Session("VNoC")+Session("VC")))*100
          %>          
          </span>          <div align="center" class="Estilo9">
            <%truncar(Vnum)%>
        </div></td>        
      </tr>
      <tr class="DetTabla">
        <td bgcolor=Green><div align="center"><span class="Estilo12"><font face="Verdana, Arial, Helvetica, sans-serif">Verde<strong><font face="Verdana, Arial, Helvetica, sans-serif">(Cumplidos fuera de Fecha)</font><font face="Verdana, Arial, Helvetica, sans-serif"> </font></strong></font></span></div></td>
        <td class="DetTablaNE"><div align="center"><span class="Estilo9"><%=Session("VNoC")%></span></div></td>
        <td class="DetTablaNE"><span class="Estilo9">
          <%
           if Session("VNoC")=0 then
           Vnum2=0
           else
		  'Vnum2=(Session("VNoC")/(Session("Rn")+Session("VNoC")+Session("VC")+Session("Sc")))*100
		  Vnum2=(Session("VNoC")/(Session("Rn")+Session("VNoC")+Session("VC")))*100
          end if
          %>
        </span>
          <div align="center" class="Estilo9">
            <%truncar(Vnum2)%>
        </div></td>
      </tr>
      <tr class="DetTabla"> 
        <td bgcolor=Red width="71%"><div align="center" class="Estilo9"><strong><font face='Verdana, Arial, Helvetica, sans-serif'>Rojo</font></strong></div></td>
        <td width="10%" class="DetTablaNE">          <div align="center" class="Estilo9"><%=Session("Rn")%></div></td>
        <td width="19%" class="DetTablaNE">          <span class="Estilo9">
        <%
        if Session("Rn") = 0 then
        Rnum=0 
        else
		  'Rnum=(Session("Rn")/(Session("Rn")+Session("VNoC")+Session("VC")+Session("Sc")))*100
		  Rnum=(Session("Rn")/(Session("Rn")+Session("VNoC")+Session("VC")))*100
        end if  
          %>
</span>          <div align="center" class="Estilo9">
            <%truncar(Rnum)%>
        </div></td>
      </tr>  
      <tr class="DetTabla"> 
        <td bgcolor=White width="71%"><div align="center" class="Estilo9"><strong><font face='Verdana, Arial, Helvetica, sans-serif'>Blanco</font></strong></div></td>
        <td width="10%" class="DetTablaNE">          <div align="center" class="Estilo9"><%=Session("Sc")%></div></td>
        <td width="19%" class="DetTablaNE">          <span class="Estilo9">
          <%
		  'Bnum=(Session("Sc")/(Session("Rn")+Session("VNoC")+Session("VC")+Session("Sc")))*100
          %>          
          </span><div align="center" class="Estilo9">
            <%'truncar(Bnum)%>&nbsp;
        </div></td>
      </tr>
      
      <tr class="DetTablaNE"> 
        <td width="71%"><div align="center" class="Estilo9 Estilo10"><strong><font face='Verdana, Arial, Helvetica, sans-serif'>Totales</font></strong></div></td>
        <td width="10%" class="DetTablaNE"><div align="center" class="Estilo9"><%=Session("Rn")+Session("VC")+Session("VNoC")+Session("Sc")%></div></td>   	     
        
   		<td width="19%" class="DetTablaNE">          <span class="Estilo9">
   		  <%
          'Total=(Vnum+Vnum2+Rnum+Bnum)
		  Total=(Vnum+Vnum2+Rnum)
          %>          
   		  </span>   		  <div align="center" class="Estilo9">
            <%truncar(Total)%>
        </div></td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <table width="100%" border="0">
  <tr>
    <td><div align="center"><font face="Arial, Helvetica, sans-serif" size="2"><a href="javascript:close()">Cerrar 
          Ventana</a></font></div></td>
  </tr>
</table>
  </div>
</form>
</body>
</html>
