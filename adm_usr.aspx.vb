﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Class adm_usr
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Button1.Attributes.Add("onclick", "javascript:url('guardar_registro.asp');")
        Button2.Attributes.Add("onclick", "javascript:url('administrarGrupos.asp');")

        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Reload()
        End If

    End Sub

    Public Sub prcCargarOrigen(ByVal Objeto As String)
        Select Case Objeto
            Case "edicionR"

                Me.ds_evalua.Tables(0).Clear()
                Me.evalu.admon_usuarios(1, "")
                Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))

            Case "reuniones"
                Me.ds_evalua.Tables("dtt_reunion").Clear()
                Me.evalu.Tp_Roles()
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "Buscar_Usuario"
                Me.ds_evalua.Tables(0).Clear()
                If txt_registro.Text = "" Then
                    Me.evalu.admon_usuarios(3, Me.txt_nombre.Text.ToUpper)
                    Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill.Tables(0))
                Else
                    Me.evalu.admon_usuarios(2, Me.txt_registro.Text.ToUpper)
                    Me.ds_evalua.Tables(0).Merge(Me.evalu.DatasetFill2.Tables(0))
                End If




        End Select
    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo

            Case "edicionR"
                Me.grd2.DataSource = Me.ds_evalua.Tables(0)
                Me.grd2.DataBind()

        End Select
    End Sub
    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_users As New DataTable   '--0
        Dim dtt_reunion As New DataTable

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_users")
        Me.ds_evalua.Tables.Add("dtt_reunion")


    End Sub

#Region "Propertys"
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property
#End Region

    Protected Sub grd2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grd2.PageIndexChanging
        Me.grd2.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("edicionR")
    End Sub

    Protected Sub grd2_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grd2.RowCancelingEdit
        Me.grd2.EditIndex = -1
        Reload()

    End Sub

    Protected Sub grd2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grd2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            prcCargarOrigen("reuniones")

            Try
                CType(e.Row.FindControl("dl1"), DropDownList).DataSource = Me.ds_evalua.Tables(1).CreateDataReader
                CType(e.Row.FindControl("dl1"), DropDownList).DataTextField = "Rol"
                CType(e.Row.FindControl("dl1"), DropDownList).DataValueField = "id"
                CType(e.Row.FindControl("dl1"), DropDownList).DataBind()

            Catch ex As Exception

            End Try

        End If
    End Sub


    Protected Sub grd2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grd2.RowDeleting
        Dim i, j As Integer
        Dim Fila As GridViewRow = Nothing
        Dim index As Integer
        Dim dtt_users As New DataTable

        i = e.RowIndex + 1
        For j = i To dtt_users.Rows.Count - 1
            Dim ae As Integer = dtt_users.Rows(j).Item(0)
            dtt_users.Rows(j).Item(0) = ae - 1
        Next

        index = i - 1
        Fila = Me.grd2.Rows(index)
        Dim login As String = CType(Fila.FindControl("Label1"), Label).Text.ToUpper
        Me.evalu.Usuarios_Deleted(login)

        Reload()

    End Sub

    Protected Sub grd2_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grd2.RowEditing
        Me.grd2.EditIndex = e.NewEditIndex
        Me.grd2.ShowFooter = False
        PrcCargarDatos("edicionR")
    End Sub
 
    Public Sub grabar_user(ByVal Accion As String)

        Dim Fila As GridViewRow = Nothing
        Dim index As Integer
        Dim var As String
        Select Case Accion
            Case "UPDATE"
                index = Me.grd2.EditIndex
                Fila = Me.grd2.Rows(index)

                'Dim login As String = CType(Fila.FindControl("txt_login"), TextBox).Text.ToUpper
                'Dim login As String = CType(Fila.FindControl("txt_login"), TextBox).Text
                Dim login As String = CType(Fila.FindControl("lbl_login"), Label).Text
                Dim nombre As String = CType(Fila.FindControl("txt_nombre"), TextBox).Text
                Dim rol As String = CType(Fila.FindControl("dl1"), DropDownList).SelectedValue
                Dim mail As String = CType(Fila.FindControl("txt_mail"), TextBox).Text
                Dim activo As String = CType(Fila.FindControl("act2"), CheckBox).Checked

                If activo = True Then
                    var = 1
                Else
                    var = 0
                End If

                Me.evalu.Usuarios_Update(login, nombre, rol, mail, var)
                Me.MsgBox1.ShowMessage("USUARIO SE HA ACTUALIZADO...................")



        End Select
    End Sub

    Protected Sub grd2_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grd2.RowUpdating
        Me.grabar_user("UPDATE")
        Reload()

    End Sub

    Private Sub Reload()
        Me.prcCargarOrigen("edicionR")
        Me.PrcCargarDatos("edicionR")
    End Sub

   
    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click
        Me.prcCargarOrigen("Buscar_Usuario")
        Me.PrcCargarDatos("edicionR")
    End Sub

    Protected Sub btn_todos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_todos.Click
        Reload()
    End Sub
End Class
