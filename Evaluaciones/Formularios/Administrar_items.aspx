<%@ Page Language="VB" MasterPageFile="~/Evaluaciones/Plantilla/P_Plantilla_Aux.master" AutoEventWireup="false" CodeFile="Administrar_items.aspx.vb" Inherits="Evaluaciones_Formularios_Administrar_items" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CntCuerpo" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td style="color: white; font-weight: bold; font-size: 16px; background-image: url(../../img/barra2.JPG); font-family: Verdana,Arial,Helvetica,sans-serif; height: 18px;" colspan="6">
                Administraci�n de �tems de la Evaluaci�n</td>
        </tr>
        <tr>
            <td style="width: 20%; height: 20px;">
            </td>
            <td style="width: 15%; height: 20px;">
            </td>
            <td style="width: 25%; height: 20px;">
            </td>
            <td style="width: 15%; height: 20px;">
            </td>
            <td style="width: 25%; height: 20px;">
            </td>
            <td style="width: 10%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 20%">
            </td>
            <td style="width: 15%" align="left">
                <asp:Label ID="LblOpcion1" runat="server" SkinID="TitTabla" Text="Crear �tem:"></asp:Label></td>
            <td style="width: 25%" align="left">
                <asp:Button ID="Button1" runat="server" BackColor="#E0E0E0" Font-Bold="False" Font-Italic="False"
                    Text="Nuevo Item" Width="146px" /></td>
            <td style="width: 15%" align="left">
                <asp:Label ID="LblOpcion2" runat="server" SkinID="TitTabla" Text="Paginar Lista:"></asp:Label></td>
            <td style="width: 25%" align="left">
                <asp:CheckBox ID="ChkPaginar" runat="server" AutoPostBack="True" /></td>
            <td style="width: 10%">
            </td>
        </tr>
        <tr>
            <td style="width: 20%; height: 20px">
            </td>
            <td style="width: 15%; height: 20px">
            </td>
            <td style="width: 25%; height: 20px">
            </td>
            <td style="width: 15%; height: 20px">
            </td>
            <td style="width: 25%; height: 20px">
            </td>
            <td style="width: 10%; height: 20px">
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width: 20%">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Height="210px" DataKeyNames="Id_item" SkinID="GvwEvaluacion" Width="670px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Select"
                                    Height="22px" ImageUrl="~/Evaluaciones/Img/Imb_EditarRegistro.png" ToolTip="Editar Item"
                                    Width="20px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item a Evaluar">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# bind("Item") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# bind("Item") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Peso">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# bind("peso") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# bind("Peso") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comentario">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Height="39px" Text='<%# bind("Comentario") %>'
                                    Width="227px"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# bind("Comentario") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Calificacion">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# bind("calificacion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# bind("calificacion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activo">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# bind("activo") %>' Enabled="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Green" ForeColor="White" />
                    <AlternatingRowStyle BackColor="WhiteSmoke" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

