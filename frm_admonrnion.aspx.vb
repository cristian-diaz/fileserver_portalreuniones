﻿Imports System
Imports System.Data
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.Office.Interop
Imports System.Globalization
Imports System.Threading.Thread
Imports System.IO
Imports System.Diagnostics
Imports System.Windows

Partial Class frm_admonrnion
    Inherits System.Web.UI.Page
    Dim concls As New Consultas
    Dim bandera As Integer
    Dim nombreXdefecto As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Me.picbtn1.Attributes.Add("onclick", "javascript:url('Nuevo_nombre.aspx');")
        'Me.picbtn2.Attributes.Add("onclick", "javascript:url('nuevo_tipo.asp');")
        'Me.picbtn3.Attributes.Add("onclick", "javascript:url('nuevo_tema.asp');")
        'Me.picbtn4.Attributes.Add("onclick", "javascript:url('Borrar_reu.asp');")


        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            Me.prcCargarOrigen("tpreuniones")
            Me.PrcCargarDatos("tpreuniones")
            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")
        End If

    End Sub



    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet
        Dim dtt_reunion As New DataTable   '--0

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion")
        Me.ds_evalua.Tables.Add("dtt_tpreuniones")
        Me.ds_evalua.Tables.Add("dtt_compromisos")
   


    End Sub

    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property

    Public Sub prcCargarOrigen(ByVal Objeto As String)

        Select Case Objeto
            Case "tpreuniones"
                Me.concls.Tp_Reunion()
                Me.ds_evalua.Tables("dtt_tpreuniones").Merge(Me.concls.DatasetFill.Tables(0))


            Case "reuniones"

                Me.ds_evalua.Tables("dtt_reunion").Clear()
                Me.concls.Consultar_Reuniones("todas", "")
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.concls.DatasetFill.Tables(0))

            Case "filtro"
                Select Case bandera
                    Case 1

                        Me.ds_evalua.Tables("dtt_reunion").Clear()

                        Me.concls.Consultar_Reuniones("parametros", , IIf(Me.dr_estado.SelectedItem.Text <> "---Seleccione Estado---", dr_estado.SelectedValue, dr_estado.SelectedItem.Text), IIf(Me.dr_tipo.SelectedItem.Text <> "---Seleccione Tipo---", dr_tipo.SelectedValue, dr_tipo.SelectedItem.Text), Me.txt_nombre.Text.Trim)
                        If concls.DatasetFill.Tables(0).Rows.Count > 0 Then
                            Me.ds_evalua.Tables("dtt_reunion").Merge(Me.concls.DatasetFill.Tables(0))
                        Else
                            Me.ds_evalua.Tables("dtt_reunion").Clear()
                        End If

                    Case 2
                        Me.ds_evalua.Tables("dtt_reunion").Clear()
                        Me.concls.Consultar_Reuniones("tipo", Me.dr_tipo.SelectedValue)
                        Me.ds_evalua.Tables("dtt_reunion").Merge(Me.concls.DatasetFill.Tables(0))
                    Case 3
                        Me.ds_evalua.Tables("dtt_reunion").Clear()
                        Me.concls.Consultar_Reuniones("estado", Me.dr_estado.SelectedValue)
                        Me.ds_evalua.Tables("dtt_reunion").Merge(Me.concls.DatasetFill.Tables(0))

                End Select

        End Select

    End Sub



    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "tpreuniones"
                Me.dr_tipo.DataSource = Me.ds_evalua.Tables("dtt_tpreuniones").CreateDataReader
                Me.dr_tipo.DataTextField = "NombreReunion"
                Me.dr_tipo.DataValueField = "id"
                Me.dr_tipo.DataBind()
                Me.dr_tipo.Items.Insert(0, New ListItem("---Seleccione Tipo---", ""))


            Case "reuniones"

                Dim nregistros As Integer = Me.ds_evalua.Tables("dtt_reunion").Rows.Count
                Me.lbl_count.Text = "Numero de Registros:  " & nregistros

                Me.grd_rniones.DataSource = Me.ds_evalua.Tables("dtt_reunion")
                Me.grd_rniones.DataBind()

        End Select

    End Sub

    Protected Sub grd_rniones_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grd_rniones.PageIndexChanging

        Me.grd_rniones.PageIndex = e.NewPageIndex
        Me.PrcCargarDatos("reuniones")

    End Sub

    Protected Sub grd_rniones_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grd_rniones.RowDataBound
        Dim gvr As GridViewRow = e.Row
        'Dim actividad As DataRow = Me.ds_evalua.Tables("dtt_reunion").Rows(0)
        'Dim valor As String = actividad("Activa")
        Dim estado As String

        If gvr.RowType = DataControlRowType.DataRow Then
            estado = e.Row.Cells(8).Text
            If estado = False Then
                CType(e.Row.FindControl("img_button"), ImageButton).ImageUrl = "~/img/inactivo.png"
                CType(e.Row.FindControl("img_button"), ImageButton).ToolTip = "Inactivo"
                e.Row.Cells(8).ForeColor = Drawing.Color.White
                e.Row.Cells(8).BackColor = Drawing.Color.White
                e.Row.Cells(7).BackColor = Drawing.Color.White
            End If

            If estado = True Then
                CType(e.Row.FindControl("img_button"), ImageButton).ImageUrl = "~/img/activo.png"
                CType(e.Row.FindControl("img_button"), ImageButton).ToolTip = "Activo"
                e.Row.Cells(7).BackColor = Drawing.Color.White
                e.Row.Cells(8).BackColor = Drawing.Color.White
                e.Row.Cells(8).ForeColor = Drawing.Color.White
            End If

        End If




    End Sub

    Protected Sub grd_rniones_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grd_rniones.RowDeleting

        'ESTE PROCEDIEMIENTO ACTUALIZA EL ESTADO ACTIVA O INACTIVA DE LA REUNION
        Dim i, j As Integer
        Dim Fila As GridViewRow = Nothing
        Dim index As Integer
        Dim idrnion, rnombre As String
        Dim dtt_users As New DataTable

        i = e.RowIndex + 1
        For j = i To dtt_users.Rows.Count - 1
            Dim ae As Integer = dtt_users.Rows(j).Item(0)
            dtt_users.Rows(j).Item(0) = ae - 1
        Next

        index = i - 1
        Fila = Me.grd_rniones.Rows(index)
        idrnion = CType(Fila.FindControl("lnk_actividad1"), LinkButton).Text.ToUpper
        rnombre = CType(Fila.FindControl("Label1"), Label).Text.ToUpper


        If concls.prod_busca_compromisos(idrnion) = False Then
            Me.MsgBox1.ShowMessage("Esta Reunion Tiene Compromisos que No Han sido verificados")

        Else
            Me.hfield.Value = idrnion
            Me.MsgBox1.ShowConfirmation("Esta seguro de cambiar el estado de la reunión " & rnombre, "Estado", True, False)
           
        End If

    End Sub


    Private Sub MsgBox1_YesChoosed(ByVal sender As Object, ByVal Key As String) Handles MsgBox1.YesChoosed
        Select Case Key
            Case "Estado"
                Me.concls.prod_cambiar_estado(Me.hfield.Value)
                Me.prcCargarOrigen("reuniones")
                Me.PrcCargarDatos("reuniones")

        End Select

    End Sub

    Protected Sub grd_rniones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grd_rniones.SelectedIndexChanged
        Me.PrcEdicionReuniones(Me.grd_rniones.SelectedValue)
    End Sub
    Public Sub PrcEdicionReuniones(ByVal idrnion As String)


        Dim URL As String = "frm_edicionreuniones.aspx?idReunion=" & idrnion

        Dim popupScript As String = "<script language='JavaScript'>" & _
                                            "window.open('" & URL & "', 'CustomPopUp', " & _
                                            "'dependent=yes,toolbar=no,scrollbars=no,resizable=yes,status=yes,left=120,Top=20,width=650 height=450')" & _
                                            "</script>"
        Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        bandera = 1
        Me.prcCargarOrigen("filtro")
        Me.PrcCargarDatos("reuniones")


    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Me.prcCargarOrigen("reuniones")
        Me.PrcCargarDatos("reuniones")
    End Sub

    Public Sub exportarreporte()

        'se carga el datatable con los datos
        Dim dtable As New DataTable

        If chkAllReuniones.Checked = True Then

            Me.ds_evalua.Tables("dtt_reunion").Clear()
            Me.concls.Consultar_Reuniones("todas", "")
            If concls.DatasetFill.Tables(0).Rows.Count > 0 Then
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.concls.DatasetFill.Tables(0))
                dtable = Me.concls.DatasetFill.Tables(0)
            Else
                Me.ds_evalua.Tables("dtt_reunion").Clear()
                lblExpExcel.Text = "La Consulta No Tiene Datos"
            End If
        Else
            Me.ds_evalua.Tables("dtt_reunion").Clear()
            Me.concls.Consultar_Reuniones("parametros", , Me.dr_estado.SelectedValue, Me.dr_tipo.SelectedValue, Me.txt_nombre.Text.Trim)
            If concls.DatasetFill.Tables().Count > 0 Then
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.concls.DatasetFill.Tables(0))
                dtable = Me.concls.DatasetFill.Tables(0)
            Else
                Me.ds_evalua.Tables("dtt_reunion").Clear()
                lblExpExcel.Text = "La Consulta No Tiene Datos"
            End If
        End If

        Response.Clear()
        Response.Buffer = True
        'Response.ContentType = "application/vnd.ms-excel"
        ' Establece el tipo de documento
        Response.ContentType = TipoDocumento()
        ' Fuerza a hacer un download del archivo
        Response.AddHeader("Content-Disposition", "attachment;filename=" + Format(Now, "dd-MM-yyyy-HH-mm-ss") + nombreXdefecto)
        ' Escribe el contenido del
        Dim sep As String = ""
        For Each dc As DataColumn In dtable.Columns
            Response.Write(sep + dc.ColumnName)
            sep = "" & vbTab & ""

        Next
        Response.Write("" & vbLf & "")

        Dim i As Integer
        For Each dr As DataRow In dtable.Rows
            sep = ""
            For i = 0 To dtable.Columns.Count - 1
                Response.Write(sep + dr(i).ToString())
                sep = "" & vbTab & ""

            Next
            Response.Write("" & vbLf & "")

        Next
        Response.End()

    End Sub

    Private Function TipoDocumento() As String
        Dim tipo As String
        'Select Case Integer.Parse(ddlTipos.SelectedValue) ' De acuerdo al valor seleccionado en el combo
        '    Case ExportFormatType.Excel
        tipo = "application/vnd.ms-excel"
        nombreXdefecto = "_REPORTE_REUNIONES_PORTAL_DE_COMPROMISOS" + ".xls"
        '    Case ExportFormatType.RichText
        'tipo = "application/rtf"
        'nombreXdefecto &= ".rtf"
        '    Case ExportFormatType.WordForWindows
        'tipo = "application/msword"
        'nombreXdefecto &= ".doc"
        '    Case Else ' Tipo por defecto
        'tipo = "application/pdf"
        'nombreXdefecto &= ".pdf"
        'End Select
        Return tipo
    End Function

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_report.Click
        exportarreporte()
    End Sub

    Protected Sub picbtn1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles picbtn1.Click
        Dim popupScript As String = "<script language='JavaScript'>" & _
                                            "window.open('Nuevo_nombre.aspx', 'CustomPopUp', " & _
                                            "'dependent=yes,toolbar=no,scrollbars=yes,resizable=yes,status=yes,left=120,Top=20,width=640 height=540')" & _
                                            "</script>"
        Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)
    End Sub

    Protected Sub picbtn2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles picbtn2.Click

        Dim popupScript As String = "<script language='JavaScript'>" & _
                                            "window.open('nuevo_tipo.asp', 'CustomPopUp', " & _
                                            "'dependent=yes,toolbar=no,scrollbars=yes,resizable=yes,status=yes,left=120,Top=20,width=640 height=200')" & _
                                            "</script>"
        Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)
    End Sub

    Protected Sub picbtn3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles picbtn3.Click
        Dim popupScript As String = "<script language='JavaScript'>" & _
                                                 "window.open('nuevo_tema.asp', 'CustomPopUp', " & _
                                                 "'dependent=yes,toolbar=no,scrollbars=yes,resizable=yes,status=yes,left=120,Top=20,width=640 height=200')" & _
                                                 "</script>"
        Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)
    End Sub

    Protected Sub picbtn4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles picbtn4.Click
        Dim popupScript As String = "<script language='JavaScript'>" & _
                                                     "window.open('Borrar_reu.asp', 'CustomPopUp', " & _
                                                     "'dependent=yes,toolbar=no,scrollbars=yes,resizable=yes,status=yes,left=120,Top=20,width=640 height=200')" & _
                                                     "</script>"
        Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", popupScript)

    End Sub

    'Protected Sub dr_tipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dr_tipo.SelectedIndexChanged
    '    bandera = 2
    '    Me.prcCargarOrigen("filtro")
    '    Me.PrcCargarDatos("reuniones")
    'End Sub

    'Protected Sub dr_estado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dr_estado.SelectedIndexChanged
    '    bandera = 3
    '    Me.prcCargarOrigen("filtro")
    '    Me.PrcCargarDatos("reuniones")
    'End Sub

    
End Class
