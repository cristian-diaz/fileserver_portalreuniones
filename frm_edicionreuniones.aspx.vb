﻿Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.Odbc


Partial Class frm_edicionreuniones
    Inherits System.Web.UI.Page
    Dim evalu As New Consultas
    Dim idrnion As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Me.prcCrearDataset()
            llenar_drow()

            Me.prcCargarOrigen("tp_reuniones")
            Me.PrcCargarDatos("tp_reuniones")

            Me.prcCargarOrigen("reuniones")
            Me.PrcCargarDatos("reuniones")

            Me.prcCargarOrigen("tp_dependecias")
            Me.PrcCargarDatos("tp_dependecias")

            '  Me.dr_dependencia.Items.Insert(0, New ListItem("------------SELECCIONE UNA DEPENDENCIA-----------", ""))

        End If

    End Sub


    Public Sub prcCrearDataset() 'TABLAS
        Dim ds As New DataSet

        Dim dtt_reunion As New DataTable   '--0
        Dim dtt_frecuencia As New DataTable '--1

        Me.ds_evalua = ds
        Me.ds_evalua.Tables.Add("dtt_reunion")
        Me.ds_evalua.Tables.Add("dtt_tprnion")
        Me.ds_evalua.Tables.Add("dtt_tp")
        Me.ds_evalua.Tables.Add("dtt_areas")
        Me.ds_evalua.Tables.Add("dtt_area")
        Me.ds_evalua.Tables.Add("dtt_frecuencias")


    End Sub
    Public Property ds_evalua() As DataSet
        Get
            Return ViewState("vw_dsevaluac")
        End Get
        Set(ByVal value As DataSet)
            ViewState("vw_dsevaluac") = value
        End Set
    End Property


    Public Sub prcCargarOrigen(ByVal Objeto As String)
        'idrnion = 496
        idrnion = Request.QueryString("idReunion")

        Select Case Objeto
            Case "reuniones"
                Me.ds_evalua.Tables("dtt_reunion").Clear()
                Me.evalu.R_edicion(idrnion)
                Me.ds_evalua.Tables("dtt_reunion").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "tp_reuniones"
                Me.ds_evalua.Tables("dtt_tprnion").Clear()
                Me.evalu.Tp_Reunion()
                Me.ds_evalua.Tables("dtt_tprnion").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "tp_dependecias"
                Me.ds_evalua.Tables("dtt_tp").Clear()
                Me.evalu.Tp_dependencias()
                Me.ds_evalua.Tables("dtt_tp").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "areas"
                Me.ds_evalua.Tables("dtt_areas").Clear()
                Me.evalu.Trae_area(Me.opt_dpendencias.SelectedItem.Text)
                Me.ds_evalua.Tables("dtt_areas").Merge(Me.evalu.DatasetFill.Tables(0))

            Case "frecuencia"
                Me.ds_evalua.Tables("dtt_frecuencias").Clear()
                Me.evalu.consulta_frecuencia(idrnion)
                Me.ds_evalua.Tables("dtt_frecuencias").Merge(Me.evalu.DatasetFill.Tables(0))

        End Select

    End Sub

    Public Sub PrcCargarDatos(ByVal tipo As String)
        Select Case tipo
            Case "reuniones"
                Dim reunion As DataRow = ds_evalua.Tables("dtt_reunion").Rows(0)

                Dim name As String = Convert.ToString(reunion("nombre"))
                Dim tp_rnion As String = Convert.ToString(reunion("NombreReunion"))
                Dim area As String = Convert.ToString(reunion("area"))
                Dim seguimiento As String = Convert.ToString(reunion("seguimiento"))
                Dim Valida_asistente As Integer = reunion("ValidarAsistentes")

                'VALIDAR QUE LAS FECHAS NO VENGAN NULAS
                If Not IsDBNull(reunion("horainicio")) Then

                    Dim horainicio As String = Format(CDate(reunion("horainicio")), "hh:mm tt")
                    Dim horas_i As String = Mid(horainicio, 1, 2)
                    Dim minutos_i As String = Mid(horainicio, 4, 2)
                    Dim hrrio_i As String = Mid(horainicio, 7)


                    'HORAS(INICIO)
                    Me.i_h.SelectedItem.Text = horas_i
                    Me.i_m.SelectedItem.Text = minutos_i
                    Me.i_hh.SelectedItem.Text = hrrio_i
                Else
                    Me.i_h.SelectedItem.Text = "12"
                    Me.i_m.SelectedItem.Text = "00"
                    Me.i_hh.SelectedItem.Text = "a.m."

                End If


                If Not IsDBNull(reunion("horaFin")) Then
                    Dim horafin As String = Format(CDate(reunion("horaFin")), "hh:mm tt")
                    Dim horas_f As String = Mid(horafin, 1, 2)
                    Dim minutos_f As String = Mid(horafin, 4, 2)
                    Dim hrrio_f As String = Mid(horafin, 7)

                    Me.f_h.SelectedItem.Text = horas_f
                    Me.f_m.SelectedItem.Text = minutos_f
                    Me.f_hh.SelectedItem.Text = hrrio_f

                Else
                    Me.f_h.SelectedItem.Text = "12"
                    Me.f_m.SelectedItem.Text = "00"
                    Me.f_hh.SelectedItem.Text = "a.m."


                End If


                'TRAE AREA DEPENDIENDO DE LA REUNION ESCOGIDA
                Me.ds_evalua.Tables("dtt_area").Clear()
                Me.evalu.T_areas(area)
                Me.ds_evalua.Tables("dtt_area").Merge(Me.evalu.DatasetFill.Tables(0))
                Me.dr_dependencia.DataSource = Me.ds_evalua.Tables("dtt_area").CreateDataReader
                Me.dr_dependencia.DataTextField = "NombreDependencia"
                Me.dr_dependencia.DataValueField = "id"
                Me.dr_dependencia.DataBind()

                If seguimiento <> "" Then
                    Me.prcCargarOrigen("frecuencia")
                    Dim frecuencias As DataRow = ds_evalua.Tables("dtt_frecuencias").Rows(0)

                    Select Case seguimiento
                        Case "1"
                            Me.frecuencia.SelectedValue = "1"
                            Me.MultiView1.ActiveViewIndex = 3
                        Case "2"
                            Me.frecuencia.SelectedValue = "2"
                            Me.MultiView1.ActiveViewIndex = 3
                        Case "3"
                            'SEMANAL
                            Me.frecuencia.SelectedValue = "3"
                            Me.MultiView1.ActiveViewIndex = 0
                            Me.txt_sem.Text = Convert.ToString(frecuencias("a"))
                            Me.opt_semanal.SelectedValue = Convert.ToString(frecuencias("b"))

                        Case "4"
                            'MENSUAL
                            Me.frecuencia.SelectedValue = "4"
                            Me.MultiView1.ActiveViewIndex = 1

                            Me.txt_ms1.Text = Convert.ToString(frecuencias("a"))
                            Me.txt_Mensual.Text = Convert.ToString(frecuencias("b"))
                            Me.dr_ms1.SelectedValue = Convert.ToString(frecuencias("c"))
                            Me.dr_ms2.SelectedValue = Convert.ToString(frecuencias("d"))
                            Me.txt_ms3.Text = Convert.ToString(frecuencias("e"))

                            Dim comtrio As String = Convert.ToString(frecuencias("comentario"))
                            If comtrio = "Mensualmente No determinado" Then
                                Me.opt_mensual.SelectedValue = "3"
                            End If



                        Case "5"
                            'ANUAL
                            Me.frecuencia.SelectedValue = "5"
                            Me.MultiView1.ActiveViewIndex = 2

                    End Select
                Else
                    Me.popup_tx.Show()
                    Dim leyenda As String = "Esta Reunion No tiene Una frecuencia Programada"
                    Me.Msg_alert.Text = leyenda



                End If


                'AQUI LLEVO LOS DATOS DEL DATASET A LOS CAMPOS
                Me.txt_nombre.Text = name
                Me.hf_name.Value = name
                Me.dr_reunion.Items.Insert(0, New ListItem(tp_rnion, ""))

                If Valida_asistente = 0 Then
                    Me.ch_valida.Checked = False
                Else
                    Me.ch_valida.Checked = True
                End If



            Case "tp_reuniones"
                Me.dr_reunion.DataSource = Me.ds_evalua.Tables("dtt_tprnion").CreateDataReader
                Me.dr_reunion.DataTextField = "NombreReunion"
                Me.dr_reunion.DataValueField = "id"
                Me.dr_reunion.DataBind()

            Case "tp_dependecias"
                Me.opt_dpendencias.DataSource = Me.ds_evalua.Tables("dtt_tp").CreateDataReader
                Me.opt_dpendencias.DataTextField = "TIPO"
                Me.opt_dpendencias.DataBind()
                Me.opt_dpendencias.RepeatDirection = RepeatDirection.Horizontal
                Me.opt_dpendencias.AutoPostBack = True

            Case "areas"
                Me.dr_dependencia.DataSource = Me.ds_evalua.Tables("dtt_areas").CreateDataReader
                Me.dr_dependencia.DataTextField = "NombreDependencia"
                Me.dr_dependencia.DataValueField = "id"
                Me.dr_dependencia.DataBind()



        End Select

    End Sub


    Public Sub llenar_drow()
        'i_h.Items.Insert(0, New ListItem("--Select A Lunch Time--", ""))

   
        Me.i_h.Items.Add(New ListItem("--", "--"))
        Me.i_h.Items.Add(New ListItem("01", "01"))
        Me.i_h.Items.Add(New ListItem("02", "02"))
        Me.i_h.Items.Add(New ListItem("03", "03"))
        Me.i_h.Items.Add(New ListItem("04", "04"))
        Me.i_h.Items.Add(New ListItem("05", "05"))
        Me.i_h.Items.Add(New ListItem("06", "06"))
        Me.i_h.Items.Add(New ListItem("07", "07"))
        Me.i_h.Items.Add(New ListItem("08", "08"))
        Me.i_h.Items.Add(New ListItem("09", "09"))
        Me.i_h.Items.Add(New ListItem("10", "10"))
        Me.i_h.Items.Add(New ListItem("11", "11"))
        Me.i_h.Items.Add(New ListItem("12", "12"))

        Me.i_m.Items.Add(New ListItem("--", "--"))
        Me.i_m.Items.Add(New ListItem("00", "00"))
        Me.i_m.Items.Add(New ListItem("15", "15"))
        Me.i_m.Items.Add(New ListItem("30", "30"))
        Me.i_m.Items.Add(New ListItem("45", "45"))

        Me.i_hh.Items.Add(New ListItem("--", "--"))
        Me.i_hh.Items.Add(New ListItem("a.m.", "a.m."))
        Me.i_hh.Items.Add(New ListItem("p.m.", "p.m."))




        Me.f_h.Items.Add(New ListItem("--", "--"))
        Me.f_h.Items.Add(New ListItem("01", "01"))
        Me.f_h.Items.Add(New ListItem("02", "02"))
        Me.f_h.Items.Add(New ListItem("03", "03"))
        Me.f_h.Items.Add(New ListItem("04", "04"))
        Me.f_h.Items.Add(New ListItem("05", "05"))
        Me.f_h.Items.Add(New ListItem("06", "06"))
        Me.f_h.Items.Add(New ListItem("07", "07"))
        Me.f_h.Items.Add(New ListItem("08", "08"))
        Me.f_h.Items.Add(New ListItem("09", "09"))
        Me.f_h.Items.Add(New ListItem("10", "10"))
        Me.f_h.Items.Add(New ListItem("11", "11"))
        Me.f_h.Items.Add(New ListItem("12", "12"))

        Me.f_m.Items.Add(New ListItem("--", "--"))
        Me.f_m.Items.Add(New ListItem("00", "00"))
        Me.f_m.Items.Add(New ListItem("15", "15"))
        Me.f_m.Items.Add(New ListItem("30", "30"))
        Me.f_m.Items.Add(New ListItem("45", "45"))

        Me.f_hh.Items.Add(New ListItem("--", "--"))
        Me.f_hh.Items.Add(New ListItem("a.m.", "a.m."))
        Me.f_hh.Items.Add(New ListItem("p.m.", "p.m."))




    End Sub

    Protected Sub opt_dpendencias_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_dpendencias.SelectedIndexChanged
        Me.prcCargarOrigen("areas")
        Me.PrcCargarDatos("areas")
    End Sub

    Protected Sub frecuencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles frecuencia.SelectedIndexChanged
        Select Case frecuencia.SelectedValue

            Case "1"
                Me.MultiView1.ActiveViewIndex = 3
            Case "2"
                Me.MultiView1.ActiveViewIndex = 3
            Case "3"
                Me.MultiView1.ActiveViewIndex = 0

            Case "4"
                Me.MultiView1.ActiveViewIndex = 1
            Case "5"
                Me.MultiView1.ActiveViewIndex = 2

        End Select

    End Sub


    Protected Sub guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles guardar.Click
        'idrnion = 496
        idrnion = Request.QueryString("idReunion")
        guardar_edicion(idrnion)



    End Sub


    Public Sub guardar_edicion(ByVal id_r As String)

        Dim reunion As DataRow = ds_evalua.Tables("dtt_reunion").Rows(0)


        Dim Comentario As String
        Dim idrenion As Integer = id_r

        Dim nombre_reunion As String = Me.txt_nombre.Text
        Dim nombre_antiguo As String = Me.hf_name.Value
        Dim tipo_reunion As String = Me.dr_reunion.SelectedValue
        Dim h_inicio As String = Me.i_h.SelectedItem.Text & ":" & Me.i_m.SelectedItem.Text & " " & Me.i_hh.SelectedItem.Text
        Dim h_fin As String = Me.f_h.SelectedItem.Text & ":" & Me.f_m.SelectedItem.Text & " " & Me.f_hh.SelectedItem.Text
        Dim frc As String = Me.frecuencia.SelectedValue
        Dim dependencia As String = Me.dr_dependencia.SelectedValue
        Dim validaasistentes As Integer

        If tipo_reunion = "" Then
            tipo_reunion = reunion("tip_asoc")
        End If

        If Me.ch_valida.Checked = True Then
            validaasistentes = 1
        Else : Me.ch_valida.Checked = False
            validaasistentes = 0
        End If

        If frc = "" Then
            Me.popup_tx.Show()
            Dim leyenda As String = "Debe escoger Una Frecuencia Para Toda Reunion"
            Me.Msg_alert.Text = leyenda
        Else
            Me.evalu.update_reuniones(idrenion, nombre_reunion, tipo_reunion, h_inicio, h_fin, frc, dependencia, validaasistentes)
            Me.evalu.compromisos_update(Me.hf_name.Value, nombre_reunion)

            If evalu.FlagError = True Then
                Me.MsgBox1.ShowMessage("ERROR" & evalu.ERRORR)
            Else
                Me.popup_tx.Show()
                Dim leyenda As String = "EDICION TERMINADA"
                Me.Msg_alert.Text = leyenda
            End If

        End If

        Select Case frecuencia.SelectedValue
            Case "1"
                Comentario = "Esta Frecuencia es Programada Diaria Por Turnos."
                Me.evalu.update_frecuencia("diaria", id_r, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

            Case "2"
                Comentario = "Esta Frecuencia es Programada Diaria"
                Me.evalu.update_frecuencia("diaria_t", id_r, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

            Case "3"
                Dim d_sem2 As String = Me.txt_sem.Text
                Dim d_semanal As String = Me.opt_semanal.SelectedValue
                Comentario = "Repetir cada " & d_sem2 & " semanas el " & d_semanal
                Me.evalu.update_frecuencia("semanal", id_r, Me.txt_nombre.Text, d_sem2, d_semanal, "", "", "", Comentario)
            Case "4"
                If Me.opt_mensual.SelectedValue <> "" Then

                    If Me.opt_mensual.SelectedValue = "1" Then
                        Dim v_men As String = Me.txt_ms1.Text
                        Dim v_men1 As String = Me.txt_Mensual.Text
                        Comentario = "El dia " & v_men & " de cada " & v_men1 & " meses"
                        Me.evalu.update_frecuencia("Mensual1", id_r, Me.txt_nombre.Text, v_men, v_men1, "", "", "", Comentario)
                    End If

                    If Me.opt_mensual.SelectedValue = "2" Then
                        Dim v_men2 As String = Me.dr_ms1.SelectedValue
                        Dim v_men3 As String = Me.dr_ms2.SelectedValue
                        Dim v_men4 As String = Me.txt_ms3.Text
                        Comentario = "El " & v_men2 & " " & v_men3 & " de cada " & v_men4 & " meses"
                        Me.evalu.update_frecuencia("Mensual2", id_r, Me.txt_nombre.Text, "", "", v_men2, v_men3, v_men4, Comentario)

                    End If


                    If Me.opt_mensual.SelectedValue = "3" Then
                        Comentario = "Mensualmente No determinado"
                        Me.evalu.update_frecuencia("Mensual3", id_r, Me.txt_nombre.Text, "", "", "", "", "", Comentario)

                    End If

                Else
                    Me.MsgBox1.ShowMessage("Elija Una Opcion Mensual")
                End If
            Case "5"
                If Me.opt_an.SelectedValue = "1" Then
                    Dim v_an1 As String = Me.txt_anual.Text
                    Dim v_an2 As String = Me.dr_anual.SelectedValue
                    Comentario = "Cada " & v_an1 & " de " & v_an2
                    Me.evalu.update_frecuencia("Mensual1", id_r, Me.txt_nombre.Text, v_an1, v_an2, "", "", "", Comentario)
                End If

                If Me.opt_an.SelectedValue = "2" Then
                    Dim v_an3 As String = Me.dr_an1.SelectedValue
                    Dim v_an4 As String = Me.dr_an2.SelectedValue
                    Dim v_an5 As String = Me.dr_an3.SelectedValue
                    Comentario = "El " & v_an3 & " " & v_an4 & " de " & v_an5
                    Me.evalu.update_frecuencia("Mensual2", id_r, Me.txt_nombre.Text, "", "", v_an3, v_an4, v_an5, Comentario)

                End If


        End Select

        Me.prcCargarOrigen("reuniones")
        Me.PrcCargarDatos("reuniones")


    End Sub


End Class
